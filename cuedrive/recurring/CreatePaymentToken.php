<?php
 class CreatePaymentToken 
 {
 
  function create_payment_token($data)
  {
    $testUrl = "https://www.eway.com.au/gateway/ManagedPaymentService/test/managedCreditCardPayment.asmx";
    $liveUrl = "https://www.eway.com.au/gateway/ManagedPaymentService/managedCreditCardPayment.asmx";
  
    $eWaySOAPActionURL    = "https://www.eway.com.au/gateway/managedpayment";
    $eWayCustomerId       = "18005954"; //"91398650";
    /* test account */
    $eWayCustomerEmail    =  "info@cuedrive.com";		//"Bevan_eagle@hotmail.com"; //"debartha.nandi@teks.co.in.sand";
    /* test email */
    $eWayCustomerPassword = "Longyear15";//"Deb08nandi";
    /* test password */
  
    $updateCustomer = false;
    $updateCustomerEWayId = "";
    $customTag      = "ProcessPayment";
    /* For query customer: "queryCustomer". */
     
    $directXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
        <soap12:Envelope 
            xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
            xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
            xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
            <soap12:Header>
            <eWAYHeader xmlns=\"" . $eWaySOAPActionURL . "\">
                <eWAYCustomerID>" . $eWayCustomerId . "</eWAYCustomerID>
                <Username>" . $eWayCustomerEmail . "</Username>
                <Password>" . $eWayCustomerPassword . "</Password>
            </eWAYHeader>
        </soap12:Header>
          <soap12:Body>
            <" . $customTag . " xmlns=\"" . $eWaySOAPActionURL . "\">
                " . $updateCustomerEWayId . "     
                <managedCustomerID>".$data['token']."</managedCustomerID>   
				<amount>".$data['amount']."</amount>	
            </" . $customTag . ">
        </soap12:Body>
        </soap12:Envelope>";
		
	//print_r($data);
	//echo $directXML;
	
    $result = CreatePaymentToken::makeCurlCall(
    $liveUrl , /* CURL URL */ 
    "POST", /* CURL CALL METHOD */  
    array(
        /* CURL HEADERS */
        "Content-Type: text/xml; charset=utf-8",
        "Accept: text/xml",
        "Pragma: no-cache",
        "SOAPAction: " . $eWaySOAPActionURL . "/" . $customTag,
        "Content_length: " . strlen(trim($directXML))
    ), 
    null, /* CURL GET PARAMETERS */  
    $directXML /* CURL POST PARAMETERS AS XML */ );
	
	//echo '<pre>'; print_r($result); echo '</pre>';
	//die;
	
    if ($result != null && isset($result["response"])) {
        $xmlString = $result["response"]; /* Result printed below */
        require 'recurring/xmlparser.php';
	    $xmlObject = new XmlToArrayParser($xmlString);
	    $data =  json_encode($xmlObject->array);
	    $data = json_decode($data);
		return $data->{'soap:Envelope'}->{'soap:Body'}->ProcessPaymentResponse->ewayResponse;
    }
    die("");
} 

/* makeCurlCall */ 
function makeCurlCall($url, $method = "GET", $headers = null, $gets = null, $posts = null)
{
    $ch = curl_init();
    if ($gets != null) {
        $url .= "?" . (http_build_query($gets));
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  
    if ($posts != null) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
    }
    if ($method == "POST") {
        curl_setopt($ch, CURLOPT_POST, true);
    } else if ($method == "PUT") {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    } else if ($method == "HEAD") {
        curl_setopt($ch, CURLOPT_NOBODY, true);
    }
    if ($headers != null && is_array($headers)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    $response = curl_exec($ch);
    $code     = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  
    curl_close($ch);
    return array(
        "code" => $code,
        "response" => $response
    );
} 
 
 
 
 
 }

?>