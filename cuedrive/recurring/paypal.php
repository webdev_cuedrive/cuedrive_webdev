<?php
/**
 *
 * @author debartha
 *
 */

class Paypal
{
    # Sandbox

	/**
     * function to read stdin
     *
     * @return string
     */
    
    function read_stdin() 
    {
    	$fr=fopen("php://stdin","r");  // open our file pointer to read from stdin
    	$input = fgets($fr,128);      // read a maximum of 128 characters
    	$input = rtrim($input);      // trim any trailing spaces.
    	fclose ($fr);               // close the file handle
    	return $input;             // return the text entered
    }
    
    
    /**
     * function to Get Access Token
     *
     * @param unknown_type $url
     * @param unknown_type $postdata
     */
    
    function get_access_token($url, $postdata) 
    {
     	
     # Sandbox
     $clientId="AfyIghA7HXzce1rs0b2ApmwdgO0QQY3BGXIhbvMq2t3bogRNeRUTTmV3BWhM";       // AX2ZVhCcXhhwGuo_3AFXRD_2DdS704h0Cq68-M3YVqs5KdFiXIx9vuRiGkBW
     
     $clientSecret="ENr3DBCCcsdRo_eaPQgDP--eMv2FD8cBwsMV5dwfGVaUkOsDqJHRoVRibmXf";  // ECojsBCyzdq_f8il9lQ_vtWD9WiEculR1O8ke4p1mGv9Jx44WVjKanraeUmY

     
        $curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($curl, CURLOPT_USERPWD, $clientId . ":" . $clientSecret);
    	curl_setopt($curl, CURLOPT_HEADER, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    	
    	# curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    	
    	$response = curl_exec( $curl );
    	
    	if (empty($response)) {
    		// some kind of an error happened
    		die(curl_error($curl));
    		curl_close($curl); // close cURL handler
    	} 
    	else {
    		$info = curl_getinfo($curl);
    		//echo "Time took: " . $info['total_time']*1000 . "ms\n";
    		curl_close($curl); // close cURL handler
    		if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
    			echo "Received error: " . $info['http_code']. "\n";
    			echo "Raw response:".$response."\n";
    			die();
    		}
    	}
    
    	// Convert the result from JSON format to a PHP array
    	$jsonResponse = json_decode( $response );
    	return $jsonResponse->access_token;
    }
    
    
    /**
     * function to Make Post Call To Send Credit Card Information
     *
     * @param unknown_type $url
     * @param unknown_type $postdata
     * @return mixed
     */
    
    function make_post_call($url, $postdata, $token) 
    {
    	
    	$curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($curl, CURLOPT_HEADER, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    	'Authorization: Bearer '.$token,
    	'Accept: application/json',
    	'Content-Type: application/json'
    			));
    
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    	#curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    	$response = curl_exec( $curl );
    	if (empty($response)) {
    		// some kind of an error happened
    		die(curl_error($curl));
    		curl_close($curl); // close cURL handler
    	} else {
    		$info = curl_getinfo($curl);
    		//echo "Time took: " . $info['total_time']*1000 . "ms\n";
    		curl_close($curl); // close cURL handler
    		if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
    			echo "Received error: " . $info['http_code']. "\n";
    			echo "Raw response:".$response."\n";
    			die();
    		}
    	}
    
    	// Convert the result from JSON format to a PHP array
    	$jsonResponse = json_decode($response, TRUE);
    	return $jsonResponse;
    }
    
    /**
     * Function to Make Get call to Get the Response
     *
     * @param unknown_type $url
     * @return mixed
     */
    
    function make_get_call($url, $token)
     {
    	
    	$curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_POST, false);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($curl, CURLOPT_HEADER, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    	'Authorization: Bearer '.$token,
    	'Accept: application/json',
    	'Content-Type: application/json'
    			));
    
    	#curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    	$response = curl_exec( $curl );
    	if (empty($response)) {
    		// some kind of an error happened
    		die(curl_error($curl));
    		curl_close($curl); // close cURL handler
    	} else {
    		$info = curl_getinfo($curl);
    		//echo "Time took: " . $info['total_time']*1000 . "ms\n";
    		curl_close($curl); // close cURL handler
    		if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
    			echo "Received error: " . $info['http_code']. "\n";
    			echo "Raw response:".$response."\n";
    			die();
    		}
    	}
    
    	// Convert the result from JSON format to a PHP array
    	$jsonResponse = json_decode($response, TRUE);
    	return $jsonResponse;
    }
    	
  

}
?>