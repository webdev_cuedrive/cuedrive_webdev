function sendFileToServer(files,parentfolderid,parentfolder,override)
{
       
      	var expandednodesstr=$("#expandednodes").val();
      	expandednodesstr += ','+$("#data_tt_id").val();
	var fd = new FormData();
	 
       fd.append("parentfolderid", parentfolderid);
       fd.append("override", override);
      // Loop through each of the selected files.
      for (var i = 0; i < files.length; i++) {
       var file = files[i];
       // Add the file to the request.
      // fd.append('selectedfiles[]', file, file.name);
       fd.append("selectedfiles[]", file);
      }
      
     $("#progressbar").css("width","0%"); 
     $("#loaderModal").reveal();
     $("#mainloadingdiv").show();
     
     $.ajax({
     
        xhr: function()
	  {
	    var xhr = new window.XMLHttpRequest();
	    //Upload progress
	    xhr.upload.addEventListener("progress", function(evt){
	      if (evt.lengthComputable) {
	        var percentComplete = (evt.loaded / evt.total) * 100;
	            percentComplete = percentComplete.toFixed();
	        $("#percentage").html(percentComplete + '%');
	        $("#progressbar").css("width",percentComplete+"%");
	      }
	    }, false);
	    return xhr;
	  },
         url: baseUrl+'index.php/companyadmin/uploadFile',  
         type: 'POST',
         data: fd,
         success:function(data){
                    	if(data=="success")
					{
			
                    	 $("#mainloadingdiv").hide();
						$("#loadingdiv").show();
							$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
								
								$("#loadingdiv").hide();
								closefile();
								$("#tablewidget").html('');
								$("#tablewidget").html(data);
								$("#selectedfolderid").val('');
							});
							
			$("#loaderModal").trigger('reveal:close');
			
					}
		        	else
		        	{
			        	alert(data);
								$('.reveal-modal-bg').hide();
								$('#loaderModal').hide();
		        	}
             }, 
         cache: false,
         contentType: false,
         processData: false
         
     });
      	
}

function handleFileUpload(files,obj)
{
   	        var parentfolder=$("#selectedfoldername").val();
		var parentfolderid=$("#selectedfolderid").val();
		var filenamestr="";
		
			 
			// Loop through each of the selected files.
		     for (var i = 0; i < files.length; i++) {
		     var file = files[i];
		       // Add the file to the request.
		      // fd.append('selectedfiles[]', file, file.name);
		      if(filenamestr=="")
		    	  filenamestr=file.name;	
		      else
		      	  filenamestr=filenamestr+"@#"+file.name;		      
		    }
			//var newfilename=$("#upfile").val();
			if(filenamestr=="")
			{
				alert("<?php echo NOFILESELECTED;?>");
			}
			else
			{
				
				$.post(baseUrl+'index.php/companyadmin/checkFileExist',{parentfolderid:parentfolderid,filenamestr:filenamestr},function(data)
				{
				
				if(data=="") //not exist
				{	
				   //uploadFileAjax(parentfolderid,"false");
				    sendFileToServer(files,parentfolderid,parentfolder,'false');
				}
				else //exist
				{					
					if (confirm(data+" already exist.Do you want replace it?")) { 
						//uploadFileAjax(parentfolderid,"true");
					 sendFileToServer(files,parentfolderid,parentfolder,'true');
					}
				}
				});
				
				
			}
			
			
		
	
   /*for (var i = 0; i < files.length; i++) 
   {
   		var fd = new FormData();
	    	fd.append('image', files[i]);
	    	fd.append('id', '123');
          //  sendFileToServer(fd,status);
  		
   }*/
}

$(document).ready(function()
{
//var obj = $("#tablewidget");
var obj =  $(".right-side");
var obj1 = $("#dialog");

obj.on('dragover', function (e)
{
e.stopPropagation();
e.preventDefault();
});
obj1.on('drop', function (e)
{


var isChromium = window.chrome,
vendorName = window.navigator.vendor;
if(isChromium !== null && vendorName === "Google Inc.") {


}

else
{

	$(this).dialog( "close" );
	   e.preventDefault();
	   var files = e.originalEvent.dataTransfer.files;

	       
		//We need to send dropped files to Server
		if($("#selectedfolderid").val()=="")
		{
		    alert("<?php echo NOPARENTFOLDER;?>");
		}
		else
		{
			  var parentfolder=$("#selectedfoldername").val();
			  var parentfolderid=$("#selectedfolderid").val();
			  if (confirm("Do you want upload file in "+parentfolder+" ?")) {
			 
			   handleFileUpload(files,obj);
			 
			  }
		
		
		}
	  
}

});
$(document).on('dragenter', function (e)
{
e.stopPropagation();
e.preventDefault();
});
$(document).on('dragover', function (e)
{
 e.stopPropagation();
 e.preventDefault();

});
$(document).on('drop', function (e)
{
e.stopPropagation();
e.preventDefault();
	if($("#dialog").dialog("isOpen")){
               $( "#dialog" ).dialog( "close" );
       }
});



$(".inner-div").on("dragenter",function(e){
e.stopPropagation();
e.preventDefault();

if($("#dialog").dialog("isOpen")){
$( "#dialog" ).dialog( "close" );
}
});

$(".right-side").on("dragenter",function(e){
e.stopPropagation();
e.preventDefault();

if(!$("#dialog").dialog("isOpen")){
$(".right-side").css("opacity","0.1");
$( "#dialog" ).dialog( "open" );
$(".ui-dialog-titlebar").hide();
$(".ui-widget-content").css({"border":"none","background":"none"});
}
});

function addEvent(obj, evt, fn) {
   if (obj.addEventListener) {
       obj.addEventListener(evt, fn, false);
   }
   else if (obj.attachEvent) {
       obj.attachEvent("on" + evt, fn);
   }
}
addEvent(window,"load",function(e) {
   addEvent(document, "mouseout", function(e) {
       e = e ? e : window.event;
       var from = e.relatedTarget || e.toElement;
       if (!from || from.nodeName == "HTML") {
        e.stopPropagation();
        e.preventDefault();

        if($("#dialog").dialog("isOpen")){
        $( "#dialog" ).dialog( "close" );
        }
       }
   });
});

});


var flag = true;
$(window).load(function(){
       if(flag == true) {
       $("#tooltip").popover('show');
       
       setTimeout(function() { 
	$( "#tooltip" ).popover( "destroy" );
	flag = false;
	}, 5000);
	
	}
});
