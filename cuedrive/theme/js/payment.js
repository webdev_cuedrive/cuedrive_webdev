var autopayment = "";
var totaluseralloweddevices = 0;
function signupclick(pkgid)
{
	$("#paybutton").hide();
    $("#paymentinvoice").hide();
    $("#creditinfo").hide();
    $("#paybody").show();
	var details = "";
	var price = "";
	var storage = "";
	var duration = "";
	var additionaldevicecharge = "";
	var emailsupport = "";
	var mobile = "";
	var option = "";
	autopaymentval = 0;
	text = 'Next';
	document.getElementsByName("selectedpkgid")[0].value = pkgid;
	$.post(baseURL+"index.php/companyadmin/fetchPaymentOption",function(data)
	{
	var res = data.split(":");
	autopayment = res[0];
	totaluseralloweddevices = res[1];
	$('#deviceinfo').html('');
	
	var devices = ' Devices';
	if(res[1] == 1)
	{
		devices = ' Device';
	}
	var url = baseURL+"index.php/companyadmin/getPackageById?packageid="+pkgid;
	$.getJSON(url , function(data)
	{
	    $('#tbody').html('');
	    $.each(data , function(key,value)
	    {
	       if(key == 'type')
	       { 
	          details = value;
	       }
	       if(key == 'price')
	       {
	          price = value;
	       }
	       if(key == 'forduration')
	       {
	          duration = value;
	       }
	       if(key == 'storagespace')
	       {
	          storage = value;
	       }
	       if(key == 'additionaldeviceprice')
	       {
	          additionaldevicecharge = value;
	       }
	       if(key == 'emailsupport')
	       {
	          emailsupport = value;
	       }
	       if(key == 'mobile')
	       {
	          mobile = value;
	       }
	       
	       if(key == 'noofdevices')
	       {
	          noofdevices = value;
	       }
	    });
	    
		var buttonlabel = "";
		var btnDisplay = '';
		//$('#deviceinfo').html(res[1]+' Allowed '+devices);
		$('#deviceinfo').html(res[1] + devices + ' (' +noofdevices+ 'Max)');
		
		/*if(autopayment == 'true')
		{  
		 text = 'Cancel Auto Payment';
		 autopaymentval = 1;
		 btnDisplay = 'style = "display: none"';
		 buttonlabel = '<button class="pull-right btn btn-primary" id="autotpayment" onclick="makeAutoPaymentTrue('+autopaymentval+')"> Next</button>';
		}
		buttonlabel += '<button class="pull-right btn btn-primary" id="displayInvoice" '+btnDisplay+' onclick="showInvoice('+noofdevices+' , 2)"> Next</button>';*/
		
/**custom code */

			var companyuserid = $("#companyuserid").val();
			var isMobUser = $('#isMobUser').val();
			if(isMobUser == 1)	{
				buttonlabel = '<button class="pull-right btn btn-primary" id="displayInvoice" '+btnDisplay+' onclick="showAddressDetails('+companyuserid+','+noofdevices+','+pkgid+')"> Next</button>';
			}	else	{
				buttonlabel = '<button class="pull-right btn btn-primary" id="displayInvoice" '+btnDisplay+' onclick="showInvoice('+noofdevices+' , 2, 0)"> Next</button>';
			}
			/**custom code */
	    
		for(i = 0; i <= noofdevices; i++)
          {
             if(i==res[1])
			  {		   
			 	option += '<option value="'+i+'" selected="selected">'+i+'</option>';
				
			  }else{
				 option += '<option value="'+i+'">'+i+'</option>'; 
			       }
          }
			
	    $('#tbody').append('<tr> <th width="50%" align="center">Details</th>'+
                                 '<td width="50%" align="center"><b>'+details+'</b></td>'+
                                            
                                            
                                '</tr>'+ 
                                '<tr>'+
                                            '<th width="50%" align="center">Price</th>'+
                                            '<td width="50%" align="center">$'+price+'</td>'+
                                           
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Duration</th>'+
                                            '<td width="50%" align="center">'+duration+'</td>'+
                                            
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Storage</th>'+
                                            '<td width="50%" align="center">'+storage+'</td>'+
                                          
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Device charge</th>'+
                                            '<td width="50%" align="center">$'+additionaldevicecharge+' / Device</td>'+
                                            
                                           
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Number of devices</th>'+
                                            '<td width="50%" align="center"><select class="form-control" style="width:40%;" name="noofdevices" id="noofdevices" onchange="setMeg(this.value, '+noofdevices+')">'+
                                              option+
                                            '<select></td>'+
                                            
                                           
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Email Support</th>'+
                                            '<td width="50%" align="center">'+emailsupport+'</td>'+
                                            
                                           
                                '</tr>'+
                                '<tr style="display:none;">'+
                                            '<th width="50%" align="center">Mobile</th>'+
                                            '<td width="50%" align="center">'+mobile+'</td>'+
                                            
                                           
                                '</tr>'+
                                 '<tr>'+
                         '<td colspan="2"> '+buttonlabel+' <button class="pull-left btn btn-primary" id="directpayment" onclick="showInvoice('+noofdevices+', 1)" style="display:none;">Direct Payment</button> <button class="btn btn-primary pull-left" type="button" onclick="closeupdatemodal()"> Cancel</button></td>'+
                                 '</tr>');
	
		});
	});

    $('#eway').addClass('eway-button');
	$('#fadeandscale').reveal();
}


function upgradeclick(pkgid)
{
	 $("#paybutton").hide();
     $("#paymentinvoice").hide();
     $("#creditinfo").hide();
     $("#paybody").show();
     
		 /**custom code */
		 var isMobUser = $('#isMobUser').val();
			if(isMobUser == 1)	{
					var showInvoiceFlag = 1;
			} else {
				var showInvoiceFlag = 0;
			}
			/**custom code */
			
        var details = "";
        var price = "";
        var storage = "";
        var duration = "";
        var additionaldevicecharge = "";
        var emailsupport = "";
        var mobile = "";
        var option = "";
        autopaymentval = 0;
	text = 'Next';
	document.getElementsByName("selectedpkgid")[0].value = pkgid;
	
	$.post(baseURL+"index.php/companyadmin/fetchPaymentOption",function(data)
	{
	var res = data.split(":");
	autopayment = res[0];
	
	if(autopayment == 'true')
	{  
	 text = 'Cancel Auto Payment';
	 autopaymentval = 1;
	}
	totaluseralloweddevices = res[1];
	
	var devices = 'Devices';
	if(res[1] == 1)
	{
		devices = 'Device';
	}
	
	//$('#deviceinfo').html('<center>There are '+res[1]+' remaining '+devices+' left in your current package</center>');
	$('#deviceinfo').html(res[1]+' Allowed '+devices);
	
	var url = baseURL+"index.php/companyadmin/getPackageById?packageid="+pkgid;
	$.getJSON(url , function(data)
	{
	    $('#tbody').html('');
	    $.each(data , function(key,value)
	    {
	       if(key == 'type')
	       {
	          details = value;
	       }
	       if(key == 'price')
	       {
	          price = value;
	       }
	       if(key == 'forduration')
	       {
	          duration = value;
	       }
	       if(key == 'storagespace')
	       {
	          storage = value;
	       }
	       if(key == 'additionaldeviceprice')
	       {
	          additionaldevicecharge = value;
	       }
	       if(key == 'emailsupport')
	       {
	          emailsupport = value;
	       }
	       if(key == 'mobile')
	       {
	          mobile = value;
	       }
	       
	       if(key == 'noofdevices')
	       {
	          noofdevices = value;
	       }
	    });
	    
	    
	    for(i = 0 ; i <= noofdevices-2 ; i++)
            {
                option += '<option value="'+i+'">'+i+'</option>';
            }
	    
	    
	    $('#tbody').append('<tr>        <th width="50%" align="center">Details</th>'+
                                            '<td width="50%" align="center">'+details+'</td>'+
                                            
                                            
                                '</tr>'+ 
                                '<tr>'+
                                            '<th width="50%" align="center">Price</th>'+
                                            '<td width="50%" align="center">$'+price+'</td>'+
                                           
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Duration</th>'+
                                            '<td width="50%" align="center">'+duration+'</td>'+
                                            
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Storage</th>'+
                                            '<td width="50%" align="center">'+storage+'</td>'+
                                          
                                            
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Device charge</th>'+
                                            '<td width="50%" align="center">$'+additionaldevicecharge+' / Device</td>'+
                                            
                                           
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Number of devices</th>'+
                                            '<td width="50%" align="center"><select class="form-control" name="noofdevices" id="noofdevices">'+
                                              option+
                                            '<select></td>'+
                                            
                                           
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Emailsupport</th>'+
                                            '<td width="50%" align="center">'+emailsupport+'</td>'+
                                            
                                           
                                '</tr>'+
                                '<tr>'+
                                            '<th width="50%" align="center">Mobile</th>'+
                                            '<td width="50%" align="center">'+mobile+'</td>'+
                                            
                                           
                                '</tr>'+
                                 '<tr>'+
                                            '<td align="center"></td>'+
                                            '<td align="center"></td>'+
                                            
                                           
                                 '</tr>');
                                 
                                 
       $('#showinvoice').html('');
	   $('#showinvoice').html('<button class="btn btn-primary" id="directpayment" onclick="showInvoice('+noofdevices+', '+pkgid+')">Pay Now</button>');
	});
	});
    $('#eway').addClass('eway-button');
	$('#fadeandscale').reveal();
	
}



function makeAutoPaymentTrue(id)
{
 var url = baseURL+"index.php/payment/autoPayment";
  if(id == '1') //cancel auto payment
   {
          //if(confirm('Are you sure, you want to cancel Auto Payment ?'))
          //{
			 var url = baseURL+"index.php/companyadmin/cancelautoPayment";
			 $.post(url , function(data)
			 {
			   if(data == 'success')
			   {
				   //$('#fadeandscale').trigger('reveal:close');
				   $('#autotpayment').hide();
				   $('#displayInvoice').show();
			  }
			 });
          //}
 }else{
 // two conditions are here
 $.post(baseURL+"index.php/companyadmin/checkTokenExists",function(data)
 {
     var noofdevices = $("#noofdevices").val();
     var selectedpkgid = $("#selectedpkgid").val();
     
    if(data == 'error') 
    {   
    	$("#paybutton").hide();
        $("#paymentinvoice").hide();
        $("#creditinfo").hide();
        $("#paybody").hide();
	    //$('#autotpayment').prop('disabled', true);
    	
      $("#nodevice").val(noofdevices); //if token = 0)
	  $('#backInvoice').html('<button class="btn btn-primary pull-left" id="backInvoicebutton" type="button" type="backinvoicebutton" onclick="backInvoice();"> Back</button>');
	  clearFields();
      $("#creditinfo").show();
    }
    else if(data == 'success') //if token != 0
    {
	   $("#loadingdiv").show();
	   $('#autotpayment').prop('disabled', true);
	   $('#backpaybodybutton').prop('disabled', true);
	   $('#paycancel').prop('disabled', true);
	   $('#backinvoicebutton').prop('disabled', true);
	   
       $.post(url,{selectedpkgid : selectedpkgid , noofdevices : noofdevices}, function(data)
        { 
          if(data == 'success')
          {
           $.post(baseURL+'index.php/companyadmin/createInvoice',{selectedpkgid : selectedpkgid , noofdevices : noofdevices},function(data)
           {
             if(data == 'success')
             { $("#loadingdiv").hide();
              window.location = baseURL+"index.php/companyadmin/upgradePacakge/1";
             }
           });
           }
         });
     }
 
  });
 }
}


function submitCardInfo()
{
	var noofdevices = $("#nodevice").val();
	var selectedpkgid = $("#selectedpkgid").val();
	var cardnumber = $("#cardnumber").val();
	var nameoncard = $("#nameoncard").val();
	var CARDEXPIRYMONTH = $("#EWAY_CARDEXPIRYMONTH").val();
	var CARDEXPIRYYEAR = $("#EWAY_CARDEXPIRYYEAR").val();

	  if(cardnumber == "")
	  {
		alert("Please enter Credit Card Number");
		return false;
	  }
	  if(nameoncard == "")
	  {
		alert("Please enter Name on card");
		return false;		
	  }
	
	  $("#loadingdivtokenexist").show();  
	  $('#buttonpay').prop('disabled', true);
	  $('#paycancel').prop('disabled', true);
	  $('#backInvoicebutton').prop('disabled', true);
	
	  var cardtype = detectCardType(cardnumber);
	
	 if(cardtype == 'dinerclub' || cardtype == 'amex')
	 {
		 alert("Unfortunately Diners club cards and Amex cards are not accepted throughout this payment");
		 $('#buttonpay').prop('disabled', false);
		 return false;
	 }
	 else
	 {
		 
		  var today = new Date();		
			var full_month = today.getMonth(); //January is 0!
			var current_month = full_month < 10 ? '0' + full_month : full_month;
			var full_year = today.getFullYear();
			var current_year = full_year.toString().substr(2,2);

		 //console.log(CARDEXPIRYYEAR + '-' + current_year + '-' + CARDEXPIRYMONTH + '-' + current_month);
		 if(CARDEXPIRYYEAR == current_year && CARDEXPIRYMONTH <= current_month)	{
			 	alert('Please enter valid expiry date.');
				//clearFields();
				$('#buttonpay').prop('disabled', false);
				$('#loadingdivtokenexist').hide();
				return false;
		 }
		 
		 
		 
	$.post(baseURL+"index.php/payment/createTokenCustomer",{cardnumber : cardnumber , nameoncard : nameoncard , expmonth : CARDEXPIRYMONTH , expyear : CARDEXPIRYYEAR, noofdevices : noofdevices ,selectedpkgid : selectedpkgid},function(data)
	 {
		if(data == 'success')
		{
			   $.post(baseURL+'index.php/companyadmin/createInvoice',{selectedpkgid : selectedpkgid , noofdevices : noofdevices},function(data)
			   {
				 if(data == 'success')
				 {
				   $("#loadingdivtokenexist").hide();
				   window.location = baseURL+"index.php/companyadmin/upgradePacakge/1";
				 }
			   });
		}
		else
		{
		  alert('Opps! Some unexpected error occured');
		  clearFields();
		  $('#buttonpay').prop('disabled', false);
		}
	 });
   }
}

function updateCardInfo()
{
	//var noofdevices = $("#nodevice").val();
	//var selectedpkgid = $("#selectedpkgid").val();
	var cardnumber = $("#cardnumber").val();
	var nameoncard = $("#nameoncard").val();
	var CARDEXPIRYMONTH = $("#EWAY_CARDEXPIRYMONTH").val();
	var CARDEXPIRYYEAR = $("#EWAY_CARDEXPIRYYEAR").val();

	  if(cardnumber == "")
	  {
		alert("Please enter Credit Card Number");
		return false;
	  }
	  if(nameoncard == "")
	  {
		alert("Please enter Name on card");
		return false;		
	  }
	
	  $("#loadingdivtokenexist").show();  
	  $('#buttonpay').prop('disabled', true);
	  //$('#paycancel').prop('disabled', true);
	  //$('#backInvoicebutton').prop('disabled', true);
	
	  var cardtype = detectCardType(cardnumber);
	
	 if(cardtype == 'dinerclub' || cardtype == 'amex')
	 {
		 alert("Unfortunately Diners club cards and Amex cards are not accepted throughout this payment");
		 $('#buttonpay').prop('disabled', false);
		 return false;
	 }
	 else
	 {
		 
		  var today = new Date();		
			var full_month = today.getMonth(); //January is 0!
			var current_month = full_month < 10 ? '0' + full_month : full_month;
			var full_year = today.getFullYear();
			var current_year = full_year.toString().substr(2,2);

		 //console.log(CARDEXPIRYYEAR + '-' + current_year + '-' + CARDEXPIRYMONTH + '-' + current_month);
		 if(CARDEXPIRYYEAR == current_year && CARDEXPIRYMONTH <= current_month)	{
			 	alert('Please enter valid expiry date.');
				//clearFields();
				$('#buttonpay').prop('disabled', false);
				$('#loadingdivtokenexist').hide();
				return false;
		 }
		 
		 

	$.post(baseURL+"index.php/payment/updateCardDetails",{cardnumber : cardnumber , nameoncard : nameoncard , expmonth : CARDEXPIRYMONTH , expyear : CARDEXPIRYYEAR},function(data)
	 {
		//console.log('>'+data+'<');
		if(data == 'success')
		{
			   
		   $("#loadingdivtokenexist").hide();
		   //$('#editcardDetails').html('');
		   //$('#editcardDetails').html('Card is updated successfully.<br/><input type="button" id="submit" name="submit" onclick="parent.$.magnificPopup.close();" value="close" class="btn btn-primary signup" />');
		   //window.location = baseURL+"companyadmin/cardupdatesuccess";
		   $.post(baseURL+"index.php/companyadmin/cardupdatesuccess",function(data)
			{	
		 		parent.document.getElementById("ccnumberdetails").innerHTML = data;
				parent.$.magnificPopup.close();
				
			});
		   return false;
				 
		}
		else
		{
		  alert('Opps! Some unexpected error occured');
		  $("#loadingdivtokenexist").hide();  
		  //clearFields();
		  $('#buttonpay').prop('disabled', false);
		}
	 });
   }
}

function makeDirectPaymentTrue()
{
     {
        var selectedpkgid = $("#selectedpkgid").val();
        var noofdevices = $("#noofdevices").val();
            
            $("#paybody").hide();
            $("#paybutton").show();
            $("#paymentinvoice").hide();
            $("#nodevicep").val(noofdevices);  
            $('#packageid').val(selectedpkgid);     
         
      }
}


/*** code start ***/

	function showAddressDetails( id, noofdevices, pkgid )	{
			
			$.post(baseURL+"index.php/companyadmin/getAddressDetails",{userid : id, noofdevices : noofdevices, pkgid : pkgid},function(data)
		 {	
		 		$("#paybody1").show();
		 		$('#paybody').hide();
				$('#paybody1').html(data);
		 });
			
	}


/***  code end  ***/


function showInvoice(count,ID)
{
     var selectedpkgid = $("#selectedpkgid").val();
     var noofdevices = $("#noofdevices").val();
     var amountpaid = 0.00;
     //var devicecount = parseInt(totaluseralloweddevices) + parseInt(noofdevices);
	 var devicecount = parseInt(noofdevices);
     //if(devicecount > count)
	 if(noofdevices > count)
      {
    	 $("#paybutton").hide();
         $("#paymentinvoice").hide();
         $("#creditinfo").hide();
         $('#deviceinfo').html('');
         $('#deviceinfo').html('Sorry! Total number of devices exceeds package device limit');
         $("#paybody").show();
         
      }
     else {
    	 
     var url = baseURL+"index.php/companyadmin/getPackageById?packageid="+selectedpkgid;
 	 $.getJSON(url , function(data)
 	 {
 	    $.each(data , function(key,value)
 	    {
 	     
 	       if(key == 'price')
 	       {
 	          price = value;
 	       }
 	       if(key == 'additionaldeviceprice')
 	       {
 	          additionaldevicecharge = value;
 	       }

 	    });
 	   
 	 	 $.post(baseURL+'index.php/payment/fetchAmountAlreadyPaid', function(data){
 		  
 		 amountpaid = data; 
 		 
		 /*if(parseInt(data) < 0)
 		 	$('#amountpaid').html('-'+data);
		 else*/
		 
		 var buttonlable = 'Next';
		 
		  if(data > 0)
		   {
		    $('#amountpaid').html(data);
		   }else{
			   $('#amountpaid').html("0.00");
		 	  }
		  
 		  $('.packageprice').html(price);
		  $('#noofdevicess').html(devicecount);
 	      $('#deviceprice').html(additionaldevicecharge);
		  $('#totalprice').html((devicecount * additionaldevicecharge).toFixed(2));
		  $('#noofdevicessprevious').html(totaluseralloweddevices);
		  $('#devicepriceprevious').html("0.00");
 	      totalamount = parseFloat(price) + parseInt(devicecount) * parseFloat(additionaldevicecharge);
		  
		  if(totalamount > amountpaid)
		   {
			   if(amountpaid < 0) {
				   totalnetamount = parseFloat(totalamount);
			   } else {
				totalnetamount = parseFloat(totalamount) - parseFloat(amountpaid);   
			   }
		  	
			totalnetamount = totalnetamount.toFixed(2);
			$('#totalorcredit').html("Total owing");
			$('#payingamount').html(totalnetamount);
			$('#paymentorcredit').html("Payment Details");
			$('#paymentrequierdmessage').hide();
			buttonlable = 'Next';
			
		   }else{
			
			totalnetamount = parseFloat(amountpaid) - parseFloat(totalamount);
			//totalnetamount = totalnetamount.toFixed(2) + " credit";
			totalnetamount = totalnetamount.toFixed(2);
			$('#totalorcredit').html("Total credit");
			$('#paymentorcredit').html("No Payment Required");
			$('#paymentrequierdmessage').show();
			$('#paymentrequierdmessage').html("You have a credit of $"+totalnetamount+" and no payment is required");	
			buttonlable = 'Ok';
		    }

		  
 	      $('#totalpackageprice').html(totalamount);
		  
		  var gstValue = totalamount/11;
		  $('#gstValue').html(gstValue.toFixed(2));
		  
 	      $('#totalnetprice').html(totalnetamount);		  
		  
 	      $("#paybody").hide();
		  $("#creditinfo").hide();
 	      $("#paymentinvoice").show();
 	      $("#nodevicep").val(devicecount - 2);  
 	      $('#packageid').val(selectedpkgid); 
		  //signupclick(selectedpkgid);		
		  $('#btnBack').html('<button class="btn btn-primary pull-left" id="backpaybodybutton" onclick="backPayBody()"> Back</button>');
		  
 	      if(ID == 2)
 	    	  {
 	    	    $('#changebutton').html('');
 	    	    //$('#changebutton').html('<button class="pull-right btn btn-primary" id="autotpayment" onclick="makeAutoPaymentTrue('+autopaymentval+')"><i class="fa fa-forward"></i> '+text+'</button>');
				$('#changebutton').html('<button class="pull-right btn btn-primary" id="autotpayment" onclick="makeAutoPaymentTrue(2)"> '+buttonlable+'</button>');
 	    	  }
 	      else
 	    	  {
 	    	 $('#changebutton').html('');
	    	 $('#changebutton').html('<button class="btn btn-success pull-right" onclick="makeDirectPaymentTrue()"><i class="fa fa-envelope"></i> Pay Now</button>');
	    	  
 	    	  }
 		  
 	  })  ;
 	 });
     }

}

function backPayBody()
{
	$("#paybutton").hide();
	$("#paymentinvoice").hide();
	$("#creditinfo").hide();
	$("#paybody").show();
	/*custom code*/
		$("#paybody1").hide();
		/*custom code*/
}

/*custom code*/
	function backPayBody1()
	{
		$("#paybutton").hide();
		$("#paymentinvoice").hide();
		$("#creditinfo").hide();
		$("#paybody").hide();
		$("#paybody1").show();
	}
	/*custom code*/
	
function backInvoice()
{
	$("#paybutton").hide();
	$("#creditinfo").hide();
	$("#paybody").hide();
	$("#paymentinvoice").show();
}

function paypalSubmit()
{


}

function ewaySubmit()
{

$nameoncard = $_POST['nameoncardp'];
            $cardnumber = $_POST['cardnumberp'];
            $expirymonth = $_POST['EWAY_CARDEXPIRYMONTHp'];
            $expiryyear = $_POST['EWAY_CARDEXPIRYYEARp'];
            $amount = 19.95;
            $cvn = $_POST['cvnp'];
            $packageid = $_POST['selectedpkgid'];
            $noofdevice = $_POST['nodevicep'];          
            
 var nameoncardp = $("#nameoncardp").val();
 var cardnumberp = $("#cardnumberp").val();
 var EWAY_CARDEXPIRYMONTHp = $("#EWAY_CARDEXPIRYMONTHp").val();
 var EWAY_CARDEXPIRYYEARp = $("#EWAY_CARDEXPIRYYEARp").val();
 var cvnp = $("#cvnp").val();
 var selectedpkgid = $("#selectedpkgid").val();
 var nodevicep = $("#nodevicep").val();
         
            
            
}



function detectCardType(card)
{
                          $('#paypalbtn').prop('disabled', false);
                          $('#ewaybtn').prop('disabled', false);
                          
                          if(card.match('^4[0-9]{0,15}$') != null)
                          {
                               //visa
                               return 'visa';
                          }
                          else if(card.match('^5$|^5[1-5][0-9]{0,14}$') != null)
                          {
                               //MasterCard
                               return 'mastercard';
                          }
                          else if(card.match('^3$|^3[47][0-9]{0,13}$') != null)
                          {
                               //American Express
                               return 'amex';
                          }
                          else if(card.match('^3$|^3[068]$|^3(?:0[0-5]|[68][0-9])[0-9]{0,11}$') != null)
                          {
                        	       // Diners Club
                        	       return 'dinerclub';
                          }
                          else if(card.match('^6$|^6[05]$|^601[1]?$|^65[0-9][0-9]?$|^6(?:011|5[0-9]{2})[0-9]{0,12}$') != null)
                          {
                        	      // Discover
                        	      return 'discover';
                          }
                          else if(card.match('^2[1]?$|^21[3]?$|^1[8]?$|^18[0]?$|^(?:2131|1800)[0-9]{0,11}$|^3[5]?$|^35[0-9]{0,14}$') != null)
                          {
                              //JCB
                              return 'jcb';
                          }

}


function checkcard()
{

$('#paypalbtn').prop('disabled', true);
$('#ewaybtn').prop('disabled', true);
 var card = $("#cardnumberp").val();
 var cardtype = detectCardType(card);
 if(cardtype == 'dinerclub' || cardtype == 'amex')
 {
     alert("Unfortunately Diners club cards and Amex cards are not accepted throughout this payment!");
     return false;
 }
 else
 {
     $("#cardtype").val(cardtype);
     return true;
 }

}