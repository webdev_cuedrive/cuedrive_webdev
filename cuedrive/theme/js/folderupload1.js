window.requestFileSystem = window.requestFileSystem ||
                           window.webkitRequestFileSystem;
window.resolveLocalFileSystemURL = window.webkitResolveLocalFileSystemURL ||
    window.webkitResolveLocalFileSystemURL;

var fs = null;
var cwd = null;
var DONE_MSG = 'Donezo';
var NOT_IMG_MSG = 'One or more files is not an image.';



function readDirectory(dirEntry, callback) {
  var dirReader = dirEntry.createReader();
  var entries = [];

  // Call the reader.readEntries() until no more results are returned.
  var readEntries = function() {
     dirReader.readEntries (function(results) {
      if (!results.length) {
        callback(entries);
      } else {
        entries = entries.concat(toArray(results));
        readEntries();
      }
    }, onError);
  };

  readEntries(); // Start reading dirs.
}

function getEntry(fullPath, callback) {
  var fsUrl = fs.root.toURL() + fullPath;
  window.resolveLocalFileSystemURL(fsUrl, function(entry) {
    if (entry.isDirectory) {
      cwd = entry;
    }
    callback(entry);
  });
}




function renderImages(dirEntry) {
  readDirectory(dirEntry, function(entries) {
    // Handle no files case.
    if (!entries.length) {
      
      return;
    }

    

    var frag = document.createDocumentFragment();

    var span = document.createElement('span');
    span.innerHTML = '&laquo;';
    span.title = 'Move up a directory;';
    span.addEventListener('click', onThumbnailClick);
    frag.appendChild(span);

    entries.forEach(function(entry, i) {
      var div = document.createElement('div');

      div.dataset.fullPath = entry.fullPath;

      var img = new Image();
      if (entry.isDirectory) {
        img.src = 'folder.png';
        div.dataset.isDirectory = 'true';
      } else {
        //img.src = window.URL.createObjectURL(files[i]); // Equivalent to item.getAsFile().
        entry.file(function(f) {
          img.src = f.type.match('^image/') ? entry.toURL() : 'file.png';
        }, onError);
      }
    
      img.title = entry.name;
      img.alt = entry.name;

      var span = document.createElement('span');
      span.textContent = entry.name;

      var span2 = document.createElement('span');
      span2.textContent = 'X';
      span2.classList.add('close');
      span2.addEventListener('click', onClose);

      div.appendChild(span2);
      div.appendChild(img);
      div.appendChild(span);
      div.addEventListener('click', onThumbnailClick);

      frag.appendChild(div);
    });

   
  });
}

function onChange(e) {
  e.stopPropagation();
  e.preventDefault();

  var entries = e.target.webkitEntries;

  // Dragging and dropping into the file input works fine but onchange doesnt
  // populate .webkitEntries when selecting from the file dialog
  // (crbug.com/138987). Thus, we need to explicitly write out files.
  if (!entries.length) {
    var files = e.target.files;
    var numWritten = 0;

    [].forEach.call(files, function(f, i) {
      if (f.type.match('^image/')) {
        writeFile(f, cwd, function(e) {
          if (++numWritten) {
            setLoadingTxt({txt: DONE_MSG + ' writing ' + files.length + ' files.'});
            renderImages(cwd);
          }
        });
      } else {
        setLoadingTxt({txt: NOT_IMG_MSG, error: true});
      }
    });
    return;
  }

  [].forEach.call(entries, function(entry) {

    if (entry.isDirectory) {
      setLoadingTxt({
        txt: 'Importing directory: ' + entry.name,
        stayOpen: true
      });
    } else {
      setLoadingTxt({
        txt: 'Importing file: ' + entry.name,
        stayOpen: true
      });
    }

    // Copy entry over to the local filesystem.
    entry.copyTo(cwd, null, function(copiedEntry) {
      setLoadingTxt({txt: DONE_MSG});
      renderImages(cwd);
    }, onError);

  });
}

var flag = 0;
var uploadflag = 0;
function onDrop(e) {

  uploadflag = 0;
  if($("#dialog").dialog("isOpen")){
               $( "#dialog" ).dialog( "close" );
   }

  e.preventDefault();
  e.stopPropagation();

  var items = e.dataTransfer.items;
  var files = e.dataTransfer.files;

  for (var i = 0, item; item = items[i]; ++i) {
    // Skip this one if we didnt get a file.
    if (item.kind != 'file') {
      continue;
    }

    var entry = item.webkitGetAsEntry();
  
    if (entry.isDirectory) {
     
       //alert('folder::'+entry.name);
     
       readFileTree(entry, uploadFile);



    	
     } else {

        /*
         var parentfolderid=$("#selectedfolderid").val();
         var filenamestr = entry.name;
         $.post(baseUrl+'index.php/companyadmin/checkFileExist',{parentfolderid:parentfolderid,filenamestr:filenamestr},function(data)
				{
				
				if(data=="") //not exist
				{	
				    readFile(files, uploadFile);
				    
				}
				else //exist
				{					
					alert("File already exists in folder !");
				}
				});
	 
	 */
	 if(uploadflag == 0) {
	   handleFileUploadFolder(files); 			
           
           uploadflag = 1;
         }
        
		
    }
  }
}

function init() {
  document.querySelector('input[type="file"]').addEventListener('change', onChange);

  var dropZone = document.querySelector('[dropzone]');

  dropZone.addEventListener('drop', onDrop);

  dropZone.addEventListener('dragover', function(e) {
    e.preventDefault(); // Necessary. Allows us to drop.
  });

  dropZone.addEventListener('dragenter', function(e) {
    e.target.classList.add('active');
    
  });

  dropZone.addEventListener('dragleave', function(e) {
    e.target.classList.remove('active');
    
  });

  window.addEventListener('keydown', function(e) {
    if (e.keyCode == 27) { // ESC
      document.querySelector('details').open = false;
    }
  });

}

var mainEntries;
var currentIndex;
var maximumIndex;
var uploadFolderName;
var createdfolderid;

var readFileTree = function(itemEntry, fileCallback){	
	if(itemEntry.isFile){
		readFile(itemEntry, fileCallback);
		
	}
	else if(itemEntry.isDirectory){

       var expandednodesstr=$("#expandednodes").val();
	expandednodesstr += ','+$("#data_tt_id").val();


	if($("#selectedfolderid").val()=="")
	{
		alert("Please select parent folder !");	
		//$("#errmsg").html("<?php echo NOPARENTFOLDER;?>");			
	}

       else {
       
       
           var parentfolder=$("#selectedfoldername").val();
	   var parentfolderid=$("#selectedfolderid").val();
	   var level=$("#level").val();
           var permflag='parent';
           uploadFolderName = itemEntry.name;
           $.post('https://cuedrive.com/cuedrive/index.php/companyadmin/createDir',{parentfolderid:parentfolderid,newfoldername:uploadFolderName,level:level,permflag:permflag},function(data){
            
             if(parseInt(data) > 0) {
               
                       // alert('folder created');
                        createdfolderid=parseInt(data);
                        $("#selectedfolderid").val(createdfolderid);
			var dirReader = itemEntry.createReader();
			dirReader.readEntries(function(entries){

				mainEntries=entries;
				var idx = entries.length;
				currentIndex=0;
				maximumIndex=entries.length;
				readTreeFile();
			});
		  }
		else if(data == 'foldernameexists')
		{
		   alert('Folder aleready exists in cuedrive!');
		}
		else
		{
		   alert(data);
		}
	 });
       }
	}			
};

function readTreeFile()
{
	if(currentIndex+1 <= maximumIndex)
	{
	        
		if(mainEntries[currentIndex].isFile) {
			//alert(mainEntries[currentIndex].name+' '+'is a file!');
			readFile(mainEntries[currentIndex], uploadFile);
		
			}
		else if(mainEntries[currentIndex].isDirectory){
			//alert(mainEntries[currentIndex].name+' '+'is a folder!');
			//readFileTree(mainEntries[currentIndex], uploadFile);
			}
		currentIndex++;
	}
}




var readFile = function(fileEntry, callback) {
	//Get File object from FileEntry
	fileEntry.file(function(callback, file){
		if(callback){
			uploadFile(file);
		}
	}.bind(this, callback));
	
};




//Upload File ( Sample code, does not work )
var uploadFile = function(file){
     if(file && file instanceof File){
  
     var expandednodesstr=$("#expandednodes").val();
     expandednodesstr += ','+$("#data_tt_id").val();
     var folderid=$("#selectedfolderid").val();
     var fd = new FormData();
	 
       fd.append("parentfolderid", folderid);
       fd.append("override", "false");
       fd.append("selectedfiles[]", file);
      
     $("#progressbar").css("width","0%"); 
     $("#loaderModal").reveal();
     $("#mainloadingdiv").show();
     
      
          
       
          
      $.ajax({
     
        xhr: function()
	  {
	    var xhr = new window.XMLHttpRequest();
	    //Upload progress
	    xhr.upload.addEventListener("progress", function(evt){
	      if (evt.lengthComputable) {
	        var percentComplete = (evt.loaded / evt.total) * 100;
	            percentComplete = percentComplete.toFixed();
	        $("#percentage").html(percentComplete + '%');
	        $("#progressbar").css("width",percentComplete+"%");
	      }
	    }, false);
	    return xhr;
	  },
         url:'https://cuedrive.com/cuedrive/index.php/companyadmin/uploadFile',  
         type: 'POST',
         data: fd,
         success:function(data){
         
                  
                      if(flag == 0) {
                        readTreeFile();
                      }
                      
                      if(currentIndex+1 > maximumIndex || flag == 1) {
                      
                       if(data == 'success')
					{
			
                    	                        $("#mainloadingdiv").hide();
						$("#loadingdiv").show();
							$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
								
								$("#loadingdiv").hide();
								closefile();
								$("#tablewidget").html('');
								$("#tablewidget").html(data);
								$("#selectedfolderid").val('');
								 $("#loaderModal").trigger('reveal:close');
							});
							
			                       
			
					}
		        	else
		        	{
			        	alert("error");
		        	}
		  }      
		  
            }, 
         cache: false,
         contentType: false,
         processData: false
         
     }); 
       
    // alert('uploaded');
    }else alert('not a file');
};




$(document).ready(function() { 


$(".inner-div").on("dragenter",function(e){
e.stopPropagation();
e.preventDefault();

if($("#dialog").dialog("isOpen")){
$( "#dialog" ).dialog( "close" );
}
});

$(".right-side").on("dragenter",function(e){
e.stopPropagation();
e.preventDefault();

if(!$("#dialog").dialog("isOpen")){
$(".right-side").css("opacity","0.1");
$( "#dialog" ).dialog( "open" );
$(".ui-dialog-titlebar").hide();
$(".ui-widget-content").css({"border":"none","background":"none"});
}
});

function addEvent(obj, evt, fn) {
   if (obj.addEventListener) {
       obj.addEventListener(evt, fn, false);
   }
   else if (obj.attachEvent) {
       obj.attachEvent("on" + evt, fn);
   }
}
addEvent(window,"load",function(e) {
   addEvent(document, "mouseout", function(e) {
       e = e ? e : window.event;
       var from = e.relatedTarget || e.toElement;
       if (!from || from.nodeName == "HTML") {
        e.stopPropagation();
        e.preventDefault();

        if($("#dialog").dialog("isOpen")){
        $( "#dialog" ).dialog( "close" );
        }
       }
   });
});




     $(".right-side").on('dragover', function (e)
		{
		e.stopPropagation();
		e.preventDefault();
		});
	  $(document).on('dragover', function (e)
		{
		 e.stopPropagation();
		 e.preventDefault();

		});
	  $(document).on('drop', function (e)
		{
		e.stopPropagation();
		e.preventDefault();
		 if($("#dialog").dialog("isOpen")){
                     $( "#dialog" ).dialog( "close" );
                  }
			
		});
	 $(".right-side").on('drop', function (e)
		{
		e.stopPropagation();
		e.preventDefault();
		 if($("#dialog").dialog("isOpen")){
                       $( "#dialog" ).dialog( "close" );
                  }
			
		});


});




function handleFileUploadFolder(files)
{
   	        var parentfolder=$("#selectedfoldername").val();
		var parentfolderid=$("#selectedfolderid").val();
		var filenamestr="";
		
			 
			// Loop through each of the selected files.
		     for (var i = 0; i < files.length; i++) {
		     var file = files[i];
		       // Add the file to the request.
		      // fd.append('selectedfiles[]', file, file.name);
		      if(filenamestr=="")
		    	  filenamestr=file.name;	
		      else
		      	  filenamestr=filenamestr+"@#"+file.name;		      
		    }
			
			if(filenamestr=="")
			{
				alert("Please select a file !");
			}
			else
			{
				
				$.post(baseUrl+'index.php/companyadmin/checkFileExist',{parentfolderid:parentfolderid,filenamestr:filenamestr},function(data)
				{
				
				if(data=="") //not exist
				{	
				   //uploadFileAjax(parentfolderid,"false");
				    sendFileToServerFolder(files,parentfolderid,parentfolder,'false');
				}
				else //exist
				{					
					if (confirm(data+" already exist.Do you want replace it?")) { 
						//uploadFileAjax(parentfolderid,"true");
					 sendFileToServerFolder(files,parentfolderid,parentfolder,'true');
					}
				}
				});
				
				
			}

}


function sendFileToServerFolder(files,parentfolderid,parentfolder,override)
{
       
      	var expandednodesstr=$("#expandednodes").val();
      	expandednodesstr += ','+$("#data_tt_id").val();
	var fd = new FormData();
	 
       fd.append("parentfolderid", parentfolderid);
       fd.append("override", override);
      // Loop through each of the selected files.
      for (var i = 0; i < files.length; i++) {
       var file = files[i];
       // Add the file to the request.
       fd.append("selectedfiles[]", file);
      }
      
     $("#progressbar").css("width","0%"); 
     $("#loaderModal").reveal();
     $("#mainloadingdiv").show();
     
     $.ajax({
     
        xhr: function()
	  {
	    var xhr = new window.XMLHttpRequest();
	    //Upload progress
	    xhr.upload.addEventListener("progress", function(evt){
	      if (evt.lengthComputable) {
	        var percentComplete = (evt.loaded / evt.total) * 100;
	            percentComplete = percentComplete.toFixed();
	        $("#percentage").html(percentComplete + '%');
	        $("#progressbar").css("width",percentComplete+"%");
	      }
	    }, false);
	    return xhr;
	  },
         url: baseUrl+'index.php/companyadmin/uploadFile',  
         type: 'POST',
         data: fd,
         success:function(data){
                    	if(data=="success")
					{
			
                    	 $("#mainloadingdiv").hide();
						$("#loadingdiv").show();
							$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
								
								$("#loadingdiv").hide();
								closefile();
								$("#tablewidget").html('');
								$("#tablewidget").html(data);
								$("#selectedfolderid").val('');
							});
							
			$("#loaderModal").trigger('reveal:close');
			
					}
		        	else
		        	{
			        	alert(data);
								$('.reveal-modal-bg').hide();
								$('#loaderModal').hide();
		        	}
             }, 
         cache: false,
         contentType: false,
         processData: false
         
     });
      	
}