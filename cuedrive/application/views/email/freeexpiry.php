<!DOCTYPE HTML>
<html>
<head>
</head>
<body style='background-color:#f9f9f9;'>
<center>
<div style='width:900px; height:auto; padding-top:50px; padding-bottom:100px;'>
<div style='margin:0;padding:0; height:60px; background-color:Black;'>
<center>
<img alt='cuedrive' style='padding-top:5px;' src='<?php echo base_url()?>cuedriveLogowhite.png' width='150'/>
</center>
</div>
<div style='text-align:left; background-color:#fff; padding:20px;'>


<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Dear <?php echo $fullname;?>,</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Unfortunately your cuedrive account "<b><?php echo $username;?></b>" has expired. If you wish to join the cuedrive team and become a full member please cleck here <?=ORG_URL?>login/authenticate.php. 
The account will stay active with your information for one month,after this time your information will be removed from the cuedrive system.We hope you can continue your journey with cuedrive team.</span></span></p>


<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Please visit us at <a href="https://cuedrive.com">cuedrive</a> to renew the account and continue your use.</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Need extra help? Our support team <a href="https://cuedrive.com">https://cuedrive.com</a> has the answers you need to get up and running fast. Contact us by phone or email, or visit the Help Centre <a href="<?=ORG_URL?>login/faq.php"><?=ORG_URL?>login/faq.php</a>.</span></span></p>

<p>&nbsp;</p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Sincerely,<br />
<strong><b>The cuedrive team</b></strong></span></span></p>

<p>&nbsp;</p>
</div>
</div>
</center>
</body>
</html>