<!DOCTYPE HTML>
<html>
<head>
</head>
<body style='background-color:#f9f9f9;'>
<center>
<div style='width:900px; height:auto; padding-top:50px; padding-bottom:100px;'>
<div style='margin:0;padding:0; height:60px; background-color:Black;'>
<center>
<img alt='cuedrive' style='padding-top:5px;' src='<?php echo base_url()?>cuedriveLogowhite.png' width='150'/>
</center>
</div>
<div style='text-align:left; background-color:#fff; padding:20px;'>

<p><span style='font-size:20px'><span style="<?php echo FONTFAMILY;?>"><strong>Your cuedrive setup is complete.</strong></span></span></p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>"><b>Thanks for joining us at cuedrive! Your paperless business revolution begins now.</b></span></span></p>

<ul>
<li><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">User name: <?php echo $username;?></span></span></li>
<li><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">You have chosen <b><?php echo $accounttype;?></b> plan,starting <?php echo $date;?></span></span></li>
<li><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Account type: <?php echo $accounttype;?></span></span></li>
<li><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Storage available: <?php echo $storage;?></span></span></li>
<li><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Devices available: <?php echo $devices;?></span></span></li>
</ul>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Click here to begin migrating data and registering your devices <a href='<?php echo base_url()?>index.php/companyadmin/folders'><?php echo base_url()?>index.php/companyadmin/folders</a></span></span></p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">To get off to a great start with cuedrive, visit the Help centre <a href="<?=ORG_URL?>login/faq.php"><?=ORG_URL?>login/faq.php</a> to learn about administering your account and migrating your data.</span></span></p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Need extra help? Our support team <a href="<?=ORG_URL?>login/authenticate.php"><?=ORG_URL?>login/authenticate.php</a> has the answers you need to get up and running fast. Contact us by phone or email, or visit the Help Centre <a href="<?=ORG_URL?>login/aboutus.php"><?=ORG_URL?>login/aboutus.php</a>.</span></span></p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Welcome to the revolution.</span></span></p>

<p>&nbsp;</p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Sincerely,<br />
<strong><b>The cuedrive team</b></strong></span></span></p>

<p>&nbsp;</p>
</div>
</div>
</center>
</body>
</html>