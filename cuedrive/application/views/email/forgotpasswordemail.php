<!DOCTYPE HTML>
<html>
<head>
<style type="text/css">
@font-face {
   font-family: myFirstFont;
   src: url(<?php echo base_url()?>AftaSansThin-Regular.otf);
}

</style>

</head>
<body style='background-color:#f9f9f9;'>
<center>
<div style='width:900px; height:auto; padding-top:50px; padding-bottom:100px;'>
<div style='margin:0;padding:0; height:60px; background-color:Black;'>
<center>
<img alt='cuedrive' style='padding-top:5px;' src='<?php echo base_url()?>cuedriveLogowhite.png' width='150'/>
</center>
</div>
<div style='text-align:left; background-color:#fff; padding:20px;'>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Hello <?php echo $fullname;?>,</span></span></p>

<!--<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Your password has been updated,</span></span></p>-->

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Your cuedrive Password for ID "<b><?php echo $username;?></b>" has been successfully reset.</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Password: <b><?php echo $password;?></b></span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">If you didn’t make this change or if you believe an unauthorized person has accessed your account, go to <a href="<?=ORG_URL?>login/forgotpassword.php">password reset</a> to reset your password immediately.</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Need extra help? Our support team <a href="https://cuedrive.com">https://cuedrive.com</a> has the answers you need to get up and running fast. Contact us by phone or email, or visit the Help Centre <a href="<?=ORG_URL?>login/faq.php"><?=ORG_URL?>login/faq.php</a>.</span></span></p>

<p>&nbsp;</p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Sincerely,<br />
<strong><b>The cuedrive team</b></strong></span></span></p>

<p>&nbsp;</p>
</div>
</div>
</center>
</body>
</html>