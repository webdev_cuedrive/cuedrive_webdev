<!DOCTYPE HTML>
<html>
<head>
</head>
<body style='background-color:#f9f9f9;'>
<center>
<div style='width:900px; height:auto; padding-top:50px; padding-bottom:100px;'>
<div style='margin:0;padding:0; height:60px; background-color:Black;'>
<center>
<img alt='cuedrive' style='padding-top:5px;' src='<?php echo base_url()?>cuedriveLogowhite.png' width='150'/>
</center>
</div>
<div style='text-align:left; background-color:#fff; padding:20px;'>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>"><b>Thanks for joining us at cuedrive! Your paperless business revolution begins now.</b></span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">We have created a new cuedrive account for you, </span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">User: <?php echo $username;?></span></span></p>
<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Password: <?php echo $password;?></span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Click here to begin migrating data and registering your devices <a href="https://cuedrive.com">https://cuedrive.com</a>.</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">To get off to a great start with cuedrive, visit the <a href="https://cuedrive.com">Help centre</a> to learn about administering your account and migrating your data.</span></span></p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Need extra help? Our support team <a href="https://cuedrive.com">https://cuedrive.com</a> has the answers you need to get up and running fast. Or <a href="mailto:info@cuedrive.com">contact us</a> by phone or email anytime.</span></span></p>

<p><span style='font-size:16px'><span style="<?php echo FONTFAMILY;?>">Welcome to the revolution.</span></span></p>

<p>&nbsp;</p>

<p><span style="font-size:16px"><span style="<?php echo FONTFAMILY;?>">Sincerely,<br />
<strong><b>The cuedrive team</b></strong></span></span></p>

<p>&nbsp;</p>
</div>
</div>
</center>
</body>
</html>