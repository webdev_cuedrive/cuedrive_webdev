<?php
	include("header.php");
	include("sidebar.php");
	
	/*if($this->session->userdata('companyid') == 224 || $this->session->userdata('companyid') == 250) {*/
?>
	<!--<script src="<?php echo base_url()?>theme/js/jquery-1.9.1.js" type="text/javascript"></script>-->
		
	<script src="<?php echo base_url()?>theme/js/jquery.magnific-popup.js" type="text/javascript"></script>	
	<link rel="stylesheet" href="<?php echo base_url(); ?>theme/css/magnific-popup.css">
	
	
	
	<?php /*} 	*/?>
<?php
	if(!$this->session->userdata("companyid")) 
	{
		header ("Location:".base_url()."index.php/login");
		exit;
	}
	$this->load->helper('form');
	$att = array('name' => 'signup','id' => 'signup');
	$closeBtn = "";
	if(isset($_GET['id']))
	{
		$id = base64_decode($_GET['id']);
		echo '<script>
			$(document).ready(function() {
		   		signupclick('.$id.');
		 	});
			$("body").keyup(function (event) {
				if (event.which === 27) { // 27 is the keycode for the Escape key
				  modal.trigger("reveal:close");
				}
			  });
		</script>';
		$closeBtn = 'style = "display : block;"';
}
?>
       <script src="<?php echo base_url()?>theme/js/payment.js"></script>
        <input type="hidden" name="selectedpkgid" id="selectedpkgid"/>
				
								<?php /** Custom Code **/ ?>
				<input type="hidden" name="companyuserid" id="companyuserid" value="<?php echo $this->session->userdata("companyid") ?>"/>
				<?php
					if($company_name_mob_user == 'Please fill in company name')	{	?>
						<input type="hidden" id="isMobUser" name="isMobUser" value="1"	/>
				<?php	}	else	{	?>
						<input type="hidden" id="isMobUser" name="isMobUser" value="0"	/>
				<?php	}
				?>	
				<?php /** Custom Code **/ ?>
				
	<aside class="right-side">  
	<?php if(isset($errorMsg)){?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">�</button>
       <?php echo $errorMsg;?>
		</div>
	<?php } ?> 
              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Manage Subscription
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"> Manage Subscription</a></li>
                        <li class="active">Select Package</li>
                    </ol>
                </section>
	
	<section class="content invoice">  
	<?php	if(isset($paymentMessage) && $paymentMessage != '')	{	?>
		<div class="alert alert-info">
			<?php	echo $paymentMessage;	?>
		</div>		
	<?php	}	?>
	<?php if($cexpirydate !== FALSE)	{	?>
		<div class="alert alert-danger">
			<center>Your account has expired on the <?php echo $cexpirydate; ?></center>
		</div>
		<?php	}	?>	
	 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="">
                            Select Package
                            <a href="<?php echo base_url()?>index.php/companyadmin/corporate" style="float:right;" class="btn btn-primary signup">Apply for Corporate Account</a>
                            
                            </h2> <br>                   
                        </div><!-- /.col -->
           </div>
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results)>0){ 
							$i=1;
							foreach($results['price'] as $row)
							{						
							 if($row == $activepackage[0]['price'])
							 {
							   break;
							 }
							 $i++;
							}
					?>
						<table class="table">
							<thead>
							<tr>
									<th align='center' width="20%;" style="border-bottom:1px;">Details</th>
									<?php 
									$j=1;	
									foreach($results['type'] as $row)
									{
									
										if($j == $i)
										  echo "<td class='currentpackage' align='center' width='26.6%;'><b>{$row}</b></td>";
										else
										  echo "<td align='center' width='26.6%;'><b>{$row}</b></td>";
										 
									  $j++;	
									
									}
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Price</th>
									<?php 
									$j=1;	
									foreach($results['price'] as $row)
									{
										if($row != '0')
										 {
											if($j == $i)
											echo "<td class='currentpackage' align='center'>".CURRENCY."{$row}</td>";
											else
											  echo "<td align='center'>".CURRENCY."{$row}</td>";
										 }else{
											if($j == $i)
											echo "<td class='currentpackage' align='center'> Free</td>";
											else
											  echo "<td align='center'> Free</td>";
										    }	  
										 
									  $j++;	
									}
									?>
								</tr>
								
								<tr>
									<th align='center' style="border-bottom:1px;">Storage Space</th>
									<?php 	
									$j=1;
									foreach($results['storagespace'] as $row)
									{
										if($j == $i)
										  echo "<td class='currentpackage' align='center'>{$row}</td>";
										else
										  echo "<td align='center'>{$row}</td>";
										 
									  $j++;	
									}
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Number Of Devices</th>
									<?php
									$j=1; 	
									foreach($results['noofdevices'] as $row)
									{
										if($row != 2)
										 {	
											if($j == $i)
											  echo "<td class='currentpackage' align='center'>".$activepackage[0]['noofdevicesallowed'] ." Devices (Max {$row})</td>";
											else
											  echo "<td align='center'>{$row}</td>";
											  
										 }else{
										 
											if($j == $i)
											  echo "<td class='currentpackage' align='center'>{$row}</td>";
											else
											  echo "<td align='center'>{$row}</td>";
										 
									  	  }	  
											  
										  $j++;	
									   }		
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Duration</th>
									<?php
									$j=1;
									foreach($results['forduration'] as $row)
									{
										if($j == $i)
										  echo "<td class='currentpackage' align='center'>{$row}</td>";
										else
										  echo "<td align='center'>{$row}</td>";
										 
									  $j++;	
									}
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Payment Mode</th>
								<?php 
								        $j=1;
									foreach($results['additionaldeviceprice'] as $row)
									{
										if($j == $i)
										  echo "<td class='currentpackage' align='center'>Prepaid</td>";
										else
										  echo "<td align='center'>Prepaid</td>";
										 
									  $j++;	
									}
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Additional Device</th>
									<?php 
									$j=1;
									foreach($results['additionaldeviceprice'] as $row)
									{
									    if($j == $i) {
										if($row!="unavailable")
											echo "<td class='currentpackage' align='center'>".CURRENCY."{$row} / Device</td>";
										else 
											echo "<td class='currentpackage' align='center'>{$row}</td>";
											
									     }
									     else
									     {
									        if($row!="unavailable")
											echo "<td align='center'>".CURRENCY."{$row} / Device</td>";
										else 
											echo "<td align='center'>{$row}</td>";
									     
									     }
									     
									 $j++;
									}
									?>
								</tr>
								<tr>
									<th align='center' style="border-bottom:1px;">Email Support</th>
									<?php 
									$j=1;
									foreach($results['emailsupport'] as $row)
									{
										if($j == $i)
										  echo "<td class='currentpackage' align='center'>{$row}</td>";
										else
										  echo "<td align='center'>{$row}</td>";
										 
									  $j++;	
									}
									?>
								</tr>
								<tr style="display:none;">
									<th align='center'>Mobile(Android,iOS)</th>
									<?php 
									$j=1;
									foreach($results['mobile'] as $row)
									{
										if($j == $i)
										  echo "<td class='currentpackage' align='center'>{$row}</td>";
										else
										  echo "<td align='center'>{$row}</td>";
										 
									  $j++;	
									}
									?>
								</tr>
								
								<tr>
								<?php 
									echo "<td></td>";
								    $j=1;
									foreach($results['packageid'] as $row)
									{
									//echo "<br/>i = " . $i . " - j = " . $j . " - row = " . $row;
									        if($j == $i)
									        {
												
												if($row!=$j)
												{
													$formbutton=array("name" => $row,"id" => $row,"class" => "btn btn-primary signup");
													$js = 'onClick="signupclick('.$row.')"';
													echo "<td align='center'>".form_button($formbutton, 'Current Package',$js)."</td>";
												}else{
													$formbutton=array("name" => $row,"id" => $row,"class" => "btn btn-primary signup","disabled" => "true");
													$js = 'onClick="signupclick('.$row.')"';
													echo "<td align='center'>".form_button($formbutton, 'Current Package',$js)."</td>";
												     }
													
												
									        }
									        else if($j < $i)
									        {
													
												if($row!=$j && $row%$j != 0)
												{
													$formbutton=array("name" => $row,"id" => $row,"class" => "btn btn-primary signup");
													$js = 'onClick="signupclick('.$row.')"';
													echo "<td align='center'>".form_button($formbutton, 'Select',$js)."</td>";
												}else{
													$formbutton=array("name" => $row,"id" => $row,"class" => "btn btn-primary signup","disabled" => "true");
													echo "<td align='center'>".form_button($formbutton, 'Select')."</td>";
													}
									        }

									        else
									        {
												$formbutton=array("name" => $row,"id" => $row,"class" => "btn btn-primary signup");
												$js = 'onClick="signupclick('.$row.')"';
												echo "<td align='center'>".form_button($formbutton, 'Select',$js)."</td>";
											}
											
								        $j++;
										
									}
									?>
								</tr>
								
							</thead>
							<tbody>
							
							</tbody>
						</table>
						<?php }?>
				
				</div>
				<?php /*if($this->session->userdata('companyid') == 224 || $this->session->userdata('companyid') == 250)	{	*/	?>
				<div class="table">										
					<a class="amend_account" href="<?php echo base_url('index.php/companyadmin/loadpasswordfrm'); ?>">Amend Account</a>
				</div>
				<hr/>
				
				<div class="row">
					<div class="col-xs-12">
						<h2 class="">
						Billing
						</h2> <br>                   
					</div><!-- /.col -->
				</div>
				
				<div class="block-body collapse in" id="tablewidget">
					<table class="table">
						<thead>
						
							<tr style="display: none;">
									<th width="20%;" align="center" style="border-bottom:1px;">&nbsp;</th>
									<td width="26.6%;" align="center">&nbsp;</td>					
									<td width="26.6%;" align="center">&nbsp;</td>		
							</tr>
							
							<tr>
									<th align="center" style="border-bottom:1px;">Current Credit Card</th>
									
									<td align="left" id="ccnumberdetails"> 
									<?php if(is_array($tokenCustomerDetails)) {	
											echo $tokenCustomerDetails['ccnumber'];
										}	else	{
											echo 'Please add your card here.';
										}	?>
									</td>
									
									<td align="center"> 
									<?php if($Ctoken == '')	{	?>
										<a href="<?php echo base_url('index.php/companyadmin/updatecard'); ?>" class="editcreditcard">Add</a>
									<?php } else { ?>
										<a href="<?php echo base_url('index.php/companyadmin/updatecard'); ?>" class="editcreditcard">Edit</a>
									<?php } ?>
									</td>
							</tr>	
							
							<tr>
									<th align="center" style="border-bottom:1px;">&nbsp;</th>
									<td align="center">&nbsp; </td>
									<td align="center">&nbsp; </td>
							</tr>								
						
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
				
				<?php /*}*/	?>
				
			</section>
			
	
	<div id="fadeandscale" class="reveal-modal" style="background-color: white">
     
                   <div id="paybody">
                                <div class="modal-header" style="padding:0px 7px 0px 0px; border-bottom:0px;">
                                    <h4 class="modal-title">Package Details</h4>
                                    <div style="color:#990000; font-size: 0px solid;" id="deviceinfo"></div>
                                </div>
                                <div class="">
                                    <table class="table">
                                        <tbody id="tbody">
                                        </tbody>
                                    </table>
                                   
                                </div>
                                 
                                
                  </div>
									<div id="paybody1">
									</div>
                  
            <!-- ID paybutton Start  -->      
          <div style="display:none;" id="paybutton" class="row">
                   
                   <div class="modal-header" style="padding:0px 7px 0px 0px;">    
                        <h4 class="modal-title">Credit Card Information</h4>
                   </div>
                        
		   <div id="tablewidget" class="block-body collapse in">
		   <form action="<?php echo base_url()?>index.php/payment/ewayDirectPayment" method="post" id="directpaymentform" name="directpaymentform">
		   <label id="errormessage" style="color:red;">**unfortunately Diners club cards and Amex cards are not accepted throughout this payment</label> <br/><br/>
	
	            <div class="error_box error-txt"> </div>
		 	
		 	<label>Card Number <span class="text-danger">*</span> </label>
         	        <input type="text" placeholder="Credit Card number" class="form-control" name="cardnumberp" id="cardnumberp" required>
         	        <input type="hidden" name="nodevicep" id="nodevicep">  
         	        <input type="hidden" name="packageid" id="packageid">   
         	        <input type="hidden" name="cardtype" id="cardtype">   
                        <br/>
         	        <label>Name On Card <span class="text-danger">*</span> </label>
         	        
         	        <input type="text" placeholder="Name on card" class="form-control" name="nameoncardp" id="nameoncardp" required>  
         	                 
         	        <br/>
									
         					<div id="creditfloatleftdata1">
         	        <label>Expiry Month</label>
         	        
         	        
         	         <select id="EWAY_CARDEXPIRYMONTHp" name="EWAY_CARDEXPIRYMONTHp" class="form-control">
                          <?php
                           $expiry_month = date('m');
                            for($i = 1; $i <= 12; $i++) {
                            $s = sprintf('%02d', $i);
                            echo "<option value='$s'";
                            if ( $expiry_month == $i ) {
                                echo " selected='selected'";
                            }
                             echo ">$s</option>\n";
                            }
                           ?>
                         </select> <br/>
                         <label>Expiry Year</label>
                         <select id="EWAY_CARDEXPIRYYEARp" name="EWAY_CARDEXPIRYYEARp" class="form-control">
                          <?php
                            $i = date("y");
                            $j = $i+11;
                            for ($i; $i <= $j; $i++) {
                            echo "<option value='$i'>$i</option>\n";
                           }
                           ?>
                         </select>
         	        
         	        
         	        </div>
         	        <div id="creditfloatrightdata">
										<?php	/*  Begin eWAY Linking Code */	?>
										<div id="eWAYBlock">
												<div style="text-align:center;">
														<a href="https://www.eway.com.au/secure-site-seal?i=11&s=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
																<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" />
														</a>
												</div>
										</div>
									<?php	/* End eWAY Linking Code */	?>
									</div> 
									<div class="clearboth"></div>          
         	        <br/>
         	        
         	        <label>CVN <span class="text-danger">*</span> </label>
         	        
         	        <input type="text" class="form-control" name="cvnp" id="cvnp" data-inputmask='"mask": "999"' data-mask required>  
         	                 
         	        <br/>
        	
                       <!-- <a class="pull-right" href="javascript:paypalSubmit()"><img src="<?php echo base_url();?>/paypal.png" alt="paypal" /></a> -->

                       <!-- <a class="pull-left" href="javascript:ewaySubmit()"><img src="<?php echo base_url();?>/eway.gif" alt="eWAY" height="32px;" width="145px;"/></a> -->
                        <input class="btn btn-primary pull-right" type="submit" id="paypalbtn" value = "Pay with Paypal" name = "button" onclick="return checkcard()"/>
                        <input class="btn btn-primary pull-left" type="submit" id="ewaybtn" value="Pay with eWAY"  name = "button" onclick="return checkcard()"/>
                    </form>

                    </div>  
                  </div>   
                  
             <!-- ID paybutton End  -->  
             
             <!-- ID paymentinvoice Start  --> 
                <div class="" id="paymentinvoice" style="display:none;">                    
                    <!-- title row -->
                   <!-- <div class="row">
                        <div class="col-xs-12">
						 <h4 class="page-header">Payment Details
                                <small class="pull-right">Date: <?php //echo date('d-m-Y');?></small>
                            </h4>                            
                        </div>
                    </div>-->
					
					 <div class="modal-header" style="padding:0px 7px 0px 0px;">    
                        <h4 class="modal-title"><span id="paymentorcredit"> Payment Details  </span> 
							<small class="pull-right" style="padding-top:7px;"><?php echo date("jS F, Y", strtotime(date("y.m.d")));?></small>
						</h4>
						 <div style="color:#31708f; font-size:0px solid; padding-top:5px;" id="paymentrequierdmessage"></div>
                    </div>
               
                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th><center>Quantity</center></th>
                                        <th><center>Price</center></th>
                                        <!--<th><center>GST</center></th>-->
										 <th><center>&nbsp;</center></th>
                                        <th style="text-align:right;">Amount (AUD)</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Package Amount</td>
                                        <td><center>1</center></td>
                                        <td><center class="packageprice">19.95</center></td>
                                        <td style="text-align:right;" width="40px">$</td>
                                        <td style="text-align:right;" class="packageprice" width="75px">19.95</td>
                                    </tr>
									<tr>
                                        <td>Devices Previous</td>
                                        <td><center id="noofdevicessprevious">4</center></td>
                                        <td><center id="devicepriceprevious">10</center></td>
                                        <td style="text-align:right;" width="40px">$</td>
                                        <td style="text-align:right;" id="totalpriceprevious" width="75px">0.00</td>
                                    </tr>
									<tr>
                                        <td>Devices New </td>
                                        <td><center id="noofdevicess">4</center></td>
                                        <td><center id="deviceprice">10</center></td>
                                        <td style="text-align:right;" width="40px">$</td>
                                        <td style="text-align:right;" id="totalprice" width="75px">40</td>
                                    </tr>


                                  
                                </tbody>
                            </table>                            
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-5" style="padding-right:0px;">   
                            <h4 class="modal-title" style="margin-bottom:5px;">Payment Methods</h4>
                            <img src="<?php echo base_url();?>theme/images/icon/visa.png" alt="Visa" width="40"/>
                            <img src="<?php echo base_url();?>theme/images/icon/mastercard.png" alt="Mastercard" width="40"/>
                            <img src="<?php echo base_url();?>theme/images/icon/mestro.png" alt="Mestro" width="40"/>

                        </div><!-- /.col -->
                        <div class="col-xs-7" style="padding-left:0px;">
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:53%">Subtotal</th>
										 <td style="text-align:right;" width="49px">$</td>
                                        <td style="text-align:right;" id="totalpackageprice" width="75px">59.95</td>
                                    </tr>
                                    <tr>
                                        <th>Includes GST</th>
										 <td style="text-align:right;" width="49px">$</td>
                                        <td style="text-align:right;" id="gstValue" width="75px">10%</td>
                                    </tr>
                                    <tr>
                                        <th>Credit</th>
										 <td style="text-align:right;" width="49px">$</td>
                                        <td style="text-align:right;" id="amountpaid" width="75px">(-)20.95</td>
                                    </tr>
                                    <tr>
                                        <th id="totalorcredit">Total</th>
										 <td style="text-align:right;" width="49px">$</td>
                                        <td style="text-align:right;" id="totalnetprice" width="75px">59.95</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-xs-12">
                            <button class="btn btn-default" onclick="window.print();" style="display:none;"> Print</button>
							<span id="btnBack"></span>
                            <span id="changebutton"><button class="btn btn-success pull-right" onclick="makeDirectPaymentTrue()">Pay Now</button></span>
                        </div>
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
                    </div>
                </div>    
             <!-- ID paymentinvoice End  --> 
                
                
             <!-- ID creditinfo Start  --> 
                   
             <div id="creditinfo" class="row" style="display:none;">
                    
					<div class="modal-header" style="padding:0px 7px 0px 0px;">    
                        <h4 class="modal-title">Credit Card Information</h4>
                    </div>
                        
				    <div id="tablewidget" class="block-body collapse in">
		 			
                    <small class="pull-right" style="padding-top:13px;">You are paying $<span id="payingamount">0.00</span></small><br />
					<label style="padding-top:14px;">Card Number</label>
         	        <input type="text" placeholder="Credit Card number" class="form-control" name="cardnumber" id="cardnumber" required>
                    <br/>
         	        <label>Name On Card</label>
         	        <input type="text" placeholder="Name on card" class="form-control" name="nameoncard" id="nameoncard" required>  
         	        <input type="hidden" name="nodevice" id="nodevice">            
         	        <br/>
									<div id="creditfloatleftdata2">
         	        <label>Expiry Month</label>
         	         <select id="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH" class="form-control" style="width:50%;">
                          <?php
                           $expiry_month = date('m');
                            for($i = 1; $i <= 12; $i++) {
                            $s = sprintf('%02d', $i);
                            echo "<option value='$s'";
                            if ( $expiry_month == $i ) {
                                echo " selected='selected'";
                            }
                             echo ">$s</option>\n";
                            }
                           ?>
                         </select>
						 <br />
                         <label>Expiry Year</label>
                         <select id="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR" class="form-control" style="width:50%;">
                          <?php
                            $i = date("y");
                            $j = $i+11;
                            for ($i; $i <= $j; $i++) {
                            echo "<option value='$i'>$i</option>\n";
                           }
                           ?>
                         </select>
         	       </div>
								 <div id="creditfloatrightdata1">
								 	<?php	/*  Begin eWAY Linking Code */	?>
										<div id="eWAYBlock">
												<div style="text-align:center;">
														<a href="https://www.eway.com.au/secure-site-seal?i=11&s=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
																<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" />
														</a>
												</div>
										</div>
									<?php	/* End eWAY Linking Code */	?>
								 </div>  
								 <div class="clearboth"></div>
         	        <br/>
					
					 <label>CVN <span class="text-danger">*</span> </label>
         	        
         	        <input type="text" style="width: 100px;" class="form-control" name="cvnp" id="cvnp" data-inputmask='"mask": "999"' data-mask required>  
         	          <br/>
					  <br/>
        			<div class="row">
						<div class="col-xs-4" id="backInvoice"></div>
						<div class="col-xs-4" style="text-align:center;">
						<button class="btn btn-primary" type="button" onclick="closeupdatemodal()" id="paycancel"> Cancel</button>
						</div>
						<div class="col-xs-4">
							 <button class="btn btn-primary pull-right" id="buttonpay" type="submit" onclick="return submitCardInfo()"> Pay Now</button>	
         				</div>
						<center><div id="loadingdivtokenexist" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
					</div>

             </div>  
             </div>      
             <!-- ID creditinfo Start  --> 
    
     <div style="margin-top: 10px">
        <a class="close-reveal-modal" <?php echo $closeBtn; ?>>&#215;</a>
     </div>
    
</div>

<script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>		
<script src="<?php echo base_url()?>theme/js/jquery.inputmask.js" type="text/javascript"></script>
<script type="text/javascript"> 
/*
function signupclick(pkgid)
{
	document.getElementsByName("selectedpkgid")[0].value = pkgid;
	$("#signup").submit();
}
*/

$(document).ready(function() {

        $(".eway-button").attr("id","eway");
        $('.eway-button').removeClass('eway-button');
        $("[data-mask]").inputmask(); 
        
});
 $('#cardnumber').change(function(){
    cc_number_saved = $('#cardnumber').val();
    cc_number_saved = cc_number_saved.replace(/[^\d]/g, ''); 
    if(!checkLuhn(cc_number_saved)) 
    { 
      alert('Sorry, that is not a valid number - please try again');
      //$('#cardnumber').val('');
    }        
});

$('#cardnumberp').change(function(){
    cc_number_saved = $('#cardnumberp').val();
    cc_number_saved = cc_number_saved.replace(/[^\d]/g, ''); 
    if(!checkLuhn(cc_number_saved)) 
    { 
      alert('Sorry, that is not a valid number - please try again');
      //$('#cardnumberp').val('');
    }  
});


function checkLuhn(input) { 
 var sum = 0;
 var numdigits = input.length;
 var parity = numdigits % 2; 
 for(var i=0; i < numdigits; i++) { 
   var digit = parseInt(input.charAt(i));
   if(i % 2 == parity) digit *= 2; 
   if(digit > 9) digit -= 9; 
   sum += digit; 
  } 
  return (sum % 10) == 0; 
}

$(".close-reveal-modal").click(function(){
	clearFields();
});

function closeupdatemodal()
{
	$('#fadeandscale').trigger('reveal:close');
	clearFields();
}

function clearFields()
{
	$('#cardnumber').val('');
	$('#nameoncard').val('');
	$('#EWAY_CARDEXPIRYMONTH').val('');
	$('#EWAY_CARDEXPIRYYEAR').val('');
}

function setMeg(noofdevices, count)
{
	//alert(count);
	var devices = ' Devices';
	if(noofdevices == 1)
	{
		devices = ' Device';
	}
	
	//$('#deviceinfo').html(noofdevices +' (Max '+count+') Allowed '+devices);
	$('#deviceinfo').html(noofdevices + devices + ' (' +count+ 'Max)');
	
	//$('#deviceinfo').html(noofdevices+' Allowed '+devices);
}

</script>
<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#accountli").attr('class','treeview active');
$("#accountprofileicon").attr('class','fa fa-angle-down pull-right');
$("#packageli").attr('class','active');
$("#accountul").attr('style','display:block');
</script>

<?php	/** custom code **/ ?>
<script type="text/javascript">
	function formValidate()	{
	
	//console.log('here');
		
	var	txtErrFirstnameRequired = "Please enter first name.";
	var txtErrLastnameRequired = "Please enter last name.";
	var txtErrPhone = "Please enter phone number.";
	var txtErrCompanyName = "Please enter company name.";
	var txtErrAddress = "Please enter company address.";
	var txtErrCity = "Please enter city.";
	var txtErrZip = "Please enter zip code.";
	
	var zipcode = $('#zipcode').val();
	var firstname = $('#firstname').val(); 
	var lastname = $('#lastname').val();
	var company_name = $('#company_name').val();
	var address = $('#address').val(); 
	var city = $('#city').val();
	var phone = $('#phone').val();
	var country = $('#country').val();
	var isMobUser = $('#isMobUser').val();
		
		if(firstname == '')	{
			$('#firstnamemsg').text(txtErrFirstnameRequired);
			$('#firstname').focus();
			return false;
		}
		
		if(lastname == '')	{
			$('#lastnamemsg').text(txtErrLastnameRequired);
			$('#lastname').focus();
			return false;
		}
		
		if(phone == '')	{
			$('#phonemsg').text(txtErrPhone);
			$('#phone').focus();
			return false;
		}
		
		if(company_name == '')	{
			$('#company_namemsg').text(txtErrCompanyName);
			$('#company_name').focus();
			return false;
		}
		
		if(address == '')	{
			$('#addressmsg').text(txtErrAddress);
			$('#address').focus();
			return false;
		}
		
		if(city == '')	{
			$('#citymsg').text(txtErrCity);
			$('#city').focus();
			return false;
		}		
		
		if(zipcode == '')	{
			$('#zipcodemsg').text(txtErrZip);
			$('#zipcode').focus();
			return false;
		}		
		
		var id = $('#companyuserid').val();
		$.post("<?php echo base_url()?>index.php/companyadmin/updateCompanyMbDetails",{companyid : id, firstname : firstname, lastname : lastname, phone : phone, company_name : company_name, address : address, city : city, zipcode : zipcode, country: country },function(data)
		 {
				if(data == 'success')
				{
					$('#paybody1').hide();
					showInvoice(noofdevices, 2, isMobUser);
				}
				else
				{
						$('#company_namemsg').html('Company name exists.');
				}				
		 });
		 return false;
	}
	
	function removeErrorMessage(obj)	{
	
		var attributname = obj.id + 'msg';
		$('#'+attributname).text('');
	}
	
	<?php /*	if($this->session->userdata('companyid') == 224 || $this->session->userdata('companyid') == 250) {	*/ ?>
	
	$(function() {
		
		
		$('.amend_account').magnificPopup({		

			type: 'iframe',			
			mainClass: 'mfp-fade',
			removalDelay: 320,
			preloader: true,
			iframe: {
				markup: '<div style="width:500px; height:150px;">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
			}
			
		});
		
		$('.editcreditcard').magnificPopup({		

			type: 'iframe',			
			mainClass: 'mfp-fade',
			removalDelay: 320,
			preloader: true,
			iframe: {
				markup: '<div style="width:500px; height:500px;" id="editcardDetails">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
			}			
		});
/*
		
		$('#submitpass').click(function()	{
			
			if($('#acctpass').val() == '')	{
				$('#errorpass').html('<span class="error">Please enter password.<span>');
				return false;
			}
			
			$.post("<?php echo base_url('index.php/companyadmin/amendAccount'); ?>",{companyid : id,  password: '123456' },function(data)
		 {
				if(data == 'success')
				{
					
					alert('yes 1')
				}
				else
				{
						alert('yes 2')
				}				
		 });
		 return false;*/
			
			//element = '<?php echo base_url("index.php/companyadmin/amendAccount/"); ?>';
			
			/*
			$.ajax({
				url: '<?php echo base_url("index.php/companyadmin/amendAccount"); ?>',
				dataType: 'json',
				type: 'post',
				contentType: 'application/json',
				data: JSON.stringify( { "companyid": '224', "password": '123456' } ),
				processData: false,
				success: function( data, textStatus, jQxhr )	{
					//alert('if'+ JSON.stringify( data ) );
					$('#errorpass pre').html( JSON.stringify( data ) );
				},
				error: function( jqXhr, textStatus, errorThrown )	{
					//alert('else'+ JSON.stringify( data ) );
					console.log( errorThrown );
				}
			});*/
		/*	
		});*/
		
	});
	<?php	/* } */	?>
	
</script>

<?php	/** custom code **/ ?>