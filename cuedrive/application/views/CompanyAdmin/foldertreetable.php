<table class="table table-responsive" id="foldertree">
							<thead>
								<tr>
									<th>Name</th>
									<th>Permission</th>
									<th>Last Modified</th>
								</tr>
							</thead>
							<tbody>
									<?php foreach($foldertreearr as $folderarr){
										$type=$folderarr['type'];
										$levelarr=explode("-", $folderarr['htmlid']);
										$level=count($levelarr);
									if($folderarr['htmlparentid']==0)
									{
										
										?>
									<tr onclick="setFolderValue(<?php echo $folderarr['id'];?>)" data-tt-id='<?php echo $folderarr['htmlid'];?>' type='<?php echo $folderarr['accesstype'];?>' dbid='<?php echo $folderarr['id'];?>' level="<?php echo $level;?>"><td><span class='<?php echo $folderarr['type'];?>'><?php echo stripslashes($folderarr['label']);?></span></td>
									
									<?php if($type=="folder" && $folderarr['accesstype']!="public"){?>
									<td><a href="<?php echo base_url()?>index.php/companyadmin/permissions/<?php echo ($folderarr['id']);?>" style="color:black;"
									rel='popover' 
										        data-container='body' data-toggle='popover' data-placement='right' 
									                data-content='Set security permissions'><img src="<?php echo base_url()?>/padlock.png" width="20px" height="20px"></a></td>
									
									<?php }	else {	?>
									
									<td>&nbsp;</td>
									<?php }?>
									<td><?php echo $folderarr['updatedon'];?></td>	
									
									<td>
									
									<table width="40" border="0" cellspacing="0" cellpadding="0">
									<tr>
									<!-- 25 May 2015 -->
									<!--<td style="padding:5px!important;">-->
									
								<?php /*?>	<?php	if($folderarr['active_mode'])	{	?>	
										<input type="checkbox" checked="checked" id="activateFolder_<?php echo $folderarr['id'];	?>" onclick="getFolderValue(this)" name="activateFolder"	value=<?php echo $folderarr['id'];	?>	/>
									<?php	}	else	{	?>
										<input type="checkbox" id="activateFolder_<?php echo $folderarr['id'];	?>" onclick="getFolderValue(this)" name="activateFolder"	value=<?php echo $folderarr['id'];	?>	/>
									<?php	}	?><?php */?>
									<!--</td>-->
									<!-- 25 May 2015 -->									
									<td style="padding:5px!important;"><a href="javascript:editFolderclick()" title="Edit Folder" id="editfolderlink"><i class="fa fa-edit" style="font-size: 1.5em;"></i></a></td>
									<td style="padding:5px!important;"><a href="javascript:deleteitem()" title="Delete" id="deletelink" style="color:black;"><i class="fa fa-trash-o" style="font-size: 1.5em;"></i></a></td>
									<?php /* if($this->session->userdata("companyid") == '27' || $this->session->userdata("companyid") == '281' || $this->session->userdata("companyid") == '224' )	{ */	?>
							<td style="padding:5px!important;"><a href="javascript:copyfolders()" title="Copy" id="copylink"><img src="<?php echo base_url('theme/icon1/application_double.png'); ?>" /></a></td>
						<?php	/* } */	?>
									</tr>
									</table>

									
									</td>
									
									</tr>
									<?php }else{?>
									
									 <tr onclick="setFolderValue(<?php echo $folderarr['id'];?>)" data-tt-id='<?php echo $folderarr['htmlid'];?>' data-tt-parent-id='<?php echo $folderarr['htmlparentid'];?>' type='<?php echo $folderarr['accesstype'];?>' dbid='<?php echo $folderarr['id'];?>' level="<?php echo $level;?>"><td><span class='<?php echo $folderarr['type'];?>'><?php echo stripslashes($folderarr['label']);?></span></td>
									<?php if($type=="folder" && $folderarr['accesstype']!="public"){?>
									<td><a href="<?php echo base_url()?>index.php/companyadmin/permissions/<?php echo ($folderarr['id']);?>" style="color:black;"
									rel='popover' 
								        data-container='body' data-toggle='popover' data-placement='right' 
									data-content='Set security parameters'
									><img src="<?php echo base_url()?>/padlock.png" width="20px" height="20px"></a></td>
									<?php }
									else {
										?>
										<td>&nbsp;</td>
										<?php }?>
									<td><?php echo $folderarr['updatedon'];?></td>
									
									
									<td>
										
										<table width="40" border="0" cellspacing="0" cellpadding="0">
									  <tr>
									<?php if($type=="file") { ?>
									
									<!--<a href="#" title="Edit File" id="editlink"><i class="fa fa-edit" style="font-size: 1.5em; display:none;"></i></a>
									&nbsp;-->
									<td style="padding:5px!important;"><a href="javascript:editFileclick()" title="Edit File" id="editfilelink"><i class="fa fa-edit" style="font-size: 1.5em;"></i></a></td>
									<td style="padding:5px!important;"><a href="javascript:downloadfile()" title="Download File" id="downloadlink" style="color:black;"><i class="fa fa-download" style="font-size: 1.5em;"></i></a></td>
									
									<?php } else { ?>
									
									<!--<span style="margin-right: 3px;display:none;">&nbsp;</span>
									<span style="margin-right: 3px;display:none;">&nbsp;</span>-->
									<td style="padding:5px!important;"><a href="javascript:editFolderclick()" title="Edit Folder" id="editfolderlink"><i class="fa fa-edit" style="font-size: 1.5em;"></i></a></td>
									
									<?php }?>
						<td style="padding:5px!important;"><a href="javascript:deleteitem()" title="Delete" id="deletelink" style="color:black;"><i class="fa fa-trash-o" style="font-size: 1.5em;"></i></a></td>
						<?php /* if($this->session->userdata("companyid") == '27' || $this->session->userdata("companyid") == '281' || $this->session->userdata("companyid") == '224')	{ */	?>
							<?php if($type!="file") { ?>
							<td style="padding:5px!important;"><a href="javascript:copyfolders()" title="Copy" id="copylink"><img src="<?php echo base_url('theme/icon1/application_double.png'); ?>" /></a></td>
							<?php  } ?>
						<?php	/* } */	?>
						</tr>
									</table>
									
                                </td>
									</tr>
									
									<?php }}?>											
							</tbody>
						</table>
<script>
var expandedenodes="<?php echo $expandednodes;?>";
var baseUrl = "<?php echo base_url();?>";
//var companyid = <?php echo $this->session->userdata("companyid");?>;

$("#foldertree").treetable({ 
	expandable: true,
	onNodeExpand: function() 
	{
		/*if(companyid == 27)
		{*/
			//console.log(expandedenodes);
			var node = this;
			if(expandedenodes=="") 
			{
				expandedenodes=node.id;
			}
			else{
				if(String(expandedenodes).search(node.id) == -1){
					expandedenodes=expandedenodes+","+node.id;
				}
			}
			$("#expandednodes").val(expandedenodes);
		/*}
		else{
			var node = this;
			if(expandedenodes=="") 
			{
				expandedenodes=node.id;
			}
			else
				expandedenodes=expandedenodes+","+node.id;

			$("#expandednodes").val(expandedenodes);
		}*/
	},
	onNodeCollapse: function() {
		
		var node = this;
		var expandedenodesTemp = '';
		if(String(expandedenodes).search(",") > -1){
			var expnodeArray = expandedenodes.split(',');
			for (var i = 0; i < expnodeArray.length; i++) 
			{		
				if(expnodeArray[i] != node.id)	{
					if(expandedenodesTemp=="")
						expandedenodesTemp=expnodeArray[i];
					else
						expandedenodesTemp=expandedenodesTemp+","+expnodeArray[i];
				}
			}
			expandedenodes = expandedenodesTemp;
		}
		else{
			expandedenodes = "";
		}
		$("#expandednodes").val(expandedenodes);
	}
 });

	if(expandedenodes!="")
	{
		var expnodeArray = expandedenodes.split(',');

		for (var i = 0; i < expnodeArray.length; i++) 
		{		
			if(expnodeArray[i] != '')	{

				$("#foldertree").treetable("expandNode", expnodeArray[i]); 
			}
		}
	}
	  $("#selectedfolderid").val(0);
	  $("#foldertree tbody").on("mousedown", "tr", function() {
	  $(".selected").not(this).removeClass("selected");
	  $(this).toggleClass("selected");
	  $("#selectedfolderid").val($(this).attr("dbid"));
	  $("#selectedfoldertype").val($(this).attr("type"));
	  $("#selectedfoldername").val($(this).find('td:first').text());
	  $("#selectedtype").val($(this).find('td:first span').next().attr("class"));
	  $("#level").val($(this).attr("level"));
	  $("#data_tt_id").val($(this).data("ttId"));
	 //alert("filename"+$("#selectedfoldername").val().trim());
	  if($("#selectedfoldername").val().trim()=="user" || $("#selectedfoldername").val().trim()=="public")
	  {
		 // alert('in true');
		 // $("#deletelink").attr('disabled','disabled');
	  }
	  else
	  {
		 // $("#deletelink").removeAttr('disabled');
	  }

	  if($("#selectedtype").val()=="folder ui-draggable")
	  {
		 // $("#downloadlink").attr('disabled','disabled');
		  $("#addfolder").removeAttr('disabled');
		  $("#uploadfile").removeAttr('disabled');
		  $("#foldererrmsg").hide();
	  }
	  else
	  {
		//  $("#downloadlink").removeAttr('disabled');
		  //$("#addfolder").attr('disabled','disabled');
		  $("#uploadfile").attr('disabled','disabled');
	  }
	 });
	 
	
	$("#foldertree .file,#foldertree .folder").draggable({
	  helper: "clone",
	  opacity: .75,
	  refreshPositions: true,
	  revert: "invalid",
	  revertDuration: 300,
	  scroll: true
	});
	
	
	
	$("#foldertree .folder").each(function() {
	  $(this).parents("#foldertree tr").droppable({
	    accept: ".file, .folder",
	    drop: function(e, ui) {
	    
	      var droppedEl = ui.draggable.parents("tr");
	      var source = droppedEl.data("ttId").toString();
	      var destination = $(this).data("ttId").toString();
	     
	      if(source.indexOf('-') > 0) {
	      
	      $("#percentage").html('Please wait...');
	      $("#progressbar").css("width","100%");
	    
	      $("#loaderModal").reveal();
	      
	      
	       var companyid = <?php echo $this->session->userdata("companyid");?>;
	      
	       if(source.indexOf('-') > 0) {
	       var sourceArray = source.split('-');
	       var sourcelength = sourceArray.length;
	       var sourceID = sourceArray[sourcelength-1]
	       }
	       else
	       {
	       var sourceID = source;
	       }
	       
	       
	      
	      
	       if(destination.indexOf('-') > 0)
		    {
			   var destinationArray = destination.split('-');
			   var destinationlength = destinationArray.length;
			   var destinationID = destinationArray[destinationlength-1];
			   var destinationParent= destinationArray[0];
	        }else
	       		{
				   var destinationID = destination;
				   destinationParent=destination;
	  	        }
	     
	     if(droppedEl.find('td:first span').next().attr("class") == 'file ui-draggable')
	     {
	        $.post(baseUrl+'index.php/companyadmin/moveFile',{movefileid:sourceID,destinationfolderid:destinationID,companyid:companyid},function(data){
			if(data == '1')
			{
			  var expandednodesstr=destinationParent;
			  $.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
							$("#tablewidget").html('');
							$("#tablewidget").html(data);
							$("#loaderModal").trigger('reveal:close');
						});
			}
			else if(data == '0')
			{
			  $("#loaderModal").trigger('reveal:close');
			  alert('Some unexpected error occured!');
			}
			else if(data == 'fileExists')
			{
			  $("#loaderModal").trigger('reveal:close');
			  alert('File already exits in folder!');
			}
			else
			{
			  $("#loaderModal").trigger('reveal:close');
			  alert('Some unexpected error occured!');
			}
		});
	     
	     }
	     else
	     {
	       if(sourceID != destinationID) 
		   {
	    	 $.post(baseUrl+'index.php/companyadmin/moveFolder',{movefolderid:sourceID,destinationfolderid:destinationID,companyid:companyid},function(data){
			
			if(data == '1')
			{
			  var expandednodesstr=destinationParent;
			  $.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data)
			  {
					$("#tablewidget").html('');
					$("#tablewidget").html(data);
					$("#loaderModal").trigger('reveal:close');
			  });
						
			}
			else if(data == '0')
			{
			  $("#loaderModal").trigger('reveal:close');
			  alert('File already exits in folder!');
			}
			else
			{
			   $("#loaderModal").trigger('reveal:close');
			   alert('Some unexpected error occured!');
			}
		  });
		}
		else
		{
		  $("#loaderModal").trigger('reveal:close');
		  alert('Can not move!');
		  }
	     }
	     }
	     // $("#foldertree").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
	    },
	    hoverClass: "accept",
	    over: function(e, ui) {
	    
	      var droppedEl = ui.draggable.parents("tr");
	      
	      if(this != droppedEl[0] && !$(this).is(".expanded")) {
	        $("#foldertree").treetable("expandNode", $(this).data("ttId"));
	      }
	    }
	  });
	});
</script>

<script type="text/javascript">
$(function () 
      { $("[data-toggle='popover']").popover({ trigger: "hover" });;
      });
   
</script>