<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
		{
			header ("Location:".base_url()."index.php/login");
			exit;
		}
	$this->load->helper('form');
	$att = array('name' => 'userupdate','id' => 'userupdate');
	echo form_open('companyadmin/updateUser', $att);
	echo form_hidden('action',$action);
	echo form_hidden('userid',$companyuserid);
	$username = array ("name" => "username","id" => "username","class" => "form-control","autocomplete"=>"off","value" => set_value('username',$username), "onkeyup" => "nospaces(this)");
	//$userpass = array ("name" => "userpass","id" => "userpass","class" => "form-control","autocomplete"=>"off","value" => set_value('userpass',$password));
	$email = array ("name" => "email","id" => "email","class" => "form-control","autocomplete"=>"off","value" => set_value('email',$email));
	$firstname = array ("name" => "firstname","id" => "firstname","class" => "form-control","autocomplete"=>"off","value" => set_value('firstname',$firstname));
	$lastname = array ("name" => "lastname","id" => "lastname","class" => "form-control","autocomplete"=>"off","value" => set_value('lastname',$lastname));
	$user_type_v = 1;	
	if(isset($user_type) && $user_type != '') { $user_type_v = $user_type; }
	//$user_type_v = (isset($user_type))?$user_type:1;
	//$user_type = form_dropdown('user_type', array('1'  => 'Standard User', '2'  => 'Admin User'),$user_type_v);
	
	
	$formsubmit=array("name" => "submituser","id" => "submituser","class" => "btn btn-primary ");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<style>
.glyphicon-ok::before
{
 color:#01AB01;
}
.glyphicon-remove::before {
    color:#DF0027;
}

</style>
<aside class="right-side"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <?php echo $action;?> User</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url()?>index.php/companyadmin/manageUsers"><i class="fa fa-user"></i> User</a></li>
      <li class="active">Manage User</li>
    </ol>
  </section>
  <section class="content invoice">
    <div class="row"> </div>
    <div id="tablewidget" class="block-body collapse in">
      <div class="error_box error-txt"> </div>
      <label>First Name <span class="text-danger">*</span></label>
      <?php echo form_input($firstname);?> <br>
      <label>Last Name <span class="text-danger">*</span></label>
      <?php echo form_input($lastname);?> <br>
      <label>Username <span class="text-danger" id="userspace">*</span></label>
      <?php echo form_input($username);?> <br>
      <label>Email <span class="text-danger">*</span></label>
      <?php echo form_input($email);?> <br>
      <label>User Type <span class="text-danger">*</span></label>
      <?php //echo form_dropdown('user_type', array('1'  => 'Standard User', '2'  => 'Admin User'),$user_type_v, 'class="form-control" id="user_type"');?>
      <br />
      <table  class="table table-hover">
        <tr>
          <td class="text-center">&nbsp;</td>
          <td class="text-center">Log into website</td>
          <td class="text-center">Log into ipad</td>
          <td class="text-center">Restricted uploading</td>
          <td class="text-center">Upload all files</td>
          <td class="text-center">Add new users</td>
          <td class="text-center">Ammend permissions</td>
        </tr>
        <tr>
          <td><input type="radio" name="user_type" value="1"  <?=($user_type_v==1)?"checked='checked'":'';?>>&nbsp;&nbsp;User</td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-remove"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-remove"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-remove"></span></td>
        </tr>
        <tr>
          <td><input type="radio" name="user_type" value="2" <?= ($user_type_v==2)?"checked='checked'":'';?>>&nbsp;&nbsp;Administrator</td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
          <td class="text-center"><span class="glyphicon glyphicon-ok"></span></td>
        </tr>
      </table>
     
      <?php if($action=="Add"){?>
      <div id="redio_perm_me">
      <input type="radio" name="radioperm" value="full" checked>
      &nbsp;&nbsp;Give access to all files/folders on cuedrive
      &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="radio" name="radioperm" value="later">
      &nbsp;&nbsp;Set permissions manually later
      </div>
      <?php }?>
      <br/>
     
      <?php 
		$js = 'onClick="adduserclick()"';
		if($action == 'Edit') $action='OK';
		echo form_button($formsubmit, $action,$js); 
	?>
      <a href="<?php echo base_url()?>index.php/companyadmin/manageUsers"><b class="btn btn-primary pull-right">Cancel</b> </a>
      <div class="clearfix"></div>
      <?php echo form_close();?> <br/>
      <br/>
    </div>
  </section>
</aside>
<?php include_once("footer.php"); ?>
<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script>
$(document).ready(function(e) {
      $("input[name$='user_type']").click(function() {

        if($(this).attr("value")=="2"){

            $("#redio_perm_me").hide();           

        }else{
			$("#redio_perm_me").show();
			
		}
		
	});
});
</script>
<script type="text/javascript"> 


function nospaces(t)
{

   if(t.value.match(/\s/g))
   {
     $("#userspace").html('You are not allowed to enter any spaces');
     t.value=t.value.replace(/\s/g,'');
   }

}


$("#userli").attr('class','treeview active');
$("#usericon").attr('class','fa fa-angle-down pull-right');
$("#usersubli").attr('class','active');
$("#userul").attr('style','display:block');

var Validator =	new FormValidator('userupdate', [
	{
	    name: 'firstname',
	    display: 'First Name',    
	    rules: 'required'
	}, 
	{
	    name: 'lastname',
	    display: 'Last Name',    
	    rules: 'required'
	},
	{
	    name: 'username',
	    display: 'Username',    
	    rules: 'required|alpha_numeric'
	}, 
	{
	    name: 'email',
	    display: 'Email address', 
	    rules: 'required|valid_email'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }		       
	    } 
	   
	});
	
	Validator.setMessage('required', 'Please enter %s');	

	function adduserclick()
	{
		var baseUrl 	= "<?php echo base_url();?>";
		var username	= $("#username").val();
		var email		= $("#email").val();
		var userid		= "<?php echo $companyuserid;?>";
		$.post(baseUrl+'index.php/companyadmin/checkUserExist',{username:username,userid:userid,email:email},function(data)
		{
		if(data=="1") //not exist
		{	
			 $("#userupdate").submit();
		}
		else if(data == "2")
		{
		  //$('.error_box').append("<?php //echo 'Email already exists!';?>" + '<br />');
		  $('.error_box').html("<?php echo 'This email already exists, please use another email address';?>" + '<br />');		
		}
		else //exist
		{	
			//$('.error_box').append("<?php //echo USEREXIST;?>" + '<br />');	
			$('.error_box').html("<?php echo USEREXIST;?>" + '<br />');	

		}
		});
	}
</script>