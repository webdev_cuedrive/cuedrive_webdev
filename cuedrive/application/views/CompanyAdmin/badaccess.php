<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/billinghistory";
	
	//echo json_encode($results);
?>

<aside class="right-side">  


 <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        500 Error Page
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                 
                    <div class="error-page">
                        <h2 class="headline">500</h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Something went wrong.</h3>
                            <p>
                                You do not have the permission to access this page. <br/>
                                Meanwhile, you may <a href='#'>return to dashboard</a>.
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section>

</aside>
<?php
	include_once("footer.php"); 
?>