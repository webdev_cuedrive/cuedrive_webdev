<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/feedbacks";
?>

<?php


?>


<aside class="right-side">  


<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Feedback
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/feedbacks"><i class="fa fa-mobile"></i> Feedback</a></li>
                        <li class="active">Manage Feedback</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');
	if(true) //add device
	{
		$att = array('name' => 'addfeedback','id' => 'addfeedback');
		echo form_open('companyadmin/createFeedback', $att);		
		$addfeedbackbutton=array("name" => "addfeedback","id" => "addfeedback","class" => "btn btn-primary");
		echo form_submit($addfeedbackbutton, 'Make a Suggestion');
		echo form_close();
	}
	
	?>
	 </div>
	<div class="col-xs-6">
		
              
              <div class="input-group">
                           
              </div>
            
	         </div>
	         </div><br>
	
			 <div class="row">
                       
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Created By</th>	
						
									
									<th>Feedback</th>
									
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							$DeleteLink = '<a href="javascript:void(0)" onclick="deletefeedback('.$t_data->id.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							?>
							<tr id='RecRow<?php echo $t_data->id;?>'>
							<td><?php echo $t_data->username;?></td>
						
							
							<td><?php echo $t_data->comments;?></td>
							
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Feedback to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>

<script>
$(function() {

        
	   $("#supportli").attr('class','treeview active');
	   $("#supporticon").attr('class','fa fa-angle-down pull-right');
	   $("#feedbackli").attr('class','active');
	   $("#supportul").attr('style','display:block');
	   });
</script>