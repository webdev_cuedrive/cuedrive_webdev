<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/tickets";
	
	//echo json_encode($results);
?>

<?php


?>


<aside class="right-side">  


<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Tickets
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/tickets"><i class="fa fa-mobile"></i> Tickets</a></li>
                        <li class="active">Manage Tickets</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');
	if(true) //add device
	{
		$att = array('name' => 'addticket','id' => 'addticket');
		echo form_open('companyadmin/createTicket', $att);		
		$addfeedbackbutton=array("name" => "createticket","id" => "createticket","class" => "btn btn-primary");
		echo form_submit($addfeedbackbutton, 'Create Ticket to get Help');
		echo form_close();
	}
	
	?>
	 </div>
	<div class="col-xs-6">
		
              
              <div class="input-group">
                           
              </div>
            
	         </div>
	         </div><br>
	
			 <div class="row">
                        
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Created By</th>	
									<th>Ticket ID</th>
									
									<th>Priority</th>
									
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							if($t_data['ticketid'] != 0) {
							
							$DeleteLink = '<a href="javascript:void(0)" onclick="deleteticket('.$t_data['ticketid'].')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							
							$EditLink = '<a href="'.base_url().'/index.php/companyadmin/ticketconversation?id='.$t_data['ticketid'].'" ><i class="fa fa-search"></i></a>&nbsp;&nbsp;&nbsp;';
							
							
							 $username = $t_data['username'];
			          
						          if($t_data['companyuserid'] != NULL)
						          {
						             $companyuserdetails = Companyusermodel::getUserDetails($t_data['companyuserid']);
						             $companyusername = $companyuserdetails['firstname'].' '.$companyuserdetails['lastname'];
						             $username.= ' '.'('.$companyusername.')';
						          }
							
							
							
							
							
							?>
							<tr id='RecRow<?php echo $t_data['ticketid'];?>'>
							<td><?php echo $username;?></td>
							<td><?php echo $t_data['ticketid'];?></td>
							
							<td><?php echo $t_data['priority'];?></td>
							
							<td class='span1'><?php echo $EditLink?></td>
							</tr>
							<?php 
							
							}
							endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No tickets to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {
	$("#supportli").attr('class','treeview active');
	$("#supporticon").attr('class','fa fa-angle-down pull-right');
	$("#ticketli").attr('class','active');
	$("#supportul").attr('style','display:block');
	   });
</script>