<?php
	if(is_array($tokenCustomerDetails)) {
		
		$ccname = $tokenCustomerDetails['ccname'];
		$ccnumber = $tokenCustomerDetails['ccnumber'];
		$ccexpirymonth = $tokenCustomerDetails['ccexpirymonth'];
		$ccexpiryyear = $tokenCustomerDetails['ccexpiryyear'];
		
	} else {
		
		$ccname = '';
		$ccnumber = '';
		$ccexpirymonth = '';
		$ccexpiryyear = '';
	}
?>


<link rel="stylesheet" href="<?php echo base_url(); ?>theme/stylesheets/bootstrap.css">
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>theme/js/payment.js"></script>

<div id="paybutton" class="row" style="margin: 0 auto;">
                   
	<div class="modal-header" style="padding-left:0px; text-align: center;">    
		<h4 class="modal-title">Credit Card Information</h4>
	</div>
	
<div id="tablewidget" class="block-body collapse in" style="margin: 0px auto; width: 80%; ">

	
	<div class="error_box error-txt"> </div>

	<label>Card Number <span class="text-danger">*</span> </label>
	<input type="text" placeholder="Credit Card number" class="form-control" name="cardnumber" id="cardnumber" required value="<?php echo $ccnumber; ?>">
	<input type="hidden" name="nodevice" id="nodevice">  
	<input type="hidden" name="packageid" id="packageid">   
	<input type="hidden" name="cardtype" id="cardtype">   
		<br/>
	<label>Name On Card <span class="text-danger">*</span> </label>

	<input type="text" placeholder="Name on card" class="form-control" name="nameoncard" id="nameoncard" required value="<?php echo $ccname; ?>">  
			 
	<br/>

					<div id="creditfloatleftdata1" style="float:left; width:50%;	">
	<label>Expiry Month</label>


	 <select id="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH" class="form-control">
		  <?php
		   $expiry_month = date('m');
			for($i = 1; $i <= 12; $i++) {
			$s = sprintf('%02d', $i);
			echo "<option value='$s'";
			if($ccexpirymonth == '') {
				if ( $expiry_month == $i ) {
					echo " selected='selected'";
				}
			} else {
				if($ccexpirymonth == $i) {
					echo " selected='selected'";
				}
			}	
			 echo ">$s</option>\n";
			}
		   ?>
		 </select> <br/>
		 <label>Expiry Year</label>
		 <select id="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR" class="form-control">
		  <?php
			$i = date("y");
			$j = $i+11;
			for ($i; $i <= $j; $i++) {
				if($ccexpiryyear == $i)	{
					echo "<option selected='selected' value='$i'>$i</option>\n";
				} else {
					echo "<option value='$i'>$i</option>\n";
				}
		   }
		   ?>
		 </select>


	</div>
	<div id="creditfloatrightdata" style="float:left; width:50%;	">
						<?php	/*  Begin eWAY Linking Code */	?>
						<div id="eWAYBlock">
								<div style="text-align:center; padding-top: 10px;">
										<a href="https://www.eway.com.au/secure-site-seal?i=11&s=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
												<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" />
										</a>
								</div>
						</div>
					<?php	/* End eWAY Linking Code */	?>
					</div> 
					<div style="clear:both"></div>                
	<br/>

	<label>CVN <span class="text-danger">*</span> </label>

	<input type="text" class="form-control" name="cvnp" id="cvnp" data-inputmask='"mask": "999"' data-mask required>  
			 
	<br/>

	   <!-- <a class="pull-right" href="javascript:paypalSubmit()"><img src="<?php echo base_url();?>/paypal.png" alt="paypal" /></a> -->

	   <!-- <a class="pull-left" href="javascript:ewaySubmit()"><img src="<?php echo base_url();?>/eway.gif" alt="eWAY" height="32px;" width="145px;"/></a> -->
<!--		<input class="btn btn-primary pull-right" type="submit" id="paypalbtn" value = "Pay with Paypal" name = "button" onclick="return checkcard()"/>
		<input class="btn btn-primary pull-left" type="submit" id="ewaybtn" value="Pay with eWAY"  name = "button" onclick="return checkcard()"/>-->
		
		<div class="row">
			
			<div class="col-xs-4" style="text-align:center;">
			<button class="btn btn-primary" type="button" onclick="closeupdatemodal()" id="paycancel"> Cancel</button>
			</div>
			<div class="col-xs-4">
				 <button class="btn btn-primary pull-right" id="buttonpay" type="submit" onclick="return updateCardInfo()">Update</button>	
			</div>
			<center><div id="loadingdivtokenexist" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
		</div>

</div>  
</div>  

<style>
	iframe { height: 100%!important; }
</style>
