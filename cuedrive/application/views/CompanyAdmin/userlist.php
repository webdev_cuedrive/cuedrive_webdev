<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/manageUsers";
?>




	<aside class="right-side">  
	
	<?php
	if($this->session->flashdata('successmsg')!=null){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $this->session->flashdata('successmsg');?>
    </div>
	
<?php
	}
?>
	              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage User
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/manageUsers"><i class="fa fa-user"></i> User</a></li>
                        <li class="active">Manage User</li>
                    </ol>
                </section>


	<section class="content invoice">   

	
	<div  class="row">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');	
		$att = array('name' => 'addnewuser','id' => 'addnewuser');
		echo form_open('companyadmin/loadNewUserForm', $att);		
		$adduserbutton=array("name" => "adduser","id" => "adduser","class" => "btn btn-primary");
		echo form_submit($adduserbutton, 'Add');
		echo form_close();

	?>
	
	 </div>
	<div class="col-xs-6">
	<?php			
			$att = array('name' => 'userform','id' => 'userform', 'class'=>'form-inline','method'=> 'post');
			print form_open_multipart('companyadmin/manageUsers', $att);
?>			
                 
                 
                 	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#searchtext").keyup(function(){
             if ($("#searchtext").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/companyadmin/manageUsers") ;
             }
             }); 
	     });
	     
          </script>
                 
                 
                 
                 
                         
              <div class="input-group">
                            <input type="text" name="searchtext" id="searchtext" class="form-control" placeholder="Search By Username" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div>
	         
		<br><br>
			 <div class="row">
                        
           </div>
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($companyusers)>0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									
									<th>Username</th>
									<th>Email</th>
									<th>Status</th>
                                    <th>User Type</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach($companyusers as $companyuser):
							$EditLink='<a href="'.base_url().'/index.php/companyadmin/loadEditUserForm/'.$companyuser['companyuserid'].'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Edit"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							$DeleteLink = '<a href="javascript:void(0)" onclick="deleteUser('.$companyuser['companyuserid'].')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>';
							if($companyuser['isactive']=="yes")
								$statuslinktext="Deactivate";
							else 
								$statuslinktext="Activate";
							?>
							<tr id='RecRow<?php echo $companyuser['companyuserid'];?>'>
							
							<td><?php echo $companyuser['username'];?></td>
							
						
							<td><?php echo $companyuser['email'];?></td>
						
							<td><a class="statuslink" id="link<?php echo $companyuser['companyuserid'];?>" usr_id="<?php echo $companyuser['companyuserid'];?>" href="javascript:void(0)" activeval="<?php echo $companyuser['isactive'];?>" ><?php echo $statuslinktext;?></a></td>
                            
                            <td><?php if($companyuser['user_type'] == 1){echo 'User';}else{echo 'Administrator';}?></td>
							<td class='span1'><?php echo $EditLink?><?php echo $DeleteLink?></td>
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $pagination; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No User to display</h3></center>
							<?php }?>
					</div>
					
				</div>
		
</section>
</aside>


<?php
	include_once("footer.php"); 
?>

<script>
$(function() {

	 $("#userli").attr('class','treeview active');
	 $("#usericon").attr('class','fa fa-angle-down pull-right');
	 $("#usersubli").attr('class','active');
	 $("#userul").attr('style','display:block');
	
	 $(".statuslink").click( function( e ) {
		 var baseUrl = "<?php echo base_url();?>";	
			var userid=$(this).attr('usr_id');
			var actval=$(this).attr("activeval");
			var linkname="link"+userid;
			$.post(baseUrl+'index.php/companyadmin/changeUserStatus',{userid:userid},function(data){
				
				if(data==1)
				{
					if(actval=="yes") //now deactivated
					{

						$("#"+linkname).attr("activeval","no");
						$("#"+linkname).text('Activate');				
					}
					else //now activated
					{
						$("#"+linkname).attr("activeval","yes");
						$("#"+linkname).text('Deactivate');
					}
				}
			});
		});
});


function deleteUser(eID)
{
	var confirmDel = confirm('Are you sure you want to delete it?');	
	 var baseUrl = "<?php echo base_url();?>";
	if(confirmDel)
	{		
		var targetDiv ="#RecRow"+eID;
		$(targetDiv).html('<img src="'+baseURL+'theme/images/loading.gif" alt="loading...."/>');	
		$.post(baseURL+"index.php/companyadmin/deleteUser",{ 'userid': eID}, function(data)
		{
			window.location="<?php echo $baseredirecturl;?>";
		});
	}	
}
</script>