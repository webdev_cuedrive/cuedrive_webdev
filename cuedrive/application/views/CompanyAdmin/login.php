<?php
      
	include("header.php");		
		$userCredentials = get_cookie('UserCredentials');
		if(isset($userCredentials)) {
			$userPass = explode(":", $userCredentials);				
		}	
		$rememberedUsername=isset($userPass[0])? $userPass[0]: '';
		$rememberedPassword=isset($userPass[1])? $userPass[1]: '';
		if($rememberedUsername!="")
			$checked='checked';
		else 
			$checked='notchecked';
	
	?>
	<?php 
	$this->load->helper('form');
	$att = array('name' => 'login','id' => 'login');
	echo form_open('login/authenticateUser', $att);
	$username = array ("name" => "username","id" => "username","class" => "form-control","value" => $rememberedUsername,"placeholder" => "Username");
	$userpass = array ("type"=>"password","name" => "userpass","id" => "userpass","class" => "form-control","value" => $rememberedPassword,"placeholder" => "Password");
	if($checked=="checked")
	{
		$rememberme = array ("name" => "rememberme","id" => "rememberme", "value" => "1","checked"=>"checked" );
	}
	else {
		$rememberme = array ("name" => "rememberme","id" => "rememberme", "value" => "1" );
	}
	$formsubmit=array("name" => "submitlogin","id" => "submitlogin","class" => "btn bg-maroon btn-block");
//$loginOptions= array('admin'  => 'CompanyAdmin','user' => 'CompanyUser');
?>
 


  <div class="form-box" id="login-box">
            <div class="header bg-maroon">Sign In</div>
            <form>
                <div class="error-txt"><?php echo $this->session->flashdata('loginmsg');?></div>
                 <div class="error_box error-txt"></div>
                <div class="body bg-gray">
                    <div class="form-group">
                          <?php echo form_input($username);?>
                    </div>
                    <div class="form-group">
                       <?php echo form_input($userpass);?> 
                    </div>     
                    <!--
                    <div class="form-group"> 
                    <?php echo form_dropdown("loginas",$loginOptions,'', 'class="form-control"');?>	
                    </div>  
                    -->    
                    <div class="form-group">
                        <?php echo form_checkbox($rememberme) ?> Remember me
                    </div>
                    
                </div>
                <div class="footer"> 
                	<?php echo form_submit($formsubmit, 'Sign In');?>                                                               
                    <a href="javascript:forgotpassword()" class="text-center">Forgot your password ?</a> <br>           
                  <!--  <a href="<?php echo base_url()?>index.php/companyadmin/selectPacakge" class="text-center">Register a new membership</a> -->
                    
                </div>
            </form>
            </div>
   </div>        
      <?php $this->session->sess_destroy();?>      
        <div id="forgotModal" class="reveal-modal">
 		
		<h3 style="margin-top:0px; margin-left:0px">Forgot Password</h3>
		<div class="input-group input-group-sm">
           
		  <input class="form-control" id="email" name="email" placeholder="Enter Email Address" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
          <span class="input-group-btn bg-maroon">
             <button type="button" class="btn btn-primary btn-flat" onclick="sendPassword()">Send</button>
          </span>
		  
        </div>
			


 <div style="margin-top: 10px">
   
   <a class="close-reveal-modal">&#215;</a>
 </div>	
 </div>		

<?php

	form_close();
	include_once("footer.php"); 
?>
<script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>
<script>

function forgotpassword()
{
	$("#forgotModal").reveal();
	$("#email").val('');
}


function sendPassword()
{
	var email=$.trim($("#email").val());
	var baseUrl = '<?php echo base_url()?>';
	$.post(baseUrl+'index.php/companyadmin/resetPassword',{email:email},function(data){
	
	if(data=="1")
	{
		alert('Password has been sent to your email successfully');
		
	$('#forgotModal').trigger('reveal:close');
	}
	else
	{
		alert(data);
	}
	});
}
</script>

<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
	new FormValidator('login', [{
	    name: 'username',
	    display: 'Username',    
	    rules: 'required'
	},  {
	    name: 'userpass',
	    display: 'Password', 
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
	    
	    } 
	   
	});

	$('select').on('change', function() {
		  //alert( this.value ); // or $(this).val()
		  if($(this).val()=="user")
			  $('#login').attr('action', 'login/authenticateCompanyUser');
		  else
			  $('#login').attr('action', 'login/authenticateCompanyAdmin');
		});
			
	</script>