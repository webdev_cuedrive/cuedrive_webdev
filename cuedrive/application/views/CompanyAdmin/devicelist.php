<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/manageDevices";
	
		$companyid = $this->session->userdata("companyid");
        $sql = "select companyadmin.noofdevicesallowed,companyadmin.expirydate,package.noofdevices,package.additionaldeviceprice from companyadmin join package on package.packageid = companyadmin.packageid where companyid = {$companyid}";
        $query = $this->db->query($sql);
        $data = $query->row();
        
        $noofdevicecanadd = $data->noofdevices - $data->noofdevicesallowed;
	
	
	
	
	
?>




<aside class="right-side">  

<?php
	if($this->session->flashdata('successmsg')!=null){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $this->session->flashdata('successmsg');?>
    </div>
	
<?php
	}
?>
              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Device
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/manageDevices"><i class="fa fa-mobile"></i> Device</a></li>
                        <li class="active">Manage Device</li>
                    </ol>
                </section>


	<section class="content invoice">   
    
	<h3 class="alert alert-warning alert-dismissable"><?php echo $devicesleft;?>  more devices can be added</h3>
	
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');
	if($devicesleft>0) //add device
	{
		$att = array('name' => 'addnewdevice','id' => 'addnewdevice');
		echo form_open('companyadmin/loadNewDeviceForm', $att);		
		$adddevicebutton=array("name" => "adddevice","id" => "adddevice","class" => "btn btn-primary");
		echo form_submit($adddevicebutton, 'Add');
		
		echo form_close();
	}
	elseif($noofdevicecanadd > 0 && $devicesleft ==0) { //upgrade package
	        /*
		$att = array('name' => 'upgradePackage','id' => 'upgradePackage');
		echo form_open('companyadmin/upgradePacakge', $att);		
		$upgradebutton=array("name" => "adddevice","id" => "adddevice","class" => "btn btn-primary");
		echo form_submit($upgradebutton, 'Upgrade Package');
		echo form_close();
		*/
		
		$att = array('name' => 'addExtraDev','id' => 'addExtraDev');
		echo form_open('companyadmin/upgradeDevice', $att);		
		$extradevbutton=array("name" => "adddevice","id" => "adddevice","class" => "btn btn-primary");
		echo form_submit($extradevbutton, 'Add Extra Devices');
		echo form_close();
		
	}else{
		
		$att = array('name' => 'UpgradePlan','id' => 'UpgradePlan');
		echo form_open('companyadmin/upgradePacakge', $att);		
		$extradevbutton=array("name" => "upgradePlan","id" => "upgradePlan","class" => "btn btn-primary");
		echo form_submit($extradevbutton, 'Upgrade Plan');
		echo form_close();
		
	}
	?>
	
	 </div>
	<div class="col-xs-6">
	<?php			
			$att = array('name' => 'deviceform','id' => 'deviceform', 'class'=>'form-inline','method'=> 'post');
			print form_open_multipart('companyadmin/manageDevices', $att);
?>			
              
              
                      	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#searchtext").keyup(function(){
             if ($("#searchtext").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/companyadmin/manageDevices") ;
             }
             }); 
	     });
	     
          </script>
              
              
              
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="searchtext" class="form-control" placeholder="Search by Device ID" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div><br><br>
	
			 <div class="row">
                        
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($companydevices)>0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Device ID</th>
									<th>Name</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach($companydevices as $companydevice):
							$EditLink='<a href="'.base_url().'/index.php/companyadmin/loadEditDeviceForm/'.$companydevice['deviceid'].'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Edit"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							$DeleteLink = '<a href="javascript:void(0)" onclick="deleteDevice('.$companydevice['deviceid'].')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>';
							if($companydevice['isactive']=="yes")
								$statuslinktext="Deactivate";
							else 
								$statuslinktext="Activate";
							?>
							<tr id='RecRow<?php echo $companydevice['deviceid'];?>'>
							<td><?php echo $companydevice['deviceid'];?></td>
							<td><?php echo $companydevice['deviceuuid'];?></td>
							<td><?php echo $companydevice['name'];?></td>
							<td><a class="statuslink" id="link<?php echo $companydevice['deviceid'];?>" dev_id="<?php echo $companydevice['deviceid'];?>" href="javascript:void(0)" activeval="<?php echo $companydevice['isactive'];?>" ><?php echo $statuslinktext;?></a></td>
							<td class='span1'><?php echo $EditLink?>&nbsp;<?php echo $DeleteLink?></td>
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $pagination; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Device to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>

<div id="dialog-form" title="What do you want to do?">
<input type="hidden" name="seldeviceid" id="seldeviceid" value=""/>
<input type="radio" name="radiodeldev" value="deletedevice" checked>&nbsp;Delete device only.Leave the files on cuedrive
<br/><br/>
<input type="radio" name="radiodeldev" value="deletecontent">&nbsp;Delete device and also remove its home folder contents
</div>

<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>

<script>
$(function() {

	 $("#deviceli").attr('class','treeview active');
	 $("#deviceicon").attr('class','fa fa-angle-down pull-right');
	 $("#devicesubli").attr('class','active');
	 $("#deviceul").attr('style','display:block');
	   
	 $( "#dialog-form" ).dialog({
		 autoOpen: false,
		 height: 300,
		 width: 400,
		 modal: true,
		 buttons: {
		 "Delete": function() {			
		 $( this ).dialog( "close" );
			 var confirmDel = confirm('Are you sure you want to delete the device? This operation cannot be undone');	
			
			if(confirmDel)
			{	
			    var baseUrl = "<?php echo base_url();?>";	
				var eID = $("#seldeviceid").val();
				var selradio=$("input:radio[name=radiodeldev]:checked").val();
				var targetDiv ="#RecRow"+eID;
				//alert(eID+":"+selradio);
				$(targetDiv).html('<img src="'+baseURL+'theme/images/loading.gif" alt="loading...."/>');	
				$.post(baseURL+"index.php/companyadmin/deleteDevice",{'deviceid': eID,'selradio':selradio}, function(data)
				{
					window.location="<?php echo $baseredirecturl;?>";
				});
			}	
		
		 },
		 Cancel: function() {
		 $( this ).dialog( "close" );
		 }
		 }
		 });

	      $(".statuslink").click( function( e ) {
		 var baseUrl = "<?php echo base_url();?>";	
			var deviceid=$(this).attr('dev_id');
			var actval=$(this).attr("activeval");
			var linkname="link"+deviceid;
			$.post(baseUrl+'index.php/companyadmin/changeDeviceStatus',{deviceid:deviceid},function(data){
				
				if(data==1)
				{
					if(actval=="yes") //now deactivated
					{

						$("#"+linkname).attr("activeval","no");
						$("#"+linkname).text('Activate');				
					}
					else //now activated
					{
						$("#"+linkname).attr("activeval","yes");
						$("#"+linkname).text('Deactivate');
					}
				}
			});
		});
});


function deleteDevice(eID, deviceName)
{
	$("#seldeviceid").val(eID);
	$( "#dialog-form" ).dialog( "open" );
}
</script>