<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
$this->load->helper('form');
	$att = array('name' => 'sendticket','id' => 'sendticket');
	echo form_open('companyadmin/addticket', $att);

	//$type = array ("name" => "type","id" => "type","class" => "form-control","readonly"=>"true","value" => "support");
	//$email = array ("name" => "email","id" => "email","class" => "form-control","value"=>"-");
	//$phone = array ("name" => "phone","id" => "phone","class" => "form-control","value"=>"-");
	$comments = array ("name" => "comments","id" => "comments","class" => "form-control");	
	
	
	
		
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-left ");
	
	
	
	
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                         Create New Ticket
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/tickets"><i class="fa fa-mobile"></i> Ticket</a></li>
                        <li class="active"> Manage Ticket</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                       
           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

         	       			
			<?php echo form_hidden('type','support');?>	
			<label>Priority</label>
			<select name = "priority" class="form-control">
			<option value="low">Low</option>
			<option value="normal">Normal</option>
			<option value="high">High</option>
			</select>
			
			<?php echo form_hidden('email','-');?>
			
			<?php echo form_hidden('phone','-');?>
			<label>Issue Description <span class="text-danger">*</span></label>
			<?php echo form_textarea($comments);?><br>
			<input type="hidden" name="companyid" value="<?php echo $this->session->userdata('companyid')?>">
			<input type="submit" class="btn btn-primary pull-right" value="Send">
        
        	       <a href="<?php echo base_url()?>index.php/companyadmin/tickets"><p class="btn btn-primary pull-left">Cancel</p></a>
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#supportli").attr('class','treeview active');
$("#foldericon").attr('class','fa fa-angle-down pull-right');
$("#ticketli").attr('class','active');
$("#supportul").attr('style','display:block');

var Validator =	new FormValidator('sendticket', [
	{
	    name: 'comments',
	    display: 'Description',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0)
		 {
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		       
	    } 
	   
	});
Validator.setMessage('required', 'Please enter %s');	
</script>
		 