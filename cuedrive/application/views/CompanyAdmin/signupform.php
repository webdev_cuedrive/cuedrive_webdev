<?php
include_once("header.php"); 
?>
<?php 
	$this->load->helper('form');
	$att = array('name' => 'signup','id' => 'signup');
	echo form_open('companyadmin/register', $att);
	echo form_hidden('selectedpkgid',$selectedpkgid);
	$companyname = array ("name" => "company_name","id" => "company_name","class" => "form-control","autocomplete"=>"off'");	
	$email = array ("name" => "email","id" => "email","class" => "form-control","autocomplete"=>"off'");
	$verifyemail = array ("name" => "verifyemail","id" => "verifyemail","class" => "form-control","autocomplete"=>"off'");
	$address = array ("name" => "address","id" => "address","class" => "form-control","autocomplete"=>"off'");
	$phone = array ("name" => "phone","id" => "phone","class" => "form-control","autocomplete"=>"off'");
	$username = array ("name" => "username","id" => "username","class" => "form-control");
	$password = array ("type"=>"password","name" => "password","id" => "password","class" => "form-control");
	$verifypassword = array ("type"=>"password","name" => "verifypassword","id" => "verifypassword","class" => "form-control");
	$passwordhint = array ("name" => "passwordhint","id" => "passwordhint","class" => "form-control");	
	$extradevices = array ("name" => "extradevices","id" => "extradevices","class" => "form-control");
	$formsubmit=array("name" => "submitregister","id" => "submitregister","class" => "btn btn-primary ");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");	
?>

		<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Sign Up
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/login"> Sign Up</a></li>
                        <li class="active">Register</li>
                    </ol>
                </section>

	<section class="content invoice">   
	 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Register
                                  </h2>                            
                        </div><!-- /.col -->
           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"></div>
		 <label>Company name <span class="text-danger">*</span></label>
         <?php echo form_input($companyname);?>
         
          <label>Email<span class="text-danger">*</span></label>
         <?php echo form_input($email);?>
         
         <label>Verify Email<span class="text-danger">*</span></label>
         <?php echo form_input($verifyemail);?>
         
         <label>Address</label>
         <?php echo form_textarea($address);?>           
         
         <label>Phone</label>
         <?php echo form_input($phone);?>
         
         <label>Choose a Username<span class="text-danger">*</span></label>
         <?php echo form_input($username);?>
         
         <label>Choose a Password<span class="text-danger">*</span></label>
         <?php echo form_input($password);?>
         
          <label>Verify your Password<span class="text-danger">*</span></label>
         <?php echo form_input($verifypassword);?>
         
          <label>Password Hint<span class="text-danger">*</span></label>
         <?php echo form_input($passwordhint);?>
         
         <label>Extra Devices</label>
         <?php echo form_input($extradevices);?>
         <br/>
        <?php echo form_submit($formsubmit, 'Register');?>
         <?php echo form_reset($formreset, 'Reset');?> 
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         </div>
       </section>
</aside>
	<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
	new FormValidator('signup', [{
	    name: 'company_name',
	    display: 'Company Name',    
	    rules: 'required'
	},  {
	    name: 'email',
	    display: 'Email', 
	    rules: 'required|valid_email'
	},	{
	    name: 'verifyemail',
	    display: 'Verify Email', 
	    rules: 'required|matches[email]'
	},	{
	    name: 'address',
	    display: 'Address', 
	    rules: 'required'
	}, {
	    name: 'phone',
	    display: 'Phone', 
	    rules: 'numeric'
	},	{
	    name: 'username',
	    display: 'Username', 
	    rules: 'required'
	},	{
	    name: 'password',
	    display: 'Password', 
	    rules: 'required'
	},	{
		 name: 'verifypassword',
		 display: 'Verify Password',
		 rules: 'required|matches[password]'
	},	{
	    name: 'passwordhint',
	    display: 'Password Hint', 
	    rules: 'required'
	}, {
	    name: 'extradevices',
	    display: 'Extra Devices', 
	    rules: 'numeric'
	}

	], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
		        $("html, body").animate({ scrollTop: 0 }, "slow");
	    } 
	   
	});
			
	</script>