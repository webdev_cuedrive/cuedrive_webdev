
	
	<script type="text/javascript">
		function validatePassword()	{
			
			var accountPass = document.getElementById('acctpass').value;
			if(accountPass.length <= 0)	{
				document.getElementById('acctpassmsg').innerHTML = 'Please enter password.';	
				return false;
			}
		}
		
		function removeErrorMessage(obj)	{
	
		var attributname = obj.id + 'msg';
		//$('#'+attributname).text('');
		document.getElementById(attributname).innerHTML = '';
	}
		
		
	</script>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>theme/css/site.css">
	<form id="dialog-form" method="post" action="<?php echo  base_url('index.php/companyadmin/amendAccount'); ?>" onsubmit="return validatePassword();">
		<table width="90%" cellspacing="5" cellpadding="5" style="margin: 8px">			
			<tr>
				<td>
					<label for="name">Please enter your password:</label>
				</td>
				<td>
					<input type="password" name="acctpass" id="acctpass" class="text ui-widget-content ui-corner-all" onkeydown="removeErrorMessage(this)" onclick="removeErrorMessage(this)"/>	
					
				</td>
			</tr>	
			<tr>
				<td>&nbsp;</td>
				<td id = "acctpassmsg" style="font-size: 12px; color: red; "><?php if($message != '') { 			echo $message; 		} ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" id="submitpass" name="submitpass" value="Submit" class="btn btn-primary signup" />
				</td>
			</tr>	
		</table>
	</form>
