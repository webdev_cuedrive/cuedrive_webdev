<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$this->load->helper('form');
	$att = array('name' => 'createfeedback','id' => 'createfeedback');
	echo form_open('companyadmin/addfeedback', $att);

	//$type = array ("name" => "type","id" => "type","class" => "form-control","readonly"=>"true","value" => "feedback");
	//$email = array ("name" => "email","id" => "email","class" => "form-control");
	//$phone = array ("name" => "phone","id" => "phone","class" => "form-control");
	$comments = array ("name" => "comments","id" => "comments","class" => "form-control");	
	echo form_hidden('type','feedback');
	echo form_hidden('email','-');
	echo form_hidden('phone','-');
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-left ");
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                         Send Feedback
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/feedbacks"><i class="fa fa-mobile"></i> Feedback</a></li>
                        <li class="active">New Feedback</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">

           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

			
                        <input type = "hidden" value = "low" name= "priority">
			<label>Please send us your Feedback and Ideas <span class="text-danger">*</span></label>
			<?php echo form_textarea($comments);?><br>
			<input type="hidden" name="companyid" value="<?php echo $this->session->userdata('companyid')?>">
			<input type="submit" class="btn btn-primary pull-right" value="Send">
        
        	        <a href="<?php echo base_url()?>index.php/companyadmin/feedbacks"><b class="btn btn-primary pull-left">Cancel</b></a>
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#supportli").attr('class','treeview active');
$("#supporticon").attr('class','fa fa-angle-down pull-right');
$("#feedbackli").attr('class','active');
$("#supportul").attr('style','display:block');
var Validator =	new  FormValidator('createfeedback', [
	{
	    name: 'comments',
	    display: 'Feedback',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
	    } 
	});
Validator.setMessage('required', 'Please enter %s');	
</script>	 