<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
	{
		header ("Location:".base_url()."index.php/login");
		exit;
	}

$options['companyid'] = $id;
$raw_data =  Companyadminmodel::GetCompanyAdminDetails($options);
$raw_data = $raw_data [0];


$this->load->helper('form');	
$att = array('name' => 'editDetails','id' => 'editDetails', 'method'=> 'post');
print form_open_multipart('companyadmin/updateAccountDetails', $att);
	
$name = array ("name" => "name","id" => "name","class" => "form-control","value"=>$raw_data->name,"autocomplete"=>"off");
$address = array ("name" => "address","id" => "address","class" => "form-control","value"=>$raw_data->address,"autocomplete"=>"off");
$phone= array ("name" => "phone","id" => "phone","class" => "form-control","value"=>$raw_data->phone,"autocomplete"=>"off");
echo form_hidden('companyid',$raw_data->companyid);
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<aside class="right-side">    
  <?php if(isset($sucessMsg)){ ?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php echo $sucessMsg;?>
        </div>
    <?php }  ?>
	<?php if(isset($errorMsg)){?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
		   <?php echo $errorMsg;?>
		</div>
	<?php } ?>     
     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update Account Details
                     </h1>
                </section>


	<section class="content invoice">   
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

			<label>Business/Company Name <span class="text-danger">*</span></label>					
			<?php echo form_input($name);?>	
			
			<label>Street Address <span class="text-danger">*</span></label>
			<?php echo form_input($address);?>
			
			<label>City <span class="text-danger">*</span></label>
			<input type="text" name="city" placeholder="City" class="form-control" value="<?php echo $raw_data->city?>">
			
			<label>Zip Code <span class="text-danger">*</span></label>
			<input type="text" name="zipcode" placeholder="Zipcode" class="form-control" value="<?php echo $raw_data->zipcode?>">
			
			<label>Country <span class="text-danger">*</span></label>
			<input type="text" name="country" class="form-control" placeholder="Country" value="<?php echo $raw_data->country?>">		
			
			<label>Phone <span class="text-danger">*</span></label>
			<?php echo form_input($phone);?>
			
			<label>ABN </label>
			<input type="text" name="abn" class="form-control" value="<?php echo $raw_data->abn?>" >
			
			<label>ACN</label>
			<input type="text" name="acn" class="form-control" value="<?php echo $raw_data->acn?>" >
			
			<br>
			<input type="submit" class="btn btn-primary pull-left" value="Update" >
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
		 </div>
</section>
</aside>
<?php include_once("footer.php"); ?>
<script type="text/javascript"> 
$("#accountli").attr('class','treeview active');
$("#accountprofileicon").attr('class','fa fa-angle-down pull-right');
$("#accountdetailsli").attr('class','active');
$("#accountul").attr('style','display:block');

var Validator = new FormValidator('editDetails', [
	{
	    name: 'name',
	    display: 'Company Name',    
	    rules: 'required'
	},  
	{
	    name: 'address',
	    display: 'Address', 
	    rules: 'required'
	},
	{
	    name: 'city',
	    display: 'City', 
	    rules: 'required'
	},
	{
	    name: 'zipcode',
	    display: 'Zip Code',    
	    rules: 'required'
	},
	{
	    name: 'country',
	    display: 'Country', 
	    rules: 'required'
	},
	{
	    name: 'phone',
	    display: 'Phone Number',    
	    rules: 'required'
	},
	{
	    name: 'abn',
	    display: 'ABN',    
	    rules: 'callback_abn'
	},
	{
	    name: 'acn',
	    display: 'ACN',    
	    rules: 'callback_acn'
	}
	], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		 {
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
	    } 
	});
Validator.setMessage('required', 'Please enter %s');

Validator.registerCallback('abn', function(abn) {   
		var a, w, t;
		w = [0,0,3,5,7,9,11,13,15,17,19];
		a = abn.split('');
		t = ''+a[0]+a[1];
		w.forEach(function(v,i) {a[i] *= v;});
		a = a.reduce(function(x,y) {return x+y;});
		return (t == 99-(a%89));
	
	})
	.setMessage('abn', 'Please enter valid ABN');
	

Validator.registerCallback('acn', function(acn) {   
		 
		  var weights = [8, 7, 6, 5, 4, 3, 2, 1, 0];
	      var sum, x, remainder, complement;
	      // Strip non-numbers from the acn
	      acn = acn.replace(/[^\d]/g, '');
	      //if (!acn.length)  return true;  //--- nothing entered so just return
	      // Check acn is 9 chars long
	      if( acn.length != 9) {
	          return false;
	      }
	     // Sum the products
	      sum = 0;
	      for(x=0; x<acn.length; x++ ) {
	        sum += acn[x] * weights[x];
	      }
	      // Get the remainder
	      remainder = sum % 10;
	      // Get remainder compliment
	      complement = "" + (10 - remainder);
	      // If complement is 10, set to 0
	      if( complement === "10") {
	          complement = "0";
	      }
	      return ( acn[8] === complement );
	
	})
	.setMessage('acn', 'Please enter valid ACN');
</script>