<?php
	include("header.php");
	include("sidebar.php");
	if(!$this->session->userdata("userid")) 
	{
		header ("Location:".base_url()."index.php/login");
		exit;
	}
/*	echo '<pre>';
print_r($userdetails);
exit;*/
    $this->load->helper('form');
	$att = array('name' => 'passwordupdateuser','id' => 'passwordupdateuser');
	echo form_open('companyadmin/updateCompanyPasswordAu', $att);
	
	//$oldpass = array ("name" => "oldpass","id" => "oldpass","class" => "form-control","autocomplete"=>"off");
	$username = array ("name" => "username","id" => "username","class" => "form-control", "value" => $userdetails['username']);
	$newpass = array ("name" => "newpass","id" => "newpass","class" => "form-control","autocomplete"=>"off");
	$email = array ("name" => "email","id" => "email","class" => "form-control", "value" => $userdetails['email']);
	
	//$confirmnewpass = array ("name" => "confirmnewpass","id" => "confirmnewpass","class" => "form-control","autocomplete"=>"off");
	
	$formsubmit=array("name" => "submit","id" => "submit","class" => "btn btn-primary ");	
	
?>
<aside class="right-side">  

    <?php if(isset($errMsg)){ ?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php echo $errMsg;?>
        </div>
    <?php }  ?>
	<?php if(isset($errorMsg)){?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
		   <?php echo $errorMsg;?>
		</div>
	<?php } ?>     
        
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Update Profile
                     </h1>
                </section>
	<section class="content invoice">   
	 <div class="row">
        </div>
		
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
			<label>Username <span class="text-danger">*</span></label>
         	<?php echo form_input($username);?>
         	
         	<label>Password <span class="text-danger">*</span></label>
         	<?php echo form_password($newpass);?>
         	
         	<label>Email <span class="text-danger">*</span></label>
         	<?php echo form_input($email);?>
         	
         	<br/><br/>
         	<!--<input name="submit" type="submit" id="submit" class="btn btn-primary" value="Update" onclick = "return checkpassword()">-->
			<input name="submit" type="submit" id="submit" class="btn btn-primary" value="Update">
         	<div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
		 
		 </div>
		 
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 


$("#accountli").attr('class','treeview active');
$("#accountprofileicon").attr('class','fa fa-angle-down pull-right');
$("#passwordsubli").attr('class','active');
$("#accountul").attr('style','display:block');

	var Validator =	new FormValidator('passwordupdate', [{
	    name: 'username',
	    display: 'Username',    
	    rules: 'required'
	},  {
	    name: 'newpass',
	    display: 'Password', 
		rules: 'required|callback_newpass'
	    //rules: 'required|min_length[6]'
	},
	{
	    name: 'email',
	    display: 'Email address',    
	    rules: 'required|valid_email'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
	    } 
	   
	});
	
Validator.setMessage('required', 'Please enter %s');	

Validator.registerCallback('newpass', function(newpass) {   
		//var pattern = /^(?=.{6,})(?=.*[a-z])(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;	
		var pattern = /^(?=.*[a-z])(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;	
		return (pattern.test(newpass));
	})
.setMessage('newpass', '<?php echo PASSWORDWITHSPCCHAR; ?>');
</script>