<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}

	if(is_array($tokenCustomerDetails)) {
		
		$ccname = $tokenCustomerDetails['ccname'];
		$ccnumber = $tokenCustomerDetails['ccnumber'];
		$ccexpirymonth = $tokenCustomerDetails['ccexpirymonth'];
		$ccexpiryyear = $tokenCustomerDetails['ccexpiryyear'];
		
	} else {
		
		$ccname = '';
		$ccnumber = '';
		$ccexpirymonth = '';
		$ccexpiryyear = '';
	}

        $companyid = $this->session->userdata("companyid");
        $sql = "select companyadmin.noofdevicesallowed,companyadmin.expirydate,package.noofdevices,package.additionaldeviceprice from companyadmin join package on package.packageid = companyadmin.packageid where companyid = {$companyid}";
        $query = $this->db->query($sql);
        $data = $query->row();
        
        $noofdevicecanadd = $data->noofdevices - $data->noofdevicesallowed;
		
		$expiryDate = strtotime($data->expirydate);
		$currentDate = strtotime(date("Y-m-d"));
			
		/*$diff = abs($expiryDate - $currentDate);
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		*/
		$diff = $expiryDate - $currentDate;		
		$days = floor($diff/ (60*60*24));		
		
        $deviceprice = $data->additionaldeviceprice;

        $this->load->helper('form');
	$att = array('name' => 'deviceupgrade','id' => 'deviceupgrade');
	/*echo form_open('payment/deviceupgrade', $att);*/
	echo form_open('payment/deviceupgrade_quintity', $att);
	$formsubmit=array("name" => "submitdevice","id" => "submitdevice","class" => "btn btn-primary ");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>

<style>

.invoice1 {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #f4f4f4;
    margin: 10px auto;
    position: relative;
    width: 50%;
}

</style>

<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Upgrade Device
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/manageDevices"><i class="fa fa-mobile"></i> Device</a></li>
                        <li class="active">Manage Device</li>
                    </ol>
                </section>


	<section class="content invoice1">   
	 <div class="row">
                        
        </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
	
		<label>Device Quantity</label>
         	<select class="form-control" name="quantity" id="devicequantity">
         	<?php
         	for($i = 0; $i <= $noofdevicecanadd; $i++) {
                           
                             echo "<option value='".$i."'>".$i."</option>";
                            }
         	?>
         	</select>
         	<br>
            <input type="hidden" name="deviceprice" id="deviceprice" value="<?php echo $deviceprice;?>">
         	<?php /*?><label>Card Number <span class="text-danger">*</span> </label>
                <input type="text" value="<?php echo $ccnumber; ?>" placeholder="Creditcard number" class="form-control" name="cardnumber" id="cardnumber" required>
         	        
         	   
                <br/>
                <label>Name On Card <span class="text-danger">*</span> </label>
         	        
         	<input type="text" value="<?php echo $ccname; ?>" placeholder="Name on card" class="form-control" name="nameoncard" id="nameoncard" required>  
         	                 
                <br/>
         	
         	<label>Expiry Month</label>
         	        
         	        
         	<select id="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH" class="form-control">
                <?php
                           $expiry_month = date('m');
                            for($i = 1; $i <= 12; $i++) {
                            $s = sprintf('%02d', $i);
                            echo "<option value='$s'";
							if($ccexpirymonth == '') {
								if ( $expiry_month == $i ) {
									echo " selected='selected'";
								}
							} else {
								if($ccexpirymonth == $i) {
									echo " selected='selected'";
								}
							}	
                             echo ">$s</option>\n";
                            }
                 ?>
                 </select> <br/>
                 <label>Expiry Year</label>
                 <select id="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR" class="form-control">
                          <?php
                            $i = date("y");
                            $j = $i+11;
                            for ($i; $i <= $j; $i++) {
								if($ccexpiryyear == $i)	{
									echo "<option selected='selected' value='$i'>$i</option>\n";
								}	else {	
									echo "<option value='$i'>$i</option>\n";
								}
							}	
                           ?>
                 </select>
         	        
         	        
         	        
         	                  
         	 <br/>
         	        
         	 <label>CVN <span class="text-danger">*</span> </label>
         	        
         	 <input type="text" class="form-control" name="cvn" id="cvn" data-inputmask='"mask": "999"' data-mask required>  
         	 <br/>
         	 
         	 <label> <center id="inv" style="font-size:28px"> </center></label>
         	        
         
         	 <br/><?php */?>
             
           
             
             <br/>
         	<!-- <input type="submit" name="submit" value="Pay Now" class="btn btn-primary"/>-->
            <input type="submit" name="submit" value="Add Device" class="btn btn-primary"/>
         	
                 <a href="<?php echo base_url()?>index.php/companyadmin/manageDevices"><b class="btn btn-primary pull-right">Cancel</b></a> 	
                  <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#deviceli").attr('class','treeview active');
$("#deviceicon").attr('class','fa fa-angle-down pull-right');
$("#devicesubli").attr('class','active');
$("#deviceul").attr('style','display:block');
	new FormValidator('deviceupgrade', [{
	    name: 'cardnumber',
	    display: 'Card Number',    
	    rules: 'required'
	},  {
	    name: 'nameoncard',
	    display: 'Name On Card', 
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
		       
	    } 
	   
	});
	
	</script>
	
	<script src="<?php echo base_url()?>theme/js/jquery.inputmask.js" type="text/javascript"></script>

		
<script type="text/javascript"> 
/*
function signupclick(pkgid)
{
	document.getElementsByName("selectedpkgid")[0].value = pkgid;
	$("#signup").submit();
}
*/

$(document).ready(function() {

        $(".eway-button").attr("id","eway");
        $('.eway-button').removeClass('eway-button');
        $("[data-mask]").inputmask(); 
        
});
 $('#cardnumber').change(function(){
    cc_number_saved = $('#cardnumber').val();
    cc_number_saved = cc_number_saved.replace(/[^\d]/g, ''); 
    if(!checkLuhn(cc_number_saved)) 
    { 
      alert('Sorry, that is not a valid number - please try again!');
      //$('#cardnumber').val('');
    }        
});

$('#devicequantity').change(function(){
  
    perdeviceprice = <?php echo $deviceprice;?>;
	quantity = $('#devicequantity').val();
	
	<?php
	
		/*if($this->session->userdata("companyid") == 224)		{*/	
			if($days <= 15 && $days >= 0)	{	?>
				
				/*perdeviceprice = (1 * 15 / 30) * <?php echo $days ?>;*/
				perdeviceprice = 7.5;
				
	<?php			
			}	
		/*}*/	
	
	?>    
    
    
	<?php
	/*
	if($this->session->userdata("companyid") == 224)		{*/ ?>
	
		price = quantity * perdeviceprice;
		$('#inv').html('Total Amount : '+quantity+' '+ '* '+perdeviceprice+' =' +' '+'$'+price);
		$('#deviceprice').val(perdeviceprice);
	
	<?php	/*} else {	?>
		price = quantity * perdeviceprice;
		$('#inv').html('Total Amount : '+quantity+' '+ '* $15 =' +' '+'$'+price);
		$('#deviceprice').val(perdeviceprice);
	<?php	}	*/ ?>
  
});


function checkLuhn(input) { 
 var sum = 0;
 var numdigits = input.length;
 var parity = numdigits % 2; 
 for(var i=0; i < numdigits; i++) { 
   var digit = parseInt(input.charAt(i));
   if(i % 2 == parity) digit *= 2; 
   if(digit > 9) digit -= 9; 
   sum += digit; 
  } 
  return (sum % 10) == 0; 
}

</script>
		 