<style type="text/css">
.blockdiv
	{
		border:2px solid;
		box-shadow: 10px 10px 5px #888888;
		max-height: 600px;
		overflow-y: scroll;
	}
 
</style>

<?php

include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}

?>

<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Permissions 
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/folders"><i class="fa fa-folder"></i> Folder</a></li>
                        <li class="active">Manage Permissions</li>
                    </ol>
                </section>

                       <div >
                            <h2 class="page-header">
                            &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $foldername?>
                             
                            </h2>                            
                        </div>

	<section class="content invoice"> 
			<div class="">		
				
			
					<div class="box-body blockdiv">
					<?php 
					$this->load->helper('form');
				$att = array('name' => 'savedevice','id' => 'savedevice');
				echo form_open('companyadmin/saveFolderDevicePermissions', $att);
					?>
					<div>
				
					<table id="devices" class="table table-hover">							
							
							<thead>
								<tr>													
									<?php 
								
									
									$info = unserialize(PERMISSION_INFORMATION);
									$data = array ("name" => "CheckAll","id" => "selectall","title"=>"Check All");
									echo '<th width="15%">'.''.'&nbsp;<b>Device</b></th>' ;
									$width = '11';
									for($i=0;$i<count($allpermissions);$i++)
									{
										if($i==1)
										{
										 $width = '10';
										}elseif($i==2){
										 $width = '10';
										}elseif($i==3){
										 $width = '10';
										}elseif($i==4){
										 $width = '10';
										}elseif($i==5){
										 $width = '16';
										}
										
										
										if($i == 5)	{
											$i++;
											
										echo '<th width="'.$width.'%"><center>&nbsp;'.$allpermissions[$i]['name'].'&nbsp;<a style="color:black;"
										rel="popover" 
								                data-container="body" data-toggle="popover" data-placement="top"
									        data-content="'.$info[$i].'"
										><span class="fa fa-question-circle">
								                </span></a></center></th>';
											$i--;					
										} else	if($i == 6)			{
											$i--;
												echo '<th width="'.$width.'%"><center>&nbsp;'.$allpermissions[$i]['name'].'&nbsp;<a style="color:black;"
										rel="popover" 
								                data-container="body" data-toggle="popover" data-placement="top"
									        data-content="'.$info[$i].'"
										><span class="fa fa-question-circle">
								                </span></a></center></th>';
											$i++;
										} else	{
											echo '<th width="'.$width.'%"><center>&nbsp;'.$allpermissions[$i]['name'].'&nbsp;<a style="color:black;"
										rel="popover" 
								                data-container="body" data-toggle="popover" data-placement="top"
									        data-content="'.$info[$i].'"
										><span class="fa fa-question-circle">
								                </span></a></center></th>';
										}
										//echo '<br/> >-'.$i . '<br/><br/>';
									}
									?>
								</tr>
								
							</thead>
							<tbody>
							<?php
								
									/*$filefp = fopen($_SERVER['DOCUMENT_ROOT'] . '/cuedrive/log.txt', 'w');
									fwrite($filefp, print_r($devicepermissions));
									fclose($filefp);*/
									 
							for($i=0;$i<count($devicepermissions);$i++)
							{
								echo '<tr>';
								
								/*if($i == 5)	{
								$i++;
									$data = array ("name" => "devicechk[]","id" => $devicepermissions[$i]['deviceid'],"class" => "checkboxdev","onclick" => "selectDevRow(".$devicepermissions[$i]['deviceid'].")");							
									echo '<td width="15%"><i>'.form_checkbox($data).'</i>&nbsp;'.$devicepermissions[$i]['devicename'].'</td>' ;
								$i--;
								} else if($i == 6)	{
									$i--;
										$data = array ("name" => "devicechk[]","id" => $devicepermissions[$i]['deviceid'],"class" => "checkboxdev","onclick" => "selectDevRow(".$devicepermissions[$i]['deviceid'].")");							
										echo '<td width="15%"><i>'.form_checkbox($data).'</i>&nbsp;'.$devicepermissions[$i]['devicename'].'</td>' ;
									$i++;
								} else	{*/
									$data = array ("name" => "devicechk[]","id" => $devicepermissions[$i]['deviceid'],"class" => "checkboxdev","onclick" => "selectDevRow(".$devicepermissions[$i]['deviceid'].")");							
									echo '<td width="15%"><i>'.form_checkbox($data).'</i>&nbsp;'.$devicepermissions[$i]['devicename'].'</td>' ;
								/*}*/
								
								
								for($j=0;$j<count($allpermissions);$j++)
								{ 
									$found=false;
									for($k=0;$k<count($devicepermissions[$i]['permissions']);$k++)
									{
										if($j == 5)	{
											$j++;
											if($allpermissions[$j]['permissionid']==$devicepermissions[$i]['permissions'][$k]['permissionid'])
											$found=true;
											$j--;
										} elseif($j == 6)	{
											$j--;
											if($allpermissions[$j]['permissionid']==$devicepermissions[$i]['permissions'][$k]['permissionid'])
											$found=true;
											$j++;
										} else	{
											if($allpermissions[$j]['permissionid']==$devicepermissions[$i]['permissions'][$k]['permissionid'])
											$found=true;
										}
										
										
									}
									
									$datagroup=$devicepermissions[$i]['deviceid'];
									
									if($j == 5)	{
										$j++;
									if($found)	{
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'], "class" => "checkboxdevperm", "checked"=>"checked","data-group"=>$datagroup);	}
									else 	{
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'],"class" => "checkboxdevperm","data-group"=>$datagroup);
										}
										$j--;
										
									}	elseif($j == 6)	{

										$j--;
									if($found)	{	
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'], "class" => "checkboxdevperm", "checked"=>"checked","data-group"=>$datagroup);	}
									else {
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'],"class" => "checkboxdevperm","data-group"=>$datagroup);
									}	
										$j++;		
											
									}	else	{
									
									if($found)	
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'], "class" => "checkboxdevperm", "checked"=>"checked","data-group"=>$datagroup);
									else 
										$data1 = array ("name" => "permissionchk[]","id" => $devicepermissions[$i]['deviceid'].'-'.$allpermissions[$j]['permissionid'],"class" => "checkboxdevperm","data-group"=>$datagroup);

										
									}
										
									echo '<td><center>'.form_checkbox($data1).'</center></td>' ;
								}
								echo '</tr>';
					 		}?>
													
							<tr>
							<!--<td>With Selected</td>-->
							<?php 
							for($j=0;$j<count($allpermissions);$j++)
								{
									//$data1 = array ("name" => "multidevchk[]","id" => $allpermissions[$j]['permissionid'],"class"=>"chkmultidiv");
									//echo '<td><center>'.form_checkbox($data1).'</center></td>' ;
								}
								echo '</tr>';
							?>
							</tbody>
							</table>
							<br/>
						<?php 
						if(count($devicepermissions) > 0) {
						$savedevicebutton=array("name" => "savedevice","id" => "savedevice","class" => "btn btn-primary");
						
						}
						else
						{
						 $savedevicebutton=array("name" => "savedevice","id" => "savedevice","class" => "btn btn-primary","style" => "display:none;");
						 echo '<center> <h3>No Device to display</h3></center>'; 
						}
						
						$js = 'onClick="savedeviceclick()"';
						
							echo '<center> &nbsp; <input type="checkbox" name="repeatpermision" id="repeatpermision"/>&nbsp;Repeat Pemissions &nbsp;<span data-toggle="popover" data-placement="bottom" data-content="Repeat permissions continue your settings through the sub folders in this folder" class="fa fa-question-circle"></span> &nbsp;'.form_button($savedevicebutton, 'Save',$js).'</center>'; 
						
						?>	
						<br/>
					
					</div>
					<?php 
					
					echo form_close();
					?>
					
				</div>	
				
				
					<!--  User Permissions -->
				<div class="box-body blockdiv">	
					<?php 
					$this->load->helper('form');
				$att = array('name' => 'saveuser','id' => 'saveuser');
				echo form_open('companyadmin/saveFolderUserPermissions', $att);
					?>
					<br>
					<div class="form-group">
					<table  id="users" class="table table-hover">							
							
							<thead>
								<tr>													
									<?php 
									$info = unserialize(PERMISSION_INFORMATION);
									$data = array ("name" => "CheckAllUser","id" => "selectalluser","title"=>"Check All");
									echo '<th width="15%">'.''.'&nbsp;<b>User</b></th>' ;
									$width = '12';
									for($i=0;$i<count($allpermissions) - 1;$i++)
									{
										if(in_array($i,array(0,3)))
										{ //if($i == 3){ $width = '73'; };										
											
											echo '<th width="'.$width.'%"><center>&nbsp;'.$allpermissions[$i]['name'].'&nbsp;<a style="color:black;"
											rel="popover" 
													data-container="body" data-toggle="popover" data-placement="top"
												data-content="'.$info[$i].'"
											><span class="fa fa-question-circle">
													</span></a></center></th>';
													
										}
									}
									?>
                                    <th width="61%">&nbsp;</th>
								</tr>
								
							</thead>
							<tbody>
							<?php 
							for($i=0;$i<count($userpermissions);$i++)
							{
								if($userpermissions[$i]['user_type'] == 1 ){
								
								echo '<tr>';
								$data = array ("name" => "userchk[]","id" => $userpermissions[$i]['userid'],"class" => "checkboxuser","onclick" => "selectRow(".$userpermissions[$i]['userid'].")");
								echo '<td width="15%"><i>'.form_checkbox($data).'</i>&nbsp;'.$userpermissions[$i]['username'].'</td>' ;
								for($j=0;$j<count($allpermissions) - 1;$j++)
								{ 
								
								if(in_array($j,array(0,3))){
									$found=false;
									for($k=0;$k<count($userpermissions[$i]['permissions']);$k++)
									{
										if($allpermissions[$j]['permissionid']==$userpermissions[$i]['permissions'][$k]['permissionid'])
										$found=true;
									}
									$datagroup=$userpermissions[$i]['userid'];
									if($found)
										$data1 = array ("name" => "permissionchk[]","id" => $userpermissions[$i]['userid'].'-'.$allpermissions[$j]['permissionid'], "class" => "checkboxuserperm", "checked"=>"checked","data-group-usr"=>$datagroup);
									else 
										$data1 = array ("name" => "permissionchk[]","id" => $userpermissions[$i]['userid'].'-'.$allpermissions[$j]['permissionid'],"class" => "checkboxuserperm","data-group-usr"=>$datagroup);
									echo '<td><center>'.form_checkbox($data1).'</center></td>' ;
									
									
									}
									
								}
								echo '<td width="61%">&nbsp;</td>';
								echo '</tr>';
								
								
								
								}
								
								
					 		}?>
													
							<tr>
							<!--<td>With Selected</td>-->
							<?php 
							for($j=0;$j<count($allpermissions);$j++)
								{
									$data1 = array ("name" => "multiuserchk[]","id" => $allpermissions[$j]['permissionid'],"class"=>"chkmultiuser");
									//echo '<td><center>'.form_checkbox($data1).'</center></td>' ;
								}
								echo '</tr>';
							?>
							</tbody>
							</table>
							<br/>
						<?php 
						
						if(count($userpermissions) > 0){
						$saveuserbutton=array("name" => "saveuser","id" => "saveuser","class" => "btn btn-primary");
						}
						else
						{
						$saveuserbutton=array("name" => "saveuser","id" => "saveuser","class" => "btn btn-primary","style" => "display:none;");
						echo '<center><h3>No User to display</h3></center>'; 
						}
						$js = 'onClick="saveuserclick()"';
						
						echo '<center> &nbsp; <input type="checkbox" title="Check this to repeat the permissions for the rest of the folders in the set." name="repeatpermisionuser" id="repeatpermisionuser"/>&nbsp;Repeat Pemissions &nbsp;<span data-toggle="popover" data-placement="bottom" data-content="Repeat permissions continue your settings through the sub folders in this folder" class="fa fa-question-circle"></span> &nbsp;'.form_button($saveuserbutton, 'Save',$js).'</center>'; 
						
						
						?>	
						<br/>
					
				</div>
					<?php 
					
					echo form_close();
					?>
					</div>
	
	
	
	
	
	
	<div class="box-body blockdiv">
	<table>
	<br>
	<?php
	$email1 = "";
	$email2 = "";
	$email3 = "";
	$email4 = "";
	$email5 = "";
	if($emails != ""){
	
	$email_array = explode(',',$emails);
	$email1 = isset($email_array[0]) ? $email_array[0] : "";
	$email2 = isset($email_array[1]) ? $email_array[1] : "";
	$email3 = isset($email_array[2]) ? $email_array[2] : "";
	$email4 = isset($email_array[3]) ? $email_array[3] : "";
	$email5 = isset($email_array[4]) ? $email_array[4] : "";

	}
	
	
	?>
	
	<div class="form-group">
	<h3>&nbsp;&nbsp;Add approved emails for file sending</h3>
	<i>&nbsp;&nbsp;&nbsp;<b>Note:</b>If no emails are entered below,this allows devices to send files to any email. </i>
	</div>
	<div class="col-md-12 ">	
	<div class="form-group">
	<input type="email" class="form-control" placeholder="Email address" name="email1" value="<?php echo $email1?>" id="email1"> 
	</div>
	<div class="form-group">
	<input type="email" class="form-control" placeholder="Email address" name="email2" value="<?php echo $email2?>" id="email2"> 
	</div>
	<div class="form-group">
	<input type="email" class="form-control" placeholder="Email address" name="email3" value="<?php echo $email3?>" id="email3"> 
	</div>
	<div class="form-group">
	<input type="email" class="form-control" placeholder="Email address" name="email4" value="<?php echo $email4?>" id="email4"> 
	</div>
	<div class="form-group">
	<input type="email" class="form-control" placeholder="Email address" name="email5" value="<?php echo $email5?>" id="email5"> <br>
	</div>
	<div class="form-group">
	 NumberCue : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 <?php if($numbercue == 0) { ?>
	 <input type="checkbox" name="numbercue" id="numbercue"> &nbsp;&nbsp;&nbsp;&nbsp; With Number Cue active your files will be sent as device serial # +time+date<br>
	 <?php }  else { ?>
	 <input type="checkbox" name="numbercue" id="numbercue" checked> &nbsp;&nbsp;&nbsp;&nbsp; With Number Cue active your files will be sent as device serial # +time+date <br>
	 <?php } ?>
	 
	</div>
	
	</div>
	<?php 
			$saveuserbutton=array("name" => "savemail","id" => "savemail","class" => "btn btn-primary");
			$js = 'onClick="saveemailclick()"';
			echo '<center>'.form_button($saveuserbutton, 'Save',$js).'</center>'; 
	?>
	
	
	<br>
	
	</table>
	</div>
	
	
	
	
	
	
	
	
	
 </div>
<section>
</aside>
<?php
	include_once("footer.php"); 
?>	
<script>
var baseUrl = "<?php echo base_url();?>";
$(document).ready(function() {

	 $("#folderli").attr('class','treeview active');
	   $("#foldersubli").attr('class','active');
	   $("#folderul").attr('style','display:block');

	   
    $('#selectall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkboxdev').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
            $('.checkboxdevperm').each(function() { 
	  		 	$(this).attr("disabled", true);
		 });
        }else{
            $('.checkboxdev').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });      
            $('.checkboxdevperm').each(function() { 
				 $(this).removeAttr("disabled");
			 });
        }
    });

  /*  $('.checkboxdev').click(function(event) {  //on click    	       	 
    		 if(this.checked) {
    			 $('.checkboxdevperm').each(function() { 
        	  		 	$(this).attr("disabled", true);
    			 });
    		 }
    		 else
    		 {
        		 if($(".checkboxdev:checked").length==0)
    			 {
    				 $('.checkboxdevperm').each(function() { 
    					 $(this).removeAttr("disabled");
    				 });
    			 }
    		 } 
    });*/


    //user
    $('#selectalluser').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkboxuser').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
            $('.checkboxuserperm').each(function() { 
	  		 	$(this).attr("disabled", true);
		 });
        }else{
            $('.checkboxuser').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });      
            $('.checkboxuserperm').each(function() { 
				 $(this).removeAttr("disabled");
			 });
        }
    });
});
    function selectRow(ID) {  //on click   
    
    // alert(ID);
	       	 
		 if($("#"+ID+":checked").length==1) {
			 for(i = 1 ; i <= 5 ; i++) { 
   	  		 	$('#'+ID+'-'+i).prop('checked', true);
			 }
		 }
		 else
		 {
   		 if($("#"+ID+":checked").length==0)
			 {
				for(i = 1 ; i <= 5 ; i++) { 
	   	  		 	$('#'+ID+'-'+i).prop('checked', false);
				 }
			 }
		 } 
		 
   }
   
   
   
   
   
   function selectDevRow(ID)
   {
   
		 if($("#"+ID+":checked").length==1) {
			 for(i = 1 ; i <= 5 ; i++) { 
   	  		 	$('#'+ID+'-'+i).prop('checked', true);
			 }
		 }
		 else
		 {
   		 if($("#"+ID+":checked").length==0)
			 {
				for(i = 1 ; i <= 5 ; i++) { 
	   	  		 	$('#'+ID+'-'+i).prop('checked', false);
				 }
			 }
		 } 
   
   
   }


function savedeviceclick(){
	var folderid = "<?php echo $folderid?>";
	var repeatpermision = $('#repeatpermision:checked').length;
	
	var retdevstr="";
	/* if($(".checkboxdev:checked").length>0) //with selected
	 {
		 $('.checkboxdev').each(function() { 
			 if(this.checked) {
				 var devid=$(this).attr('id');					 
				 $('.chkmultidiv').each(function() {
					  if(this.checked) {					  
						  var permid=$(this).attr('id');
						  	retdevstr=retdevstr+devid+":"+permid+"@";
					  }
				 });
			 }
		 });
	 }*/
	// else //individual
	 {		
		 $('.checkboxdev').each(function() { 
			 //var parent = $(this);
			 var devid=$(this).attr('id');
			// alert( 'data-group="' + devid + '"]');	
			 $('[data-group="'+devid+'"]').each(function() {
				  if(this.checked) {					  
					  var permid=$(this).attr('id');
					          permidarr = permid.split('-');
						  permid = permidarr[1];
					  	retdevstr=retdevstr+devid+":"+permid+"@";
				  }
			 });
		 });
	 }
	
		
	 //alert(retdevstr);
		$.post(baseUrl+'index.php/companyadmin/saveFolderDevicePermissions',{folderid:folderid,retdevstr:retdevstr,repeatpermision:repeatpermision},function(data){
		
		if(data=="1")
			{
			 if ($('#selectall').is(':checked')) 
				{		
					$('#selectall').prop('checked', false);            
		  		}
			 $('.checkboxdev').each(function() { //loop through each checkbox
	               if(this.checked )
	            	   this.checked = false;              
	            });

			 $('.chkmultidiv').each(function() {
				  if(this.checked) {
					  this.checked = false;  
				  }            
		         });
	         
			 $('.checkboxdevperm').each(function() { 
				 if ($(this).attr("disabled"))
				 	$(this).removeAttr("disabled");

				 if(this.checked) {
					  this.checked = false;  
				  }   
			 });
			 var devpermarr = retdevstr.split('@');
	         for(i=0;i<	devpermarr.length;i++)
	         { 
		         if(devpermarr[i]!="")
		         {
			         var temp=devpermarr[i].split(':');
			         var devid=temp[0];
			         var permid=temp[1];
			         $('[data-group="'+devid+'"]').each(function() {
			        	 if($(this).attr('id')==devid+'-'+permid)
			        	 {
			        		 this.checked = true; 
			        	 }
			         });
		         }
	         }		
				alert('Saved successfully');
				
			}
		else
		{
			alert('Some problem occured while saving');
			
		}
		});
}

function saveuserclick()
{
	var folderid = "<?php echo $folderid?>";
	var repeatpermisionuser = $('#repeatpermisionuser:checked').length;
	var retuserstr="";
	 /*if($(".checkboxuser:checked").length>0) //with selected
	 {
		 $('.checkboxuser').each(function() { 
			 if(this.checked) {
				 var userid=$(this).attr('id');					 
				 $('.chkmultiuser').each(function() {
					  if(this.checked) {					  
						  var permid=$(this).attr('id');
						  permidarr = permid.split('-');
						  permid = permidarr[1];
						  
						  retuserstr=retuserstr+userid+":"+permid+"@";
					  }
				 });
			 }
		 });
	 }*/
	// else //individual
	 {		
		 $('.checkboxuser').each(function() { 
			 //var parent = $(this);
			 var userid=$(this).attr('id');
			// alert( 'data-group="' + devid + '"]');	
			 $('[data-group-usr="'+userid+'"]').each(function() {
				  if(this.checked) {					  
					  var permid=$(this).attr('id');
					  
						  permidarr = permid.split('-');
						  permid = permidarr[1];
					  retuserstr=retuserstr+userid+":"+permid+"@";
				  }
			 });
		 });
	 }
	
		
	 //alert(retdevstr);
		$.post(baseUrl+'index.php/companyadmin/saveFolderUserPermissions',{folderid:folderid,retuserstr:retuserstr,repeatpermisionuser:repeatpermisionuser},function(data){
		
		if(data=="1")
			{
			 if ($('#selectalluser').is(':checked')) 
				{		
					$('#selectalluser').prop('checked', false);            
		  		}
			 $('.checkboxuser').each(function() { //loop through each checkbox
		               if(this.checked )
		            	   this.checked = false;              
		            });

			 $('.chkmultiuser').each(function() {
				  if(this.checked) {
					  this.checked = false;  
				  }            
		         });
	         
			 $('.checkboxuserperm').each(function() { 
				 if ($(this).attr("disabled"))
				 	$(this).removeAttr("disabled");

				 if(this.checked) {
					  this.checked = false;  
				  }   
			 });
			 var userpermarr = retuserstr.split('@');
	         for(i=0;i<	userpermarr.length;i++)
	         { 
		         if(userpermarr[i]!="")
		         {
			         var temp=userpermarr[i].split(':');
			         var userid=temp[0];
			         var permid=temp[1];
			         $('[data-group-usr="'+userid+'"]').each(function() {
			        	 if($(this).attr('id')==userid+'-'+permid)
			        	 {
			        		 this.checked = true; 
			        	 }
			         });
		         }
	         }		
				alert('Saved successfully');
				
			}
		else
		{
			alert('Some problem occured while saving');
			
		}
		});
	
}

function saveemailclick()
{
var folderid = "<?php echo $folderid?>";
var email1 = $("#email1").val();
var email2 = $("#email2").val();
var email3 = $("#email3").val();
var email4 = $("#email4").val();
var email5 = $("#email5").val();
var req_email = email1+','+email2+','+email3+','+email4+','+email5;
var numbercue = 0;
var numcue = $('#numbercue');
if(numcue.is(':checked'))
{
 numbercue = 1;
}

/*if(email1 == "" && email2 == "" && email3 == "" && email4 =="" && email5 == "") {
    alert('Please enter atleast one email address!');
}
else */{
$.post(baseUrl+'index.php/companyadmin/saveFolderEmailPermissions',{folderid:folderid,emails:req_email,numbercue:numbercue},function(data){
		 
		 if(data == '1')
		 {
		 alert('Saved successfully');
		 }
		 else
		 {
		 alert('Some problem occured while saving');
		 }
		
		
		});
    }
}



</script>

<script type="text/javascript">
$(function () 
      { $("[data-toggle='popover']").popover({ trigger: "hover" });;
      });
   
</script>