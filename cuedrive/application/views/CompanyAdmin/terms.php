<?php


include_once("header.php");
include("sidebar.php");

if(!$this->session->userdata("adminid"))
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

$content = "";
$contentid = 0;


#print_r($raw_data);exit;
$raw_data = Contentmanagementmodel::getContent($type);
if(count($raw_data) > 0) {
if($type == 'article')
{
	$content = base64_decode($raw_data->articles);
	$contentid = $raw_data->contentid;
	$text = 'Article';
}
if($type == 'privacy')
{
	$content = base64_decode($raw_data->privacy);
	$contentid = $raw_data->contentid;
	$text = 'Privacy';
}
if($type == 'terms')
{
	$content = base64_decode($raw_data->terms);
	$contentid = $raw_data->contentid;
	$text = 'Terms & Condition';
}
}
?>


<div class="content">
<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">√ó</button>
        <?php echo $errorMsg?>
    </div>
	
<?php
	}

	if(isset($success))
	{
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php echo $success?>
    </div>
<?php
	}
?>
	<ul class="breadcrumb">
		<li><a href="#">Content Management</a> <span class="divider">/</span></li>
		<li class="active"><?php echo $text?></li>
	</ul>

	<div class="container-fluid">
		<div class="row-fluid">
  
<?php
					$this->load->helper('form');
					
					$att = array('name' => 'terms','id' => 'terms', 'method'=> 'post', 'onsubmit'=>"return validateForm();");
					print form_open('siteowner/updatecontent', $att);

							
					$page_content = array ("name" => "terms","id" => "terms","class" => "span12","value"=>$content);			
					echo form_hidden('contentid',$contentid);
					echo form_hidden('type',$type);
?>				
					
					<label><?php echo $text?></label>
					<?php echo form_textarea($page_content);?> 
					<input type="submit" class="btn btn-primary pull-right" value="Update">
                   <!-- <label class="remember-me"><input type="checkbox"> I agree with the <a href="terms-and-conditions.html">Terms and Conditions</a></label>-->
                    <div class="clearfix"></div>
                <?php echo form_close();?>
            </div>
       <!-- <p><a href="privacy-policy.html">Privacy Policy</a></p>-->

	</div>
</div>
<?php

 include_once("footer.php"); 
?>