<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
$this->load->helper('form');
	$att = array('name' => 'deviceupdate','id' => 'deviceupdate');
	echo form_open('companyadmin/updateDevice', $att);
	echo form_hidden('action',$action);
	echo form_hidden('deviceid',$deviceid);
	$devidceuuid = array ("name" => "device_uuid","id" => "device_uuid","class" => "form-control","autocomplete"=>"off","value" => set_value('device_uuid',$deviceuuid));
	$devidcename = array ("name" => "device_name","id" => "device_name","class" => "form-control","autocomplete"=>"off","value" =>  set_value('device_name',$devicename));
	$formsubmit=array("name" => "submitdevice","id" => "submitdevice","class" => "btn btn-primary ");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $action;?> Device
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/manageDevices"><i class="fa fa-mobile"></i> Device</a></li>
                        <li class="active">Manage Device</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                        
           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
			<label>Device Name <span class="text-danger">*</span></label>
         	<?php echo form_input($devidcename);?>
         	<br/>
		 	<label>Device ID <span class="text-danger">*</span> &nbsp;&nbsp; (Case sensitive)</label>
         	<?php echo form_input($devidceuuid);?>
         	<br>
         	
         	<?php if($action=="Add"){?>
         	<input type="radio" name="radioperm" value="full" checked>&nbsp;&nbsp;Give access to all files/folders on cuedrive
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="radioperm" value="later">&nbsp;&nbsp;Set permissions manually later
         	<?php }?>
         	<br/><br/>
         	<?php 
		$js = 'onClick="adddeviceclick()"';
		echo form_button($formsubmit, 'Save',$js); 
	?>
        	
         <a href="<?php echo base_url()?>index.php/companyadmin/manageDevices"><b class="btn btn-primary pull-right">Cancel</b></a>
         <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#deviceli").attr('class','treeview active');
$("#deviceicon").attr('class','fa fa-angle-down pull-right');
$("#devicesubli").attr('class','active');
$("#deviceul").attr('style','display:block');
var Validator =	new FormValidator('deviceupdate', [{
	    name: 'device_name',
	    display: 'Device Name',    
	    rules: 'required'
	},  {
	    name: 'device_uuid',
	    display: 'Device UUID', 
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		       
	    } 
	   
	});
	
Validator.setMessage('required', 'Please enter %s');	

	function adddeviceclick()
	 {
		var baseUrl = "<?php echo base_url();?>";
		var devuuid=$("#device_uuid").val();
		var devname=$("#device_name").val();
		var devid="<?php echo $deviceid;?>";
		$.post(baseUrl+'index.php/companyadmin/checkDeviceExist',{devuuid:devuuid,devname:devname,devid:devid},function(data)
			{
			if(data=="1") //not exist
			{	
				  $("#deviceupdate").submit();
			}
			else if(data=="2")
			{
				  //$('.error_box').append("<?php //echo 'You have entered a wrong serial number.';?>" + '<br />');	
				  $('.error_box').html("<?php echo 'Please enter valid Device ID';?>" + '<br />');	
			}
			else //exist
			{	
				//$('.error_box').append("<?php //echo DEVICEEXIST;?>" + '<br />');	
				$('.error_box').html("<?php echo DEVICEEXIST;?>" + '<br />');	
			}
			});
	}
</script>	 