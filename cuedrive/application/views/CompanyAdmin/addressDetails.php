<style>#signupformdata .error {    color: red;    font-size: 12px; }</style>
<form class="form-horizontal signup" role="form" id="signupformdata" action="" method="post" onSubmit="return formValidate(); return false;">
  <label>First Name <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="firstname" id="firstname"  placeholder="First Name" type="text" onKeyDown="removeErrorMessage(this)"  >
  <span class="error" id="firstnamemsg"></span> <br>
  <label>Last Name <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="lastname" id="lastname" placeholder="last Name" type="text"   onKeyDown="removeErrorMessage(this)">
  <span class="error" id="lastnamemsg"></span> <br>
  <label >Phone Number <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="phone" id="phone" placeholder="Phone Number" type="text" pattern="[0-9]{10}"  onKeyDown="removeErrorMessage(this)"  title="Enter Valid Phone Number format: 999999999">
  <span class="error" id="phonemsg"></span> <br>
  <label  >Company <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="company_name" id="company_name" placeholder="Company Name" type="text"  onKeyDown="removeErrorMessage(this)" >
  <span class="error" id="company_namemsg"></span> <br>
  <label  >Street Address <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="address" id="address" placeholder="Street Address" type="text"   onKeyDown="removeErrorMessage(this)">
  <span class="error" id="addressmsg"></span> <br>
  <label  >City <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control city" name="city" id="city" placeholder="City" type="text"  onKeyDown="removeErrorMessage(this)" >
  <span class="error" id="citymsg"></span> <br>
  <label  >Zip Code <span class="error" style="color:#f0776c">*</span></label>
  <input class="form-control" name="zipcode" id="zipcode" placeholder="Zip Code" type="text"  title="Enter Valid Zip Code"  onKeyDown="removeErrorMessage(this)" >
  <span class="error" id="zipcodemsg"></span> <br>
  <input class="form-control"  name="passwordhint" id="passwordhint" value="123" placeholder="Password Hint" type="hidden" >
  <label >Country <span class="error" style="color:#f0776c">*</span></label>
	<?php	
	echo '<select name="country" id="country" class="form-control">';
		foreach($country as $count)
		{	
			if($count->country_code == 'AUS')
			{
					echo '<option value="'.$count->country_name.'" selected="selected">'.$count->country_name.'</option>';
			}	else	{
				echo '<option value="'.$count->country_name.'">'.$count->country_name.'</option>';
			}	
		}
	echo '</select>';
	?>
  <span class="error"></span> <br>  
  <br>
  
  <button type="reset" class="btn btn-inverse btn-primary cancel" id="reset" onClick="backPayBody()">Back</button>
	<input type="submit" id="submit" class="btn btn-inverse btn-primary signup" value="Next">
	<input type="hidden" id="pkgid" name="pkgid" value="<?php	echo $pkgid; ?>" />
	<input type="hidden" id="noofdevices" name="noofdevices" value="<?php	echo $noofdevices; ?>" />
</form>