<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("companyid"))  
	{
		header ("Location:".base_url()."index.php/login");
		exit;
	}
	$baseredirecturl=base_url()."index.php/companyadmin/auditloglist";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Auditlog
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/auditloglist"><i class="fa fa-mobile"></i>Log List</a></li>
                        <li class="active">Auditlog</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-2">
	
		
	<?php 
	    $this->load->helper('form');	
	    $filename = 'log_'.$this->session->userdata("companyid").'.txt';
		$att = array('name' => 'addnewuser','id' => 'addnewuser');
		echo form_open('companyadmin/downloadLogFile', $att);	
		echo form_hidden('filename', $this->encrypt->encode($filename));
		echo form_hidden('companyid', $this->encrypt->encode($this->session->userdata("companyid")));
		$adduserbutton=array("name" => "export","id" => "export","class" => "btn btn-primary");
		
		if(count($results[0]) > 0)
			echo form_submit($adduserbutton, 'Export');
		
		echo form_close();

	?>
	
	</div>
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/companyadmin/auditloglist") ;
             }
             }); 
	     });
	     
          </script>
    

	    
	    
	 <div class="col-xs-3">
	   <input type="text" name="from" id="from" class="form-control" placeholder="From date" onkeydown="return false;"/>
	 </div>  
	 
	 
	 
	 <div class="col-xs-3">
	   <div class="input-group">
	   <input type="text" name="to" id="to" class="form-control" placeholder="To date" onkeydown="return false;"/>
	    <span class="input-group-btn">
		
               <button type='submit' name='srchbtn1' id='srchbtn1' class="btn btn-flat"><i class="fa fa-search"></i></button>
		
								
            </span>
            </div>
	 </div> 
	 
	 
	 <script>
		$(document).ready(function() {
			$( "#from" ).datepicker({ dateFormat: 'dd-mm-yy', maxDate: 0, onSelect: function(selected){$("#to").datepicker("option","minDate", selected)}});
			$( "#to" ).datepicker({ dateFormat: 'dd-mm-yy', maxDate: 0, onSelect: function(selected) {$("#from").datepicker("option","maxDate", selected)}});
			$('#srchbtn1').live('click', function () 
			{
	              	//if (ev.which === 13) {
                    //ev.preventDefault();
                    var to = $("#to").val();
		        	var from = $("#from").val();
	                window.location.replace("<?php echo base_url()?>index.php/companyadmin/auditloglist?to="+to+"&from="+from) ;
	                //}
				
	          });
		});
	 </script>
	  <div class="col-xs-4">
	   <?php			
            $this->load->helper('form');
			
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'post', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('companyadmin/auditloglist', $att);
            $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
           ?>			
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
              </div>
             
             
              	         
             
        <?php echo form_close();?>

                
                
	 </div>
	   
	   
	   
	   
	   
	       
	      
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){
					
					 $filename = 'log/log_'.$this->session->userdata("companyid").'.txt';
					 $fh = fopen($filename, 'w');
					?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Event</th>
									<th>Device ID</th>
									
									
									
									<th>Details</th>
									
								
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data): 
							
							
							$devicedetails = Companydevicemodel::getDeviceDetails($t_data->deviceid);
							$devicename = isset($devicedetails['name']) ? $devicedetails['name'] : 'web';
							$deviceuuid = isset($devicedetails['deviceuuid']) ? $devicedetails['deviceuuid'] : 'web';
							
							$content =  $t_data->activitydetails.' '.'on '.' '.$devicename.' '.'at'.' '.date('d/m/Y g:i a',strtotime($t_data->datetime)).' '.'with IP address :'.$t_data->ipaddress;
							$content.="\r\n";
							$content.="\r\n";
							fwrite($fh, $content);
							
							?>


							<tr id='RecRow<?php echo $t_data->logid;?>'>
							<td width='10%'><?php echo $t_data->action;?></td>
							<td width='10%'><?php echo $deviceuuid;?></td>
							
							
						
							<td width='80%'><?php echo $t_data->activitydetails.' '.'on '.' '.'<b>'.$devicename.'</b>'.' '.'at'.' '.'<b>'.date('d/m/Y g:i a',strtotime($t_data->datetime)).'</b>'.' '.'with IP address :'.'<b>'.$t_data->ipaddress.'</b>';?></td>
						
						   
							</tr>
							<?php endforeach;
							
							
    
                                                        fclose($fh);
							
							?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Log to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
</div>
<?php
	include_once("footer.php"); 
?>
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/datepicker-jquery-ui.css">
<script>
$(function() {
	 $("#logli").attr('class','treeview active');
	 $("#auditlogicon").attr('class','fa fa-angle-down pull-right');
	 $("#logsubli").attr('class','active');
	 $("#logul").attr('style','display:block');
});
</script>