<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$baseredirecturl=base_url()."index.php/companyadmin/helpdocs";
?>

<?php


?>


<aside class="right-side">  


<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Help Documents
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/helpdocs"><i class="fa fa-mobile"></i> Documents</a></li>
                        <li class="active">Manage Documents</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	
	<div class="col-xs-6">
		
              
              <div class="input-group">
                           
              </div>
            
	         </div>
	         </div>
	
			 <div class="row">
                      
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){
					
					  $i=0;
					?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table">
							<thead>
								<tr>
									<th>Document</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							
				            $EditLink = '<a href="'.base_url().'/index.php/companyadmin/modifydocument?id='.$t_data->contentid.'" ><i class="fa fa-search" style="color:black;"></i></a>&nbsp;&nbsp;&nbsp;';
							?>
							<tr id='RecRow<?php echo $t_data->contentid;?>'>
							<td><?php echo $t_data->articletitle?></td>
							<td class='span1'><?php echo $EditLink?></td>
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Document to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {

        
	   $("#supportli").attr('class','treeview active');
	   $("#supporticon").attr('class','fa fa-angle-down pull-right');
	   $("#helpdocli").attr('class','active');
	   $("#supportul").attr('style','display:block');
	   });
</script>