<?php
$scriptload = 1;
include_once("header.php"); 
include("sidebar.php");

   if(strpos($_SERVER['HTTP_USER_AGENT'],"Chrome") > -1){
	echo '<script src="'.base_url().'theme/js/folderupload1.js"></script>';
    }
   else {
        echo '<script src="'.base_url().'theme/js/fileupload.js"></script>';
    }


if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
$this->load->helper('form');
//$addfolderbutton=array("name" => "addfolder","id" => "addfolder","class" => "btn btn-primary", "disabled" => "disabled");
$addfolderbutton=array("name" => "addfolder","id" => "addfolder","class" => "btn btn-primary");
$pastefolderbutton=array("name" => "pastefolder","id" => "pastefolder","class" => "btn btn-primary", "disabled" => "disabled");
$pastefoldersbutton=array("name" => "pastefolders","id" => "pastefolders","class" => "btn btn-primary", "disabled" => "disabled");
$uploadbutton=array("name" => "","id" => "uploadfile","class" => "btn btn-primary", "disabled" => "disabled");
?>
 <div class="inner-div" style="padding:10px;">
 <aside class="right-side" style="padding:5px;">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Folder
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="javascript:selectRoot();"><i class="fa fa-folder"></i> Folder</a></li>
                        <li class="active">Manage Folder</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<?php 
		$js = 'onClick="addfolderclick()"';
		echo form_button($addfolderbutton, 'Create Folder',$js); 
	?>
	<?php 
		if($this->session->userdata('companyid') == '27')	{
			//$js = 'onClick="pastefolderclick()"';
			echo form_button($pastefolderbutton, 'Paste'/*,$js*/); 
		}
	?>
	<?php 
		/* if($this->session->userdata('companyid') == '27' || $this->session->userdata('companyid') == '281' || $this->session->userdata('companyid') == '224')	{ */
			//$js = 'onClick="pastefolderclick()"';
			echo form_button($pastefoldersbutton, 'Paste'/*,$js*/); 
		/* } */
	?>
	<?php 
		$jsfile = 'onClick="uploadfileclick()"';
		echo form_button($uploadbutton, 'Upload',$jsfile); 		 
	?>
	
	<span id="mainloadingdiv" style="display:none;"><!--<image src="<?php echo base_url()?>theme/images/loading.gif"/>--></span>
	<!-- Upload new file div -->	
	       <div id="uploadModal" class="reveal-modal" align="center">
		   <h3 style="margin-top: 0;padding-left: 32px; text-align: left;"> <?php echo UPLOADFILE; ?> </h3>
	       <label id="errmsgfile" style="color:red;font-weight:normal;"></label>
		 <span id="inputfilediv" style="display:none;border: 1px solid;padding:13px;">
		<form id="fileinfo" enctype="multipart/form-data" method="post" name="fileinfo" class="navbar-form">
		<div class="form-group"><input id="upfile" name="upfile[]" type="file" multiple/></div>
		<div id="loadingdiv1" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div>
		<div id="uploadbtn" class="form-group"><a href="javascript:uploadfile();"><b class="btn btn-primary btn-sm" style="font-size: 1.0em;">Upload</b></a>
		<!--<a href="javascript:closefile();"><i class="fa fa-times" style="font-size: 1.5em;"></i></a>-->
		</div>
			</form>
		</span>
	       <div style="margin-top: 10px">
   
                <a class="close-reveal-modal">&#215;</a>
               </div>	
               </div>	
		<input type="hidden" name="selectedfolderid" id="selectedfolderid" value="0"/>
		<input type="hidden" name="sourcefolder" id="sourcefolder" value="0"/>
		<input type="hidden" name="sourcefoldername" id="sourcefoldername" value=""/>
		<input type="hidden" name="selectedfoldertype" id="selectedfoldertype" value=""/>	
		<input type="hidden" name="selectedfoldername" id="selectedfoldername" value=""/>
		<input type="hidden" name="selectedtype" id="selectedtype" value=""/>	
		<input type="hidden" name="expandednodes" id="expandednodes" value=""/>	
		<input type="hidden" name="data_tt_id" id="data_tt_id" value=""/>	
		<input type="hidden" name="level" id="level" value=""/>
		

		  <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" style="margin:10px 0 10px;">
                              
                            </h2>  
							<div id="foldererrmsg" style="color:red; display:none;"><?php echo NOPARENTFOLDER;?></div>                          
                        </div><!-- /.col -->
                 </div>
				 
				<div class="alert alert-success" id="copydata" style="display: none;"></div>
				<div class="alert alert-danger" id="copydata_error" style="display: none;"></div>
				 
								 <div id="txtHint" style="text-align:center;"></div>
		<a href="javascript:selectRoot();" id="selecthome" style="padding-bottom:5px;">Home</a>
		                       <div id="tablewidget">
		                      
					<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						
					</div>
					
					
		                       <?php if(strpos($_SERVER['HTTP_USER_AGENT'],"Chrome") > -1){ ?>
		                        
		                            <section id="droparea">
					        <div id="">
					         <div id="dialog" style="text-align:center;display:none;" dropzone webkitdropzone="copy file:image/png file:image/gif file:image/jpeg">
					          <image class="img-responsive" src="<?php echo base_url()?>/blue-drop.png"/>
					         </div>
					        </div>
					   </section>
					    
                                            <section id="fileinput" style="display: none;">
                                               <div> 
                                                 <input type="file" accept="image/*" multiple>
                                               </div>
                                             </section>

				     <?php } else {?>
					
					
					<div id="dialog" style="text-align:center;display:none;">
					     <image class="img-responsive" src="<?php echo base_url()?>/blue-drop.png"/>
					</div>
					    
				    <?php } ?>
		
		
	</section>
</aside>

</div>

 	<div id="myModal" class="reveal-modal">
        <h3 style="margin-top:0px;"> <?php echo CREATEFOLDER; ?> </h3>
		<label id="errmsg" style="color:red;font-weight:normal;"></label>
		<div class="input-group input-group-sm">
        <input type="text" class="form-control" id="txtFolderName" name="txtFolderName" placeholder="Enter Folder Name">
           <span class="input-group-btn">
             <button type="button" class="btn btn-primary btn-flat" onclick="addfolder()">Go</button>
           </span>
     </div>
     <div id="permission" style="display:none;">
         <br>
		<input type="radio" name="radioperm" id="defaultselect" value="parent" checked>&nbsp;&nbsp;&nbsp;&nbsp;Inherit current folder permissions<br />
		<input type="radio" name="radioperm" value="later">&nbsp;&nbsp;&nbsp;&nbsp;Set permissions manually later
	 </div>	
	 
     <div id="rootfolderpermission" style="display:none;">
         <br>
		<input type="radio" id="defaultselectroot" name="radioperm" value="alluser" checked>&nbsp;&nbsp;&nbsp;&nbsp;Full Permissions for all Users<br />
		<input type="radio" name="radioperm" value="alldevice">&nbsp;&nbsp;&nbsp;&nbsp;Full Permissions for all Devices<br />
		<input type="radio" name="radioperm" value="later" >&nbsp;&nbsp;&nbsp;&nbsp;Set permissions manually later
	 </div>			

	 		
	 <div style="margin-top: 10px">
   		<a class="close-reveal-modal">&#215;</a>
 	 </div>	
 </div>	
 
 
	 <div id="editFolderModal" class="reveal-modal">
        <h3 style="margin-top:0px;"> <?php echo EDITFOLDER; ?> </h3>
		<label id="editerrmsg" style="color:red; font-weight:normal;"></label>
		<div class="input-group input-group-sm">
        <input type="text" class="form-control" id="edittxtFolderName" name="edittxtFolderName" placeholder="Enter Folder Name">
           <span class="input-group-btn">
             <button type="button" class="btn btn-primary btn-flat" onclick="editfolder()">Go</button>
           </span>
        </div>
        <div id="editpermission" style="display:none;">
        <br>
		<input type="radio" name="radioperm" value="parent">&nbsp;&nbsp;&nbsp;&nbsp;Inherit current folder permissions<br>
		<input type="radio" name="radioperm" value="later" checked>&nbsp;&nbsp;&nbsp;&nbsp;Set permissions manually later
	</div>			



 <div style="margin-top: 10px">
   
   <a class="close-reveal-modal">&#215;</a>
 </div>	
 </div>	
 
 
 <div id="editFileModal" class="reveal-modal">
        <h3 style="margin-top:0px;"> Edit File Name </h3>
		<label id="editfileerrmsg" style="color:red"></label>
		<div class="input-group input-group-sm">
        <input type="text" class="form-control" id="edittxtFileName" name="edittxtFileName" placeholder="Enter File Name">
           <span class="input-group-btn">
             <button type="button" class="btn btn-primary btn-flat" onclick="editfile()">Go</button>
           </span>
        </div>
		<div style="margin-top: 10px"> <a class="close-reveal-modal">&#215;</a></div>	
 </div>
 
 
 
 
 <div id="loaderModal" class="reveal-modal" style="opacity: 1;  bottom: 5%;left: 61%;top: unset;margin-left: 0px;">
           <h2 align="center" id="percentage">Please wait...</h2>
           <div class="progress sm progress-striped active">
           <div style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar progress-bar-success" id="progressbar">
                   <span class="sr-only">20% Complete</span>
           </div>
           </div>
	
 </div>		

 <div id="loaderModal-2" class="reveal-modal" style="opacity: 1; bottom: 5%; left: 5%;top: unset;margin-left: 0px; position: fixed;">
           <h2 align="center" id="percentage-2">Please wait...</h2>
           <div class="progress sm progress-striped active">
           <div style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar progress-bar-success" id="progressbar-2">
                   <span class="sr-only">20% Complete</span>
           </div>
           </div>
	
 </div>		

<!-- Upload A New File 
 
-->
<?php
	include_once("footer.php"); 
?>	

<link href="<?php echo base_url()?>theme/stylesheets/jquery.treetable.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>theme/stylesheets/jquery.treetable.theme.default.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>theme/js/jquery.treetable.js"></script>

<script>
var baseUrl = "<?php echo base_url();?>";
$(document).ready(function() 
{ 
  var isChromium = window.chrome,
  vendorName = window.navigator.vendor;
  if(isChromium !== null && vendorName === "Google Inc.") 
  {
	//alert('Drag-drop file/folder upload will not work on Chrome for now. Folder upload is in process!');
  } 
	
	
	
	
});

/** 25 May 2015 **/
function getFolderValue(obj)	{

		var confirminput = confirm("Do you wish to activate the autmoatic folder sharing");
		var value = obj.checked;
		alert(value);
		if (confirminput == true) {
				
				var flag = obj.checked ? 1 : 0;
				
				var xmlhttp=new XMLHttpRequest();
		    xmlhttp.onreadystatechange=function() {
        	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        	}
    		}
				
    		xmlhttp.open("POST",baseUrl+"index.php/companyadmin/FolderActivation",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    		xmlhttp.send("checked="+flag+"&folderid="+obj.value);				
				
		} else {	
				
				if(value)	{		
					obj.checked = false;				
				}	else	{
					obj.checked = true;				
				}
		}
}

/** 25 May 2015 **/

$(function() {
	$( "#dialog" ).dialog({
		autoOpen: false,
		width:"450px",
		show: {
			effect: "puff",
			duration: 100
		},
		
		close: function() {
  			$(".right-side").css("opacity","1");
		}
	});
	
});



$(document).ready(function(){

   $("#folderli").attr('class','treeview active');
   $("#foldersubli").attr('class','active');
   $("#foldericon").attr('class','fa fa-angle-down pull-right');
   $("#folderul").attr('style','display:block');
   
	
	$("#loadingdiv").show();
	$.post(baseUrl+'index.php/companyadmin/loadFolderTree',function(data){
		$("#loadingdiv").hide();
		$("#tablewidget").html(data);
		
	});

    var isChromium = window.chrome,
    vendorName = window.navigator.vendor;
    if(isChromium !== null && vendorName === "Google Inc.") {
	init();
     }	
});


/* Add folder*/
function addfolderclick()
{
	$("#errmsg").html('');
	$("#txtFolderName").val('');
	
	if($('#selectedfoldertype').val() == 'public')
	{
	  $('#permission').hide();
	}
	else 
	{
	  if($('#selectedfolderid').val() == '0')
	   {
			 $('#permission').hide();
			 $("#defaultselectroot").attr('checked', true);
			 $('#rootfolderpermission').show();			 
	   }else{
			 $("#defaultselect").attr('checked', true);
			$('#permission').show();
			$('#rootfolderpermission').hide();
	   }
	}
	$("#myModal").reveal();	
	$('#txtFolderName').focus();
}

	function addfolder()
	{
	$('#myModal').trigger('reveal:close');
	var expandednodesstr=$("#expandednodes").val();
	//expandednodesstr += ','+$("#data_tt_id").val();
	
	if($("#selectedfolderid").val()=="")
	{
		$("#errmsg").html("<?php echo NOPARENTFOLDER;?>");			
	}
	else
	{
		var parentfolder=$("#selectedfoldername").val();
		var parentfolderid=$("#selectedfolderid").val();
		var parentfoldertype=$('#selectedfoldertype').val();
		var level=$("#level").val();
		if (true) 
		{ 
			var newfoldername=$.trim($("#txtFolderName").val());
			var permflag=$("input:radio[name=radioperm]:checked").val();
			
			if(parentfoldertype == 'public')
			{
			var permflag = 'parent';
			}
			
			if(newfoldername=="")
			{
				//alert("<?php echo NOFOLDERNAME;?>");
				$("#errmsg").html("<?php echo NOFOLDERNAME;?>");
			}
			else
			{
				//upload folder
				$.post(baseUrl+'index.php/companyadmin/addFolder',{parentfolderid:parentfolderid,newfoldername:newfoldername,level:level,permflag:permflag},function(data){
				if(data=="1")
				 {
					//$('#myModal').trigger('reveal:close');
					$("#loadingdiv").show();
					if(expandednodesstr=="" && parentfolderid != 0) 
					{
						expandednodesstr=parentfolderid;
					}
					else{
						if(String(expandednodesstr).search(parentfolderid) == -1 && level == 1){
							expandednodesstr=expandednodesstr+","+parentfolderid;
						}
					}
					$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data)
					 {
							$("#loadingdiv").hide();
							closefolder();
							$("#tablewidget").html('');
							$("#selectedfolderid").val(0);
							//location.reload();
							$("#tablewidget").html(data);
							//$("#addfolder").attr('disabled','disabled');
							$("#uploadfile").attr('disabled','disabled');
							//$("#foldererrmsg").show();
						});
						
				 }else{
						$('#copydata_error').hide().html('<center> '+data+' </center>').fadeIn('slow').delay(3000).hide('slow');
						//$("#errmsg").html(data);
						//alert(data);
						//alert('An unexpected error occured !');
					}
				});
			}
		}
	}
}

	// Select root folder
	function selectRoot()
	{
		$("#selectedfolderid").val(0);
		$("#selectedfoldertype").val('');
		$("#expandednodes").val('');
		$("#data_tt_id").val('');
		$("#level").val('');
		$("#foldertree tr").removeClass("selected");
		$("#uploadfile").attr('disabled','disabled');
	}



	function closefolder()
	{
		$("#uploadfile").removeAttr('disabled');
		$("#txtFolderName").val('');
		$("#inputdiv").hide();	
	}

	/* Upload File*/
	function uploadfileclick()
	{
		$("#errmsgfile").html('');
		//$("#addfolder").attr('disabled','disabled');
		$("#inputfilediv").css( "display", "inline-block");
		//$("#inputfilediv").show();
		$("#uploadbtn").show();	
		$("#uploadModal").reveal();
	}

	function uploadfile()
	{
		if($("#selectedfolderid").val()=="")
		{
			//alert("<?php echo NOPARENTFOLDER;?>");
			$("#errmsgfile").html("<?php echo NOPARENTFOLDER;?>");				
		}
		else
		{
			var parentfolder=$("#selectedfoldername").val();
			var parentfolderid=$("#selectedfolderid").val();
			var filenamestr="";
			//if (confirm("Do you want upload file in "+parentfolder+" ?")) { 
			if (true) { 
				 var files = $("#upfile")[0].files;
				// Loop through each of the selected files.
				 for (var i = 0; i < files.length; i++) {
				   var file = files[i];
				   // Add the file to the request.
				  // fd.append('selectedfiles[]', file, file.name);
				  if(filenamestr=="")
					  filenamestr=file.name;	
				  else
					  filenamestr=filenamestr+"@#"+file.name;		      
				 }
				//var newfilename=$("#upfile").val();
				if(filenamestr=="")
				{
					//alert("<?php echo NOFILESELECTED;?>");
					$("#errmsgfile").html("<?php echo NOFILESELECTED;?>");	
				}
				else
				{
					$.post(baseUrl+'index.php/companyadmin/checkFileExist',{parentfolderid:parentfolderid,filenamestr:filenamestr},function(data)
					{
					
					if(data=="") //not exist
					{	
						uploadFileAjax(parentfolderid,"false");
					}
					else //exist
					{					
						if (confirm(data+" already exist.Do you want replace it?")) { 
							uploadFileAjax(parentfolderid,"true");
						}
					}
					});
				}
				
			}
		}
		}

	function uploadFileAjax(parentfolderid,override)
	{
		
	
		$("#progressbar").css("width","0%");
		$('#uploadModal').trigger('reveal:close');
		$("#loaderModal-2").reveal();
		$(".reveal-modal-bg").css("position", "static"); 
		var expandednodesstr=$("#expandednodes").val();
		expandednodesstr += ','+$("#data_tt_id").val();
		
		// var fd = new FormData(document.getElementById("fileinfo"));
		 var fd = new FormData();
		 var files = $("#upfile")[0].files;
		 fd.append("parentfolderid", parentfolderid);
		 fd.append("override", override);
	  // Loop through each of the selected files.
		 for (var i = 0; i < files.length; i++) {
		   var file = files[i];
		   // Add the file to the request.
		  // fd.append('selectedfiles[]', file, file.name);
		 // alert(file.name);
		   fd.append("selectedfiles[]", file);
		   
		 }
	   
		 $("#mainloadingdiv").show();
		 
		 
		 $.ajax({
		 xhr: function()
		  {
			var xhr = new window.XMLHttpRequest();
			//Upload progress
			xhr.upload.addEventListener("progress", function(evt){
			  if (evt.lengthComputable) {
				var percentComplete = (evt.loaded / evt.total) * 100;
					percentComplete = percentComplete.toFixed();
					$("#percentage-2").html(percentComplete + '%');
					$("#progressbar-2").css("width",percentComplete+"%");
			  }
			}, false);
			return xhr;
		  },
			 url: baseUrl+'index.php/companyadmin/uploadFile',  
			 type: 'POST',
			 data: fd,
			 success:function(data){
					 if(data=="success")
						{
					// $('#uploadModal').trigger('reveal:close');
							 $("#mainloadingdiv").hide();
							 $("#uploadbtn").hide();
							 $("#loadingdiv1").show();
								$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
									$("#loadingdiv1").hide();
									//closefile();
									$('#loaderModal-2').trigger('reveal:close');
									$("#addfolder").removeAttr('disabled');
									$("#selectedfolderid").val(0);
									$("#upfile").val('');
									$("#tablewidget").html('');
									$("#tablewidget").html(data);
									//$("#addfolder").attr('disabled','disabled');
									$("#uploadfile").attr('disabled','disabled');
									//$("#foldererrmsg").show();
								});
						}
						else
						{
							alert(data);
							//window.location.reload();
							$('.reveal-modal-bg').hide();
							$('#loaderModal-2').hide();
						}
				 }, 
			 cache: false,
			 contentType: false,
			 processData: false
		 });
	}

	function closefile()
	{
		$("#addfolder").removeAttr('disabled');
		$("#upfile").val('');
		//$("#inputfilediv").hide();	
		$('#uploadModal').trigger('reveal:close');
	}

	/* Download file*/
	function downloadfile()
	{
		
	//$('#downloadlink').click( function( e ) {
	
		if($("#downloadlink").attr('disabled'))
		{
			 e.preventDefault();
			return false;
		}
		var selectedfile=$("#selectedfoldername").val();
		var selectedfileid=$("#selectedfolderid").val();
		var selectedtype=$("#selectedtype").val();
		if(selectedfile=="")
		{
			alert("<?php echo NOFILESELECTED;?>");				
		}
	
		else
		{
			//download file		
			$.post(baseUrl+'index.php/companyadmin/downloadFile',{selectedfileid:selectedfileid},function(data){
				//window.location.href = data;
				if(!data)	{
					alert('File does not exist.');
				}	else	{
					window.open(data);
				}	
			});
		}
	//});
	}


	var index;
	/* Delete File*/
	function deleteitem() 
	{
		$("#progressbar").css("width","0%");
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		var expandednodesstr=$("#expandednodes").val();
		//expandednodesstr += ','+$("#data_tt_id").val();
		expandednodesstr += ','+arrayString.slice(0, -1);
		/*if(expandednodesstr.indexOf('-') > 0) 
		{
		   var sourceArray = expandednodesstr.split('-');
		   var sourcelength = sourceArray.length;
		   var expandednodesstr = sourceArray[0];
		}*/
		var selected = $("#selectedfoldername").val();
		
		var	countword = selected.split(' ').length;
		if(countword > 1)
		 {
		 	selected = selected;	
		 }else{
		 	selected = selected.replace(/\s/g,'');
		 }
		 
		var selectedid=$("#selectedfolderid").val();
		var selectedtype=$("#selectedtype").val();
		if(selected=="")
		{
			alert("<?php echo NOFILEFOLDERSELECTED;?>");				
		}
		else if(selectedtype=="file ui-draggable")//delete file
		{
			if (confirm("Are you sure you want to delete '"+selected+"'?")) {
			
					$("#percentage").html('Please wait...');
					$("#progressbar").css("width","100%");
					$("#loaderModal").reveal();	
					$(".reveal-modal-bg").css("position", "static");
				//delete File			
				$.post(baseUrl+'index.php/companyadmin/deleteFile',{selectedid:selectedid},function(data){
				if(data=="1")
					{
					
					$("#loadingdiv").show();
						$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
							$("#selectedfolderid").val(0);
							$("#loadingdiv").hide();
							$("#tablewidget").html('');
							$("#tablewidget").html(data);
							$("#loaderModal").trigger('reveal:close');
						});
					}
				else //delete folder
				{
					alert(data);
				}
				});
			}
		}
		else if(selectedtype=="folder ui-draggable") //delete folder
		{
			if (confirm("Are you sure you want to delete '" +selected+ "' and its contents?")) 
			{
				//delete folder	
				$("#percentage").html('Please wait...');
				$("#progressbar").css("width","100%");	
				$("#loaderModal").reveal();
				$(".reveal-modal-bg").css("position", "static");
				$.post(baseUrl+'index.php/companyadmin/deleteFolder',{selectedid:selectedid},function(data){
				if(data=="1")
					{
						$("#loadingdiv").show();
						$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
							$("#loaderModal").trigger('reveal:close');
							$("#selectedfolderid").val(0);
							$("#loadingdiv").hide();
							$("#tablewidget").html('');
							//location.reload();
							$("#tablewidget").html(data);
						});
					}
				else //delete folder
				{
					alert(data);
				}
				});
			}
		}
	}
	
	
	/* Delete File*/
	function copyfolders() 
	{
		$("#progressbar").css("width","0%");
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		var expandednodesstr=$("#expandednodes").val();
		//expandednodesstr += ','+$("#data_tt_id").val();
		expandednodesstr += ','+arrayString.slice(0, -1);
		/*if(expandednodesstr.indexOf('-') > 0) 
		{
		   var sourceArray = expandednodesstr.split('-');
		   var sourcelength = sourceArray.length;
		   var expandednodesstr = sourceArray[0];
		}*/
		var selected = $.trim($("#selectedfoldername").val());
		
		var	countword = selected.split(' ').length;
		if(countword > 1)
		 {
		 	selected = selected;	
		 }else{
		 	selected = selected.replace(/\s/g,'');
		 }
		 
		var selectedid=$("#selectedfolderid").val();
		var sourcefoldername=$("#selectedfoldername").val();
		var selectedtype=$("#selectedtype").val();
		if(selected=="")
		{
			alert("<?php echo NOFILEFOLDERSELECTED;?>");				
		}		
		else if(selectedtype=="folder ui-draggable") //delete folder
		{
			
			//delete folder	
			$("#pastefolder").removeAttr('disabled');
			$("#pastefolders").removeAttr('disabled');
			$("#copydata").show();
			$("#sourcefolder").val(selectedid);
			$("#sourcefoldername").val(sourcefoldername);
			$('#copydata').hide().html('<center>'+selected+' directory and its contents are copied.</center>').fadeIn('slow').delay(3000).hide('slow');
			//$("#copydata").html();
			//$("#copydata").delay('slow').fadeIn();
		
		}
	}
	
	$("#pastefolder").click(function()	{
					
		var destinationselectid=$("#selectedfolderid").val();
		var selectedfoldername=$("#selectedfoldername").val();
		var selectedid=$("#sourcefolder").val();
		var sourcefoldername=$("#sourcefoldername").val();
		
		$("#progressbar").css("width","0%");
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		
		var expandednodesstr=$("#expandednodes").val();		
		expandednodesstr += ','+arrayString.slice(0, -1);
		
		$("#percentage").html('Please wait...');
		$("#progressbar").css("width","100%");	
		$("#loaderModal").reveal();
		$(".reveal-modal-bg").css("position", "static");
		$.post(baseUrl+'index.php/companyadmin/copyFolder',{selectedid:selectedid, destinationselectid:destinationselectid, sourcefoldername: $.trim(sourcefoldername)},function(data)	{
			if(data=="1")
			{
				$("#loadingdiv").show();
				$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
					$("#loaderModal").trigger('reveal:close');
					$("#selectedfolderid").val(0);
					$("#loadingdiv").hide();
					$("#tablewidget").html('');
					//location.reload();
					$("#tablewidget").html(data);
				});
			}
			else //delete folder
			{
				alert(data);
			}
		});
	
	});
	
	$("#pastefolders").click(function()	{
					
		var destinationselectid=$("#selectedfolderid").val();
		var selectedfoldername=$("#selectedfoldername").val();
		var selectedid=$("#sourcefolder").val();
		var sourcefoldername=$("#sourcefoldername").val();
		
		$("#progressbar").css("width","0%");
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		
		var expandednodesstr=$("#expandednodes").val();		
		expandednodesstr += ','+arrayString.slice(0, -1);
		
		$("#percentage").html('Please wait...');
		$("#progressbar").css("width","100%");	
		$("#loaderModal").reveal();
		$(".reveal-modal-bg").css("position", "static");
		$.post(baseUrl+'index.php/companyadmin/copyFolders',{selectedid:selectedid, destinationselectid:destinationselectid, sourcefoldername: $.trim(sourcefoldername)},function(data)	{
			
			if(data=="1")
			{
				$("#loadingdiv").show();
				$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
					$("#loaderModal").trigger('reveal:close');
					$("#selectedfolderid").val(0);
					$("#loadingdiv").hide();
					$("#tablewidget").html('');
					//location.reload();
					$("#tablewidget").html(data);
				});
			}
			else if(data == "2" || data == "3")
			{
				alert("Some error occured while uploading");
			}
			else //delete folder
			{
				alert(data);
			}
		});
	
	});
	
	function setFolderValue(folderId)	{
		
		
		$("#selectedfolderid").val(folderId);
		
	}
	
	
	/* Edit file model*/
	function editFileclick()
	{	
		$("#editfileerrmsg").html('');
		$("#editFileModal").reveal();	
		var selected = $("#selectedfoldername").val();
	    //var sourceArray = selected.split('.');
		var filename = selected.substr(0, selected.lastIndexOf('.')) || selected;
		//var sourcelength = sourceArray.length;
		$("#edittxtFileName").val(filename);
		//$("#edittxtFileName").val(sourceArray[0]);
	}

	/* Edit file*/
	function editfile()
	 { 
		var selectedid=$("#selectedfolderid").val();
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		var expandednodesstr = $("#expandednodes").val();
			expandednodesstr = expandednodesstr.replace('-'+selectedid, '');
			expandednodesstr += ','+arrayString.slice(0, -1);

		//var expandednodesstr=$("#expandednodes").val();
		var selected = $("#selectedfoldername").val();
		//var selectedid=$("#selectedfolderid").val();
		var selectedtype=$("#selectedtype").val();
		$("#editfileerrmsg").html('');
		$('#editFileModal').trigger('reveal:close');
		$("#percentage").html('Please wait...');
		$("#progressbar").css("width","100%");
		$("#loaderModal").reveal();	
		$(".reveal-modal-bg").css("position", "static"); 
		var editfilename = $.trim($("#edittxtFileName").val());
		 if(editfilename=="")
		 {
			$("#editfileerrmsg").html("<?php echo NOFOLDERNAME;?>");
		 }else{
			//edit file
			$.post(baseUrl+'index.php/companyadmin/editFileName',{selectedid:selectedid, editfilename:editfilename},function(data){
			if(data=="1")
				{
					$('#loaderModal').trigger('reveal:close');
					$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
					$("#tablewidget").html('');
					$("#tablewidget").html(data);
			   }); 
			 }
				else //delete folder
				{
					$("#editerrmsg").html(data);
					//alert(data);
				}
			});
		 }	
	}
	 
	
	/* Edit folder model*/
	function editFolderclick()
	{
		$("#editerrmsg").html('');
		$("#editFolderModal").reveal();	
		$('#editpermission').hide();
		var selected = $("#selectedfoldername").val();
		$("#edittxtFolderName").val(selected);
		
		
	}


	
	
	/* Edit folder*/
	function editfolder() 
	{
		var selectedid=$("#selectedfolderid").val();
		var partsOfStr = $("#data_tt_id").val().split('-');
		var arrayString = "";
		for(var i = 0; i < (partsOfStr.length-1); i++) 
		{
			arrayString += partsOfStr[i]+'-';
		}
		
		var expandednodesstr = $("#expandednodes").val();
			expandednodesstr = expandednodesstr.replace('-'+selectedid, '');
		//var expandednodesstr=$("#expandednodes").val();
			expandednodesstr += ','+arrayString.slice(0, -1);
		//expandednodesstr += ','+$("#data_tt_id").val();
		
		/*if(expandednodesstr.indexOf('-') > 0) 
		{
		   var sourceArray = expandednodesstr.split('-');
		   var sourcelength = sourceArray.length;
		   var expandednodesstr = sourceArray[0];
		}*/
		
		var selected = $("#selectedfoldername").val();
		var selectedtype=$("#selectedtype").val();
		$("#editerrmsg").html('');
		
		if(selected=="")
		{
			alert("<?php echo NOFILEFOLDERSELECTED;?>");				
		}
		else if(selectedtype=="folder ui-draggable") //edit folder
		 {
			 	$('#editpermission').hide();
				$("#percentage").html('Please wait...');
				$("#progressbar").css("width","100%");
				//$("#loaderModal").reveal();	
				$(".reveal-modal-bg").css("position", "static"); 
				var edittxtFolderName = $.trim($("#edittxtFolderName").val());
				if(edittxtFolderName=="")
				{
					$("#editerrmsg").html("<?php echo NOFOLDERNAME;?>");
				}
				else
				{
					//edit folder
					$.post(baseUrl+'index.php/companyadmin/editFolderName',{selectedid:selectedid, editfoldername:edittxtFolderName},function(data){
					if(data=="1")
						{
						$("#loadingdiv").show();
						$.post(baseUrl+'index.php/companyadmin/deleteFolder',{selectedid:selectedid},function(data){
						if(data=="1")
						{
							$("#loadingdiv").show();
							//$('#loaderModal').trigger('reveal:close');
							$('#editFolderModal').trigger('reveal:close');
							$.post(baseUrl+'index.php/companyadmin/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
							$("#tablewidget").html('');
							$("#tablewidget").html(data);
						  });
						 }	
					   }); 
					 }else{
							//$('#loaderModal').trigger('reveal:close');
							$("#editerrmsg").html(data);
						}
					});
				}	
		 }
	}
</script>