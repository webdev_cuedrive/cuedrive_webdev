<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
$raw_data = Contentmanagementmodel::fetcharticle($id);	
       
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     <?php echo $raw_data->articletitle; ?>
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/helpdocs"><i class="fa fa-mobile"></i> Document</a></li>
                        <li class="active">View Document</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                       
           </div>
		<div id="tablewidget" class="block-body collapse in">
                <?php if(count($raw_data) > 0) 
					  { 
					  	echo base64_decode($raw_data->articles);
				      }  else {
					  
					   	echo "<center><h2>No Document Found</h2></center>";
				      }
				?>
		 </div>
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#supportli").attr('class','treeview active');
$("#supporticon").attr('class','fa fa-angle-down pull-right');
$("#helpdocli").attr('class','active');
$("#supportul").attr('style','display:block');
	new FormValidator('addhelpdoc', [{
	    name: 'article',
	    display: 'Document',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		       
	    } 
	   
	});
</script>		 