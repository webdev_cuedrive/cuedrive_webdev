<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
$this->load->helper('form');
	$att = array('name' => 'addhelpdoc','id' => 'addhelpdoc');
	echo form_open('companyadmin/addhelpdoc', $att);
        $comments = array ("name" => "article","id" => "article","class" => "form-control");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-left ");
	
	
	
	
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Article
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/companyadmin/helpdocs"><i class="fa fa-mobile"></i> Article</a></li>
                        <li class="active">New Article</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Add New Article
                                  </h2>                            
                        </div><!-- /.col -->
           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

			<label>Document<span class="text-danger">*</span></label>
			<?php echo form_textarea($comments);?><br>
			<input type="hidden" name="companyid" value="<?php echo $this->session->userdata('companyid')?>">
			<input type="submit" class="btn btn-primary pull-right" value="Add">
        
        	        <?php echo form_reset($formreset, 'Reset');?> 
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#supportli").attr('class','treeview active');
$("#supporticon").attr('class','fa fa-angle-down pull-right');
$("#helpdocli").attr('class','active');
$("#supportul").attr('style','display:block');
	new FormValidator('addhelpdoc', [{
	    name: 'article',
	    display: 'Document',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
	    } 
	   
	});
</script>	 