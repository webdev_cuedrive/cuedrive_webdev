<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("companyid")) 
	{
		header ("Location:".base_url()."index.php/login");
		exit;
	}

	$this->load->helper('form');
	$att = array('name' => 'corporateform','id' => 'corporateform');
	echo form_open('companyadmin/corporaterequest', $att);

	$abn = array ("name" => "abn","id" => "abn","class" => "form-control", "value" => $userdetails[0]['abn']);
	
	$email = array ("name" => "email","id" => "email","class" => "form-control", "value" => $userdetails[0]['email']);
	
	$acn = array ("name" => "acn","id" => "acn","class" => "form-control", "value" => $userdetails[0]['acn'], "onkeypress" => "vaditationACN();");
	
	$comments = array ("name" => "comments","id" => "comments","class" => "form-control");	
	
	$username = array ("name" => "username","id" => "username","class" => "form-control", "value" => $userdetails[0]['username']);
	
	$phone = array ("name" => "phone","id" => "phone","class" => "form-control", "value" => $userdetails[0]['phone']);

	//$price = array ("name" => "price","id" => "price","class" => "form-control");
	//$storage = array ("name" => "storage","id" => "storage","class" => "form-control");
	//$priceperdevice = array ("name" => "priceperdevice","id" => "priceperdevice","class" => "form-control");
	//$maxdevices = array ("name" => "maxdevices","id" => "maxdevices","class" => "form-control");	
	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-left ");
?>
<aside class="right-side"> 
    
<?php if(isset($errorMsg)){?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
<?php } ?> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Upgrade To Corporate Account
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-mobile"></i> Corporate Application</a></li>
                        <li class="active"> Application</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Application
                                  </h2>                            
                        </div><!-- /.col -->
           </div>
		
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
		 
		 	<label>User Name <span class="text-danger">*</span></label>	
			<?php echo form_input($username);?> <br/>
			
			<label>Phone <span class="text-danger">*</span></label>	
			<?php echo form_input($phone);?> <br/>


			<label>Email <span class="text-danger">*</span></label>	
			<?php echo form_input($email);?> <br/>

			<label>ABN</label>
			<?php echo form_input($abn);?> <br/>
			
			<label>ACN </label>
			<?php echo form_input($acn);?> <br/>
			
                       
			<!-- <label>Package Price &nbsp;(AUD)<span class="text-danger">*</span></label> -->
			<input type="hidden" name = "price" id = "price" value = "0">
 			
 			
 			<label>Storage Space Required <span class="text-danger">*</span></label>
			<select name="storage" id="storage" class="form-control">
			<?php for($i=5;$i<=9;$i++) { ?>
			<option value="<?php echo $i*10?>"><?php echo $i*10?></option>
			<?php } ?>
			<?php for($i=10;$i<=100;$i+=10) { ?>
			<option value="<?php echo $i*10?>"><?php echo $i*10?></option>
			<?php } ?>
			
			<br/>
			<!-- <label>Price Per Device &nbsp;(AUD) <span class="text-danger">*</span></label> -->
			<input type="hidden" name="priceperdevice" id="priceperdevice" value="0"> <br/>
			
			<label>Maximum Devices Required <span class="text-danger">*</span></label>
			<select name="maxdevices" id="maxdevices" class="form-control">
			<option value="45">45</option>
			<?php for($i=5;$i<=9;$i++) { ?>
			<option value="<?php echo $i*10?>"><?php echo $i*10?></option>
			<?php } ?>
			<?php for($i=10;$i<=100;$i+=10) { ?>
			<option value="<?php echo $i*10?>"><?php echo $i*10?></option>
			<?php } ?>
			<br/>
			<input type="hidden" name="emailsupport" value="yes"> <br/>
			
			<label>Notes</label>
			<?php echo form_textarea($comments);?><br>
			
			
			
			
			<input type="hidden" name="companyid" value="<?php echo $this->session->userdata('companyid')?>">
			
			<input type="submit" id="corporateSubmit" class="btn btn-primary pull-left" value="Apply now"> 
			
        	        
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#accountli").attr('class','treeview active');
$("#accountprofileicon").attr('class','fa fa-angle-down pull-right');
$("#corporateli").attr('class','active');
$("#accountul").attr('style','display:block');

	var Validator = new FormValidator('corporateform', [
	{
	    name: 'username',
	    display: 'User Name',    
	    rules: 'required'
	},
	
	{
	    name: 'phone',
	    display: 'Phone Number',    
	    rules: 'required|numeric'
	},
	
	{
	    name: 'email',
	    display: 'Email address',    
	    rules: 'required|valid_email'
	},
	
	{
	    name: 'abn',
	    display: 'ABN',    
	    rules: 'numeric|callback_abn'
	},
	
	{
	    name: 'storage',
	    display: 'storage space',    
	    rules: 'required'
	},
	{
	    name: 'priceperdevice',
	    display: 'price per device',    
	    rules: 'required|numeric'
	},
	{
	    name: 'maxdevices',
	    display: 'maximum devices',    
	    rules: 'required|numeric'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++)
			 {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	         }
	    } 
	   
	});
	
	Validator.setMessage('required', 'Please enter %s');
	
     Validator.registerCallback('abn', function(abn) {   
		var a, w, t;
		w = [0,0,3,5,7,9,11,13,15,17,19];
		a = abn.split('');
		t = ''+a[0]+a[1];
		w.forEach(function(v,i) {a[i] *= v;});
		a = a.reduce(function(x,y) {return x+y;});
		return (t == 99-(a%89));
	
	})
	.setMessage('abn', 'Please enter valid ABN');

	 function vaditationACN()
      { 
	    var acn=$('#acn').val();
		var isTrue=isAcCN(acn);
		if(!isTrue)
		{
			$('.error_box').html('Please enter valid ACN');
			return false;
		}
	  }

/*     function vaditation()
      {
        var abn=$('#abn').val();
		var isTrue=validateABN(abn);
		if(!isTrue)
		{
			$('.error_box').html('Please enter a valid ABN');
			 return false;
		}
        
        var acn=$('#acn').val();
		var isTrue=isAcCN(acn);
		if(!isTrue)
		{
			$('.error_box').html('Please enter a valid ACN');
			return false;
		}
     }*/

	
	
	function isAcCN( acn ) {

	      var weights = [8, 7, 6, 5, 4, 3, 2, 1, 0];
	      var sum, x, remainder, complement;
	      // Strip non-numbers from the acn
	      acn = acn.replace(/[^\d]/g, '');
	      //if (!acn.length)  return true;  //--- nothing entered so just return
	      // Check acn is 9 chars long
	      if( acn.length != 9) {
	          return false;
	      }
	     // Sum the products
	      sum = 0;
	      for(x=0; x<acn.length; x++ ) {
	        sum += acn[x] * weights[x];
	      }
	      // Get the remainder
	      remainder = sum % 10;
	      // Get remainder compliment
	      complement = "" + (10 - remainder);
	      // If complement is 10, set to 0
	      if( complement === "10") {
	          complement = "0";
	      }
	      return ( acn[8] === complement );
		  
	}
	
$('#corporateSubmit').click(function(){
	
	var email = $('#email').val();
	var username = $('#username').val();
	var phone = $('#phone').val();
	var abn = $('#abn').val();
	var acn = $('#acn').val();
	
	if(abn == "" && acn == "" && email != "" && username != "" && phone != "")
	{
		$('.error_box').html('Please enter ABN or ACN');
		return false;
	}
	if(acn != "")
	{
		return(vaditationACN());
	}
});	
</script>