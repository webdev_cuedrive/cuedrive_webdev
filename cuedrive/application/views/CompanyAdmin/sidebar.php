<div class="wrapper row-offcanvas row-offcanvas-left">
<aside class="left-side sidebar-offcanvas" style="min-height: 315px;margin-top:10px;">
<section class="sidebar">
<ul class="sidebar-menu">

	<li class="treeview" id="folderli">
	<a href="#">
	<i class="fa fa-folder"></i>
	<span>Folder</span>
	<i class="fa fa-angle-right pull-right" id="foldericon"></i>
	</a>
		<ul class="treeview-menu" id="folderul">
			<li id="foldersubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/folders" style="margin-left: 10px;">
			<i class="fa fa-folder-open"></i>Manage Folder
			</a>
			
			</li>
		</ul>
	</li>
	
	<li class="treeview" id="deviceli">
	<a href="#">
	<i class="fa fa-mobile"></i>
	<span>Device</span>
	<i class="fa fa-angle-right pull-right" id="deviceicon"></i>
	</a>
		<ul class="treeview-menu" id="deviceul">
			<li id="devicesubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/manageDevices" style="margin-left: 10px;">
			<i class="fa fa-tablet"></i>Manage Device
			</a>
			</li>
		</ul>
	</li>
	
	
	<li class="treeview" id="userli">
	<a href="#">
	<i class="fa fa-user"></i>
	<span>User</span>
	<i class="fa fa-angle-right pull-right" id="usericon"></i>
	</a>
		<ul class="treeview-menu" id="userul">
			<li id="usersubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/manageUsers" style="margin-left: 10px;">
			<i class="fa fa-male"></i>Manage User
			</a>			
			</li>
		</ul>
	</li>
	
	
	
	<li class="treeview" id="logli">
	<a href="#">
	<i class="fa fa-book"></i>
	<span>Auditlog</span>
	<i class="fa fa-angle-right pull-right" id="auditlogicon"></i>
	</a>
		<ul class="treeview-menu" id="logul">
			<li id="logsubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/auditloglist" style="margin-left: 10px;">
			<i class="fa fa-stack-exchange"></i>Manage Log
			</a>			
			</li>
			
		</ul>
	</li>
	<?php if(empty($check_admin_user)){ ?>
	<li class="treeview" id="accountli">
	<a href='#' data-content='Click here to upgrade your account' id='tooltip' data-placement='bottom' data-container='body'>
	<i class="fa fa-credit-card"></i>
	<span>Account Profile</span>
	<i class="fa fa-angle-right pull-right" id="accountprofileicon"></i>
	</a>
		<ul class="treeview-menu" id="accountul">
			<li id="billingli">
			<a href="<?php echo base_url()?>index.php/companyadmin/billinghistory" style="margin-left: 10px;">
			<i class="fa fa-money"></i>Billing
			</a>			
			</li>
			
			<li id="packageli">
			<a href="<?php echo base_url()?>index.php/companyadmin/upgradePacakge" style="margin-left: 10px;">
			<i class="fa fa-level-up"></i>Manage Subscription
			</a>			
			</li>
			
			<li id="accountdetailsli">
			<a href="<?php echo base_url()?>index.php/companyadmin/updateDetails" style="margin-left: 10px;">
			<i class="fa fa-envelope-o"></i>Account Details
			</a>			
			</li>
			
			<li id="passwordsubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/updatepassword" style="margin-left: 10px;">
			<i class="fa fa-edit"></i>Update Profile
			</a>			
			</li>
			
			
			
		</ul>
	</li>
	
	<li class="treeview" id="supportli">
	<a href="#">
	<i class="fa fa-headphones"></i>
	<span>Support</span>
	<i class="fa fa-angle-right pull-right" id="supporticon"></i>
	</a>
	    
		<ul class="treeview-menu" id="supportul">
			<li id="ticketli" height="25px;">
			<a href="<?php echo base_url()?>index.php/companyadmin/tickets" style="margin-left: 10px;">
			<i class="fa fa-ticket"></i>Tickets
			</a>			
			</li>
			
			<li id="feedbackli">
			<a href="<?php echo base_url()?>index.php/companyadmin/feedbacks" style="margin-left: 10px;">
			<i class="fa fa-envelope-o"></i>Feedback
			</a>			
			</li>
			
			<li id="helpdocli">
			<a href="<?php echo base_url()?>index.php/companyadmin/helpdocs" style="margin-left: 10px;">
			<i class="fa fa fa-edit"></i>Help Documents
			</a>			
			</li>
			<!--
			<li>
			<a href="<?php echo base_url()?>index.php/companyadmin/feedbacks" style="margin-left: 10px;">
			<i class="fa fa-angle-double-right"></i>Terms & Conditions
			</a>			
			</li>
			
				<li>
			<a href="<?php echo base_url()?>index.php/companyadmin/feedbacks" style="margin-left: 10px;">
			<i class="fa fa-angle-double-right"></i>Privacy Policy
			</a>			
			</li>
			-->
		
		</ul>
	
	</li>
	
	<?php } else{?>
    
    <li class="treeview" id="accountli">
	<a href='#' data-content='Click here to upgrade your account' id='tooltip' data-placement='bottom' data-container='body'>
	<i class="fa fa-credit-card"></i>
	<span>Account Profile</span>
	<i class="fa fa-angle-right pull-right" id="accountprofileicon"></i>
	</a>
		<ul class="treeview-menu" id="accountul">	
			
			
			<li id="passwordsubli">
			<a href="<?php echo base_url()?>index.php/companyadmin/updatepasswordAu" style="margin-left: 10px;">
			<i class="fa fa-edit"></i>Update Profile
			</a>			
			</li>
			
			
			
		</ul>
	</li>
    
    
    <?php } ?>
	

</ul>
</section>
</aside>

    
		    <!--start - css and js files for slider window-->
		<link href="<?php echo base_url()?>jscript/mbExtruder.css" media="all" rel="stylesheet" type="text/css">
		<!--<script type="text/javascript" src="jquery.js"></script>-->
		   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>jscript/jquery.hoverIntent.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>jscript/jquery.mb.flipText.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>jscript/mbExtruder.js"></script>
		<!--end - css and js files for slider window-->
		  <script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>  
		   <script src='<?php echo base_url()?>chat1/ui.tabs.closable.min.js'></script>


 <style>
    
.loading {
  display: inline-block;
  margin: 5em;
  border-width: 30px;
  border-radius: 50%;
  -webkit-animation: spin 1s linear infinite;
     -moz-animation: spin 1s linear infinite;
       -o-animation: spin 1s linear infinite;
          animation: spin 1s linear infinite;
  }

.style-1 {
  
  }

.style-2 {
  
  }

.style-3 {
  border-style: double;
  border-color: #444 #fff #fff;
  }

@-webkit-keyframes spin {
  100% { -webkit-transform: rotate(359deg); }
  }

@-moz-keyframes spin {
  100% { -moz-transform: rotate(359deg); }
  }

@-o-keyframes spin {
  100% { -moz-transform: rotate(359deg); }
  }

@keyframes spin {
  100% {  transform: rotate(359deg); }
  }
 </style>

    
		    <div id="extruderRight" class="{title:'Chat'}">
		    
		      <center id="load">
		        

				<span class="loading style-1"></span>
				
				<span class="loading style-2"></span>
				
				<span class="loading style-3"></span>


                      </center>
			       <div id='roster-area' class='ui-menu'>
                        <ul></ul>
                   </div>
				
			</div>	
    
    
    
		     <div id="chatModal" class="reveal-modal">
 
			    <div id='chat-area'>
			          <ul>
			          </ul>
			    </div>        
							
			
			
			 <div style="margin-top: 10px">
			   
			   <a class="close-reveal-modal">&#215;</a>
			 </div>	
		      </div>	
		      
		      
		      
		      
		 
		      
		      
<?php		      
	
       $jidpassword = "";
       $jid = "";
       $username1 = $this->session->userdata("username");
       $service_url = ORG_URL."chat/?type=fetch&username=".$username1;
       $curl = curl_init($service_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	if ($curl_response === false) {
		$info = curl_getinfo($curl);
		curl_close($curl);
		die('error occured during curl exec. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
        $response = json_decode($curl_response);
        if(count($response) > 0) {
        $jidpassword = $response[0]->{'plainPassword'};
        $jid = $response[0]->{'username'}."@cuedrive.com.au";
        }



?>

 <input type = "hidden" id = "jid" value = "<?php echo $jid?>">
 <input type = "hidden" id = "jidpassword" value = "<?php echo $jidpassword?>">	      
		      
		      
  <!-- Chat libraries -->

   
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js"></script>-->
   <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.0/themes/smoothness/jquery-ui.css'>
   <!-- <link rel='stylesheet' href='https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'>-->
  <!-- <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
    <script src='<?php echo base_url()?>chat1/strophe.js'></script>
    <script src='<?php echo base_url()?>chat1/flXHR.js'></script>
    <script src='<?php echo base_url()?>chat1/strophe.flxhr.js'></script>
<?php /*?>    <script src='<?php echo base_url()?>chat1/ui.tabs.closable.min.js'></script><?php */?>

    <link rel='stylesheet' href='<?php echo base_url()?>chat1/gab.css'>
    <script src='<?php echo base_url()?>chat1/gab.js'></script>
    <!-- Chat libraries end -->		      
<script>		      
$(function() {
var height=(document.body.clientHeight-200)/2;

$("#extruderRight").buildMbExtruder({
      position:"right",
      width:300,
      top:height,
      extruderOpacity:.8,
      textOrientation:"tb",
      onExtOpen:function(){},
      onExtContentLoad:function(){},
      onExtClose:function(){}
    });
   
    $("#extruderRight").find(".flapLabel").css({"right":"10px"});
    $("#extruderRight").find(".flip_label").css({"padding":"10px 0px 10px 0px","height":"100px","top":"10px"});
  
});
</script>     