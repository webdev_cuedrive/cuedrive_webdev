<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}
	$baseredirecturl=base_url()."index.php/siteowner/tickets";
?>

<?php


?>


<aside class="right-side">  


<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Ticket Conversation
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/tickets"><i class="fa fa-mobile"></i> Tickets</a></li>
                        <li class="active">Manage Tickets</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');
	if(count($results[0])>0) {
	foreach($results as $t_data):
		$ticketid = $t_data->ticketid;
		$companyid = $t_data->companyid;
		$priority = $t_data->priority;
	endforeach;
	
	if(true) //add device
	{
		$att = array('name' => 'replyticket','id' => 'replyticket');
		echo form_open('siteowner/replyTicket', $att);		
		$addfeedbackbutton=array("name" => "replyticket","id" => "replyticket","class" => "btn btn-primary");
		echo form_hidden('ticketid',$ticketid);
		echo form_hidden('companyid',$companyid);
		echo form_hidden('priority',$priority);
		echo form_submit($addfeedbackbutton, 'Reply');
		echo form_close();
	}
	}
	?>
	
	 </div>
	<div class="col-xs-6">
		
              
              <div class="input-group">
                           
              </div>
            
	         </div>
	         </div>
	
			 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Conversation
                                  </h2>                            
                        </div><!-- /.col -->
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){ $i=0;?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table">
							<thead>
								<tr>
									
									<th>Subject</th>
									<th>Time</th>
									<th>Status</th>
									
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							
							  if($t_data->sent == 1) { 
							  $status = "user received";
							  $cssClass = "alert alert-warning";
							  }
							  
							  else {
							  $status = "cuedrive team sent";
							  $cssClass = "alert alert-success";
							  }
							
							?>
							<tr id='RecRow<?php echo $t_data->id;?>' class = '<?php echo $cssClass?>'>
							
							<td><?php echo $t_data->comments;?></td>
							<td width="15%"><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
							<td width="10%"><?php echo $status;?></td>
							
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Conversation to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {

        
	   $("#supportli").attr('class','treeview active');
	   $("#ticketli").attr('class','active');
	   $("#supportul").attr('style','display:block');
	    $("#tickets").attr('class','fa fa-angle-down pull-right');   
	   });
</script>