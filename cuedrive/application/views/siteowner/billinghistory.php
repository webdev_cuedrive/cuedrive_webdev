<?php
	include("header.php");
	include("sidebar.php");
	if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/archiveinvoices";
	//echo json_encode($results);
?>
<script src="<?php echo base_url()?>theme/js/jquery.magnific-popup.js" type="text/javascript"></script>	
	<link rel="stylesheet" href="<?php echo base_url(); ?>theme/css/magnific-popup.css">
<aside class="right-side">  

<?php if(isset($errorMsg)){ ?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php } ?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Archive Invoices
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/archiveinvoices"><i class="fa fa-mobile"></i> Billing</a></li>
                        <li class="active">Manage Invoices</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">  </div>
	<div class="col-xs-6" style="text-align:right; color:#189cd9; padding-right:50px">
              <div class="input-group"></div>
		            <b>You have a credit of $<?php echo $creditamount; ?></b>
	         </div>
	         </div>
	
			 <div class="row">
                        
           </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Invoice Number</th>	
									<th>Transaction Amount</th>
									<!--<th>Invoice Amount</th>-->
									<th>Transaction ID</th>
									<th>Transaction Date</th>
									<th>Status</th>
									<th><center>Invoice</center></th>
									<!--<th><center>Payment</center></th>-->
									
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
								//echo '<pre>'; print_r($t_data); echo '</pre>';
			//$DeleteLink = '<a href="javascript:void(0)" onclick="deleteticket('.$t_data->id.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							$inv = '<a target="_blank" href="'.base_url().'index.php/payment/downloadInvoice?invoiceid='.$t_data->invoiceid.'" ><i class="fa fa-search"></a>';
							
							$payment = '<a target="_blank" href="'.base_url().'index.php/payment/downloadInvoice?invoiceid='.$t_data->customerToken.'" ><i class="fa fa-search"></a>';
							
							if($t_data->comment == 'device') {	
								$amounttopay = number_format($t_data->totalamounttopay,2);
							}	else	{	
								$amounttopay = number_format($t_data->totalamount,2);
							}	
							
							if($t_data->ispaid == 0)
							{
							  //$status = '<a href="javascript:openPayment()">Pay Now</a>';
							  $status = '<a href="'.base_url('index.php/siteowner/invoicepayment/'.$t_data->customerToken.'/'.$amounttopay.'/'.$t_data->invoicenumber.'/'.$t_data->id).'" class="paymentdone">Pay Now</a>';
							  $classDyn = 'alert-danger';
							}
							else
							{
							 $status = 'Paid';
							 $classDyn = '';
							}
						
							?>
							<tr id='RecRow<?php echo $t_data->id;?>' class="<?php echo $classDyn; ?>">
							<td><?php echo $t_data->invoicenumber;?></td>
							<td>
							<?php	if($t_data->comment == 'device') {	?>
								<?php echo CURRENCY.number_format($t_data->totalamounttopay,2);?>
							<?php	}	else	{	?>
								<?php echo CURRENCY.number_format($t_data->totalamount,2);?>
							<?php	}	?>
							</td>
							<!--<td><?php //echo CURRENCY.number_format($t_data->invoiceamount,2);?></td>-->
							<td><?php echo $t_data->transactionid;?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->transactiondate));?></td>
							<td><?php echo $status;?></td>
							<td><center><?php echo $inv;?></center></td>
							<!--<td><center><?php //echo $payment;?></center></td>-->
							
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No billing to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>




	  <div id="payment" class="reveal-modal" style="background:url("modal-gloss.png") no-repeat scroll -200px -80px white">		
	  <div id="fadeandscale" class="reveal-modal"> 
                  <div id="paybutton" class="row">
                   
                   <div class="modal-header">    
                        <h4 class="modal-title"><i class="fa fa-credit-card"></i> Credit Card Information</h4>
                   </div>
                        
		   <div id="tablewidget" class="block-body collapse in">
		   <form action="<?php echo base_url()?>index.php/payment/corporateDirectPayment" method="post" id="directpaymentform" name="directpaymentform">
		   <label id="errormessage" style="color:red;">**unfortunately Diners club cards and Amex cards are not accepted throughout this payment</label> <br/><br/>
	
	            <div class="error_box error-txt"> </div>
		 	
		 	<label>Card Number <span class="text-danger">*</span> </label>
         	        <input type="text" placeholder="Creditcard number" class="form-control" name="cardnumberp" id="cardnumberp" required>
         	        
         	        <input type="hidden" name="cardtype" id="cardtype">   
                        <br/>
         	        <label>Name On Card <span class="text-danger">*</span> </label>
         	        
         	        <input type="text" placeholder="Name on card" class="form-control" name="nameoncardp" id="nameoncardp" required>  
         	                 
         	        <br/>
         	 <div id="creditfloatleftdata2">
         	        <label>Expiry Month</label>
         	        
         	        
         	         <select id="EWAY_CARDEXPIRYMONTHp" name="EWAY_CARDEXPIRYMONTHp" class="form-control">
                          <?php
                           $expiry_month = date('m');
                            for($i = 1; $i <= 12; $i++) {
                            $s = sprintf('%02d', $i);
                            echo "<option value='$s'";
                            if ( $expiry_month == $i ) {
                                echo " selected='selected'";
                            }
                             echo ">$s</option>\n";
                            }
                           ?>
                         </select> <br/>
                         <label>Expiry Year</label>
                         <select id="EWAY_CARDEXPIRYYEARp" name="EWAY_CARDEXPIRYYEARp" class="form-control">
                          <?php
                            $i = date("y");
                            $j = $i+11;
                            for ($i; $i <= $j; $i++) {
                            echo "<option value='$i'>$i</option>\n";
                           }
                           ?>
                         </select>            
							</div>
							<div id="creditfloatrightdata1">
								 	<?php	/*  Begin eWAY Linking Code */	?>
										<div id="eWAYBlock">
												<div style="text-align:center;">
														<a href="https://www.eway.com.au/secure-site-seal?i=11&s=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
																<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" />
														</a>
												</div>
										</div>
									<?php	/* End eWAY Linking Code */	?>
								 </div>  
								 <div class="clearboth"></div>
												 
         	        <br/>
         	        
         	        <label>CVN <span class="text-danger">*</span> </label>
         	        
         	        <input type="text" class="form-control" name="cvnp" id="cvnp" data-inputmask='"mask": "999"' data-mask required>  
         	                 
         	        <br/>
        	
                       <!-- <a class="pull-right" href="javascript:paypalSubmit()"><img src="<?php echo base_url();?>/paypal.png" alt="paypal" /></a> -->

                       <!-- <a class="pull-left" href="javascript:ewaySubmit()"><img src="<?php echo base_url();?>/eway.gif" alt="eWAY" height="32px;" width="145px;"/></a> -->
                        <input class="btn btn-primary pull-right" type="submit" value = "Pay with Paypal" name = "button" onclick="return checkcard()"/>
                        <input class="btn btn-primary pull-left" type="submit"  value="Pay with eWAY"  name = "button" onclick="return checkcard()"/>
                    </form>
                    </div>  
                  </div>  
       <div style="margin-top: 10px">
        <a class="close-reveal-modal">&#215;</a>
       </div>
   </div>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>	 
<script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>	
<script src="<?php echo base_url()?>theme/js/jquery.inputmask.js" type="text/javascript"></script>
<script>
$(function() {

        
	   $("#accountli").attr('class','treeview active');
	   $("#accountprofileicon").attr('class','fa fa-angle-down pull-right');
	   $("#billingli").attr('class','active');
	   $("#accountul").attr('style','display:block');
	   
	   $('.paymentdone').magnificPopup({
			type: 'iframe',			
			mainClass: 'mfp-fade',
			removalDelay: 320,
			preloader: true,
			iframe: {
				markup: '<div style="width:500px; height:150px;">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
			}
		});	
		
	   });
	   
	   
 function openPayment()
 {
  $("#payment").reveal();
 }	   
	$(document).ready(function() 
	{
	    $("[data-mask]").inputmask(); 
	});

	$('#cardnumberp').change(function(){
	    cc_number_saved = $('#cardnumberp').val();
	    cc_number_saved = cc_number_saved.replace(/[^\d]/g, ''); 
	    if(!checkLuhn(cc_number_saved)) 
	    { 
	      alert('Sorry, that is not a valid number - please try again');
	      //$('#cardnumberp').val('');
	    }  
	});
	
	
	function checkLuhn(input) { 
	 var sum = 0;
	 var numdigits = input.length;
	 var parity = numdigits % 2; 
	 for(var i=0; i < numdigits; i++) { 
	   var digit = parseInt(input.charAt(i));
	   if(i % 2 == parity) digit *= 2; 
	   if(digit > 9) digit -= 9; 
	   sum += digit; 
	  } 
	  return (sum % 10) == 0; 
	}	 

</script>