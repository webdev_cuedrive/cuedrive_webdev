<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/pendingApprovals";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Help Documents
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/pendingApprovals"><i class="fa fa-mobile"></i>Help Documents</a></li>
                        <li class="active">Manage Documents</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div class="row">
	<div class="col-xs-4">
	
	<?php 
	$this->load->helper('form');
		$att = array('name' => 'addNewarticle','id' => 'addNewarticle');
		echo form_open('siteowner/addarticles', $att);		
		$adddevicebutton=array("name" => "addpackage","id" => "addpackage","class" => "btn btn-primary");
		echo form_submit($adddevicebutton, 'Add');
		echo form_close();
	
	?>
    Please use drug drop to sort help boxes
	
	 </div>
     <div class="col-xs-6"></div>
 </div>
	          <div class="row">
                        <div class="col-xs-12">
                            <div id="succMsg" style="display:none" class="alert alert-success">Updating sort order...</div>                        
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){
					$i=0;
						?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover tbl_repeat">
							<thead>
								<tr>
									<th>Document</th>
                                    <th>Category</th>
									<th>Created On</th>
									<th>Actions</th>
								</tr>								
                              
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							$i++;
							$EditLink = '<a href="'.base_url().'/index.php/siteowner/articles?type=article&id='.$t_data->contentid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
			                $DeleteLink = '<a href="javascript:void(0)" onclick="deleteArticle('.$t_data->contentid.', \''.$t_data->articletitle.'\')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							?>
							<tr id='RecRow_<?php echo $t_data->contentid;?>'>
                            
							<td><?php echo $t_data->articletitle;?></td>
                            <td><?=($t_data->category_id == 1)?"Android":($t_data->category_id == 2?"iOS":($t_data->category_id == 3?"Web":"All"));?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->date));?></td>
						    <td class='span1'><?php echo $EditLink?><?php echo $DeleteLink?></td>
							</tr>
							<?php 
						
							endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Article to display</h3></center>
							<?php }?>
					</div>
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>theme/js/jquery.tablednd_0_5.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
	 $("#contentli").attr('class','treeview active');
	   $("#helpli").attr('class','active');
	   $("#contentul").attr('style','display:block');
	   $("#articlelist").attr('class','fa fa-angle-down pull-right'); 
});


$(document).ready(function() {
	$(".tbl_repeat tbody").tableDnD({
		onDrop: function(table, row) {
			var orders = $.tableDnD.serialize();
			$("#succMsg").fadeIn();			
			$.post('<?php echo base_url()."/index.php/siteowner/orderarticallist";?>', { orders : orders })
			.success(function(){
				$("#succMsg").html("Sort order updated!");
			  $("#succMsg").fadeOut(2000);
			})
			;
		}
	});

});



</script>