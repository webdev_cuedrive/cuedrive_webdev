<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/companylist";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Archive Account List
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>index.php/siteowner/archivelist"><i class="fa fa-mobile"></i>Archive List</a></li>
                        <li class="active">Manage Archive Account</li>
                       
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	 </div>
	 
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url();?>index.php/siteowner/archivelist") ;
             }
             }); 
	     });
     </script>
    
    
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url();?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th width="10%">Number</th>
									<th>Name</th>
									<th>Email</th>
									<th>Company</th>
									<th>Membership</th>
									<th>Devices</th>
									
									<th>Membership Start</th>
									<th>Membership Expire</th>
									
									<!--<th>Actions</th>-->
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
                    $EditLink = '<a href="'.base_url().'/index.php/siteowner/modifycompanyadmin?id='.$t_data->companyid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
			        $DeleteLink = '<a href="javascript:void(0)" onclick="deleteCompany('.$t_data->companyid.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Deactivate"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				    if($t_data->isactive == 'no')
				    {
					  $BannedLink = '<a href="javascript:void(0)" onclick="bannedCompany('.$t_data->companyid.','.$t_data->companyid.',1)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Remove Ban"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				    }
				   else
				   {
					  $BannedLink = '<a href="javascript:void(0)" onclick="bannedCompany('.$t_data->companyid.','.$t_data->companyid.',0)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Ban Company"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				   }
							
							$countDevice = Companydevicemodel::fetchDeviceCount($t_data->companyid);
							
							$today = strtotime(date('Y-m-d H:i:s'));
							$expdate = strtotime($t_data->expirydate);
							$diff =  $today - $expdate;
							
							
							$cssColor = ($t_data->isactive == 'no') || ($diff > 0) ? "class='alert alert-danger alert-dismissable'" : "";
							$packagedetails = Packagemodel::getDetails($t_data->packageid);
							$packagename = $packagedetails['type'];
							?>

							<tr id='RecRow<?php echo $t_data->companyid;?>'>
							<td><?php echo $t_data->account_number;?></td>
							<td><?php echo $t_data->username;?></td>
							<td><?php echo $t_data->email;?></td>
							<td><?php echo $t_data->name;?></td>
							<td><?php echo $packagename;?></td>
							<td><?php echo $t_data->noofdevicesallowed;?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->expirydate));?></td>
							
						   <!-- <td class='span1'><?php //echo $EditLink?><?php //echo $BannedLink?></td>-->
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Archive List to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>	
<script>
$(function() {

	 $("#companyli").attr('class','treeview active');
	   $("#companysubli").attr('class','active');
	   $("#companyul").attr('style','display:block');
	   $("#companylist").attr('class','fa fa-angle-down pull-right');
	 
});


</script>