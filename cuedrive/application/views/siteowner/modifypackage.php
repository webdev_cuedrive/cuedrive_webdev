<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}


	$packageid = $id;
	$raw_data =  Packagemodel::getPackages($packageid);
	$raw_data = $raw_data [0];
	
	$this->load->helper('form');	
	$att = array('name' => 'editPackage','id' => 'editPackage', 'method'=> 'post', 'onsubmit'=>"return validateForm();");
	print form_open_multipart('siteowner/updatepackage', $att);
	$price = array ("name" => "price","id" => "price","class" => "form-control","value"=>$raw_data->price);
	//$forduration = array ("name" => "forduration","id" => "forduration","class" => "span12");	
	$type = array ("name" => "type","id" => "type","class" => "form-control","value"=>$raw_data->type);
	$storagespace = array ("name" => "storagespace","id" => "storagespace","class"=>"form-control","value"=>$raw_data->storagespace);
	$additionaldeviceprice = array ("name" => "additionaldeviceprice","id" => "additionaldeviceprice","class" => "form-control","value"=>$raw_data->additionaldeviceprice);				
	$noofdevice = array ("name" => "noofdevices","id" => "noofdevices","class" => "form-control","value"=>$raw_data->noofdevices);				
	echo form_hidden('packageid',$raw_data->packageid);
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update Package
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/businesspackagelist"><i class="fa fa-mobile"></i> Package List</a></li>
                        <li class="active">Manage Package</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

                        
                        <label>Package Name</label>					
			<?php echo form_input($type);?>
		 	<input type="hidden" name="accounttype" value="business">
			<label>Monthly Fee</label>					
			<?php echo form_input($price);?>
						
			<input type="hidden" name="forduration" value="1Month">		 
			
           
			<label>Storage</label>
			<?php echo form_input($storagespace);?>
			<label>Price per device</label>
			<?php echo form_input($additionaldeviceprice);?>
			<label>Maximum number of devices</label>
			<?php echo form_input($noofdevice);?>
			<label>Email Support</label>
			<select name = "emailsupport" class="form-control">
			<option value="yes" <?php if ('yes'==$raw_data->emailsupport) echo 'selected="selected"';?>>Yes</option>
			<option value="no" <?php if ('no'==$raw_data->emailsupport) echo 'selected="selected"';?>>No</option>
			</select><br>
			<input type="submit" class="btn btn-primary" value="Update">
		 
		 
         	
        	<!-- <?php echo form_reset($formreset, 'Reset');?> -->
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#packageli").attr('class','treeview active');
$("#businessli").attr('class','active');
$("#packageul").attr('style','display:block');
 $("#businesspackagelist").attr('class','fa fa-angle-down pull-right');
var Validator = new FormValidator('editPackage', [{
	    name: 'type',
	    display: 'Package Name',    
	    rules: 'required'
	},  {
	    name: 'price',
	    display: 'Monthly Fee', 
	    rules: 'required'
	},{
	    name: 'storagespace',
	    display: 'Storage Space',    
	    rules: 'required'
	},  {
	    name: 'additionaldeviceprice',
	    display: 'Price per Device', 
	    rules: 'required'
	},{
	    name: 'noofdevices',
	    display: 'Maximum number of Device',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		} 
});
Validator.setMessage('required', 'Please enter %s');	
</script>
		 