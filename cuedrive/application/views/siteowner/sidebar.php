<div class="wrapper row-offcanvas row-offcanvas-left">
<aside class="left-side sidebar-offcanvas" style="min-height: 315px;margin-top:10px;">
<section class="sidebar">
<ul class="sidebar-menu">

	<li class="treeview" id="statisticli">
	<a href="#">
	<i class="fa fa-tachometer"></i>
	<span>Statistics</span>
	<i class="fa fa-angle-down pull-right" id="dashboard"></i>
	</a>
		<ul class="treeview-menu" id="statisticul" style="display:block">
			<!--<li id="subli1">
			<a href="<?php echo base_url()?>index.php/siteowner/billing" style="margin-left: 10px;">
			<i class="fa fa-list"></i>This month's Billed
			</a>
			
			</li> -->
			<?php if($this->session->userdata("ismanager")==1) { ?>
			<!--<li id="subli2">
			<a href="<?php echo base_url()?>index.php/siteowner/dashboard" style="margin-left: 10px;">
			<i class="fa fa-shield"></i>Total Billing
			</a>
			
			</li>-->
			<?php } ?>
			<li id="subli3">
			<a href="<?php echo base_url()?>index.php/siteowner/dashboard" style="margin-left: 10px;">
			<i class="fa fa-bar-chart-o"></i>Account Statistics
			</a>
			
			</li>
		</ul>
	</li>
	
	<li class="treeview" id="companyli">
	<a href="#">
	<i class="fa fa-list-alt"></i>
	<span>Company List</span>
	<i class="fa fa-angle-right pull-right" id="companylist"></i>
	</a>
		<ul class="treeview-menu" id="companyul" style="display:none">
			<?php if($this->uri->segment(2) == 'companylist')	{	?>
				<li id="companysubli" class="active">
			<?php } else {	?>
				<li id="companysubli">
			<?php	}	?>
			<a href="<?php echo base_url()?>index.php/siteowner/companylist" style="margin-left: 10px;">
			<i class="fa fa-th-list"></i>Manage Companies
			</a>
			</li>
			
			<?php /* ?>
			<?php //echo $this->uri->segment(2); ?>
			<?php if($this->uri->segment(2) == 'archiveinvoices')	{	?>
				<li id="companysubli" class="active">
			<?php } else {	?>
				<li id="companysubli">
			<?php	}	?>
			<a href="<?php echo base_url()?>index.php/siteowner/archiveinvoices" style="margin-left: 10px;">
			<i class="fa fa-th-list"></i>Invoices Archive
			</a>
			</li>
			<?php */ ?>
			
			<?php if($this->uri->segment(2) == 'archiveaccounts')	{	?>
				<li id="companysubli" class="active">
			<?php } else {	?>
				<li id="companysubli">
			<?php	}	?>
			<a href="<?php echo base_url()?>index.php/siteowner/archiveaccounts" style="margin-left: 10px;">
			<i class="fa fa-th-list"></i>Account  Archive
			</a>
			</li>
			
		</ul>
	</li>
	
	<?php if($this->session->userdata("ismanager")==1) { ?>
	<li class="treeview" id="packageli">
	<a href="#">
	<i class="fa fa-money"></i>
	<span>Package Management</span>
	<i class="fa fa-angle-right pull-right" id="businesspackagelist"></i>
	</a>
		<ul class="treeview-menu" id="packageul" style="display:none">
			<li id="businessli">
			<a href="<?php echo base_url()?>index.php/siteowner/businesspackagelist" style="margin-left: 10px;">
			<i class="fa fa-dollar"></i>Business
			</a>			
			</li>
			
			<li id="corporateli">
			<a href="<?php echo base_url()?>index.php/siteowner/pendingApprovals" style="margin-left: 10px;">
			<i class="fa fa-retweet"></i>Pending approvals
			</a>			
			</li>
		</ul>
	</li>
	<?php } ?>
	<li class="treeview" id="contentli">
	<a href="#">
	<i class="fa fa-credit-card"></i>
	<span>Content Management</span>
	<i class="fa fa-angle-right pull-right" id="articlelist"></i>
	</a>
		<ul class="treeview-menu" id="contentul" style="display:none">
			<li id="helpli">
			<a href="<?php echo base_url()?>index.php/siteowner/articlelist" style="margin-left: 10px;">
			<i class="fa fa-edit"></i>Help Documents
			</a>			
			</li>
			
			<li id="privacyli">
			<a href="<?php echo base_url()?>index.php/siteowner/contentmanagement?type=privacy" style="margin-left: 10px;">
			<i class="fa fa-unlock-alt"></i>Privacy Policy
			</a>			
			</li>
			
			<li id="termsli">
			<a href="<?php echo base_url()?>index.php/siteowner/contentmanagement?type=terms" style="margin-left: 10px;">
			<i class="fa fa-bullhorn"></i>Terms & Conditions
			</a>			
			</li>
			
		</ul>
	</li>
	
	<li class="treeview" id="supportli">
	<a href="#">
	<i class="fa fa-headphones"></i>
	<span>Support</span>
	<i class="fa fa-angle-right pull-right" id="tickets"></i>
	</a>
		<ul class="treeview-menu" id="supportul" style="display:none">
			<li id="ticketli">
			<a href="<?php echo base_url()?>index.php/siteowner/tickets" style="margin-left: 10px;">
			<i class="fa fa-ticket"></i>Tickets
			</a>			
			</li>
			
			<li id="feedbackli">
			<a href="<?php echo base_url()?>index.php/siteowner/feedbacks" style="margin-left: 10px;">
			<i class="fa fa-envelope-o"></i>Feedback
			</a>			
			</li>
			
		</ul>
	</li>
	
	<?php if($this->session->userdata("masteradmin")==1) { ?>
	<li class="treeview" id="userli">
	<a href="#">
	<i class="fa fa-user"></i>
	<span>cuedrive team</span>
	<i class="fa fa-angle-right pull-right" id="userlist"></i>
	</a>
		<ul class="treeview-menu" id="userul" style="display:none">
			<li id="usersubli">
			<a href="<?php echo base_url()?>index.php/siteowner/userlist" style="margin-left: 10px;">
			<i class="fa fa-male"></i>Manage cuedrive team
			</a>			
			</li>
			
		</ul>
	</li>
	<?php } ?>
	<?php if($this->session->userdata("ismanager")==1) { ?>
	
		<!--<li class="treeview" id="companyli">
	<a href="#">
	<i class="fa fa-list-alt"></i>
	<span>Account archive</span>
	<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu" id="companyul" style="display:block">
			<li id="companysubli">
			<a href="<?php //echo base_url();?>index.php/siteowner/archivelist" style="margin-left: 10px;">
			<i class="fa fa-th-list"></i>Archive account list
			</a>
			</li>
		</ul>
	</li>-->
	
	<?php } ?>
</ul>
</section>
</aside>   