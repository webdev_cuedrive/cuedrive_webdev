<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

$raw_data = Contentmanagementmodel::getContent($type,0);
if($type == 'article')
{
	$content = base64_decode($raw_data->articles);
	$text = 'Article';
	
}
if($type == 'privacy')
{
	$content = base64_decode($raw_data->privacy);
	$text = 'Privacy Policy';
	$li = "privacyli";
}
if($type == 'terms')
{
	$content = base64_decode($raw_data->terms);
	$text = 'Terms & Conditions';
	$li = "termsli";
}

$this->load->helper('form');	
$att = array('name' => 'termsandpolicy','id' => 'termsandpolicy', 'method'=> 'post');
print form_open_multipart('siteowner/updatecontent', $att);
$page_content = array ("name" => "terms","id" => "terms","class" => "form-control","value"=>$content);			
echo form_hidden('contentid',$raw_data->contentid);
echo form_hidden('type',$type);			
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side">   


<br>
<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     


                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo $text?>
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-mobile"></i> Content Management</a></li>
                        <li class="active"><?php echo $text?></li>
                    </ol>
                </section>
                
                
                
<script src="<?php echo base_url()?>theme/tinymce/tinymce.min.js" type="text/javascript"></script>			
  
<script type="text/javascript">
	
    tinymce.init({
    selector: "textarea",
    plugins: [
			 "advlist autolink jbimages lists link  charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
    ],
	
   	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
});

</script>
	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

		 	<label><?php echo $text?></label>
					 <textarea name="terms" style="height: 350px;"><?php echo $content?></textarea> <br><br>
					<input type="submit" class="btn btn-primary" value="Update">
		 
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
var li = "<?php echo $li;?>";

$("#contentli").attr('class','treeview active');
$("#"+li).attr('class','active');
$("#contentul").attr('style','display:block');
 $("#articlelist").attr('class','fa fa-angle-down pull-right'); 
var Validator = new FormValidator('termsandpolicy', [{
	    name: 'terms',
	    display: 'Description',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
	    } 
	   
	});
Validator.setMessage('required', 'Please enter %s');
</script>	 