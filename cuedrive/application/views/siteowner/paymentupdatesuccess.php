<style>
.btn-primary {
    background-color: #428bca;
    border-color: #357ebd;
    color: #ffffff;
}
.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
</style>
<div style="margin: 0 auto; padding: 200px 0px;font-family: Helvetica;" class="row" id="">
	<center>Card is updated successfully.</center>
	<br/>
	<div style="text-align:center;" class="col-xs-4">			
		<input type="button" id="submit" name="submit" onclick="parent.$.magnificPopup.close();parent.window.location.reload();" value="Close" class="btn btn-primary signup"
		/>
	</div>	
</div>	
