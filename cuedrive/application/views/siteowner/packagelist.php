<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1)  
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/businesspackagelist";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Package
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/businesspackagelist"><i class="fa fa-mobile"></i>Package List</a></li>
                        <li class="active">Manage Package</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	$this->load->helper('form');
		$att = array('name' => 'addNewPackage','id' => 'addNewPackage');
		echo form_open('siteowner/addNewPackage', $att);		
		$adddevicebutton=array("name" => "addpackage","id" => "addpackage","class" => "btn btn-primary");
		echo form_submit($adddevicebutton, 'Add');
		echo form_close();
	
	?>
	
	 </div>
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/siteowner/businesspackagelist") ;
             }
             }); 
	     });
     </script>
    
    
    
	<div class="col-xs-6">
	<?php			
            $this->load->helper('form');
			
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'get', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('siteowner/businesspackagelist', $att);
                        $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
?>			
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Monthly Fee</th>
									<th>Storage Space</th>
									
									<th>Name</th>
									<th>No. of Devices</th>
									<th>Price per device</th>
									<th>Actions</th>
								
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
                               $EditLink = '<a href="'.base_url().'/index.php/siteowner/modifypackage?id='.$t_data->packageid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
			                   $DeleteLink = '<a href="javascript:void(0)" onclick="deletepackage('.$t_data->packageid.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							?>

							<tr id='RecRow<?php echo $t_data->packageid;?>'>
							<td><?php echo $t_data->price;?></td>
							<td><?php echo $t_data->storagespace;?></td>
							<td><?php echo $t_data->type;?></td>
							<td><?php echo $t_data->noofdevices;?></td>
						
							<td><?php echo $t_data->additionaldeviceprice;?></td>
						
						    <td class='span1'><?php echo $EditLink?><?php echo $DeleteLink?></td>
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Package to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
</div>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {

	 $("#packageli").attr('class','treeview active');
	   $("#businessli").attr('class','active');
	   $("#packageul").attr('style','display:block');
	   $("#businesspackagelist").attr('class','fa fa-angle-down pull-right');
});
</script>