<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/pendingApprovals";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Pending Approvals
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/pendingApprovals"><i class="fa fa-mobile"></i>Approval List</a></li>
                        <li class="active">Manage Approvals</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	 </div>
	 
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/siteowner/pendingApprovals") ;
             }
             }); 
	     });
     </script>
    
    
    
	<div class="col-xs-6">
	<?php			
            $this->load->helper('form');
			
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'get', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('siteowner/pendingApprovals', $att);
                        $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
?>			
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search by company name or email" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Company</th>
									<th>Membership</th>
									<th>Devices</th>
									
									<th>Membership Start</th>
									<th>Membership Expire</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							
							if($t_data->type == 'corporate'&& ($t_data->status == 'waiting' || $t_data->status == 'denied')) {
                             $EditLink = '<a href="'.base_url().'/index.php/siteowner/addcorporatepackage?id='.$t_data->companyid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
			                 $DeleteLink = '<a href="javascript:void(0)" onclick="deleteCompany('.$t_data->companyid.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				             if($t_data->status == 'waiting' || $t_data->status == 'denied')
				              {
					            $BannedLink = '<a href="javascript:void(0)" onclick="activeCompany('.$t_data->companyid.','.$t_data->companyid.',1)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Activate"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				              }
				             else
				              {
					            $BannedLink = '<a href="javascript:void(0)" onclick="activeCompany('.$t_data->companyid.','.$t_data->companyid.',0)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Deactivate"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				              }
							
							$countDevice = Companydevicemodel::fetchDeviceCount($t_data->companyid);
							$cssColor = ($t_data->status == 'waiting' || $t_data->status == 'denied') ? "class='alert alert-danger alert-dismissable'" : "";
							?>

							<tr <?php echo $cssColor;?> id='RecRow<?php echo $t_data->companyid;?>'>
							<td><?php echo $t_data->username;?></td>
							<td><?php echo $t_data->email;?></td>
							<td><?php echo $t_data->name;?></td>
							<td><?php echo $t_data->type;?></td>
							<td><?php echo $countDevice;?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->expirydate));?></td>
							<td><?php echo $t_data->status;?></td>
						    <td class='span1'><?php echo $EditLink?></td>
							</tr>
							<?php 
							}
							endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Pending Account to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {
	$("#packageli").attr('class','treeview active');
	$("#corporateli").attr('class','active');
	$("#packageul").attr('style','display:block');
	$("#businesspackagelist").attr('class','fa fa-angle-down pull-right');
});
</script>