<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/userlist";
?>



<aside class="right-side">     
<br/>
<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Admins
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/userlist"><i class="fa fa-mobile"></i>Admin List</a></li>
                        <li class="active">Manage Admin</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	<?php 
	
	if($this->session->userdata("ismanager") == 1){
	$this->load->helper('form');
		$att = array('name' => 'addNewUser','id' => 'addNewUser');
		echo form_open('siteowner/addNewUser', $att);		
		$adddevicebutton=array("name" => "addNewUser","id" => "addNewUser","class" => "btn btn-primary");
		echo form_submit($adddevicebutton, 'Add');
		echo form_close();
		
	}
	
	?>
	
	 </div>
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/siteowner/userlist") ;
             }
             }); 
	     });
     </script>
    
    
    
	<div class="col-xs-6">
	<?php			
            $this->load->helper('form');
			
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'get', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('siteowner/userlist', $att);
                        $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
?>			
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Username</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Is manager</th>
							
								<?php if($this->session->userdata("ismanager")==1) {	?><th>Actions</th><?php }?>
								
								</tr>
							</thead>
							<tbody>	
							
							<?php $DeleteLink="";
							      $EditLink="";  ?>					
							<?php foreach($results as $t_data):
						
						
				          $ismanager = 'Yes';	
				          if($t_data->ismanager == 0)
				          {
				            $ismanager = 'No';
				          }
				        
						
							
				          if($this->session->userdata("ismanager")==1) {
                                          $EditLink = '<a href="'.base_url().'/index.php/siteowner/updateuser?id='.$t_data->adminid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
                                          }
                                          
                                          
                                          if($this->session->userdata("ismanager")==1){
			                  $DeleteLink = '<a href="javascript:void(0)" onclick="deleteuser('.$t_data->adminid.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Delete"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
			                  }
			                ?>

							<tr id='RecRow<?php echo $t_data->adminid;?>'>
							<td><?php echo $t_data->username;?></td>
							<td><?php echo $t_data->firstname;?></td><td><?php echo $t_data->lastname;?></td>
							<td><?php echo $t_data->email;?></td>
						        <td><?php echo $ismanager?></td> 
						 <?php if($this->session->userdata("ismanager")==1) { ?>   <td class='span1'><?php echo $EditLink?><?php echo $DeleteLink?></td> <?php } ?>
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No User to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {

	   $("#userli").attr('class','treeview active');
	   $("#usersubli").attr('class','active');
	   $("#userul").attr('style','display:block');
	   $("#userlist").attr('class','fa fa-angle-down pull-right');    
	 
});


</script>