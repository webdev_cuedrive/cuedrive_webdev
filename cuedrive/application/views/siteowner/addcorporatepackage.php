<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}



$Packages = Packagemodel::getAllPackages();
#print("<pre>");print_r($typesOptStr);exit;
$packageOptions  = array(""=>"");
foreach($Packages as $Package)
{
	$packageOptions[$Package->packageid] = $Package->price." "."USD"; 
}


$options['companyid'] = $id;
$raw_data =  Companyadminmodel::GetCompanyAdminDetails($options);
$raw_data = $raw_data [0];


$this->load->helper('form');	
$att = array('name' => 'addEmployer','id' => 'addEmployer', 'method'=> 'post', 'onsubmit'=>"return validateForm();");
print form_open_multipart('siteowner/insertcorporatepackage', $att);
	

            $username = array ("name" => "username","id" => "username","class" => "form-control","value"=>$raw_data->username);
			$email = array ("name" => "email","id" => "email","class" => "form-control","value"=>$raw_data->email);
			$name = array ("name" => "name","id" => "name","class" => "form-control","value"=>$raw_data->name);	
			$address = array ("name" => "address","id" => "address","class" => "form-control","value"=>$raw_data->address);
			$phone= array ("name" => "phone","id" => "phone","class" => "form-control","value"=>$raw_data->phone);
			$type = array ("name" => "type","id" => "type","class" => "form-control","value"=>$raw_data->type);	
			$acn = array ("name" => "acn","id" => "type","class" => "form-control","value"=>$raw_data->acn);	
			$abn = array ("name" => "abn","id" => "type","class" => "form-control","value"=>$raw_data->abn);				
			//$packageid = array ("name" => "packageid","id" => "packageid","class" => "span12","value"=>$raw_data->packageid);				
			echo form_hidden('companyid',$raw_data->companyid);
	
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update Corporate Package
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/pendingApprovals"><i class="fa fa-mobile"></i> Approval List</a></li>
                        <li class="active">Manage Approvals</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

		 	<label>Username</label>					
			<?php echo form_input($username);?>
			<label>Email</label>					
			<?php echo form_input($email);?>
			<label>Company Name</label>					
			<?php echo form_input($name);?>			
			<label>Street Address</label>
			<?php echo form_input($address);?>
			<label>Location</label>
			<input type="text" name="city" placeholder="City" class="form-control" value="<?php echo $raw_data->city?>"><input type="text" name="zipcode" placeholder="Zipcode" class="form-control" value="<?php echo $raw_data->zipcode?>">
			<input type="text" name="country" class="form-control" placeholder="Country" value="<?php echo $raw_data->country?>">		
			<label>Phone</label>
			<?php echo form_input($phone);?>
			<label>Type</label>
			<?php echo form_input($type);?>
			<label>ACN</label>
			<?php echo form_input($acn);?>
			<label>ABN<span><font color="red">&nbsp;*</font></span></label>
			<?php echo form_input($abn);?>
			
			<input type="hidden" name="packageid" value="<?php echo $raw_data->packageid?>">
			<label>Membership Expiry Date</label>
			<input type="text" name="expirydate" class="form-control" value="<?php echo $raw_data->expirydate?>">
			<label>Balance Amount</label>
			<input type="text" name="balanceamount" class="form-control" value="<?php echo $raw_data->balanceamount?>">
			
			<?php if($raw_data->status == 'waiting') { ?>
			
			 <?php 
			 
			 $tempdata = $raw_data->tempdata;
			 $data=array();
			 if(strpos($tempdata,',') > 0)
			 {
			 	$data=explode(',', $tempdata);
			 }
			 $count=count($data);
			 
			 $fee = $data[0];
			 $storage = $data[1];
			 $pricedevice = $data[2];
			 $nodevice = $data[3];

			 ?>
			<label>Storage</label>
			<input type="text" name="storage" class="form-control" value="<?php echo $storage?>">
			
			<label>Maximum Number of devices</label>
			<input type="text" name="noofdevices" class="form-control" value="<?php echo $nodevice?>">
			
			<label>Price per device &nbsp;(AUD)</label>
			<input type="text" name="priceperdevice" class="form-control" value="<?php echo $pricedevice?>">
			
			<label>Monthly Fee &nbsp;(AUD)</label>
			<input type="text" name="fee" class="form-control" value="<?php echo $fee?>">
			
			<?php } 
			      else {
			?>
			
			<label>Storage</label>
			<input type="text" name="storage" class="form-control" value="<?php echo $raw_data->storagespace?>">
			
			<label>Maximum Number of devices</label>
			<input type="text" name="noofdevices" class="form-control" value="<?php echo $raw_data->noofdevices?>">
			
			<label>Price per device</label>
			<input type="text" name="priceperdevice" class="form-control" value="<?php echo $raw_data->additionaldeviceprice?>">
			
			<label>Monthly Fee</label>
			<input type="text" name="fee" class="form-control" value="<?php echo $raw_data->price?>">
			
			
			<?php }?>
			<label>Credit Limit &nbsp;(AUD)</label>
			<input type="text" name="creditlimit" class="form-control" value="<?php echo $raw_data->creditlimit?>">
		        
		        <input type="hidden" name="emailsupport" class="" value="yes">
			
			
			<?php if($raw_data->status == 'waiting') { ?>
			<input type="hidden" name="temp" value="0">
			<?php } 
			      else {
			?>
			<input type="hidden" name="temp" value="1">
			
			<?php }?>
			
			<label>Status</label>
			<select name = "status" class="form-control">
			<option value="waiting" <?php if ('waiting'==$raw_data->status) echo 'selected="selected"';?>>Waiting</option>
			<option value="approved" <?php if ('approved'==$raw_data->status) echo 'selected="selected"';?>>Approved</option>
			
			</select>
			
			<br>
			<input type="submit" class="btn btn-primary pull-left" value="Save">
        	        <a href="javascript:void(0)" onclick="window.history.back()"><b class="btn btn-primary pull-right">Cancel</b></a>
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#packageli").attr('class','treeview active');
$("#corporateli").attr('class','active');
$("#packageul").attr('style','display:block');;
 $("#businesspackagelist").attr('class','fa fa-angle-down pull-right');
	new FormValidator('addEmployer', [{
	    name: 'abn',
	    display: 'ABN',    
	    rules: 'required|numeric'
	},  {
	    name: 'storage',
	    display: 'Storage', 
	    rules: 'required'
	},{
	    name: 'noofdevices',
	    display: 'Maximum number of Devices',    
	    rules: 'required|numeric'
	},  {
	    name: 'priceperdevice',
	    display: 'Device Price', 
	    rules: 'required|numeric'
	},{
	    name: 'fee',
	    display: 'Monthly Fee',    
	    rules: 'required|decimal'
	},{
	    name: 'creditlimit',
	    display: 'Credit Limit',    
	    rules: 'required|numeric'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
		       
	    } 
	   
	});
</script>