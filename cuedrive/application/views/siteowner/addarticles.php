<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

$this->load->helper('form');	
$att = array('name' => 'addArticle','id' => 'addArticle', 'method'=> 'post');
print form_open_multipart('siteowner/addcontent', $att);
?>
<aside class="right-side">  

       <br/>
       <?php
	if(isset($_REQUEST['err'])){
        ?>
	<div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo 'You should upload an image file';?>
        </div>
	
       <?php
	}
       ?>  
		<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>
		Add Document
		</h1>
		<ol class="breadcrumb">
		<li><a href="<?php echo base_url()?>index.php/siteowner/articlelist"><i class="fa fa-mobile"></i> Help Documents</a></li>
		<li class="active">Manage Documents</li>
		</ol>
		</section>

	<script src="<?php echo base_url()?>theme/tinymce/tinymce.min.js" type="text/javascript"></script>			
	<script type="text/javascript">
		
		tinymce.init({
		selector: "textarea",
		relative_urls : false,
		convert_urls : false,
		plugins: [
				 "advlist autolink jbimages lists link  charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste"
		],
		
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
	});
	
	</script>
                
	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
         <label>Category <span class="text-danger">*</span></label>
         <select name="Category" id="Category" class="form-control">
         	<option value="">ALL</option>
            <option value="1">Android</option>
            <option value="2">iOS</option>
            <option value="3">Web</option>
         </select>
         
                 <label>Title <span class="text-danger">*</span></label>	
                 <input type="text" name="doctitle" id="doctitle" class="form-control"> <br>



             <label>Description <span class="text-danger">*</span></label>
		 	 <textarea name="terms" id="terms" style="height: 350px;"></textarea> <br><br>
			 
             <input type="submit" class="btn btn-primary pull-right" value="Add">
		 	
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
$("#contentli").attr('class','treeview active');
$("#helpli").attr('class','active');
$("#contentul").attr('style','display:block');
 $("#articlelist").attr('class','fa fa-angle-down pull-right'); 
	var Validator = new FormValidator('addArticle', [
	{
	    name: 'doctitle',
	    display: 'Title',    
	    rules: 'required|callback_terms',
	}/*,  
	{
	    name: 'terms',
	    display: 'description',    
	    rules: 'required'
	}*/
	], 
	function (errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if(errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
				SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
				
	        }
	    }
	});
	Validator.setMessage('required', 'Please enter %s');
	
	Validator.registerCallback('terms', function(terms) {   
	var content = tinyMCE.activeEditor.getContent();
	if(content == '')
	 {
		return false;
	 }else{	
	   return true;
	 	}
	})
 
 .setMessage('terms', 'Please enter Description');
</script>