<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}
	$this->load->helper('form');
	$att = array('name' => 'sendticket','id' => 'sendticket');
	echo form_open('siteowner/addticket', $att);

	$tid = array ("name" => "tid","id" => "tid","class" => "form-control","readonly"=>"true","value" => $id);
	$email = array ("name" => "email","id" => "email","class" => "form-control");
	$phone = array ("name" => "phone","id" => "phone","class" => "form-control");
	$comments = array ("name" => "comments","id" => "comments","class" => "form-control");	
	$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-left ");
	$comapnyuserid = Contactusmodel::fetchCompanyuseridByTicketid($id);
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Reply
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/ticketconversation?id=<?php echo $id?>"><i class="fa fa-mobile"></i> Ticket</a></li>
                        <li class="active"> Manage Ticket</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Reply
                                  </h2>                            
                        </div><!-- /.col -->
           </div>
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
			<?php echo form_hidden('ticketid',$id);?>
			<?php echo form_hidden('email','-');?>
			<?php echo form_hidden('phone','-');?>
			<?php echo form_hidden('type','support');?>
			<?php echo form_hidden('companyuserid',$comapnyuserid);?>
         	<label>Ticket ID</label>					
			<?php echo form_input($tid);?>	
			<label>Priority</label>
			<select name = "priority" class="form-control">
			<option value="<?php echo $priority; ?>"><?php echo $priority; ?></option>
			</select>
			
<!--			<select name = "priority" class="form-control">
			<option value="low">Low</option>
			<option value="normal">Normal</option>
			<option value="high">High</option>
			</select>-->

			
			<label>Issue Description <span class="text-danger">*</span></label>
			<?php echo form_textarea($comments);?><br>
			<input type="hidden" name="companyid" value="<?php echo $companyid?>">
			<input type="submit" class="btn btn-primary pull-left" value="Send">
                       
        	     
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#supportli").attr('class','treeview active');
$("#feedbackli").attr('class','active');
$("#supportul").attr('style','display:block');
 $("#tickets").attr('class','fa fa-angle-down pull-right');   
var Validator =	new FormValidator('sendticket', [
	{
	    name: 'comments',
	    display: 'Description ', 
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
	    } 
	   
	});
Validator.setMessage('required', 'Please enter %s');	
</script>	 