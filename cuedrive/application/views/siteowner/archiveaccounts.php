<?php 
	include("header.php");
	include("sidebar.php");

if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/companylist";
?>
<style>.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {padding: 4px!important;}</style>	
<script src="<?php echo base_url()?>theme/js/jquery.magnific-popup.js" type="text/javascript"></script>	
	<link rel="stylesheet" href="<?php echo base_url(); ?>theme/css/magnific-popup.css">

<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Company
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/companylist"><i class="fa fa-mobile"></i>Company List</a></li>
                        <li class="active">Manage Company</li>
                       
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	<div class="col-xs-6">
	
	 </div>
	 
	 
	 
	  <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/siteowner/companylist") ;
             }
             }); 
	     });
     </script>
    
    
    
	<div class="col-xs-6">
	<?php			
            $this->load->helper('form');
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'post', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('siteowner/archiveaccounts', $att);
            $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
?>			
              
              <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search by company name or email or number" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
	         </div>
	         </div><br>
	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php 
							$count = count($results);
							if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th width="10%">Account No</th>
									<th>Name</th>
									<th>Email</th>
									<th>Company</th>
									<th>Membership</th>
									<th>Devices</th>
									
									<th>Membership Start</th>
									<th>Membership Expire</th>
									
									<th>Actions</th>
									<!--<th>Activities</th>-->
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
                    $EditLink = '<a href="'.base_url().'/index.php/siteowner/modifycompanyadmin?id='.$t_data->companyid.'" ><img src="'.base_url().'theme/images/icon/icn_edit.png" title="Update"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
					
			        $DeleteLink = '<a href="javascript:void(0)" onclick="deleteCompany('.$t_data->companyid.')" ><img src="'.base_url().'theme/images/icon/icn_trash.png" title="Deactivate"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
					
					$ViewInvoicesLink = '<a href="'.base_url().'index.php/siteowner/archiveinvoices/'.$t_data->companyid.'" ><img src="'.base_url().'theme/images/icon/approved.png" title="View Invoices" width="17px" height="17px"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
					
				    if($t_data->isactive == 'no')
				    {
					  $BannedLink = '<a href="javascript:void(0)" onclick="bannedCompany('.$t_data->companyid.','.$t_data->companyid.',1)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Remove Ban"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				    }
				   else
				   {
					  $BannedLink = '<a href="javascript:void(0)" onclick="bannedCompany('.$t_data->companyid.','.$t_data->companyid.',0)" ><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Ban Company"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
				   }
							
							$countDevice = Companydevicemodel::fetchDeviceCount($t_data->companyid);
							
							$today = strtotime(date('Y-m-d H:i:s'));
							$expdate = strtotime($t_data->expirydate);
							$diff =  $today - $expdate;
							
							$days = floor($diff/(60*60*24));
						
							if($days > 30) {
								$cssColor = ($t_data->isactive == 'no') || ($diff > 0) ? "class='alert alert-danger alert-dismissable'" : "";	
								//$DeactivateLink = '<a href="'.base_url().'/index.php/siteowner/modifycompanyadmin?id='.$t_data->companyid.'" >Deactivate</a>&nbsp;&nbsp;&nbsp;';
								//$DeactivateLink = '<a href="'.base_url('index.php/siteowner/deactivateaccount/'.$t_data->companyid).'" class="deactivate_account" >Deactivate</a>&nbsp;&nbsp;&nbsp;';
								$deleteS3Account = '<a href="'.base_url('index.php/siteowner/deletebucketdata/'.$t_data->companyid).'" class="deletes3_account" ><img src="'.base_url().'theme/images/icon/delete.png" title="Delete Bucket"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
								$DeactivateLink = '<a class="deactivate_account"  href="'.base_url('index.php/siteowner/deactivateaccount/'.$t_data->companyid).'"><img src="'.base_url().'theme/images/icon/icn_banned.png" title="Remove Ban"  border=0 /></a>&nbsp;&nbsp;&nbsp;';
							} else {
								$cssColor = ($t_data->isactive == 'no') || ($diff > 0) ? "class='alert alert-danger alert-dismissable'" : "";
								$DeactivateLink = '';
								$deleteS3Account = '';
							}
							
							if($t_data->bucketdata) {
								$cssColor = "class='greyback'";
							} 
							
							$packagedetails = Packagemodel::getDetails($t_data->packageid);
							$packagename = $packagedetails['type'];
							?>

							<tr <?php echo $cssColor;?> id='RecRow<?php echo $t_data->companyid;?>'>
							<!--<td><?php //echo $t_data->expirydate . '<-->'. date('Y-m-d') .'<-->'. $days .'<-->'. $t_data->account_number;?></td>-->
							<td><?php echo $t_data->account_number;?></td>
							<td><?php echo $t_data->username;?></td>
							<td><?php echo $t_data->email;?></td>
							<td><?php echo $t_data->name;?></td>
							<td><?php echo $packagename;?></td>
							<td><?php echo $t_data->noofdevicesallowed;?></td>
							<td><?php echo date('d-m-Y',strtotime($t_data->createdon));?></td>
							<td><?php echo date('d-m-Y',strtotime($t_data->expirydate));?></td>
							
						    <td class='span1'><?php echo $EditLink?><?php echo $ViewInvoicesLink;?><?php echo $DeactivateLink?><?php echo $deleteS3Account;?></td>
							<!-- <td class='span1'></td> -->
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Company to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
	$(function() {
	   $("#companyli").attr('class','treeview active');
	   $("#companysubli").attr('class','active');
	   $("#companyul").attr('style','display:block');
	   $("#companylist").attr('class','fa fa-angle-down pull-right');
	   
	   $('.deactivate_account').magnificPopup({		

			type: 'iframe',			
			mainClass: 'mfp-fade',
			removalDelay: 320,
			preloader: true,
			iframe: {
				markup: '<div style="width:500px; height:170px;">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
			}
			
		});
		
		 $('.deletes3_account').magnificPopup({		

			type: 'iframe',			
			mainClass: 'mfp-fade',
			removalDelay: 320,
			preloader: true,			
			iframe: {
				markup: '<div style="width:500px; height:170px;">'+
                '<div class="mfp-iframe-scaler" >'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div></div>'
			}
			
		});
	});
</script>