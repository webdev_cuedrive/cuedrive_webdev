<?php
	include("header.php");
	include("sidebar.php");
	
	if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	
	
	$countUsers = Companyadminmodel::totalCompany();
	$corporate = Companyadminmodel::countCorporateCompany();
	$business = Companyadminmodel::countBusinessCompany();
	$diskusage = Folderfilemodel::formatBytes(Folderfilemodel::getAllTotalfileStored());
	$currentTotalBilling = number_format(Transactionmodel::getThisMonthBill(), 2);;
	
?>	
	
	    <aside class="right-side">
	           <section class="content">
			
			
				<div class="row">
	                              <div class="col-sm-2">
                                         <!-- small box -->
                                         <div class="">
                                         <div class="inner">
                                         <h3 align="center">
                                            <?php echo $countUsers?>
                                         </h3>
                                   
                                         </div>
                                         
                                         
                                         <h3 align="center">
                                           <i>Total Users</i>
                                         </h3>   
                                        
                                    </div>
                                </div>

				<div class="row">
	                              <div class="col-sm-2">
                                         <!-- small box -->
                                         <div class="">
                                         <div class="inner">
                                         <h3 align="center">
                                            <?php echo $corporate?>
                                         </h3>
                                   
                                         </div>
                                         
                                         
                                         <h3 align="center">
                                           <i>Corporate</i>
                                         </h3>   
                                        
                                    </div>
                                </div>

                                <div class="row">
	                              <div class="col-sm-2">
                                         <!-- small box -->
                                         <div class="">
                                         <div class="inner">
                                         <h3 align="center">
                                            <?php echo $business?>
                                         </h3>
                                   
                                         </div>
                                         
                                        
                                         <h3 align="center">
                                           <i>Business</i>
                                         </h3>   
                                       
                                    </div>
                                </div>
                                
                                
                                
                                
                                <div class="row">
	                              <div class="col-sm-2">
                                         <!-- small box -->
                                         <div class="">
                                         <div class="inner">
                                         <h3 align="center">
                                            <?php echo $currentTotalBilling.' '.'AUD'?>
                                         </h3>
                                   
                                         </div>
                                         
                                         
                                         <h3 align="center">
                                           <i>This month's billed</i>
                                         </h3>   
                                        
                                    </div>
                                </div>
                                
                                
                                
                                
                                
                   
                       
					
                                <div class="row">
	                              <div class="col-sm-2">
                                         <!-- small box  .col-lg-3 .col-xs-4-->
                                         <div class="">
                                         <div class="inner">
                                         <h3 align="center">
                                            <?php echo $diskusage?>
                                         </h3>
                                   
                                         </div>
                                         
                                         
                                         <h3 align="center">
                                           <i>Disk Usage</i>
                                         </h3>   
                                       
                                    </div>
                                </div>

				</div>
			
		     </section>
		 </aside>
	
	
	
<div class="content">
<aside class="right-side">  


<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>              
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Account Statistics
                     </h1>
                   
                </section>


	<section class="content invoice">   
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0])>0){
					
					  $i=0;
					?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Company</th>
									<th>Membership</th>
									<th>Devices</th>
									
									<th>Data Stored</th>
									<th>This month's bill</th>
									<th>Membership Start</th>
									<th>Membership Expire</th>
								
								</tr>
							</thead>
							<tbody>						
							<?php foreach($results as $t_data):
							
							$countDevice = Companydevicemodel::fetchDeviceCount($t_data->companyid);
							$totalStorage = Folderfilemodel::formatBytes(Folderfilemodel::getTotalfileStored($t_data->companyid),'2');
							$TotalBilling = number_format(Transactionmodel::getThisMonthBill($t_data->companyid), 2);;	
							?>
							<tr id='RecRow<?php echo $t_data->companyid;?>'>
							<td><?php echo $t_data->username;?></td>
							<td><?php echo $t_data->email;?></td>
							<td><?php echo $t_data->name;?></td>
							
							<td><?php echo $t_data->type;?></td>
							<td><?php echo $t_data->noofdevicesallowed;?></td>
							
							<td><?php echo $totalStorage;?></td>
							<td><?php echo $TotalBilling.'AUD';?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
							<td><?php echo date('d-m-Y g:i a',strtotime($t_data->expirydate));?></td>
							
							</tr>
							<?php endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Record to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
 
<script>
$(function() {
	   $("#statisticli").attr('class','treeview active');
	   $("#subli3").attr('class','active');
	   $("#statisticul").attr('style','display:block');
	   $("#dashboard").attr('class','fa fa-angle-down pull-right');
	});
</script>
	

	