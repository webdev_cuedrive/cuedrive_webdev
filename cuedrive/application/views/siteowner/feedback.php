<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/feedbacks";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	   Feedback List
	 </h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url()?>index.php/siteowner/feedbacks"><i class="fa fa-mobile"></i>Feedback List</a></li>
		<li class="active">Manage Feedback</li>
	</ol>
</section>

<section class="content invoice">   
<div  class="row ">
	
		<div class="col-xs-6">
	
	 </div>
	    
		<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
                  </div>
		
		<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){ $i=1;?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									
									<th>Company Name</th>
									<th></th>
									<th>Feedback</th>
							         <th>Date</th>
								 <th>Action</th>	
								
								</tr>
							</thead>
							<tbody>						
			           <?php foreach($results as $t_data): 
			           
			           
			           $marker = '<i class="fa fa-circle text-red"></i>';
			           $sentence = $t_data->comments;
			           if(str_word_count($sentence) > 5)
			           {
			           
			           $sentence = implode(' ', array_slice(explode(' ', $t_data->comments), 0, 5)).".....";
			           
			           }
			           
			           
			           
			           
			           if($t_data->sent == 1)
			           $marker = '<i class="fa fa-circle text-green"></i>';
			           
			           ?>
							<tr id='RecRow'>
							
							<td width="10%"><?php echo $t_data->name;?></td><td width="3%"><?php echo $marker;?></td>
							<td>
								<a href="javascript:click(<?php echo $i?>)" id='aRow<?php echo $i?>' class='' style="text-decoration: blink;color:black;"><?php echo ($sentence); ?></a>
								
								<!-- <i id='divRow<?php echo $i?>' style="display:none;"><?php //echo $t_data->comments;?><i> -->
								
								<span id='divRow<?php echo $i?>' style="display:none;"><?php echo $t_data->comments;?><span>
								
								</td>
						        <td width="10%"><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
						        <td width="5%"><a href="javascript:composemail('<?php echo $t_data->companymail?>','<?php echo $t_data->id?>')"><button class="btn btn-success btn-sm">Reply</button></a></td>
							</tr>
							<?php 
							
							$i++;
							endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Feedback to display</h3></center>
							<?php }?>
					</div>
					
			
</div>		
	
</section>
</aside>



       <div id="myModal" class="reveal-modal">
           
                <div class="">
                    <div class="modal-header">
                        
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
                    </div>
                    <form method="post" action="<?php echo base_url()?>index.php/siteowner/replyFeedback">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">TO:</span>
                                    <input type="email" placeholder="Email TO" class="form-control" name="email_to" id="emaito">
                                    <input type="hidden" name="fid" id="fid">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <textarea style="height: 120px;" placeholder="Message" class="form-control" id="email_message" name="message"></textarea>
                            </div>
                            
                        </div>
                        <div class="modal-footer clearfix">

                            <button class="btn btn-danger" type="button" onclick="closemodal()"><i class="fa fa-times"></i> Discard</button>

                            <button class="btn btn-primary pull-left" type="submit"><i class="fa fa-envelope"></i> Send </button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            <!-- /.modal-dialog -->
            
            
            <div style="margin-top: 10px">
   
             <a class="close-reveal-modal">&#215;</a>
            </div>	
        </div>






<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>	
 <script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>
<script>
$(function() {

	 $("#supportli").attr('class','treeview active');
	   $("#feedbackli").attr('class','active');
	   $("#supportul").attr('style','display:block');
	    $("#tickets").attr('class','fa fa-angle-down pull-right');   
	   
	 
});

function click(id)
{
$("#aRow"+id).hide();
$("#divRow"+id).slideDown(3000);
}


function composemail(id,fid)
{
  $("#emaito").val('');	
  $("#myModal").reveal();
  $("#emaito").val(id);	
  $("#fid").val(fid);

}

function closemodal()
{
  $('#myModal').trigger('reveal:close');
}
</script>