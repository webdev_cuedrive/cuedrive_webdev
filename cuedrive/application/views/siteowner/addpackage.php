<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid") && $this->session->userdata("ismanager")!=1) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

$this->load->helper('form');	
$att = array('name' => 'addPackage','id' => 'addPackage', 'method'=> 'post');
print form_open_multipart('siteowner/addpackage', $att);
	

          
			$price = array ("name" => "price","id" => "price","class" => "form-control");
			//$forduration = array ("name" => "forduration","id" => "forduration","class" => "span12");	
			$type = array ("name" => "type","id" => "type","class" => "form-control");
			$storagespace = array ("name" => "storagespace","id" => "storagespace","class"=>"form-control");
			$additionaldeviceprice = array ("name" => "additionaldeviceprice","id" => "additionaldeviceprice","class" => "form-control");				
			$noofdevice = array ("name" => "noofdevices","id" => "noofdevices","class" => "form-control");				
			
			
	
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Package
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/businesspackagelist"><i class="fa fa-mobile"></i> Package List</a></li>
                        <li class="active">Manage Package</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

             <label>Package Name <span class="text-danger">*</span></label>					
			<?php echo form_input($type);?>
		 	<input type="hidden" name="accounttype" value="business">
			<label>Monthly Fee <span class="text-danger">*</span></label>					
			<?php echo form_input($price);?>
						
			<input type="hidden" name="forduration" value="1Month">		 
			
          
			<label>Storage <span class="text-danger">*</span></label>
			<?php echo form_input($storagespace);?>
			
			<label>Price per device <span class="text-danger">*</span></label>
			<?php echo form_input($additionaldeviceprice);?>
			
			<label>Maximum number of devices <span class="text-danger">*</span></label>
			<?php echo form_input($noofdevice);?>
			
			<label>Email Support <span class="text-danger">*</span></label>
			
			<select name = "emailsupport" class="form-control">
			<option value="yes">Yes</option>
			<option value="yes">No</option>
			</select><br>
			<input type="submit" class="btn btn-primary" value="Add">
		 
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
$("#packageli").attr('class','treeview active');
$("#businessli").attr('class','active');
$("#packageul").attr('style','display:block');
 $("#businesspackagelist").attr('class','fa fa-angle-down pull-right');
var Validator = new FormValidator('addPackage', [{
	    name: 'type',
	    display: 'Package Name',    
	    rules: 'required'
	},  {
	    name: 'price',
	    display: 'Monthly Fee', 
	    rules: 'required'
	},{
	    name: 'storagespace',
	    display: 'Storage Space',    
	    rules: 'required'
	},  {
	    name: 'additionaldeviceprice',
	    display: 'Price per Device', 
	    rules: 'required'
	},{
	    name: 'noofdevices',
	    display: 'Maximum number of Device',    
	    rules: 'required'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		{
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		} 
});
Validator.setMessage('required', 'Please enter %s');	
</script>	 