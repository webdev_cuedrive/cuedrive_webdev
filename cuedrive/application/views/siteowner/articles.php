<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

if(!isset($id))
{
	$id = 0;
}
$raw_data = Contentmanagementmodel::getContent($type,$id);

if($type == 'article')
{
	$content = base64_decode($raw_data->articles);
	$text = 'Article';
}
if($type == 'privacy')
{
	$content = base64_decode($raw_data->privacy);
	$text = 'Privacy Policy';
}
if($type == 'terms')
{
	$content = base64_decode($raw_data->terms);
	$text = 'Terms & Conditions';
}


$this->load->helper('form');	
$att = array('name' => 'editArticle','id' => 'editArticle', 'method'=> 'post');
print form_open_multipart('siteowner/updatecontent', $att);


echo form_hidden('contentid',$raw_data->contentid);
echo form_hidden('type',$type);


?>
			<aside class="right-side">  
               <br/>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update Document 
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/articlelist"><i class="fa fa-mobile"></i> Help Documents</a></li>
                        <li class="active">Manage Documents</li>
                    </ol>
                </section>

		<script src="<?php echo base_url()?>theme/tinymce/tinymce.min.js" type="text/javascript"></script>			
		<script type="text/javascript">
			
			tinymce.init({
			selector: "textarea",
			relative_urls : false,
			convert_urls : false,
		
			plugins: [
					 "advlist autolink jbimages lists link  charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
		});
		</script>
                
	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
<label>Category <span class="text-danger">*</span></label>
         <select name="Category" id="Category" class="form-control">
         	<option value="">ALL</option>
            <option value="1" <?= ($raw_data->category_id == 1) ?'selected':''?>>Android</option>
            <option value="2" <?= ($raw_data->category_id == 2) ?'selected':''?>>iOS</option>
            <option value="3" <?= ($raw_data->category_id == 3) ?'selected':''?>>Web</option>
         </select>
                 <label>Title <span class="text-danger">*</span></label>	
                 <input type="text" name="doctitle" class="form-control" value="<?php echo $raw_data->articletitle?>"> <br>

                 <label>Description <span class="text-danger">*</span></label>
			 <textarea name="terms" id="terms" style="height: 350px;"><?php   echo $content;      ?></textarea> <br><br>
             <input type="submit" class="btn btn-primary pull-right" value="Update">
		 	
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
$("#contentli").attr('class','treeview active');
$("#helpli").attr('class','active');
$("#contentul").attr('style','display:block');
 $("#articlelist").attr('class','fa fa-angle-down pull-right'); 
	var Validator = new FormValidator('editArticle', [
	{
	    name: 'doctitle',
	    display: 'Title',    
	    rules: 'required|callback_terms'
	}/*,  
	{
	    name: 'terms',
	    display: 'description',    
	    rules: 'required'
	}*/
	], 
	
	
	function (errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if(errors.length > 0) 
		{
		    SELECTOR_ERRORS.empty();
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
				SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
	    } 
	});
	
	Validator.setMessage('required', 'Please enter %s');
	
	Validator.registerCallback('terms', function(terms) {   
	var content = tinyMCE.activeEditor.getContent();
	if(content == '')
	 {
		return false;
	 }else{	
	   return true;
	 	}
	})
 
 .setMessage('terms', 'Please enter Description');
</script>