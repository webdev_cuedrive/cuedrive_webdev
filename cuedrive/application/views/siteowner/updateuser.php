<?php
$scriptload = 1;
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}
$raw_data = Siteownermodel::GetSiteownerAdmin($id);
$this->load->helper('form');	
/* Aress
   Changed form name
 */
$att = array('name' => 'editAdminUser','id' => 'editAdminUser', 'method'=> 'post');
print form_open_multipart('siteowner/ModifyUser', $att);
	

            $firstname = array ("name" => "firstname","id" => "firstname","class" => "form-control","placeholder"=>"First Name","value"=>$raw_data['firstname']);
			$lastname = array ("name" => "lastname","id" => "lastname","class" => "form-control","placeholder"=>"Last Name","value"=>$raw_data['lastname']);
			$username = array ("name" => "username","id" => "username","class" => "form-control","placeholder"=>"Username","value"=>$raw_data['username']);	
			$email = array ("name" => "email","id" => "email","class" => "form-control","placeholder"=>"Email","value"=>$raw_data['email']);		
			
	
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<aside class="right-side"> 
<script>

//window.onbeforeunload = function() { return "Are you sure you want to leave this page?"; };

</script>


               
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Update Profile
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-mobile"></i> Admin Profile</a></li>
                        <li class="active">Manage Profile</li>
                    </ol>
                </section>


	<section class="content invoice">   
	 
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>
			<label>First Name <span class="text-danger">*</span></label>					
			<?php echo form_input($firstname);?>
			<label>Last Name <span class="text-danger">*</span></label>
			<?php echo form_input($lastname);?>
			<label>Username <span class="text-danger">*</span></label>					
			<?php echo form_input($username);?>
			<label>Email <span class="text-danger">*</span></label>					
			<?php echo form_input($email);?>
			<label>Password</label>					
			<input type = "password" name = "password" class = "form-control" placeholder="Password" value="<?php //echo $raw_data['password']?>"><br>
			<input type="hidden" value="<?php echo $raw_data['adminid']?>" name="adminid">
			Is Manager ? 
			<?php 
			   if($this->session->userdata("ismanager")==1) {
			   if ($raw_data['ismanager'] == 1) { ?>					
			<input type = "checkbox" name = "ismanager" class="icheckbox_minimal" checked>
			<?php }  else {?>
			<input type = "checkbox" name = "ismanager" class="icheckbox_minimal">
			<?php }} else {echo 'No';}?>
			<br>
			<br>
			
			<input type="submit" class="btn btn-primary pull-left" value="Update">
		 
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 
$("#userli").attr('class','treeview active');
$("#usersubli").attr('class','active');
$("#userul").attr('style','display:block');
 $("#userlist").attr('class','fa fa-angle-down pull-right');    
var Validator =	new FormValidator('editAdminUser', [{
	    name: 'firstname',
	    display: 'First Name',    
	    rules: 'required'
	},
	{
	    name: 'lastname',
	    display: 'Last Name', 
	    rules: 'required'
	},
	{
	    name: 'username',
	    display: 'Username', 
	    rules: 'required'
	},
	{
	    name: 'email',
	    display: 'Email address',    
	    rules: 'required|valid_email'
	}], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) {
		        SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	              }
	    } 
	});
Validator.setMessage('required', 'Please enter %s');	
</script>	 