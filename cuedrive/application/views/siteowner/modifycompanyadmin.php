<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("adminid")) 
{
	header ("Location:".base_url()."index.php/siteowner");
	exit;
}

$Packages = Packagemodel::getBusinessAllPackages();
//$packageOptions  = array(""=>"");
foreach($Packages as $Package)
{
	$packageOptions[$Package->packageid] = $Package->price." "."AUD"; 
}
$options['companyid'] = $id;
$raw_data =  Companyadminmodel::GetCompanyAdminDetails($options);
$raw_data = $raw_data [0];


$countryList = Companyadminmodel::GetCountryList();
//print("<pre>");print_r($Packages);exit;
foreach($countryList as $country)
{
	$countryOptions[$country->country_name] = $country->country_name; 
}


$this->load->helper('form');	
$att = array('name' => 'editEmployer','id' => 'editEmployer', 'method'=> 'post');
print form_open_multipart('siteowner/updatecompany', $att);
	
$packagedetails = Packagemodel::getDetails($raw_data->packageid);
$packagename = $packagedetails['type'];
							
$username = array ("name" => "username","id" => "username","class" => "form-control","autocomplete"=>"off","value"=>$raw_data->username);
$email = array ("name" => "email","id" => "email","value"=>$raw_data->email ,"class" => "form-control","autocomplete"=>"off");
$name = array ("name" => "name","id" => "name","class" => "form-control","value"=>$raw_data->name,"autocomplete"=>"off");
$address = array ("name" => "address","id" => "address","class" => "form-control","value"=>$raw_data->address,"autocomplete"=>"off");
$phone= array ("name" => "phone","id" => "phone","class" => "form-control","value"=>$raw_data->phone,"autocomplete"=>"off");
$type = array ("name" => "type","id" => "type","class" => "form-control","value"=>$packagename,"autocomplete"=>"off");
//$type = array ("name" => "type","id" => "type","class" => "form-control","value"=>$raw_data->type,"autocomplete"=>"off");
//$packageid = array ("name" => "packageid","id" => "packageid","class" => "span12","value"=>$raw_data->packageid);
echo form_hidden('companyid',$raw_data->companyid);
	
$formreset=array("name" => "reset","id" => "reset","class" => "btn btn-primary pull-right ");
?>
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/datepicker-jquery-ui.css">

<script>
$(function() {
$( "#expirydate" ).datepicker({
		dateFormat: "yy-mm-dd",
		minDate: 0
	});
});
</script>
<aside class="right-side">    

<?php if(isset($errorMsg)){?>
	<div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">�</button>
       <?php echo $errorMsg;?>
    </div>
<?php } ?>     
            
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Company Admin
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/companylist"><i class="fa fa-mobile"></i> Companylist</a></li>
                        <li class="active">Manage Company</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div class="row">
		<div class="col-xs-6">&nbsp; <!--<a href="#" class="btn btn-primary">Invoice History</a>--></div>
		<div class="col-xs-6" style="text-align:right;"> <label>Account No:</label> <?php echo $raw_data->account_number;?></div>
	 </div>
	
		<div id="tablewidget" class="block-body collapse in">
		 <div class="error_box error-txt"> </div>

		 	<label>Username <span class="text-danger">*</span></label>					
			<?php echo form_input($username);?>
			<label>Email <span class="text-danger">*</span></label>					
			<?php echo form_input($email);?>	
			
			<label>Business/Company Name <span class="text-danger">*</span></label>					
			<?php echo form_input($name);?>	
			
			<label>First Name <span class="text-danger">*</span></label>					
			<input type="text" name="firstname" class="form-control" value="<?php echo $raw_data->firstname;?>" required/>
			
			<label>Last Name <span class="text-danger">*</span></label>					
			<input type="text" name="lastname" class="form-control" value="<?php echo $raw_data->lastname;?>" required/>
			
					
			<label>Street Address <span class="text-danger">*</span></label>
			<?php echo form_input($address);?>
			<label>City <span class="text-danger">*</span></label>
			<input type="text" name="city" placeholder="City" class="form-control" value="<?php echo $raw_data->city?>">
			<label>Zip Code <span class="text-danger">*</span></label>
			<input type="text" name="zipcode" placeholder="Zipcode" class="form-control" value="<?php echo $raw_data->zipcode?>">
			<label>Country <span class="text-danger">*</span></label>
						
			<?php echo form_dropdown("country",$countryOptions,$raw_data->country,"class='form-control'");?>	
								
			<label>Phone <span class="text-danger">*</span></label>
			<?php echo form_input($phone);?>
			
			<label>Membership Type <span class="text-danger">*</span></label>
			<?php echo form_input($type);?>
			
			<label>ABN</label>
			<input type="text" name="abn" class="form-control" value="<?php echo $raw_data->abn?>">
			
			<label>ACN </label>
			<input type="text" name="acn" class="form-control" value="<?php echo $raw_data->acn?>" >
			
			
			<?php if($raw_data->type == 'business') { ?>
			<label>Package</label>
			
			<?php if($this->session->userdata("ismanager") == 1) {  ?>
			
			<?php echo form_dropdown("packageid",$packageOptions,$raw_data->packageid,"class='form-control'");?>	
			
			<?php } else { 
			  $js = 'readonly="true" class="form-control"';  
			?>
			
			<?php echo form_dropdown("packageid",$packageOptions,$raw_data->packageid,$js);?>	
			
			<?php } ?>
			
			
			<?php }?>
			
			<?php if($raw_data->type == 'corporate') { ?>
			<input type="hidden" value="<?php echo $raw_data->packageid?>" name="packageid">
			<label>Monthly Price (AUD) <span class="text-danger">*</span></label>
			
			<?php if($this->session->userdata("ismanager") == 1) {  ?>
			<input type="text" name="price" class="form-control" value="<?php echo $raw_data->price?>" >
			<?php } else { ?>
			<input type="text" name="price" class="form-control" value="<?php echo $raw_data->price?>" readonly>
			<?php } ?>
			<input type="hidden" name="mobile" class="form-control" value="<?php echo $raw_data->mobile?>" >
			
			<input type="hidden" name="mobile" class="form-control" value="<?php echo $raw_data->emailsupport?>" >
			
			<?php }?>
			
			
			<label>Maximum devices</label>
			<?php if($this->session->userdata("ismanager") == 1) {  ?>
			<input type="text" name="devices" class="form-control" value="<?php echo $raw_data->noofdevicesallowed?>" >
			<?php } else { ?>
			<input type="text" name="devices" class="form-control" value="<?php echo $raw_data->noofdevicesallowed?>" readonly>
			<?php } ?>
			
			<label>Storage</label>
			<?php if($this->session->userdata("ismanager") == 1) {  ?>
			<input type="text" name="Storage" class="form-control" value="<?php echo $raw_data->storagespace?>" >
			<?php } else { ?>
			<input type="text" name="Storage" class="form-control" value="<?php echo $raw_data->storagespace?>" readonly>
			<?php } ?>
			
			<?php //if($raw_data->type == 'free') { ?>
			<label>Membership Expiry Date</label>
			<input type="text" name="expirydate" id="expirydate" class="form-control" onkeydown="return false;" value="<?php echo date('d-m-Y',strtotime($raw_data->expirydate));?>">
			<?php //} ?>
			
			<?php //if($raw_data->type == 'corporate') { ?>
			
			<label>Credit Limit</label>
			
			<?php if($this->session->userdata("ismanager")==1) { ?>
			
			<input type="text" name="balanceamount" class="form-control" value="<?php echo $raw_data->balanceamount?>">
			<?php }  else {?>
			<input type="text" name="balanceamount" class="form-control" value="<?php echo $raw_data->balanceamount?>" readonly>
			<?php }?>
			<?php if($this->session->userdata("ismanager")==1) { ?>
			<label>Status</label>
			<select name = "status" class="form-control">
			<option value="waiting" <?php if ('waiting'==$raw_data->status) echo 'selected="selected"';?>>Waiting</option>
			<option value="approved" <?php if ('approved'==$raw_data->status) echo 'selected="selected"';?>>Approved</option>
			<option value="denied" <?php if ('denied'==$raw_data->status) echo 'selected="selected"';?>>Cancel</option>
			</select>
			 <?php } ?>
			<?php //} ?>	
			
			<br>
			<input type="submit" class="btn btn-primary pull-left" value="Update" >
		 
         	
        	
       
        <div class="clearfix"></div>
			<?php echo form_close();?>
			<br/><br/>
         
		 </div>
	
	
</section>
</aside>

<?php include_once("footer.php"); ?>
	<!-- put this validation script at the bottom always, as need form to be loaded before executing it. -->
<script type="text/javascript"> 

$("#companyli").attr('class','treeview active');
$("#companysubli").attr('class','active');
$("#companyul").attr('style','display:block');
$("#companylist").attr('class','fa fa-angle-down pull-right');
var Validator = new FormValidator('editEmployer', [{
	    name: 'username',
	    display: 'Company UserName',    
	    rules: 'required'
	},  {
	    name: 'email',
	    display: 'Email address', 
	    rules: 'required|valid_email'
	},
	{
	    name: 'name',
	    display: 'Company Name',    
	    rules: 'required'
	},  

	{
	    name: 'firstname',
	    display: 'First Name',    
	    rules: 'required'
	},
		{
	    name: 'lastname',
	    display: 'Lastame',    
	    rules: 'required'
	},  
	{
	    name: 'address',
	    display: 'Address', 
	    rules: 'required'
	},
	{
	    name: 'city',
	    display: 'City', 
	    rules: 'required'
	},
	{
	    name: 'zipcode',
	    display: 'Zip Code',    
	    rules: 'required'
	},
	{
	    name: 'country',
	    display: 'Country', 
	    rules: 'required'
	},
	{
	    name: 'phone',
	    display: 'Phone Number',    
	    rules: 'required'
	},
	{
	    name: 'type',
	    display: 'Type', 
	    rules: 'required'
	},
	{
	    name: 'abn',
	    display: 'ABN',    
	    rules: 'callback_abn'
	},
	{
	    name: 'acn',
	    display: 'ACN',    
	    rules: 'callback_acn'
	}

	], function(errors, evt) {
		var SELECTOR_ERRORS = $('.error_box');	        
	    if (errors.length > 0) 
		 {
		    SELECTOR_ERRORS.empty();	        
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) 
			{
	            SELECTOR_ERRORS.append(errors[i].message + '<br />');
				break;
	        }
		       
	    } 
	   
	});

Validator.setMessage('required', 'Please enter %s');
Validator.registerCallback('abn', function(abn) {   
		var a, w, t;
		w = [0,0,3,5,7,9,11,13,15,17,19];
		a = abn.split('');
		t = ''+a[0]+a[1];
		w.forEach(function(v,i) {a[i] *= v;});
		a = a.reduce(function(x,y) {return x+y;});
		return (t == 99-(a%89));
	
	})
	.setMessage('abn', 'Please enter valid ABN');
	

Validator.registerCallback('acn', function(acn) {   
		 
		  var weights = [8, 7, 6, 5, 4, 3, 2, 1, 0];
	      var sum, x, remainder, complement;
	      // Strip non-numbers from the acn
	      acn = acn.replace(/[^\d]/g, '');
	      //if (!acn.length)  return true;  //--- nothing entered so just return
	      // Check acn is 9 chars long
	      if( acn.length != 9) {
	          return false;
	      }
	     // Sum the products
	      sum = 0;
	      for(x=0; x<acn.length; x++ ) {
	        sum += acn[x] * weights[x];
	      }
	      // Get the remainder
	      remainder = sum % 10;
	      // Get remainder compliment
	      complement = "" + (10 - remainder);
	      // If complement is 10, set to 0
	      if( complement === "10") {
	          complement = "0";
	      }
	      return ( acn[8] === complement );
	
	})
.setMessage('acn', 'Please enter valid ACN');
</script>