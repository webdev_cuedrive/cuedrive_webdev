<?php
	include("header.php");
	include("sidebar.php");
	
if(!$this->session->userdata("adminid")) 
	{
		header ("Location:".base_url()."index.php/siteowner");
		exit;
	}
	$baseredirecturl=base_url()."index.php/siteowner/tickets";
?>



<aside class="right-side">     

<?php
	if(isset($errorMsg)){
?>
	<div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
       <?php echo $errorMsg;?>
    </div>
	
<?php
	}
?>     
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Ticket List
                     </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url()?>index.php/siteowner/tickets"><i class="fa fa-mobile"></i>Ticket List</a></li>
                        <li class="active">Manage Ticket</li>
                    </ol>
                </section>


	<section class="content invoice">   
	<div  class="row ">
	
	 <script type="text/javascript">

	    $(document).ready(function(){
	         $("#appendedInputButton").keyup(function(){
             if ($("#appendedInputButton").val().length <= 0) {
            	 window.location.replace("<?php echo base_url()?>index.php/siteowner/tickets") ;
             }
             }); 
	     });
     </script>
	
	
	<div class="col-xs-6">
	</div>
	
	
	<div class="col-xs-6">
	<?php			
            $this->load->helper('form');
			
			$att = array('name' => 'addEmployer','id' => 'addEmployer', 'class'=>'form-inline','method'=> 'get', 'onsubmit'=>"return validateForm();");
			print form_open_multipart('siteowner/tickets', $att);
                        $searchtext = "";
			if(isset($search))
			{
				$searchtext = $search;
			}
	
       ?>
        <div class="input-group">
                            <input type="text" name="searchtext" id="appendedInputButton" class="form-control" placeholder="Search by ticket id" value="<?php echo $searchtext;?>"/>
                            <span class="input-group-btn">
                                <button type='submit' name='srchbtn' id='srchbtn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                <?php echo form_close();?>
                </div>	
	 </div><br>
	 

	          <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              
                            </h2>                            
                        </div><!-- /.col -->
             </div>
				
					<div id="tablewidget" class="block-body collapse in">
					<?php if(count($results[0]) > 0){?>
						
						<center><div id="loadingdiv" style="display:none;"><image src="<?php echo base_url()?>theme/images/loading.gif"/></div></center>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Username</th>
									<th></th>
									<th>Company Name</th>
									
									<th>Ticket ID</th>
							         <th>Date</th>
									<th>View</th>
								
								</tr>
							</thead>
							<tbody>						
			           <?php foreach($results as $t_data):
			           
			             if($t_data->ticketid !=0) {

			          $replylink = '<a href="'.base_url().'/index.php/siteowner/ticketconversation?id='.$t_data->ticketid.'" ><i class="fa fa-search"></a>';
			          $marker = '<i class="fa fa-circle text-green"></i>';
			          $markervalue = Contactusmodel::getMarkerValue($t_data->ticketid);
			          if($markervalue == 'green')
			          {
			           $marker = '<i class="fa fa-circle text-red"></i>';
			          }
			          
			          $username = $t_data->username;
			          
			          if($t_data->companyuserid != NULL)
			          {
			             $companyuserdetails = Companyusermodel::getUserDetails($t_data->companyuserid);
			             if(count($companyuserdetails) > 0) {
			             $companyusername = $companyuserdetails['firstname'].' '.$companyuserdetails['lastname'];
			             
			             $username.= ' '.'('.$companyusername.')';
			             }
			          }
			          
			          
			          
			           ?>
							<tr id='RecRow'>
							<td width="10%"><?php echo $username;?></td><td width="3%"><?php echo $marker?></td>
							<td><?php echo $t_data->name;?></td>
							<td><?php echo $t_data->ticketid;?></td>
						    <td><?php echo date('d-m-Y g:i a',strtotime($t_data->createdon));?></td>
						    <td class='span1'><?php echo $replylink?></td>
							</tr>
							<?php 
							}
							endforeach;?>
						
							</tbody>
						</table>
							<div id="pagination"><?php echo $links; ?></div>
							
							<?php }
							else
							{
							?>
							<center><h3>No Ticket to display</h3></center>
							<?php }?>
					</div>
					
			
		
	
</section>
</aside>
<?php
	include_once("footer.php"); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/jquery-ui.css">
<script src="<?php echo base_url()?>theme/js/jquery-ui.js" type="text/javascript"></script>
<script>
$(function() {

	 $("#supportli").attr('class','treeview active');
	   $("#ticketli").attr('class','active');
	   $("#supportul").attr('style','display:block');
	 $("#tickets").attr('class','fa fa-angle-down pull-right');   
	   
	 
});


</script>