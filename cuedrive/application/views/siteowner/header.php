<?php
	$this->load->helper('url'); 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>cuedrive :: Admin Panel</title>
 <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon"  href="<?php echo base_url();?>fav.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/stylesheets/bootstrap.css">   
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/stylesheets/ionicons.min.css"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/stylesheets/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/stylesheets/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/stylesheets/reveal.css">

<style>
.blink {
  animation: blink 1s steps(5, start) infinite;
  -webkit-animation: blink 1s steps(5, start) infinite;
}
@keyframes blink {
  to { visibility: hidden; }
}
@-webkit-keyframes blink {
  to { visibility: hidden; }
}
</style>
    <script src="<?php echo base_url();?>theme/js/jquery-1.7.2.min.js" type="text/javascript"></script>   
 
	<script src="<?php echo base_url();?>theme/js/app.js" type="text/javascript"></script>
 
    <script type="text/javascript" src="<?php echo base_url();?>theme/js/validate.js"></script>
    
    
    
    
    
    
    
    <script type="text/javascript">

var message="Sorry, right-click has been disabled";

function clickIE() {if (document.all) {(message);return false;}}
function clickNS(e) {if
(document.layers||(document.getElementById&&!document.all)) {
if (e.which==2||e.which==3) {(message);return false;}}}
if (document.layers)
{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
document.oncontextmenu=new Function("return false")

</script> 
    
    
    
    
    
    
    
    
    
    
    
    

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
        
        
        
#floatdiv {
    display:none;
}

/* if screen size gets wider than 1024 */

@media screen and (max-width:1000px){
    #floatdiv {
        display:block;
    }
}
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
	<script>
		var baseURL = "<?php echo base_url();?>";
	</script>
  
   <script src="<?php echo base_url();?>theme/js/jsfunctions1.js" type="text/javascript"></script>
  <body class="skin-blue"> 
  <!--<![endif]-->
     
    
   <header class="header">
   
  
        
   
            <div class="logo">
             
             <a  href="<?php echo base_url();?>index.php/siteowner/updateuser?id=<?php echo $this->session->userdata("adminid")?>"> <img src="<?php echo base_url();?>/cuedriveLogowhite.png"   width="150"> </a>&nbsp;&nbsp;&nbsp;
            </div>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
            
            
            
            
            
            
            
             <?php
             
              if(!isset($scriptload)) { 
               if($this->session->userdata("adminid")) 
		{	
	      ?>	
              <img src="<?php echo base_url();?>/back_iPad.png" id="back" style="margin-top:15px;margin-left:5px;">
              <?php
		}
		}
	     ?>
	     
	    
	       <a role="button" data-toggle="offcanvas" class="navbar-btn sidebar-toggle" href="#" id="floatdiv">
                   
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a> 
	     
	     
	     <div class="navbar-right">
	     
                <ul class="nav navbar-nav  pull-right">
               
               <li>
               
              
               
               
               </li>
               <li>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
               </li>
               
               
               <?php
		if($this->session->userdata("adminid")) 
		{	
			?>	
                  <li><a  href="<?php echo base_url();?>index.php/siteowner/updateuser?id=<?php echo $this->session->userdata("adminid")?>"><?php echo $this->session->userdata("usernam")?></a></li>
		<?php
		}
		?>	
               
               
               
               
               
        	<?php
		if($this->session->userdata("adminid")) 
		{	
		?>	
                  <li><a  href="<?php echo base_url();?>index.php/siteowner/logout">Logout</a></li>
		<?php
		}
		?>	
		</ul>
	</div>
	</nav>
   </header>
     
   <script>
   $(document).ready(function() {
    $("#back").click(function()
    {
    window.history.back() 
    });
   });
   
   
   </script>   
    
    