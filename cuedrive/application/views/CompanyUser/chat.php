<?php
include_once("header.php"); 
include("sidebar.php");

if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
?>

 <aside class="right-side"> 
	
	
	<section class="content">
                
                    <div class="mailbox row">
                        <div class="col-xs-12">
                            <div class="box box-solid">
                                <div class="box-body">
                                    <div class="row">
                                        
                                        
                                        <div class="col-md-3 col-sm-8">
                                            
                                           <div id='chat-area'>
                                                 <ul></ul>
                                           </div>
                                            
                                        </div><!-- /.col (LEFT) -->
                                       
                                       
                                       
                                       
                                       
                                       
                                        <div class="col-md-9 col-sm-8">
                                           
                                         <div class="table-responsive">     
                                           <div id='roster-area'>
                                           <ul></ul>
                                           </div>
      
                                         </div><!-- /.table-responsive -->
                                            
                                            
                                        </div><!-- /.col (RIGHT) -->
                                   
                                   
                                    </div><!-- /.row -->
                                </div><!-- /.box-body -->
                                
                               
                               
                            </div><!-- /.box -->
                        </div><!-- /.col (MAIN) -->
                    </div>
                 

                </section>

</aside>

<?php
	include_once("footer.php"); 
?>