<?php if(count($foldertreearr)>0){/*echo'<pre>'; print_r($foldertreearr);*/?>

<table class="table table-responsive" id="foldertree">
							<thead>
								<tr>
									<th>Name</th>
									<th>Permission</th>
									<th>Last Modified</th>
								</tr>
							</thead>
							<tbody>
									<?php 
										$idArray = array();
										foreach($foldertreearr as $folderarr){
										$type=$folderarr['type'];
										$levelarr=explode("-", $folderarr['htmlid']);
										$level=count($levelarr);
										$permstr="";
										$permidstr="";
										
										if(isset($folderarr['permissions']))
										{
											foreach($folderarr['permissions'] as $permarr)
											{
												$perFlag = 0;	
												if($permstr!="")
												{
												   $permstr=$permstr." / ";
												   //$permidstr=$permidstr.",";
												}
												$permstr=$permstr.$permarr['name'];
												//$permidstr=$permidstr.$permarr['permissionid'];
												if($permarr['permissionid'] == 1)
												{
													$perFlag = 1;
												}
											}
										}
									if($folderarr['htmlparentid']==0)
									{
										if($perFlag == 1)
										{
											array_push($idArray, $folderarr['id']);
										}
										
										?>
									<tr data-tt-id='<?php echo $folderarr['htmlid'];?>' dbid='<?php echo $folderarr['id'];?>' level="<?php echo $level;?>" permstr="<?php echo $permstr;?>"><td><span class='<?php echo $folderarr['type'];?>'><?php echo stripslashes($folderarr['label']);?></span></td>
									<?php if($type=="folder" && $folderarr['accesstype']!="public"){?>
									<!--<td><?php // echo $permstr;?></td>-->
									
									
									<td><a title="" rel='popover' title='Popover title'  
										        data-container='body' data-toggle='popover' data-placement='right' 
									                data-content='<?php echo nl2br($permstr);?>'><img src="<?php echo base_url()?>/padlock.png" width="20px" height="20px"></a></td>
									
									<?php }
									else {
										?>
										<td>&nbsp;</td>
										<?php }?>
									<td><?php echo $folderarr['updatedon'];?></td>	
									
									
									
									<td>
									
									<a href="#" title="Download File" id="downloadlink"><i class="fa fa-download" style="font-size: 1.5em;display:none;"></i></a>
					                                &nbsp;
					                                
					                                <a href="#" title="Delete" id="deletelink"><i class="fa fa-trash-o" style="font-size: 1.5em;display:none;"></i></a></td>
									
									
																	
									</tr>
									<?php }else{
										$displayDownload = "";
										$permissionArray = explode('-', $folderarr['htmlparentid']);
										
										if(in_array($permissionArray[0],$idArray))
										{
											$displayDownload = "display:none";
										}
									?>
									 <tr data-tt-id='<?php echo $folderarr['htmlid'];?>' data-tt-parent-id='<?php echo $folderarr['htmlparentid'];?>' dbid='<?php echo $folderarr['id'];?>' level="<?php echo $level;?>" permstr="<?php echo $permstr;?>"><td><span class='<?php echo $folderarr['type'];?>'><?php echo stripslashes($folderarr['label']);?></span></td>
									 <?php if($type=="folder" && $folderarr['accesstype']!="public"){?>
									
									
									<td><a title="" rel='popover' title='Popover title'  
										        data-container='body' data-toggle='popover' data-placement='right' 
									                data-content='<?php echo nl2br($permstr);?>'><img src="<?php echo base_url()?>/padlock.png" width="20px" height="20px"></a></td>
									
									<?php }
									else {
										?>
										<td>&nbsp;</td>
										<?php }?>
									<td><?php echo $folderarr['updatedon'];?></td>
									
									
									
									<td>
									<?php if($type=="file") { ?>
											<span style="margin-right: 3px;visibility: hidden;<?php echo $displayDownload;?>">&nbsp;</span>
											<a href="javascript:downloadfile()" title="Download File" id="downloadlink" style="color:black;<?php echo $displayDownload;?>"><i class="fa fa-download" style="font-size: 1.5em;"></i></a>                           &nbsp;
											
					               	<?php } else { ?>
									
									<span style="margin-right: 3px;visibility: hidden;<?php echo $displayDownload;?>">&nbsp;</span>
									
									<?php }?>
									
									
					                <a href="javascript:deleteitem()" title="Delete" id="deletelink" style="color:black;"><i class="fa fa-trash-o" style="font-size: 1.5em;"></i></a></td>
									 </tr>
									 <?php }}?>											
								
					
							</tbody>
						</table>
						<?php } else{?>
						<center><h3>No Data Found</h3></center>
						<?php }?>
<script>
var expandedenodes="<?php echo $expandednodes;?>";
$("#foldertree").treetable({ 
	expandable: true,
	 onNodeExpand: function() {
		var node = this;
		
		if(expandedenodes=="")
			expandedenodes=node.id;
		else
			expandedenodes=expandedenodes+","+node.id;

			$("#expandednodes").val(expandedenodes);
	}
	 });

if(expandedenodes!="")
{
	var expnodeArray = expandedenodes.split(',');
	for (var i = 0; i < expnodeArray.length; i++) {
		$("#foldertree").treetable("expandNode", expnodeArray[i]);       
      }
}

$("#foldertree tbody").on("mousedown", "tr", function() {
	  $(".selected").not(this).removeClass("selected");
	  $(this).toggleClass("selected");
	  $("#selectedfolderid").val($(this).attr("dbid"));
	  $("#selectedfoldername").val($(this).find('td:first').text());
	  $("#selectedtype").val($(this).find('td:first span').next().attr("class"));
	  $("#level").val($(this).attr("level"));
	  $("#selectedfolderperms").val($(this).attr("permstr"));
	 
	 //alert("filename"+$("#selectedfoldername").val().trim());
	  if($("#selectedfoldername").val().trim()=="user" || $("#selectedfoldername").val().trim()=="public")
	  {
		 // alert('in true');
		  $("#deletelink").attr('disabled','disabled');
	  }
	  else
	  {
		  $("#deletelink").removeAttr('disabled');
	  }

	  if($("#selectedtype").val()=="folder ui-draggable")
	  {
		  $("#downloadlink").attr('disabled','disabled');
		  $("#addfolder").removeAttr('disabled');
		  $("#uploadfile").removeAttr('disabled');
	  }
	  else
	  {
		  $("#downloadlink").removeAttr('disabled');
		  $("#addfolder").attr('disabled','disabled');
		  $("#uploadfile").attr('disabled','disabled');
	  }
	 });
	 
	 
	 
	 
	 
	 
	 $("#foldertree .file,#foldertree .folder").draggable({
	  helper: "clone",
	  opacity: .75,
	  refreshPositions: true,
	  revert: "invalid",
	  revertDuration: 300,
	  scroll: true
	});
	
	
	
	$("#foldertree .folder").each(function() 
	{
		$(this).parents("#foldertree tr").droppable({
			accept: ".file, .folder",
			drop: function(e, ui){

				var droppedEl = ui.draggable.parents("tr");
				var source = droppedEl.data("ttId").toString();
				var destination = $(this).data("ttId").toString();

				$("#selectedfolderperms").val($(this).attr("permstr"));

				if(source.indexOf('-') > 0) 
				{
					$("#percentage").html('Please wait...');
					$("#progressbar").css("width","100%");

					var companyid = <?php echo $this->session->userdata("companyid");?>;
					var mainFloderId;
					if(source.indexOf('-') > 0) //For SubFolder
					{
						var sourceArray = source.split('-');
						var sourcelength = sourceArray.length;
						var sourceID = sourceArray[sourcelength-1];
					}
					else
					{
						var sourceID = source;
					}

					/**
					Add Below ajax for getting folder permission
					**/

					mainFloderId = sourceArray[0];
					/* $.post(baseUrl+'index.php/companyuser/getFolderPermission',{mainFloderId:mainFloderId},function(data)
					{
					if(data == 1)
					{
					alert("<?php echo 'You currently do not have access to move folder';?>");
					return false;
					}

					/*****/

					if(destination.indexOf('-') > 0) 
					{
						var destinationArray = destination.split('-');
						var destinationlength = destinationArray.length;
						var destinationID = destinationArray[destinationlength-1];
						var destinationParent = destinationArray[0];
					}
					else
					{
						var destinationID = destination;
						destinationParent = destination;
					}
					
					if(droppedEl.find('td:first span').next().attr("class") == 'file ui-draggable')
					{
						if(validatePermission("movefile")) 
						{
							$("#loaderModal").reveal();
							$.post(baseUrl+'index.php/companyuser/moveFile',{movefileid:sourceID,destinationfolderid:destinationID,companyid:companyid},function(data){
								
								if(data == '1')
								{
									var expandednodesstr=destinationParent;
									$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
										$("#tablewidget").html('');
										$("#tablewidget").html(data);
										$("#loaderModal").trigger('reveal:close');
									});		
								}
								else if(data == '0')
								{
									$("#loaderModal").trigger('reveal:close');
									alert('Some unexpected error occured');
								}
								else if(data == 'fileExists')
								{
									$("#loaderModal").trigger('reveal:close');
									alert('File already exits in folder');
								}
								else
								{
									$("#loaderModal").trigger('reveal:close');
									alert('Some unexpected error occured');
								}			
							});
						}
						else
						{
							$("#loaderModal").trigger('reveal:close');
							alert("You currently do not have access to move folder");
						}
					}
					else
					{
						if(validatePermission("movefile")) 
						{
							$("#loaderModal").reveal();
							$.post(baseUrl+'index.php/companyuser/moveFolder',{movefolderid:sourceID,destinationfolderid:destinationID,companyid:companyid},function(data){
								if(data == '1')
								{
									var expandednodesstr=destinationParent;
									$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
										$("#tablewidget").html('');
										$("#tablewidget").html(data);
										$("#loaderModal").trigger('reveal:close');
									});
								}
								else if(data == '0')
								{
									$("#loaderModal").trigger('reveal:close');
									alert('File already exits in folder');
								} 
								else
								{
									$("#loaderModal").trigger('reveal:close');
									alert('Some unexpected error occured');
								}
							});
						}
						else
						{
							$("#loaderModal").trigger('reveal:close');
							alert("You currently do not have access to move folder");		
						}
					}
				}
				// $("#foldertree").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
			},
			hoverClass: "accept",
			over: function(e, ui) 
			{
				var droppedEl = ui.draggable.parents("tr");

				if(this != droppedEl[0] && !$(this).is(".expanded")) 
				{
					$("#foldertree").treetable("expandNode", $(this).data("ttId"));
				}
			}
		});
	});	 
</script>




<script type="text/javascript">
$(function () 
      { $("[data-toggle='popover']").popover({ trigger: "hover" });;
      });
   
</script>