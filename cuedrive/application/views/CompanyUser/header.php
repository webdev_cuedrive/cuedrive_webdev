<?php
	$this->load->helper('url'); 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>cuedrive :: Admin Panel</title>
 <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon"  href="<?php echo base_url()?>fav.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/bootstrap.css">   
<!--<link rel="stylesheet" type="text/css" href="<?php //echo base_url()?>theme/stylesheets/ionicons.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo base_url()?>theme/stylesheets/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>theme/stylesheets/reveal.css">
    
    <!-- Chat libraries -->
 
    <script src="<?php echo base_url()?>theme/js/jquery-1.7.2.min.js" type="text/javascript"></script> 

   <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.0/themes/smoothness/jquery-ui.css'>
   <!-- <link rel='stylesheet' href='https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'> -->
    <script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js'></script>
    <script src='<?php echo base_url()?>chat1/strophe.js'></script>
    <script src='<?php echo base_url()?>chat1/flXHR.js'></script>
    <script src='<?php echo base_url()?>chat1/strophe.flxhr.js'></script>
    <script src='<?php echo base_url()?>chat1/ui.tabs.closable.min.js'></script>

    <link rel='stylesheet' href='<?php echo base_url()?>chat1/gab.css'>
    <script src='<?php echo base_url()?>chat1/gab.js'></script>
  
    <!-- Chat libraries end -->
    
  


    
    <script src="<?php echo base_url()?>theme/js/app.js" type="text/javascript"></script>
 
    <script type="text/javascript" src="<?php echo base_url()?>theme/js/validate.js"></script>


    <!-- Demo page code -->

    <style type="text/css">

        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
        
        
        
  #slidebox{
    width:400px;
    height:100px;
    padding:10px;
    background-color:#fff;
    border-top:3px solid #E28409;   
    position:fixed;
    bottom:5px;
    right:-430px;
	-moz-box-shadow:-2px 0px 5px #aaa;
	-webkit-box-shadow:-2px 0px 5px #aaa;
	box-shadow:-2px 0px 5px #aaa;
}
#slidebox p, a.more{
    font-size:11px;
    text-transform:uppercase;
    font-family: Arial,Helvetica,sans-serif;
    letter-spacing:1px;
    color:#555;
}
a.more{
    cursor:pointer;
    color:#E28409;
}
a.more:hover{
    text-decoration:underline;
}
#slidebox h2{
    color:black;
    font-size:18px;
    margin:10px 20px 10px 0px;
}
a.close{
   /*background:transparent url(../images/close.gif) no-repeat top left;*/
    width:13px;
    height:13px;
    position:absolute;
    cursor:pointer;
    top:10px;
    right:10px;
}
a.close:hover{
    background-position:0px -13px;
}
        
        
        
        
    </style>
    
    
    
 <style>
    
.loading {
  display: inline-block;
  margin: 5em;
  border-width: 30px;
  border-radius: 50%;
  -webkit-animation: spin 1s linear infinite;
     -moz-animation: spin 1s linear infinite;
       -o-animation: spin 1s linear infinite;
          animation: spin 1s linear infinite;
  }

.style-1 {
  
  }

.style-2 {
  
  }

.style-3 {
  border-style: double;
  border-color: #444 #fff #fff;
  }

@-webkit-keyframes spin {
  100% { -webkit-transform: rotate(359deg); }
  }

@-moz-keyframes spin {
  100% { -moz-transform: rotate(359deg); }
  }

@-o-keyframes spin {
  100% { -moz-transform: rotate(359deg); }
  }

@keyframes spin {
  100% {  transform: rotate(359deg); }
  }
  
  
  
#floatdiv {
    display:none;
}

/* if screen size gets wider than 1024 */

@media screen and (max-width:1000px){
    #floatdiv {
        display:block;
    }
}
  
  
  
 </style>
    
    
    
    
    
<script type="text/javascript">

var message="Sorry, right-click has been disabled";

function clickIE() {if (document.all) {(message);return false;}}
function clickNS(e) {if
(document.layers||(document.getElementById&&!document.all)) {
if (e.which==2||e.which==3) {(message);return false;}}}
if (document.layers)
{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
document.oncontextmenu=new Function("return false")

</script> 
    
    
    
    
    
    
    
    
    
    
    
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
	<script>
		var baseURL = "<?php echo base_url()?>";
	</script>
   <script src="<?php echo base_url()?>theme/js/jsfunctions.js" type="text/javascript"></script>
  <body class="skin-blue"> 
  <!--<![endif]-->
    
     <header class="header">
            <a href="<?php echo base_url()?>index.php/" class="logo">
              <img src="<?php echo base_url()?>/cuedriveLogowhite.png"   width="150">
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
            
             <?php
		if($this->session->userdata("companyid")) 
		{	
	    ?>
              <img src="<?php echo base_url()?>/back_iPad.png" id="back" style="margin-top:15px;margin-left:8px;">
              
            <?php
	        }
	    ?>  
            
            
            
	       <a role="button" data-toggle="offcanvas" class="navbar-btn sidebar-toggle" href="#" id="floatdiv">
                   
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a> 
            
            
            
            <div class="navbar-right">
            
                <ul class="nav navbar-nav  pull-right">
               
               
        		<?php
		if($this->session->userdata("companyid")) 
		{	
			?>	
			 <li> <a href="#">Welcome <?php echo $this->session->userdata("username");?>	</a></li>
                  <li><a  href="<?php echo base_url()?>index.php/logout" id="logout">Logout</a></li>
		<?php
		}
		?>	
		</ul>
		
	</nav>
   </header>
        
    
    