<?php
include_once("header.php"); 
include("sidebar.php");

   if(strpos($_SERVER['HTTP_USER_AGENT'],"Chrome") > -1){
	echo '<script src="'.base_url().'theme/js/companyuserfolderupload.js"></script>';
    }
   else {
        echo '<script src="'.base_url().'theme/js/companyuserfileupload.js"></script>';
    }



if(!$this->session->userdata("companyid")) 
{
	header ("Location:".base_url()."index.php/login");
	exit;
}
	$this->load->helper('form');
	$addfolderbutton	=array("name" => "addfolder","id" => "addfolder","class" => "btn btn-primary");
	$uploadbutton		=array("name" => "uploadfile","id" => "uploadfile","class" => "btn btn-primary");
    $useridd 			= $this->session->userdata("userid");
    $userdetails 		= Companyusermodel::getUserDetails($useridd);
    $username 			= $this->session->userdata("username");
    $service_url 		= ORG_URL."chat/?type=fetch&username=".$username;
    $curl 				= curl_init($service_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response 		= curl_exec($curl);
	if ($curl_response === false) {
			$info 	= curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
	curl_close($curl);
	//echo $curl_response;
    $response 		= json_decode($curl_response);
    $jidpassword 	= $response[0]->{'plainPassword'};
    $jid 			= $response[0]->{'username'}."@cuedrive.com.au";
?>

<input type = "hidden" id = "jid" value = "<?php echo $jid?>">
<input type = "hidden" id = "jidpassword" value = "<?php echo $jidpassword?>">
<div class="inner-div" style="padding:10px;">
  <aside class="right-side" style="padding:15px;"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Manage Folder </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Folder</a></li>
        <li class="active">Manage Folder</li>
      </ol>
    </section>
    <section class="content invoice">
      <?php 
		$js = 'onClick="addfolderclick()"';
		echo form_button($addfolderbutton, 'Create Folder',$js); 
	?>
      <?php 
		$jsfile = 'onClick="uploadfileclick()"';
		echo form_button($uploadbutton, 'Upload',$jsfile); 		 
	?>
      <span id="mainloadingdiv" style="display:none;"><!--<image src="<?php echo base_url()?>theme/images/loading.gif"/>--></span> 
      <!-- Add new folder div --> 
      <!-- &nbsp;<span id="inputdiv" style="display:none;border: 1px solid;padding:13px;">
		Enter Folder Name &nbsp;<input id="txtFolderName1" name="txtFolderName1" type="text" />
		<input type="radio" name="radioperm" value="parent">Set default parent folder permission
		<input type="radio" name="radioperm" value="later" checked>Set permissions manually later
		<a href="javascript:addfolder();"><i class="fa fa-check-circle" style="font-size: 1.5em;"></i></a>
		<a href="javascript:closefolder();"><i class="fa fa-times" style="font-size: 1.5em;"></i></a>			
		</span> --> 
      
      <!-- Upload new file div -->
      <div id="uploadModal" class="reveal-modal">
        <h3 style="margin-top: 0;padding-left:10px; text-align: left;"> <?php echo UPLOADFILE; ?> </h3>
        <label id="errmsgfile" style="color:red; font-weight:normal;"></label>
        &nbsp; <span id="inputfilediv" style="display:none;border: 1px solid;padding:13px;">
        <form id="fileinfo" enctype="multipart/form-data" method="post" name="fileinfo" class="navbar-form">
          <div class="form-group">
            <input id="upfile" name="upfile[]" type="file" multiple/>
          </div>
          <div id="loadingdiv1" style="display:none;">
            <image src="<?php echo base_url()?>theme/images/loading.gif"/>
          </div>
          <div id="uploadbtn" class="form-group"><a href="javascript:uploadfile();"><i class="btn btn-primary btn-sm" style="font-size: 1.0em;">Upload</i></a> 
            <!--<a href="javascript:closefile();"><i class="fa fa-times" style="font-size: 1.5em;"></i></a>--> 
          </div>
        </form>
        </span>
        <div style="margin-top: 10px"> <a class="close-reveal-modal">&#215;</a> </div>
      </div>
      <input type="hidden" name="selectedfolderid" id="selectedfolderid" value=""/>
      <input type="hidden" name="selectedfoldername" id="selectedfoldername" value=""/>
      <input type="hidden" name="selectedtype" id="selectedtype" value=""/>
      <input type="hidden" name="expandednodes" id="expandednodes" value=""/>
      <input type="hidden" name="level" id="level" value=""/>
      <input type="hidden" name="selectedfolderperms" id="selectedfolderperms" value=""/>
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header"> 
            
            <!-- <small class="pull-right">
                                	<a href="#" title="Download File" id="downloadlink"><i class="fa fa-download" style="font-size: 1.5em;"></i></a>
					&nbsp;<a href="#" title="Delete" id="deletelink"><i class="fa fa-trash-o" style="font-size: 1.5em;"></i></a>
                                
                                </small>--> 
          </h2>
        </div>
        <!-- /.col --> 
      </div>
      
      <!-- Folder list -->
      <div id="tablewidget">
        <center>
          <div id="loadingdiv" style="display:none;">
            <image src="<?php echo base_url()?>theme/images/loading.gif"/>
          </div>
        </center>
      </div>
      <!-- Folder list end --> 
      
      <!-- Drag-drop image --> 
      <!--
						<div id="dialog" style="text-align:center;display:none;">
						<image class="img-responsive" src="<?php echo base_url()?>blue-drop.png"/>
						</div>
					-->
      <?php if(strpos($_SERVER['HTTP_USER_AGENT'],"Chrome") > -1){ ?>
      <section id="droparea">
        <div id="">
          <div id="dialog" style="text-align:center;display:none;" dropzone webkitdropzone="copy file:image/png file:image/gif file:image/jpeg">
            <image class="img-responsive" src="<?php echo base_url()?>/blue-drop.png"/>
          </div>
        </div>
      </section>
      <section id="fileinput" style="display: none;">
        <div>
          <input type="file" accept="image/*" multiple>
        </div>
      </section>
      <?php } else {?>
      <div id="dialog" style="text-align:center;display:none;">
        <image class="img-responsive" src="<?php echo base_url()?>/blue-drop.png"/>
      </div>
      <?php } ?>
      
      <!-- Drag-drop image end --> 
      
      <!-- Chat UI -->
      <div id="extruderRight" class="{title:'Chat'}">
        <center id="load">
          <span class="loading style-1"></span> <span class="loading style-2"></span> <span class="loading style-3"></span>
        </center>
        <div id='roster-area' class='ui-menu'>
          <ul>
          </ul>
        </div>
      </div>
      
      <!-- Chat UI end --> 
      
    </section>
  </aside>
</div>
<div id="chatModal" class="reveal-modal">
  <div id='chat-area'>
    <ul>
    </ul>
  </div>
  <div style="margin-top: 10px"> <a class="close-reveal-modal">&#215;</a> </div>
</div>
<div id="myModal" class="reveal-modal">
  <h3 style="margin-top:0px;"> <?php echo CREATEFOLDER; ?> </h3>
  <label id="errmsg" style="color:red;font-weight:normal;"></label>
  <div class="input-group input-group-sm">
    <input type="text" class="form-control" id="txtFolderName" name="txtFolderName" placeholder="Enter Folder Name">
    <span class="input-group-btn">
    <button type="button" class="btn btn-primary btn-flat" onclick="addfolder()">Go</button>
    </span> </div>
  <br>
  <input type="radio" name="radioperm" value="parent">
  &nbsp;&nbsp;&nbsp;&nbsp;Inherit current folder permissions<br>
  <input type="radio" name="radioperm" value="later" checked>
  &nbsp;&nbsp;&nbsp;&nbsp;Set permissions manually later 
  <!-- 		<a href="javascript:addfolder();"></i></a> -->
  
  <div style="margin-top: 10px"> <a class="close-reveal-modal">&#215;</a> </div>
</div>
<div id="loaderModal" class="reveal-modal" style="opacity: 1;    
    bottom: 5%;
    left: 5%;top: unset;margin-left: 0px;">
  <h2 align="center" id="percentage">Please wait...</h2>
  <div class="progress sm progress-striped active">
    <div style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar progress-bar-success" id="progressbar"> <span class="sr-only">20% Complete</span> </div>
  </div>
</div>
<div id="updateprofileModal" class="reveal-modal">
  <div class="">
    <div class="modal-header" style="padding-left:0px;">
      <h4 class="modal-title">Update Profile</h4>
    </div>
    <div id="tablewidget" class="block-body collapse in">
      <label id="errormessage" style="color:red;font-weight:normal;"></label>
      <br>
      <label>Username <span class="text-danger">*</span></label>
      <input type="text" placeholder="Username" class="form-control" name="username" id="username" value = "<?php echo $userdetails['username']?>">
      <input type="hidden" name="userid" id="userid" value="<?php echo  $useridd?>">
      <br>
      <label>Password <span class="text-danger">*</span></label>
      <input type="password" placeholder="Password" class="form-control" name="password" id="password" value = "<?php //echo $userdetails['password']?>">
      <br/>
      <label>Email <span class="text-danger">*</span></label>
      <input type="email" placeholder="Email" class="form-control" name="email" id="email" value = "<?php echo $userdetails['email']?>">
      <br/>
      <button class="btn btn-primary pull-left" type="submit" onclick="updateuserprofile()">Update </button>
      <button class="btn btn-primary pull-right" type="button" onclick="closeupdatemodal()">Cancel</button>
    </div>
    <div style="margin-top: 10px"> <a class="close-reveal-modal">&#215;</a> </div>
  </div>
</div>
<!-- Chat Section -->
<?php
 // include("chatsection.php");
  ?>
<!--start - css and js files for slider window-->
<link href="<?php echo base_url()?>jscript/mbExtruder.css" media="all" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="jquery.js"></script>--> 
<script type="text/javascript" src="<?php echo base_url()?>jscript/jquery.hoverIntent.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>jscript/jquery.mb.flipText.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>jscript/mbExtruder.js"></script> 
<!--end - css and js files for slider window--> 
<!-- Chat Section end --> 
<script>
$(function() {
var height=(document.body.clientHeight-200)/2;
$("#extruderRight").buildMbExtruder({
      position:"right",
      width:300,
      top:height,
      extruderOpacity:.8,
      textOrientation:"tb",
      onExtOpen:function(){},
      onExtContentLoad:function(){},
      onExtClose:function(){}
    });
   
    $("#extruderRight").find(".flapLabel").css({"right":"10px"});
    $("#extruderRight").find(".flip_label").css({"padding":"10px 0px 10px 0px","height":"100px","top":"10px"});
   
   
    $( "#dialog" ).dialog({
		autoOpen: false,
		width:"450px",
		show: {
			effect: "puff",
			duration: 100
		},
		
		close: function() {
  			$(".right-side").css("opacity","1");
		}
	});
});
$("#myButton").click(function () {
      $("#badge").html('0');
});
</script>
<div id="slidebox"> <a class="close"></a>
  <h2></h2>
</div>
<!-- Upload A New File 
-->
<?php
	include_once("footer.php"); 
?>
<link href="<?php echo base_url()?>theme/stylesheets/jquery.treetable.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>theme/stylesheets/jquery.treetable.theme.default.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>theme/js/jquery.treetable.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>theme/js/jquery.reveal.js"></script>
<script>



var baseUrl = "<?php echo base_url();?>";
$(document).ready(function(){

   $("#folderli").attr('class','treeview active');
   $("#foldersubli").attr('class','active');
   $("#folderul").attr('style','display:block');
   
	
	$("#loadingdiv").show();
	$.post(baseUrl+'index.php/companyuser/loadFolderTree',function(data){
		$("#loadingdiv").hide();
		$("#tablewidget").html(data);
		
	});

	
    var isChromium = window.chrome,
    vendorName = window.navigator.vendor;
    if(isChromium !== null && vendorName === "Google Inc.") {
	init();
     }
});



function updateprofile()
{
$("#updateprofileModal").reveal();
$("#errormessage").html('');	
}

function closeupdatemodal()
{
$('#updateprofileModal').trigger('reveal:close');
}

function updateuserprofile()
{
  var username = $("#username").val();
  var userid = $("#userid").val();
  var email = $("#email").val();
  var password = $("#password").val();
  
  if(username=='')
  {
    $("#errormessage").html('Please enter Username');
	return false;
  }
  if(password=='')
  {
    $("#errormessage").html('Please enter Password');
	return false;
  }
  
  if(password!='')
  {
    var pattern = /^(?=.*[a-z])(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;
	if(pattern.test(password))
	 {
		$.post(baseUrl+'index.php/companyuser/updateuserprofile',{username:username,userid:userid,email:email,password:password},function(data)
   		 {
	   if(data == '1')
	   {
		 alert('Profile has been updated successfully');
		 closeupdatemodal();
	   }
	   else 
	   {
	   $("#errormessage").html(data);
	   }
	  });
	 
	 }else{
	 	 $("#errormessage").html('<?php echo PASSWORDWITHSPCCHAR; ?>');	
	     return false
		 }
  }
  if(email=='')
  {
    $("#errormessage").html('Please enter Email address');
	return false;
  }
}


function validatePermission(action)
{
	if(action=="addfolder" || action=="uploadfile")
	{
         var folderpermarr = $("#selectedfolderperms").val().split(' / ');
         for(i=0;i<folderpermarr.length;i++)
         { 
			console.log(folderpermarr[i]);
             //if(folderpermarr[i] == "Cannot Write")
			if(folderpermarr[i] == "Write")	 
             {
				 console.log(folderpermarr[i]);
                  return true;
             }
         }
         return false;
	}
	else if(action=="delete")
	{
		 var folderpermarr = $("#selectedfolderperms").val().split(' / ');
        for(i=0;i<folderpermarr.length;i++)
        { 
            if(folderpermarr[i]=="Delete")
            {
                 return true;
            }
        }
        return false;
	}
	else if(action=="movefile")
	{
		 var folderpermarr = $("#selectedfolderperms").val().split(' / ');
        for(i=0;i<folderpermarr.length;i++)
        { 
            if(folderpermarr[i]=="Cannot Write" || folderpermarr[i]=="ReadOnly")
            {
                 return true;
            }
        }
        return false;
	}

}

/* Add folder*/
function addfolderclick()
{
	 
	if($("#selectedfolderid").val()!="")
	{
	

		if(validatePermission("addfolder"))
		{ 
		         
			//$("#uploadfile").attr('disabled','disabled');
	               //$("#inputdiv").show();
	               $("#errmsg").html('');
	                $("#txtFolderName").val('');
	                $("#myModal").reveal();	

		}
		else
		{
			alert("<?php echo 'You currently do not have access to add folder';?>");	
			//$("#errmsg").html("<?php echo NOPERMISSION;?>");
		}
	}
	else
	{
		alert("<?php echo NOPARENTFOLDER;?>");	
		//$("#errmsg").html("<?php echo NOPERMISSION;?>");
	}
	
}

function addfolder()
{
	var expandednodesstr=$("#expandednodes").val();
	
	if($("#selectedfolderid").val()=="")
	{
		//alert("<?php echo NOPARENTFOLDER;?>");
		$("#errmsg").html("<?php echo NOPARENTFOLDER;?>");				
	}
	else
	{
		var parentfolder=$("#selectedfoldername").val();
		var parentfolderid=$("#selectedfolderid").val();
		var level=$("#level").val();
		if (true) { 
			var newfoldername=$.trim($("#txtFolderName").val());
			var permflag=$("input:radio[name=radioperm]:checked").val();
			if(newfoldername=="")
			{
				//alert("<?php echo NOFOLDERNAME;?>");
				$("#errmsg").html("<?php echo NOFOLDERNAME;?>");	
			}
			else
			{
				//upload folder
				
				$.post(baseUrl+'index.php/companyuser/addFolder',{parentfolderid:parentfolderid,newfoldername:newfoldername,level:level,permflag:permflag},function(data){
				
				if(data=="1")
					{
					$('#myModal').trigger('reveal:close');
					$("#loadingdiv").show();
						$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
							$("#loadingdiv").hide();
							closefolder();
							$("#tablewidget").html('');
							$("#tablewidget").html(data);
						});
					}
				else
				{
					alert(data);
				}
				});
			}
		
		}
	}
}

function closefolder()
{
	$("#uploadfile").removeAttr('disabled');
	$("#txtFolderName").val('');
	$("#inputdiv").hide();	
}

/* Upload File*/
function uploadfileclick()
{
	
	if($("#selectedfolderid").val()!="")
	{
		if(validatePermission("uploadfile"))
		{
			//$("#addfolder").attr('disabled','disabled');
	                $("#inputfilediv").css( "display", "inline-block");
	                //$("#inputfilediv").show();
	                $("#uploadbtn").show();	
	                $("#uploadModal").reveal();	
		}
		else
		{
			alert("<?php echo 'You currently do not have access to upload in this folder';?>");	
		}
	}
	else
	{
		alert("<?php echo NOPARENTFOLDER;?>");	
	}

}

function uploadfile()
{
	
	if($("#selectedfolderid").val()=="")
	{
		//alert("<?php echo NOPARENTFOLDER;?>");	
		$("#errmsgfile").html("<?php echo NOPARENTFOLDER;?>");				
	}
	else
	{
		var parentfolder=$("#selectedfoldername").val();
		var parentfolderid=$("#selectedfolderid").val();
		var filenamestr="";
		//if (confirm("Do you want upload file in "+parentfolder+" ?")) { 
		if (true) { 
			 var files = $("#upfile")[0].files;
			// Loop through each of the selected files.
		     for (var i = 0; i < files.length; i++) {
		       var file = files[i];
		       // Add the file to the request.
		      // fd.append('selectedfiles[]', file, file.name);
		      if(filenamestr=="")
		    	  filenamestr=file.name;	
		      else
		      	  filenamestr=filenamestr+"@#"+file.name;		      
		     }
			//var newfilename=$("#upfile").val();
			if(filenamestr=="")
			{
				//alert("<?php echo NOFILESELECTED;?>");
				$("#errmsgfile").html("<?php echo NOFILESELECTED;?>");	
			}
			else
			{
				
				$.post(baseUrl+'index.php/companyuser/checkFileExist',{parentfolderid:parentfolderid,filenamestr:filenamestr},function(data)
				{
				
				if(data=="") //not exist
				{	
					uploadFileAjax(parentfolderid,"false");
				}
				else //exist
				{					
					if (confirm(data+" already exist.Do you want replace it?")) { 
						uploadFileAjax(parentfolderid,"true");
					}
				}
				});
			}
			
		}
	}
}

function uploadFileAjax(parentfolderid,override)
{
	
	
	$("#progressbar").css("width","0%");
        $('#uploadModal').trigger('reveal:close');
        $("#loaderModal").reveal();
	$(".reveal-modal-bg").css("position", "static"); 
	var expandednodesstr=$("#expandednodes").val();
	// var fd = new FormData(document.getElementById("fileinfo"));
	 var fd = new FormData();
	 var files = $("#upfile")[0].files;
     fd.append("parentfolderid", parentfolderid);
     fd.append("override", override);
  // Loop through each of the selected files.
     for (var i = 0; i < files.length; i++) {
       var file = files[i];
       // Add the file to the request.
      // fd.append('selectedfiles[]', file, file.name);
       fd.append("selectedfiles[]", file);
     }
     
     $("#mainloadingdiv").show();
     //return false;
     $.ajax({
     
         xhr: function()
	  {
	    var xhr = new window.XMLHttpRequest();
	    //Upload progress
	    xhr.upload.addEventListener("progress", function(evt){
	      if (evt.lengthComputable) {
	        var percentComplete = (evt.loaded / evt.total) * 100;
	            percentComplete = percentComplete.toFixed();
	        $("#percentage").html(percentComplete + '%');
	        $("#progressbar").css("width",percentComplete+"%");
	      }
	    }, false);
	    return xhr;
	  },
     
         url: baseUrl+'index.php/companyuser/uploadFile',  
         type: 'POST',
         data: fd,
         success:function(data){
                    	if(data=="success")
					{
		        // $('#uploadModal').trigger('reveal:close');
                    	 $("#mainloadingdiv").hide();
                    	 
                    	                        $("#uploadbtn").hide();
						
						
							$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
								
								$("#loadingdiv1").hide();
								$('#loaderModal').trigger('reveal:close');
								closefile();
								$("#tablewidget").html('');
								$("#tablewidget").html(data);
							});
					}
		        	else
		        	{
			        	alert(data);
		        	}
             }, 
         cache: false,
         contentType: false,
         processData: false
     });
}
function closefile()
{
	$("#addfolder").removeAttr('disabled');
	$("#upfile").val('');
	//$("#inputfilediv").hide();	
	$('#uploadModal').trigger('reveal:close');
}

/* Download file*/
function downloadfile() {
//$('#downloadlink').click( function( e ) {

	if($("#downloadlink").attr('disabled'))
	{
		 e.preventDefault();
		return false;
	}
	var selectedfile=$("#selectedfoldername").val();
	var selectedfileid=$("#selectedfolderid").val();
	var selectedtype=$("#selectedtype").val();
	if(selectedfile=="")
	{
		alert("<?php echo NOFILESELECTED;?>");				
	}

	else
	{
		//download file		
		$.post(baseUrl+'index.php/companyuser/downloadFile',{selectedfileid:selectedfileid},function(data){
			//window.location.href = data;
			window.open(data);
		});
	}
//});
}

/* Delete File*/
// $('#deletelink').click( function( e ) {
function deleteitem() {
	 var expandednodesstr=$("#expandednodes").val();
	if($("#deletelink").attr('disabled'))
	{
		 e.preventDefault();
		return false;
	}

	var selected=$("#selectedfoldername").val();
	var	countword = selected.split(' ').length;
	if(countword > 1)
	  {
	 	selected = selected;	
	  }else{
		 	selected = selected.replace(/\s/g,'');
		   }
	var selectedid=$("#selectedfolderid").val();
	var selectedtype=$("#selectedtype").val();
	if(selected=="")
	{
		alert("<?php echo NOFILEFOLDERSELECTED;?>");				
	}
	else if(validatePermission("delete"))
	{
	
	if(selectedtype=="file ui-draggable")//delete file
	{
		if (confirm("Are you sure you want to delete '"+selected+"'?")) {
		
		     $("#percentage").html('Please wait...');
		     $("#progressbar").css("width","100%");
		     $("#loaderModal").reveal();	
			//delete File			
			$.post(baseUrl+'index.php/companyuser/deleteFile',{selectedid:selectedid},function(data){
			if(data=="1")
				{
				
				$("#loadingdiv").show();
					$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
						$("#loadingdiv").hide();
						$("#tablewidget").html('');
						$("#tablewidget").html(data);
						$("#loaderModal").trigger('reveal:close');
					});
				}
			else //delete folder
			{
				alert(data);
			}
			});
		}
	}
	else if(selectedtype=="folder ui-draggable") //delete folder
	{
		if (confirm("Are you sure you want to delete '" +selected+ "' and its contents?")) {
			//delete folder	
			$("#percentage").html('Please wait...');
		        $("#progressbar").css("width","100%");	
			$("#loaderModal").reveal();
			$.post(baseUrl+'index.php/companyuser/deleteFolder',{selectedid:selectedid},function(data){
			if(data=="1")
				{
				
				$("#loadingdiv").show();
					$.post(baseUrl+'index.php/companyuser/loadFolderTree',{expandednodesstr:expandednodesstr},function(data){
						$("#loadingdiv").hide();
						$("#tablewidget").html('');
						$("#tablewidget").html(data);
						$("#loaderModal").trigger('reveal:close');
					});
				}
			else //delete folder
			{
				alert(data);
			}
			});
			
		}
	}
	}
	else
	{
	alert("<?php echo 'You currently do not have access to delete this folder';?>");
	
	}
}
</script>