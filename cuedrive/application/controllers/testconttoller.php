<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testconttoller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('companyusermodel');	
		$this->load->model('invoice');	
		$this->load->helper('url');
	}

	function index()
	{
		$this->load->helper('url');
		$this->load->view('testuploadform');
		//$this->load->view('testupdateform');
	}
	
	
	
	
	function generateInvoice()
	{
	$invnumber='INV-0003';
	$date = '2014-03-11';
	$accounttype = 'basic';
	$PACKAGEPAY = '21.95';
	$DEVICEPAY='50.00';
	Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		 Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
	
	
	}
	
	
	
	function cronEmail()
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to('debartha.nandi@teks.co.in');
       
        
        $this->email->subject('cuedrive cron email');
       
        
        $message = "Hi <br><br><br>
                    Regards,<br>
                    cuedrive Team";
     
        $this->email->message($message);

        $this->email->send();
   
     }
	
	
	function saveChat()
	{
		$sender = $_POST['sender'];
		$message = $_POST['message'];
		$companyid = $this->session->userdata('companyid');
		$companyuserid = ($this->session->userdata('userid')) ? $this->session->userdata('userid') : 0;
		$reciever = $_POST['reciever'];
		$datacc = array();
		$datacc['sender'] = $sender;
		$datacc['message'] = $message;
		$datacc['companyid'] = $companyid;
		$datacc['companyuserid'] = $companyuserid;
		$datacc['reciever'] = $reciever;
		
		$this->db->insert('chat',$datacc);
	}
	
	function fetchChat()
	{
		$companyid = $this->session->userdata('companyid');
		$companyuserid = ($this->session->userdata('userid')) ? $this->session->userdata('userid') : 0;
		
		$sql = "select sender,message from chat where companyid = {$this->db->escape($companyid)} and companyuserid = {$this->db->escape($companyuserid)} order by id desc limit 10";
		$query = $this->db->query($sql);
		$query = $query->result_array();
		echo json_encode($query);
	}
	
	
	
	
	function uploadfilee()
	{
		
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
        $this->load->model('classpush');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$_REQUEST['deviceid'];
	$sessiontoken=$_REQUEST['sessiontoken'];
	$uploadfolderid=$_REQUEST['uploadfolderid'];
	
	$fileName = $_FILES['uploadedfile']['name'];
    $fileTempName = $_FILES['uploadedfile']['tmp_name'];
	
    
	//optional request parameters
	$username=isset($_REQUEST['username'])?$_REQUEST['username']:"";
	$password=isset($_REQUEST['password'])?$_REQUEST['password']:"";
	$userauth=false;
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		// $this->response($devauthresult,400);
		echo json_encode($devauthresult);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
			else {//user authenticated
				$userauth=true;
				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth)
		{
			//$this->response($usrauthresult,400);
			echo json_encode($usrauthresult);
		}
		else {	
			
			$companyid=$devauthresult['companyid'];
			$bucketname="cuedrive.".$companyid;
			//check bucket size validation
			$bucketsize=$this->getBucketSize($bucketname)+$_FILES["uploadedfile"]["size"]; //in bytes
			$companypackagesize=$this->Companyadminmodel->getPackageSize($companyid);//in GB
			$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
			//$companypackagesize=1024*(str_replace("KB","",$companypackagesize)); //in bytes for testing
			if($bucketsize<=$companypackagesize)
			{
				$replaceflag=false;
				if($this->isFileExist($fileName, $uploadfolderid) )
				{
					if(($_REQUEST['override']) )
					{
						if($_REQUEST['override']=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=false;
						}
					}
					else {
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
				
				if($uploadflag)
				{
			
				$uploadfolderpath=$this->foldermodel->getFolderPath($uploadfolderid);
				$foldername=$uploadfolderpath;
				if($this->s3->putObjectFile($fileTempName, $bucketname, $foldername.'/' .$fileName, S3::ACL_PRIVATE))
				{
					//file uploaded successfullly
					//update DB
					
				    $datacc=array();
				    $datacc['filename']=$fileName;
				    $datacc['fullpath']=$foldername.'/' .$fileName;
				    $datacc['folderid']=$uploadfolderid;
				    $datacc['filesize']=$_FILES["uploadedfile"]["size"];
				    
				    
				    
				    $activitydetails = 'uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
				    if($username!="")
				    {
				     $activitydetails = $username.' '.'has uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
				    }
				    
				    
				    
				    
				    $logcc['deviceid'] = $deviceid;
                                    $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				    $logcc['action'] = 'upload';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']  = '-';
				    $logcc['from']  = '-';
					   
					   
					 
				    
				    $newfileid = 0;
				    
				    
				    
				  
				    if($replaceflag)//update existing
				    {	
				    	 $datacc['updatedbydevice']=$deviceid;
					     if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['updatedbyuser']=$companyuserid;
					    }		
				    	$replacefileid=$this->folderfilemodel->getfileID($fileName,$uploadfolderid);	     
				    	$this->folderfilemodel->updateFile($datacc,$replacefileid);	
				    }
				    else //add new
				    {
				    	$datacc['createdbydevice']=$deviceid;
					    	  if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['createdbyuser']=$companyuserid;
					    	 $logcc['companyuserid'] = $companyuserid;
					    }				   
				   		 $datacc['createdon']=date('Y-m-d H:i');	
				    	$newfileid=$this->folderfilemodel->insertFile($datacc);	
				    }
                                          Classpush::sendFileUploadNotification($uploadfolderid,'upload');
				    	  $this->auditlogmodel->updateAuditLog($logcc);	
                                          	    				
					echo json_encode(array('success' => APIFILEUPLOADSUCCESS,'fileID' => $newfileid));
	
				}	
				else {
					echo json_encode(array('error' => APIFILEUPLOADFAIL), 400);
				}	
			}//duplicate file validation
			else {
				echo json_encode(array('error' => APIFILEEXIST), 400);
			}
		}//end of if size validation
		else {
			//$this->response(APIFILESIZEEXCEED,400);
			echo json_encode(array('error' => APIFILESIZEEXCEED), 400);
		}
		}
	}
	
	}
	
function uploadFilesFolder()
{
if(isset($_FILES['image'])) {
	 if(is_uploaded_file($_FILES['image']['tmp_name']))
	 {
		$uploaddir = realpath('./uploads/');
		$fname=time().".jpeg";
		$uploadfile = $uploaddir ."/". $fname;
		if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
				
			 $imgurl='upload/'.$fname;
			 echo json_encode(array('img' => $imgurl));
					
		}
		else
		{
			 $imgurl = 0;
			 echo json_encode(array('img' => $imgurl));
		}
	  }
}
else {
 echo json_encode(array('img' => 'error'));
}
}
function info()
{
echo phpinfo();

}


function data()
{
echo $_POST['val']."<br>";
echo $_POST['val1'];

}

	
}