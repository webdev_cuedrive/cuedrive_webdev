<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session'); 
		$this->load->model('transactionmodel');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('companyadminmodel');
		$this->load->model('packagemodel');
		$this->load->model('invoice');
		
	}

	function index()
	{
		//echo phpinfo();
		
	}
	
	
	/**
	
	Method to create eway token for a particular customer 
	
	*/
	
	function createTokenCustomer()
	{
		  require 'recurring/createTokenEway.php';
	      $cardnumber = $_POST['cardnumber'];
	      $nameoncard = $_POST['nameoncard'];
	      $expmonth = $_POST['expmonth'];
	      $expyear = $_POST['expyear'];
	      $selectedpkgid = $_POST['selectedpkgid'];
	      $noofdevices  = $_POST['noofdevices'];
	      
	      if($this->session->userdata("companyid")) {
	      
	          $id = $this->session->userdata("companyid");
	          $data = Companyadminmodel::fetchCompanyAdminDetails($id);
	          $userData = array (
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'address' => $data['address'],
	            'state' => $data['city'],
	            'companyname' => $data['name'],
	            'postalcode' => $data['zipcode'],
	            'email' => $data['email'],
	            'cardnumber' => $cardnumber,
	            'nameoncard' => $nameoncard,
	            'expirymonth' => $expmonth,
	            'expiryyear' => $expyear
	            );
	         $token = new CreateTokenEway();
	         $tokenCustomer = $token->create_token_eway($userData);
	         //echo $tokenCustomer;
	         $data = array (
	                 'customerToken' => $tokenCustomer
	         );
	         $this->db->where('companyid',$id);
	         $this->db->update('companyadmin',$data);
	         $updateid = $this->db->affected_rows();
	         if($updateid > 0)
	         {
				$this->doPayment($tokenCustomer,$selectedpkgid,$noofdevices,'INV-001');
				$companyid = $this->session->userdata("companyid");
				$data = array('autopayment' => 'true');
				$this->db->where('companyid',$companyid);
				$this->db->update('companyadmin',$data);
				echo 'success';
	         }
	         else
	         {
	           echo 'error';
	         }
	         
	         
	      }
	      else {
		redirect('login');
	      }

	}
	
	
	function updateCardDetails()
	{
		
		  require 'recurring/createTokenEway.php';
	      $cardnumber = $_POST['cardnumber'];
	      $nameoncard = $_POST['nameoncard'];
	      $expmonth = $_POST['expmonth'];
	      $expyear = $_POST['expyear'];
	      //$selectedpkgid = $_POST['selectedpkgid'];
	      //$noofdevices  = $_POST['noofdevices'];
	      
	      if($this->session->userdata("companyid")) {
	      
	          $id = $this->session->userdata("companyid");
	          $data = Companyadminmodel::fetchCompanyAdminDetails($id);
			  
	          $userData = array (
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'address' => $data['address'],
	            'state' => $data['city'],
	            'companyname' => $data['name'],
	            'postalcode' => $data['zipcode'],
	            'email' => $data['email'],
	            'cardnumber' => $cardnumber,
	            'nameoncard' => $nameoncard,
	            'expirymonth' => $expmonth,
	            'expiryyear' => $expyear
	            );
	         $token = new CreateTokenEway();
	         $tokenCustomer = $token->create_token_eway($userData);
	         
	         $data = array (
	                 'customerToken' => $tokenCustomer
	         );
			 
	         $this->db->where('companyid',$id);
	         $this->db->update('companyadmin',$data);			 
	         $updateid = $this->db->affected_rows();
			 //echo this->db->last_query();
			 //echo '<pre>';print_r($data); echo $id; echo '</pre>';
			 
			//print_r($tokenCustomer);
			//print($this->db->last_query());
			 	
			// echo this->db->last_query();die;
	         if($updateid > 0 || $tokenCustomer != '' || $tokenCustomer != 0)
	         {
				/*
					$this->doPayment($tokenCustomer,$selectedpkgid,$noofdevices,'INV-001');
					$companyid = $this->session->userdata("companyid");
					$data = array('autopayment' => 'true');
					$this->db->where('companyid',$companyid);
					$this->db->update('companyadmin',$data);
					$this->load->view('CompanyAdmin/cardupdatesuccess');
				*/
				echo 'success';
				$data['token'] = $tokenCustomer;
				require 'recurring/GetCardFromTokenEway.php';
				$tokenNew = new GetCardFromTokenEway();
				$tokenCustomerDetails = $tokenNew->get_card_from_token_eway($data);			
				
				//print($tokenCustomerDetails);
				
				//Send email
				$this->load->library('email');
				$config = array (
				  'mailtype' => 'html',
				  'charset'  => 'utf-8',
				  'priority' => '1'
				);
				$this->email->initialize($config);
				$this->email->from('admin@cuedrive.com', 'cuedrive');
				//$this->email->to('sunil.game@aressindia.net');
				$this->email->to($data['email']);
				$this->email->subject('Cuedrive - Amend Account'); 
				
				$message = "Hi ".$this->session->userdata('username').",<br><br><br> This is a courtesy email to inform you that your credit card details have changed.<br/><br/>Your new card number contains the following numbers: ". $tokenCustomerDetails['ccname'] ."<br/><br/>If you did not initiate	this change, please	contact	Cuedrive support.<br/><br/>Kind Regards,<br>Cuedrive team";
			 
				$this->email->message($message);
				$this->email->send();
				echo 'success';
				
	         }
	         else
	         {
	           echo 'error';
	         }
	         
	         
	      }
	      else {
		redirect('login');
	      }

	}
	
	function doPayment($ewaytoken,$selectedpkgid,$noofdevices,$invoice)
	{
	   $customerID = $ewaytoken;
	   $companyid = $this->session->userdata("companyid");
	   /**
	    Calculation of amount already paid in previous transaction
	    */
	   $totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
	   $currentPackageDetails = Companyadminmodel::fetchCurrentPackageID($companyid);
	   $currentPackagePrice = $currentPackageDetails->price;
	   $pricePerDevice = $currentPackageDetails->additionaldeviceprice;
	   $noOfUsedDays = Companyadminmodel::fetchProfileUsedForDays($companyid);
	   
	   //Package
	   $totalAmountToPayInLastTransactionOfPackage = Companyadminmodel::fetchLastTransactionTotalAmountOfPackage($companyid);
	   $amountAlreadyUsedOfPackage = Invoice::calculateAmountAlreadyUsedOfPackage($currentPackagePrice, $noOfUsedDays);
	   
	   //Device
	   $totalAmountToPayInLastTransactionOfDevice = Companyadminmodel::fetchLastTransactionTotalAmountOfDevice($companyid);
	   $amountAlreadyUsedOfDevice = Invoice::calculateAmountAlreadyUsedOfDevice($noOfUsedDays, $pricePerDevice, $totalAllowedDevices);

	   //already paid extra
	   $extraPaidForDeviceAndPackage = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);
	   $amountAlreadyPaidOfPackage = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfPackage, $amountAlreadyUsedOfPackage);
	   $amountAlreadyPaidOfDevice = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfDevice, $amountAlreadyUsedOfDevice);

	   
	   /**
	    End of calculation
	    */
	   $name = Companyadminmodel::getCompanyName($companyid);
	   $email = Companyadminmodel::getCompanyEmail($companyid);
	   $date = date('Y-m-d');
	   $numberofdaysused = Companyadminmodel::fetchProfileUsedForDays($companyid);
	   $currentpackage = Companyadminmodel::fetchCurrentPackageID($companyid);
	  
	   $currentpackageprice = $currentpackage->price;	//$currentpackage['price'];
	   $currentpackagedeviceprice = $currentpackage->additionaldeviceprice;	//$currentpackage['additionaldeviceprice'];
	   
	   $packInfo = Packagemodel::getDetails($selectedpkgid);
       $deviceprice = $packInfo['additionaldeviceprice'];      
	   $packageBasePrice = $packInfo['price'];
	   $PACKAGEPAY = $packageBasePrice;
		
	   $totalAmountToPaid = $packageBasePrice + ($noofdevices * $deviceprice);
	   $totalPackageAmount = $packageBasePrice;
	   $DEVICEPAY = $noofdevices * $deviceprice;
	   $amount = ($totalAmountToPaid - (ltrim($extraPaidForDeviceAndPackage, '-') + ($amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice))) * 100;
		
	   $packageAmountToPay = ($totalPackageAmount - $amountAlreadyPaidOfPackage);
		//$deviceAmountToPay = (($noofdevices * $deviceprice) - $amountAlreadyPaidOfDevice);
	   
	   //$deviceAmountToPay = (($noofdevices * $deviceprice) - ($amountAlreadyPaidOfDevice + (ltrim($extraPaidForDeviceAndPackage, '-'))) - $packageAmountToPay);
	   //$deviceAmountToPay = (($noofdevices * $deviceprice) - ($amountAlreadyPaidOfDevice + (ltrim($extraPaidForDeviceAndPackage, '-'))));
	   $deviceAmountToPay = (($noofdevices * $deviceprice) - (($amountAlreadyPaidOfDevice + (ltrim($extraPaidForDeviceAndPackage, '-'))) - $packageAmountToPay));
	   
	   $price = $this->ceil_dec((($packageBasePrice * 10)/11),3,".");

	   
		if($selectedpkgid == 1)
		{                                                        
		 $accounttype = 'trial';
		}
		else if($selectedpkgid == 4)
		{
		 $accounttype = 'basic';
		}
		else if($selectedpkgid == 5)
		{
		 $accounttype = 'pro';
		}
            /**
               Invoicing part 
             */
           $totalPaidAmount = $amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice;
   		   $dataInv = Invoice::createInvoice($name,$date,$price,$noofdevices,$deviceprice,$accounttype,$totalPaidAmount);
           $invid = $dataInv['invid'];
           $invnumber = $dataInv['invnumber'];
		   //$invid = 'INV-DEMO';
           //$invnumber = 'inv12345';    
		   $invref = $invnumber;
		   require 'recurring/CreateProcessPayment.php';
		   $data = array(
					'customerid' => $customerID,
					'amount' => $amount,
					'invref' => $invref
		   );
	   
	        $token = new CreateProcessPayment();
	        $txnid= $token->create_payment_eway($data);
		    Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		    Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
		    Invoice::emailInvoice($invid,$email);
	        
			//Insert Package
			$dataInPackage = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $packageBasePrice,
	        		  'totalamounttopay' => $packageAmountToPay,
					  'totalamount' => ($packageBasePrice + $DEVICEPAY),
					  //'invoiceamount' => (ltrim($packageAmountToPay, '-') + ltrim($deviceAmountToPay, '-')),
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'package',
	                  'invoiceid' => $invid ,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
	        $this->db->insert('transaction',$dataInPackage);
			
			//Insert device 
		   $dataInDevice = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $DEVICEPAY,
		         	  'totalamounttopay' => $deviceAmountToPay,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'device',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        		);
		 $this->db->insert('transaction',$dataInDevice);			
	}
	
	
	
	function autoPayment()
	{
	   require 'recurring/xmlparser.php';
	   $companyid = $this->session->userdata("companyid");
	   $email = Companyadminmodel::getCompanyEmail($companyid);
	   $selectedpkgid = $_REQUEST['selectedpkgid'];
	   $noofdevice = $_REQUEST['noofdevices'];
	   /**
	    Calculation of amount already paid in previous transaction
	    */
	   
	   $totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
	   $currentPackageDetails = Companyadminmodel::fetchCurrentPackageID($companyid);
	   $currentPackagePrice = $currentPackageDetails->price;
	   $pricePerDevice = $currentPackageDetails->additionaldeviceprice;
	   $noOfUsedDays = Companyadminmodel::fetchProfileUsedForDays($companyid);
	   
	   
	   //Package
	   $totalAmountToPayInLastTransactionOfPackage = Companyadminmodel::fetchLastTransactionTotalAmountOfPackage($companyid);
	   $amountAlreadyUsedOfPackage = Invoice::calculateAmountAlreadyUsedOfPackage($currentPackagePrice, $noOfUsedDays);
	   
	   //Device
	   $totalAmountToPayInLastTransactionOfDevice = Companyadminmodel::fetchLastTransactionTotalAmountOfDevice($companyid);
	   $amountAlreadyUsedOfDevice = Invoice::calculateAmountAlreadyUsedOfDevice($noOfUsedDays, $pricePerDevice, $totalAllowedDevices);

	   //already paid extra
	   $extraPaidForDeviceAndPackage = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);
	   $amountAlreadyPaidOfPackage = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfPackage, $amountAlreadyUsedOfPackage);
	   $amountAlreadyPaidOfDevice = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfDevice, $amountAlreadyUsedOfDevice);
	   
	   /**
	    End of calculation
	    */
	   
	    $noofdevices = $noofdevice;
	    $query = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	    $res = $query->row();
	    $customerID = $res->customerToken; 
	    $name = $res->name; 
	    $date = date('Y-m-d');
		$packInfo = Packagemodel::getDetails($selectedpkgid);
		$deviceprice = $packInfo['additionaldeviceprice'];   
		$packageBasePrice = $packInfo['price'];
		$PACKAGEPAY = $packageBasePrice;
		$totalAmountToPaid = $packageBasePrice + ($noofdevices * $deviceprice);
		$totalPackageAmount = $packageBasePrice;
		$DEVICEPAY = $noofdevices * $deviceprice;
		
		$amount = ($totalAmountToPaid - (ltrim($extraPaidForDeviceAndPackage, '-') + ($amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice))) * 100;
		
		$packageAmountToPay = ($totalPackageAmount - $amountAlreadyPaidOfPackage);
		
		
		$deviceAmountToPay = (($noofdevices * $deviceprice) - (($amountAlreadyPaidOfDevice + (ltrim($extraPaidForDeviceAndPackage, '-'))) - $packageAmountToPay));
		//$deviceAmountToPay = (($noofdevices * $deviceprice) - ($amountAlreadyPaidOfDevice + (ltrim($extraPaidForDeviceAndPackage, '-'))));
		
		
		$price = $this->ceil_dec((($packageBasePrice * 10)/11),3,".");
	     
		//echo $deviceAmountToPay ."<<<>>>". $packageAmountToPay ."<<>>". ($noofdevices * $deviceprice) . "<<<>>>" . $amountAlreadyPaidOfDevice ."<<<>>>". $amountAlreadyPaidOfPackage ."<<<>>>".$extraPaidForDeviceAndPackage;
		// die;
		  
		if($selectedpkgid == 1)
		{                                                        
		 $accounttype = 'trial';
		}
		else if($selectedpkgid == 4)
		{
		 $accounttype = 'basic';
		}
		else if($selectedpkgid == 5)
		{
		 $accounttype = 'pro';
		}
            
            $totalPaidAmount = $amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice;
			
			$dataInv = Invoice::createInvoice($name,$date,$price,$noofdevice,$deviceprice,$accounttype,$totalPaidAmount);
			
            $invid = $dataInv['invid'];
            $invnumber = $dataInv['invnumber'];
			
		   //$invid = 'INV-DEMO';
           //$invnumber = 'inv12345';
		   $invref = $invnumber;
		   
		   $txnid = 0;
		   if($amount > 0){
			   require 'recurring/CreateProcessPayment.php';
			   $data1 = array(
						'customerid' => $customerID,
						'amount' => $amount,
						'invref' => $invref
			   );
		   
				 $token = new CreateProcessPayment();
				 $txnid= $token->create_payment_eway($data1);
			}  
				 Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
				 Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
				 Invoice::emailInvoice($invid,$email);
		  

			
			//Insert Package
			$dataInPackage = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $packageBasePrice,
	        		  'totalamounttopay' => $packageAmountToPay,
					  'totalamount' => ($packageBasePrice + $DEVICEPAY),
					  //'invoiceamount' => (ltrim($packageAmountToPay, '-') + ltrim($deviceAmountToPay, '-')),
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'package',
	                  'invoiceid' => $invid ,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
	        $this->db->insert('transaction',$dataInPackage);
			
			//Insert device 
		   $dataInDevice = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $DEVICEPAY,
		         	  'totalamounttopay' => $deviceAmountToPay,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'device',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
			
			
	     $this->db->insert('transaction',$dataInDevice);
	     $companyid = $this->session->userdata("companyid");
         $data = array('autopayment' => 'true');
         $this->db->where('companyid',$companyid);
         $this->db->update('companyadmin',$data);
         echo 'success';
	}
	
	
	function testPayment()
	{
	
	   $companyid = 6;
	   $query = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	   $res = $query->row();
	   $customerID = $res->customerToken;  
	   $amount = '200';
	   $invref = 'INV-001';
	   require 'recurring/CreateProcessPayment.php';
	   $data1 = array(
	            'customerid' => $customerID,
	            'amount' => $amount,
	            'invref' => $invref
	   );
	   
	  
	         $token = new CreateProcessPayment();
	         $txnid= $token->create_payment_eway($data1);
	         echo $txnid;
	         echo "<br/>";
	         echo json_encode($data1);
	        $dataIn = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => 19.95,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => '-',
	                  'invoiceid' => '-',
	                  'invoicenumber' => 'INV-TEST',
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
	        $this->db->insert('transaction',$dataIn);
            echo 'success';
	}
	
	
	function ceil_dec($number,$precision,$separator)
	{
	    $numberpart=explode($separator,$number);  
	    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
	    if($numberpart[0]>=0)
	    {$numberpart[1]=ceil($numberpart[1]);}
	    else
	    {$numberpart[1]=floor($numberpart[1]);}
	
	    $ceil_number= array($numberpart[0],$numberpart[1]);
	    return implode($separator,$ceil_number);
	}
	
	function ewayDirectPayment()
	{	
	    $companyid = $this->session->userdata("companyid");
	    $name = Companyadminmodel::getCompanyName($companyid);
	    $email = Companyadminmodel::getCompanyEmail($companyid);
	    
	    
	    /**
	     Calculation of amount already paid in previous transaction
	     */
	    $totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
	    $currentPackageDetails = Companyadminmodel::fetchCurrentPackageID($companyid);
	    $currentPackagePrice = $currentPackageDetails->price;
	    $pricePerDevice = $currentPackageDetails->additionaldeviceprice;
	    $noOfUsedDays = Companyadminmodel::fetchProfileUsedForDays($companyid);
	    $totalAmountToPayInLastTransaction = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);
	    $amountAlreadyUsed = Invoice::calculateAmountAlreadyUsed($currentPackagePrice , $noOfUsedDays , $pricePerDevice , $totalAllowedDevices);
	    $amountAlreadyPaid = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransaction , $amountAlreadyUsed);
	    
	    /**
	     End of calculation
	     */
	    
	        $nameoncard = $_POST['nameoncardp'];
            $cardnumber = $_POST['cardnumberp'];
            $expirymonth = $_POST['EWAY_CARDEXPIRYMONTHp'];
            $expiryyear = $_POST['EWAY_CARDEXPIRYYEARp'];
            //$amount = 100;
            $cvn = $_POST['cvnp'];
            $packageid = $_POST['packageid'];
            $noofdevice = $_POST['nodevicep'];
            $cardtype = $_POST['cardtype'];
            $date = date('Y-m-d');
            //$device = $totalAllowedDevices+$noofdevice;
			$device = $noofdevice;
	        $date1= date("Y-m-d H:i:s");
	        $today = strtotime($date1);
	        $expiry = strtotime('+30 days',$today);
	        $expirydate = date("Y-m-d H:i:s",$expiry);
            $name_array = explode(" ",$nameoncard);
            $countA = count($name_array);
            $firstname = $name_array [0];
            $lastname = $name_array [$countA-1];
            $packInfo = Packagemodel::getDetails($packageid);
            $deviceprice = $packInfo['additionaldeviceprice'];
            $price = $packInfo['price'];
            $PACKAGEPAY = $price;
            $totalprice = $price + ($noofdevice * $deviceprice); 
            $totalAmount = $price + ((($noofdevice + $totalAllowedDevices) - 2) * $deviceprice);
            $DEVICEPAY = $noofdevice * $deviceprice;
            $price = $this->ceil_dec((($price * 10)/11),3,".");
            $amount = ($totalAmount - $amountAlreadyPaid) * 100;
            $amountPaypal = ($totalAmount - $amountAlreadyPaid);
            
            
            /**
               Invoicing part 
             */
             
             
            if($packageid == 1)
            {
             $accounttype = 'trial';
            }
            else if($packageid == 4)
            {
             $accounttype = 'basic';
            }
            else if($packageid == 5)
            {
             $accounttype = 'pro';
            }
            
            $dataInv = Invoice::createInvoice($name,$date,$price,$noofdevice,$deviceprice,$accounttype,$amountAlreadyPaid);
            
            $invid = $dataInv['invid'];
            $invnumber = $dataInv['invnumber'];
            
            
	
	    if(isset($_POST['button']) && $_POST['button'] == 'Pay with eWAY') {
	    require_once( 'recurring/EwayPayment.php' );
           
        $query = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	    $res = $query->row();

		$eway = new EwayPayment();
		$eway->setCustomerFirstname($res->firstname); 
		$eway->setCustomerLastname($res->lastname);
		$eway->setCustomerEmail($res->email);
		$eway->setCustomerAddress($res->address);
		$eway->setCustomerPostcode($res->zipcode);
		$eway->setCustomerInvoiceDescription('-');
		$eway->setCustomerInvoiceRef($invnumber);
		$eway->setCardHoldersName($nameoncard);
		$eway->setCardNumber($cardnumber);
		$eway->setCardExpiryMonth($expirymonth);
		$eway->setCardExpiryYear($expiryyear);
		$eway->setTotalAmount($amount);
		$eway->setCVN($cvn);
		
		
		if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
		   
		  // if(true) {
		    Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		    Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
		    Invoice::emailInvoice($invid,$email);
		   
		  $txnid = $eway->getTrxnNumber();	 
		 // $txnid = 0;
		    $dataIn = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $amountPaypal,
		    		  'totalamounttopay' => $totalAmount,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'package',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
	    $this->db->insert('transaction',$dataIn);
		if($this->db->insert_id() > 0)
		{
		
		   $sql= "update companyadmin set packageid = {$packageid} ,expirydate ='{$expirydate}',noofdevicesallowed = {$device} where companyid = {$companyid}";
                   $this->db->query($sql);
		
		   redirect('companyadmin/billinghistory');
		}    
		} else {
		    echo "Error occurred (".$eway->getError()."): " . $eway->getErrorMessage();
		   // redirect('companyadmin/updatepassword');
		}
      }
	}
	
	public function corporateDirectPayment()
	{
	    $companyid = $this->session->userdata("companyid");
	    $name = Companyadminmodel::getCompanyName($companyid);
	    $email = Companyadminmodel::getCompanyEmail($companyid);
	    $nameoncard = $_POST['nameoncardp'];
		$cardnumber = $_POST['cardnumberp'];
		$expirymonth = $_POST['EWAY_CARDEXPIRYMONTHp'];
		$expiryyear = $_POST['EWAY_CARDEXPIRYYEARp'];
	   // $amount = 100;
		$cvn = $_POST['cvnp'];
	   
		$cardtype = $_POST['cardtype'];
		$date = date('Y-m-d');
	   
		$name_array = explode(" ",$nameoncard);
		$countA = count($name_array);
		$firstname = $name_array [0];
		$lastname = $name_array [$countA-1];
           
	    $date1= date("Y-m-d H:i:s");
	    $today = strtotime($date1);
	    $expiry = strtotime('+30 days',$today);
	    $expirydate = date("Y-m-d H:i:s",$expiry);
            
            
		$qury = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
		$result = $qury->row();
		$packageid = $result->packageid;// find packageid
            
		if($packageid == 1)
		{
		 $accounttype = 'trial';
		}
		else if($packageid == 4)
		{
		 $accounttype = 'basic';
		}
		else if($packageid == 5)
		{
		 $accounttype = 'pro';
		}
            
		$packInfo = Packagemodel::getDetails($packageid);
		$deviceprice = $packInfo['additionaldeviceprice'];
		 
		$noofdevice =  $packInfo['noofdevices'];
		$device = $noofdevice-2;
		
		$price = $packInfo['price'];
		$PACKAGEPAY = $price;
		$totalprice = $price + ($device * $deviceprice); 
		$DEVICEPAY = $device * $deviceprice;
		$price = $this->ceil_dec((($price * 10)/11),3,".");
		$amount = $totalprice * 100;
            
            /**
               Invoicing part 
             */
            
		$dataInv = Invoice::fetchInvoice($companyid);
		$invid = $dataInv['invoiceid'];
		$invnumber = $dataInv['invoicenumber'];
	
	    if(isset($_POST['button']) && $_POST['button'] == 'Pay with eWAY') {
	    require_once( 'recurring/EwayPayment.php' );
           
        $query = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	    $res = $query->row();

		$eway = new EwayPayment();
		$eway->setCustomerFirstname($res->firstname); 
		$eway->setCustomerLastname($res->lastname);
		$eway->setCustomerEmail($res->email);
		$eway->setCustomerAddress($res->address);
		$eway->setCustomerPostcode($res->zipcode);
		$eway->setCustomerInvoiceDescription('-');
		$eway->setCustomerInvoiceRef($invnumber);
		$eway->setCardHoldersName($nameoncard);
		$eway->setCardNumber($cardnumber);
		$eway->setCardExpiryMonth($expirymonth);
		$eway->setCardExpiryYear($expiryyear);
		$eway->setTotalAmount($amount);
		$eway->setCVN($cvn);
		
		
		if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
		   
		   
		    Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		    Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
		    Invoice::emailInvoice($invid,$email);
		   
		    $txnid = $eway->getTrxnNumber();	    
		    $dataIn = array(
	                   'ispaid' => 1,
	                   'transactionid' => $txnid,
	                   'transactiondate' => date('Y-m-d H:i:s')
	                   );
	            $this->db->where('invoiceid',$invid);      
	           $this->db->update('transaction',$dataIn);
	           
	           $sql= "update companyadmin set expirydate ='{$expirydate}' where companyid = {$companyid}";
	           $this->db->query($sql);
	           
		  if($this->db->affected_rows() > 0)
	          {
		   redirect('companyadmin/billinghistory');
		  }    
			} else {
				echo "Error occurred (".$eway->getError()."): " . $eway->getErrorMessage();
			   // redirect('companyadmin/updatepassword');
			}
       }
	}
	
	public function billinghistoryPagination()
	{

		$options = array();
		$customUrl = "";
		$config["base_url"] = base_url() . "index.php/companyadmin/billinghistory";
		$config["total_rows"] = count(Transactionmodel::GetBillingbycompanyid($this->session->userdata("companyid")));
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
		$options['companyid']=$this->session->userdata("companyid");
		$data["results"] = Transactionmodel::GetBillingDetailsbycompanyid($options);
		$data["links"] = $this->pagination->create_links();
		
		return $data;
	}
	
	
	function processTokenPayment()
	{
	   require 'recurring/xmlparser.php';
	   $sql = "select * from companyadmin where DATEDIFF(CURRENT_TIMESTAMP(),expirydate)= 0 and autopament = 'true'";
	   $query = $this->db->query($sql);
	   if($query->num_rows() > 0) {
	   $query = $query->result();
	   foreach($query as $row) {
	   $customerID = $row->customerToken;
	   $companyid = $row->companyid;
	   
	   $device = $row->noofdevicesallowed;
	   $packageid = $row->packageid;
	   
		if($packageid == 1)
		{
		 $accounttype = 'trial';
		}
		else if($packageid == 4)
		{
		 $accounttype = 'basic';
		}
		else if($packageid == 5)
		{
		 $accounttype = 'pro';
		}
	   
	   
	   $packInfo = Packagemodel::getDetails($packageid);
	   $deviceprice = $packInfo['additionaldeviceprice'];
	   $price = $packInfo['price'];
	   $PACKAGEPAY = $price;
	   $totalprice = $price + ($device * $deviceprice); 
	   $DEVICEPAY = $device * $deviceprice;
	   $price = $this->ceil_dec((($price * 10)/11),3,".");
	   $dataInv = Invoice::fetchInvoice($companyid);
	   $invid = $dataInv['invoiceid'];
	   $invnumber = $dataInv['invoicenumber'];
	   $date = date('Y-m-d');
	   $amount = $totalprice * 100;
	   $invref = $invnumber;
	   require 'recurring/CreateProcessPayment.php';
	   $data = array(
	            'customerid' => $customerID,
	            'amount' => $amount,
	            'invref' => $invref
	   );
	   
		 $token = new CreateProcessPayment();
		 $txnid= $token->create_payment_eway($data);
	     Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		 Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
		 Invoice::emailInvoiceCron($invid,$row->email);
         $dataIn = array(
	                   'ispaid' => 1,
	                   'transactionid' => $txnid,
	                   'transactiondate' => date('Y-m-d H:i:s')
	                   );
	            $this->db->where('invoiceid',$invid);      
	            $this->db->update('transaction',$dataIn);
	            $date1= date("Y-m-d H:i:s");
	            $today = strtotime($date1);
	            $expiry = strtotime('+30 days',$today);
	            $expirydate = date("Y-m-d H:i:s",$expiry);
	            $sql= "update companyadmin set expirydate ='{$expirydate}' where companyid = {$companyid}";
	            $this->db->query($sql);
	        
	       }
	    }
	}
	
	
	
	public function downloadInvoice()
	{
	  Invoice::downloadInvoice($_REQUEST['invoiceid']);
	}
	
	public function corporatePaymentCronJob()
	{
		$data = array();
	    $sql = "SELECT * FROM companyadmin WHERE DATEDIFF(CURRENT_TIMESTAMP(),expirydate) = 7 AND (autopayment = 'true' OR type = 'corporate')";
	    $query = $this->db->query($sql);
	    if($query->num_rows() > 0)
	    {
	     $data = $query->result_array();
	     foreach($data as $row)
	     {
	        if($row['type'] == 'corporate')
	        {
	        
	           $noofdevice = $row['noofdevicesallowed'];
	           $packageid = $row['packageid'];
	           $companyid = $row['companyid'];
	           $packInfo = Packagemodel::getDetails($packageid);
			   $deviceprice = $packInfo['additionaldeviceprice'];
			   $device = $noofdevice-2;
	           $price = $packInfo['price'];
	           $totalprice = $price + ($device * $deviceprice); 
	           $price = $this->ceil_dec((($price * 10)/11),3,".");
	           $date = date('Y-m-d H:i:s');
	           $accounttype = 'business';
	           $dataInv = Invoice::createInvoiceCron($row['name'],$date,$price,$noofdevice,$deviceprice,$accounttype);    
			   $invid = $dataInv['invid'];
			   $invnumber = $dataInv['invnumber'];
                   
                   $dataIn = array(
	                  'companyid' => $row['companyid'],
	                  'transactionid' => 0,
	                  'amount' => $totalprice,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'package',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 0,
	                  'createdon' => date('Y-m-d H:i:s')
	               );
	          $this->db->insert('transaction',$dataIn);
	          
	          Invoice::emailInvoiceCron($invid,$row['email']);
	        
	        }
	        else if($row['type'] != 'corporate' && $row['autopayment'] == 'true')
	        {
	           $noofdevice = $row['noofdevicesallowed'];
	           $packageid = $row['packageid'];
	           
	           
				if($packageid == 1)
				{
				 $accounttype = 'trial';
				}
				else if($packageid == 4)
				{
				 $accounttype = 'basic';
				}
				else if($packageid == 5)
				{
				 $accounttype = 'pro';
				}
	           
				   $packInfo = Packagemodel::getDetails($packageid);
				   $deviceprice = $packInfo['additionaldeviceprice'];
				   $device = $noofdevice-2;
				   $price = $packInfo['price'];
				   $totalprice = $price + ($device * $deviceprice); 
				   $price = $this->ceil_dec((($price * 10)/11),3,".");
				   $date = date('Y-m-d H:i:s');
				   $dataInv = Invoice::createInvoiceCron($row['name'],$date,$price,$noofdevice,$deviceprice,$accounttype);    
                   $invid = $dataInv['invid'];
                   $invnumber = $dataInv['invnumber'];
                   $dataIn = array(
	                  'companyid' => $row['companyid'],
	                  'transactionid' => 0,
	                  'amount' => $totalprice,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => '-',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 2,
	                  'createdon' => date('Y-m-d H:i:s')
	               );
	          $this->db->insert('transaction',$dataIn);
	          
	          Invoice::emailInvoiceCron($invid,$row['email']);
	        
	        }
	     
	     }
	     
	    }
	}
	
	/*public function fetchAmountAlreadyPaid()
	{
		$companyid = $this->session->userdata("companyid");
		$currentPackageDetails = Companyadminmodel::fetchCurrentPackageID($companyid);
		$currentPackagePrice = $currentPackageDetails->price;
		$pricePerDevice = $currentPackageDetails->additionaldeviceprice;
		$totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
		$noOfUsedDays = Companyadminmodel::fetchProfileUsedForDays($companyid);
		
		$totalAmountToPayInLastTransaction = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);
		
		$amountAlreadyUsed = Invoice::calculateAmountAlreadyUsed($currentPackagePrice, $noOfUsedDays, $pricePerDevice, $totalAllowedDevices);
		
		$amountAlreadyPaid = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransaction, $amountAlreadyUsed);
		
		echo $amountAlreadyPaid;
	}*/
	
	public function fetchAmountAlreadyPaid()
	{
		$companyid = $this->session->userdata("companyid");
		$currentPackageDetails = Companyadminmodel::fetchCurrentPackageID($companyid);
		$currentPackagePrice = $currentPackageDetails->price;
		$pricePerDevice = $currentPackageDetails->additionaldeviceprice;
		$totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
		$noOfUsedDays = Companyadminmodel::fetchProfileUsedForDays($companyid);
		
		//echo $noOfUsedDays;
		//die;
		
		/*$totalAmountToPayInLastTransaction = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);
		$amountAlreadyUsed = Invoice::calculateAmountAlreadyUsed($currentPackagePrice, $noOfUsedDays, $pricePerDevice, $totalAllowedDevices);
		$amountAlreadyPaid = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransaction, $amountAlreadyUsed);
		echo $amountAlreadyPaid;
		*/
		
		//Package
	    $totalAmountToPayInLastTransactionOfPackage = Companyadminmodel::fetchLastTransactionTotalAmountOfPackage($companyid);
	    $amountAlreadyUsedOfPackage = Invoice::calculateAmountAlreadyUsedOfPackage($currentPackagePrice, $noOfUsedDays);
	   
	   //Device
	    $totalAmountToPayInLastTransactionOfDevice = Companyadminmodel::fetchLastTransactionTotalAmountOfDevice($companyid);
	    $amountAlreadyUsedOfDevice = Invoice::calculateAmountAlreadyUsedOfDevice($noOfUsedDays, $pricePerDevice, $totalAllowedDevices);
		
		//already paid extra
	    $extraPaidForDeviceAndPackage = Companyadminmodel::fetchLastTransactionTotalAmountToPay($companyid);


	    $amountAlreadyPaidOfPackage = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfPackage, $amountAlreadyUsedOfPackage);
	    $amountAlreadyPaidOfDevice = Invoice::calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfDevice, $amountAlreadyUsedOfDevice);
		
		//echo $noOfUsedDays ."<<<>>>". $extraPaidForDeviceAndPackage ."<<<>>>". $amountAlreadyPaidOfPackage ."<<<>>>". $amountAlreadyPaidOfDevice;
		//die;
		echo $amountAlreadyPaid = (ltrim($extraPaidForDeviceAndPackage, '-') + ($amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice));
		
	}
	
	public function deviceupgrade()
	{
	   $quantity = $_REQUEST['quantity'];
	   $cardnumber = $_REQUEST['cardnumber'];
	   $nameoncard = $_REQUEST['nameoncard'];
	   $expmonth = $_REQUEST['EWAY_CARDEXPIRYMONTH'];
	   $expyear = $_REQUEST['EWAY_CARDEXPIRYYEAR'];
	   $cvn = $_REQUEST['cvn'];
	   $deviceprice = $_REQUEST['deviceprice'];
	   $amount = $quantity * $deviceprice * 100;
	   $paidamount = $quantity * $deviceprice;
	   $companyid = $this->session->userdata("companyid");
	   $name = Companyadminmodel::getCompanyName($companyid);
	   $email = Companyadminmodel::getCompanyEmail($companyid);
	   $date = date('Y-m-d');
	   $dataInv = Invoice::createDeviceInvoice($name,$date,$quantity,$deviceprice);
       $invid = $dataInv['invid'];
       $invnumber = $dataInv['invnumber'];
       require_once( 'recurring/EwayPayment.php' );
                      
       $query = $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	   $res = $query->row();

		$eway = new EwayPayment();
		$eway->setCustomerFirstname($res->firstname); 
		$eway->setCustomerLastname($res->lastname);
		$eway->setCustomerEmail($res->email);
		$eway->setCustomerAddress($res->address);
		$eway->setCustomerPostcode($res->zipcode);
		$eway->setCustomerInvoiceDescription('-');
		$eway->setCustomerInvoiceRef($invnumber);
		$eway->setCardHoldersName($nameoncard);
		$eway->setCardNumber($cardnumber);
		$eway->setCardExpiryMonth($expmonth);
		$eway->setCardExpiryYear($expyear);
		$eway->setTotalAmount($amount);
		$eway->setCVN($cvn);
		
		
		if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
		   //if(true) {
		    //Invoice::payInvoice($invnumber,$date,$PACKAGEPAY,$accounttype);  /** Make the invoice paid */
		    //Invoice::payInvoice($invnumber,$date,$DEVICEPAY,'');
		    Invoice::emailInvoice($invid,$email);
		   
		  $txnid = $eway->getTrxnNumber();	 
		    //$txnid = 0;
		  $dataIn = array(
	                  'companyid' => $companyid,
	                  'transactionid' => $txnid,
	                  'amount' => $paidamount,
		         	  'totalamounttopay' => $paidamount,
	                  'transactiondate' => date('Y-m-d H:i:s'),
	                  'type' => 'creditcard',
	                  'comment' => 'device',
	                  'invoiceid' => $invid,
	                  'invoicenumber' => $invnumber,
	                  'ispaid' => 1,
	                  'createdon' => date('Y-m-d H:i:s')
	        );
	        $this->db->insert('transaction',$dataIn);
		if($this->db->insert_id() > 0)
		{
		
		   //$sql= "update companyadmin set noofdevicesallowed = noofdevicesallowed+{$quantity} where companyid = {$companyid}";
		   $sql= "update companyadmin set noofdevicesallowed = {$quantity} where companyid = {$companyid}";
           $this->db->query($sql);
		
		   redirect('companyadmin/billinghistory');
		}    
		} else {
		    echo "Error occurred (".$eway->getError()."): " . $eway->getErrorMessage();
		   // redirect('companyadmin/updatepassword');
		}
           
	}
	
	
	public function deviceupgrade_quintity()
	{
	   $quantity 	= $_REQUEST['quantity'];
	   $deviceprice = $_REQUEST['deviceprice'];
	   $amount		= $quantity * $deviceprice * 100;
	   $paidamount 	= $quantity * $deviceprice;
	   $companyid 	= $this->session->userdata("companyid");
	   $query 		= $this->db->get_where('companyadmin',array('companyid'=>$companyid));
	   $res 		= $query->row_array();
	 //  print_r($res);exit;
	   $old_device_quantity = $res['noofdevicesallowed'];
	   $total_device= $quantity+$old_device_quantity;   
	   
	   $sql			= "update companyadmin set noofdevicesallowed = {$total_device} where companyid = {$companyid}";
       $this->db->query($sql);
	   redirect('companyadmin/manageDevices');
	}
	
}
// for cron job : http://localhost/cuedrive/cuedrive/index.php/payment/processTokenPayment     // http://localhost/cuedrive/cuedrive/index.php/payment/corporatePaymentCronJob
?>