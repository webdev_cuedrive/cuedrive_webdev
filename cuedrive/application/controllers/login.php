<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session'); 
		$this->load->helper(array('form','url','cookie'));
		$this->load->model('companyadminmodel');
		$this->load->model('companyusermodel');	
		
		
	}

	function index()
	{
		$this->load->view('CompanyAdmin/login');
	}
	
	function authenticateCompanyAdmin()
	{
		$username			= strtolower($this->input->post('username'));
		$password			= md5($this->input->post('userpass'));
		$rememberPassword	= $this->input->post('userpass');
		$rememberMe 		= $this->input->post('rememberme');
		$retval				= $this->companyadminmodel->userAuth($username, $password);
		//echo $retval;
		if($retval=="invalid") //authentication failed
			{
				$this->session->set_flashdata('loginmsg', INVALIDLOGIN);
				redirect('login');
			}
		else if($retval=="notapproved")//not approved
		{
			$this->session->set_flashdata('loginmsg', NOTAPPROVED);
			redirect('login');
		}
		else if($retval=="notactive") //not active
		{
			$this->session->set_flashdata('loginmsg', NOTACTIVE);
			redirect('login');
		}
			else { //authenticated
				$companyid=$retval;
				if((int) $rememberMe == 1)
				{
					$cookie = array(
                   	'name'   => 'UserCredentials',
                   	'value'  => "$username :$rememberPassword",
                   	'expire' => 60*60*24*365,
              	 	);
					set_cookie($cookie); 
					//echo "set cookie";
				} else {
					delete_cookie('UserCredentials');
				}
				
				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('companyid', $companyid);		
				$_SESSION['userName'] 		= $username;
				$_SESSION['comapnyAdmin'] 	= $companyid;
				redirect('companyadmin/folders');
				
			}
	}
	
	function authenticateCompanyUser()
	{
		$username			= strtolower($this->input->post('username'));
		$password			= md5($this->input->post('userpass'));
		$rememberPassword	= $this->input->post('userpass');
		$rememberMe 		= $this->input->post('rememberme');
		$retval				= $this->companyusermodel->userauthWeb($username, $password);
		//echo $retval;
		if($retval=="invalid") //authentication failed
			{
				$this->session->set_flashdata('loginmsg', INVALIDLOGIN);
				redirect('login');
			}
		else if($retval=="notactive") //not active
		{
			$this->session->set_flashdata('loginmsg', NOTACTIVE);
			redirect('login');
		}
			else { //authenticated
				$userdetails=$retval;
				$companyid=$userdetails['companyid'];
				$userid=$userdetails['companyuserid'];
				if((int) $rememberMe == 1)
				{
					$cookie = array(
                   	'name'   => 'UserCredentials',
                   	'value'  => "$username :$rememberPassword",
                   	'expire' => 60*60*24*365,
              	 	);
					set_cookie($cookie); 
					//echo "set cookie";
				} else {
					delete_cookie('UserCredentials');
				}
				
				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('userid', $userid);	
				$this->session->set_userdata('companyid', $companyid);				
				
				redirect('companyuser');
				
			}
	}
	
	
	
	function authenticateUser()
	{
		$username			= strtolower($this->input->post('username'));
		$password			= md5($this->input->post('userpass'));
		$rememberPassword	= $this->input->post('userpass');
		$rememberMe 		= $this->input->post('rememberme');
		$retval				= $this->companyadminmodel->userAuth($username, $password);
		//echo $retval;
		if($retval=="invalid") //authentication failed
			{
			
			
			        $retval	=	$this->companyusermodel->userauthWeb($username, $password);
			        
			        if($retval=="invalid") //authentication failed
			        {
					   $this->session->set_flashdata('loginmsg', INVALIDLOGIN);
					   redirect('login');
			        }
			        
			        else if($retval == "expired"){
			        
			            $this->session->set_flashdata('loginmsg', 'Your account has been expired');
			            redirect('login');
			        }
		            else if($retval=="notactive") //not active
		                {
			           		$this->session->set_flashdata('loginmsg', NOTACTIVE);
			           		redirect('login');
		                }
			        else { //authenticated
				       		$userdetails	= $retval;
				       		$companyid		= $userdetails['companyid'];
				       		$userid			= $userdetails['companyuserid'];
							$user_type	= $userdetails['user_type'];
				       if((int) $rememberMe == 1)
				        {
					   	$cookie = array(
                   	                   'name'   => 'UserCredentials',
                   	                   'value'  => "$username :$rememberPassword",
                   	                   'expire' => 60*60*24*365,
              	 	                 );
					   	set_cookie($cookie); 
					  //echo "set cookie";
				        } else {
					  			delete_cookie('UserCredentials');
				        }
				if($user_type == 1){
						$this->session->set_userdata('username', $username);
						$this->session->set_userdata('userid', $userid);	
						$this->session->set_userdata('companyid', $companyid);				
						
						redirect('companyuser');
				}elseif($user_type == 2){
					$comany_user_name	=	$this->companyadminmodel->getCompanyUsername($companyid);
					$this->session->set_userdata('username', $comany_user_name);
					$this->session->set_userdata('admin_username', $username);
					$this->session->set_userdata('userid', $userid);	
					$this->session->set_userdata('companyid', $companyid);			
					redirect('companyadmin/folders');
					//echo 'success';
						
				}
			}

				//$this->session->set_flashdata('loginmsg', 'Invalid Username or Password!');
				//redirect('login');
			}
			
	        else if($retval == "expired")
	        {		
		        
				redirect('companyadmin/selectPacakge');	
			}	
			else if($retval=="notapproved")//not approved
			{
				$this->session->set_flashdata('loginmsg', NOTAPPROVED);
				redirect('login');
			}
			else if($retval=="notactive") //not active
			{
				$this->session->set_flashdata('loginmsg', NOTACTIVE);
				redirect('login');
			}
			else { //authenticated
				$companyid	=	$retval;
				if((int) $rememberMe == 1)
					{
						$cookie = array(
										'name'   => 'UserCredentials',
										'value'  => "$username :$rememberPassword",
										'expire' => 60*60*24*365,
									);
						set_cookie($cookie); 
						//echo "set cookie";
					} else {
						delete_cookie('UserCredentials');
					}
				
				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('companyid', $companyid);		
				
				redirect('companyadmin/folders');
				
			}
	}
	
	
	
	function authenticateUserWeb()
	{
		$username			= strtolower($this->input->post('username'));
		$password			= md5($this->input->post('userpass'));
		$rememberPassword 	= $this->input->post('userpass');
		$rememberMe 		= $this->input->post('rememberme');
		$retval				= $this->companyadminmodel->userAuth($username, $password);
		if($retval=="invalid") //authentication failed
		 {
			        $retval=$this->companyusermodel->userauthWeb($username, $password);
					if($retval=="invalid") { //authentication failed
				  		echo INVALIDLOGIN;
			        }elseif($retval == "expired") {
			            echo 'Your account has been expired';
			        }elseif($retval=="notactive") { //not active
			        	echo NOTACTIVE;
			        }elseif($retval=="companynotactive") { //not active
			        	 echo 'Your company account has been deactivated';
		            }else{ //authenticated
				       $userdetails	= $retval;
				       $companyid	= $userdetails['companyid'];
				       $userid		= $userdetails['companyuserid'];
					   $user_type	= $userdetails['user_type'];	
				       if((int) $rememberMe == 1){
					  			 $cookie = array(
                   	                            'name'   => 'UserCredentials',
                   	                            'value'  => "$username :$rememberPassword",
                   	                            'expire' => 60*60*24*365,
              	 	                            );
					   		set_cookie($cookie); 
					  //echo "set cookie";
				        } else {
					  		delete_cookie('UserCredentials');
				        }
				if($user_type == 1){
					$this->session->set_userdata('username', $username);
					$this->session->set_userdata('userid', $userid);	
					$this->session->set_userdata('companyid', $companyid);			
					//redirect('companyuser');
					echo 'successuser';
				}elseif($user_type == 2){
					$comany_user_name	=	$this->companyadminmodel->getCompanyUsername($companyid);
					$this->session->set_userdata('username', $comany_user_name);
					$this->session->set_userdata('admin_username', $username);
					$this->session->set_userdata('userid', $userid);	
					$this->session->set_userdata('companyid', $companyid);			
					//redirect('companyuser');
					echo 'success';
						
				}
			 }
				//$this->session->set_flashdata('loginmsg', 'Invalid Username or Password!');
				//redirect('login');
			}else if($retval == "expired"){		
			
	            $this->session->set_userdata('username', $username);
				$this->session->set_userdata('expired', true);
		        echo 'expired';
				
		}else if($retval=="notapproved")//not approved
			{
			
			echo NOTAPPROVED;
			
		}else if($retval=="notactive") //not active
	    	{
			echo NOTACTIVE;
			
		}
			else { //authenticated
				$companyid=$retval;
				if((int) $rememberMe == 1)
				{
					$cookie = array(
									'name'   => 'UserCredentials',
									'value'  => "$username :$rememberPassword",
									'expire' => 60*60*24*365,
								);
					set_cookie($cookie); 
					//echo "set cookie";
				} else {
					delete_cookie('UserCredentials');
				}
				
				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('companyid', $companyid);		
				
				//$_SESSION['loginUserName'] = $username;
				//$_SESSION['comapnyAdmin'] = $companyid;
				
				//redirect('companyadmin');
				echo 'success';
			}
	}
}