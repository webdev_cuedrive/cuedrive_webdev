<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller {
	
		function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form','url'));
	}

	function index() {
		$this->session->unset_userdata('companyid');
		$this->session->sess_destroy();
		//redirect('login');
		header ("Location:".base_url());
	}
}
?>