<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companyadmin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		header('Content-Type:text/html; charset=UTF-8');
		ini_set('max_file_uploads', 100);
		$this->load->database();
		$this->load->library('session'); 
		$this->load->library('s3');//amazon lib
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('companyadminmodel');
		$this->load->model('packagemodel');
		$this->load->model('foldermodel');
		$this->load->model('folderfilemodel');
		$this->load->model('folderdevicesmodel');
		$this->load->model('folderusersmodel');	
		$this->load->model('companydevicemodel');
		$this->load->model('companyusermodel');	
		$this->load->model('permissionsmodel');
		$this->load->model('contactusmodel');
		$this->load->model('contentmanagementmodel');
		$this->load->model('transactionmodel');
		$this->load->model('email');
		$this->load->model('auditlogmodel');
		$this->load->model('openfiremodel');
		$this->load->model('classpush');
		$this->load->model('alldevicesmodel');
		$this->load->helper('htmlpurifier');
		$this->load->library('encrypt');
		$this->load->helper('creditamount');
		
		if($this->session->userdata("username")!= "")
		 {
			$username = $this->session->userdata("username");
			$isActiveAccount = Companyadminmodel::validateAccountActive($username);
			if($isActiveAccount == FALSE)
			 {
				$this->session->unset_userdata('companyid');
				$this->session->sess_destroy();
				$this->load->view('CompanyAdmin/nomoreaccess');	

			 }
		 }	
	}

	function index()
	{
		
		$this->load->view('CompanyAdmin/home');
	}
	
	function FolderActivation()	{
	
		$checked = $this->input->post('checked');
		$folderid = $this->input->post('folderid');		
		return $this->companyadminmodel->FolderActivation($folderid, $checked);
	}
       /* Register functions */	
	function selectPacakge()
	{
		$data = array();
		$pkgarr=array();
		$packages = $this->packagemodel->getBusinessPackages();
		foreach($packages as $package){
			$pkgarr[]=$package;
		}
		//transpose the array
		$transposedarr=$this->transpose($packages);
		$data['results']=$transposedarr;
		//print_r($transposedarr);
		$this->load->view('CompanyAdmin/selectpackage',$data);
	}
	function signUpForm()
	{
		$data = array();
		$data['selectedpkgid']= $this->input->post('selectedpkgid');
		$this->load->view('CompanyAdmin/signupform',$data);
	}
	
	function register()
	{
		//update database	
		$id					= 0;
		$datacc 			= array(); 
        $name 				= html_purify($this->input->post('company_name'));
		//$datacc['name']= html_purify($this->input->post('company_name'));
		$datacc['name']		= $this->input->post('company_name');
		$datacc['email']	= html_purify($this->input->post('email'));
		//$datacc['address']= html_purify($this->input->post('address'));
		$datacc['address']	= $this->input->post('address');
		$firstname 			= html_purify($this->input->post('firstname'));
		$lastname 			= html_purify($this->input->post('lastname'));
		$datacc['firstname']= $firstname;
		$datacc['lastname']	= $lastname;
		$datacc['city']		= html_purify($this->input->post('city'));
		$datacc['zipcode']	= html_purify($this->input->post('zipcode'));
		$datacc['country']	= html_purify($this->input->post('country'));
		$datacc['phone']	= html_purify($this->input->post('phone'));
		$username			= html_purify(strtolower($this->input->post('username')));
		$datacc['username']	= $username;
		$datacc['password']	= md5(html_purify($this->input->post('password')));
		$datacc['passwordhint']= html_purify($this->input->post('passwordhint'));
		$datacc['isactive']	= 'yes';
		$datacc['type']		= 'Trial';
		//$datacc['type'] = html_purify($this->input->post('usertype'));
		$createdon 			= date('Y-m-d H:i:s');
        $datacc['createdon']=$createdon;
		$today 				= strtotime($createdon);
		$expiry 			= strtotime('+30 days',$today);
		//$expirydate= date("Y-m-d H:i:s",$expiry);
		$expirydate			= date("Y-m-d",$expiry);
		$datacc['expirydate']= $expirydate;
		
		$datacc['status']	= 'approved';
		//$extradevices= $this->input->post('extradevices');
		$extradevices 		= 0;
		//$packageid=	$this->input->post('selectedpkgid');
		$packageid	=	$this->input->post('package_id');
		if(empty($packageid)){
			$packageid	=	1;
		}
		$datacc['packageid']= $packageid;
		$pkgdetails			= $this->packagemodel->getDetails($packageid);
		//print_r($pkgdetails);
		$pkgdefaulttype		= $pkgdetails['type'];
		$pkgdefaultdevies	= $pkgdetails['noofdevices'];
		$pkgdefaultstorage	= $pkgdetails['storagespace'];
		$datacc['noofdevicesallowed']=$extradevices+$pkgdefaultdevies;
		
		$email 				= html_purify($this->input->post('email'));
		$password 			= html_purify($this->input->post('password'));
		$companyname 		= $this->input->post('company_name');
		
		$usernameExist 		= Companyadminmodel::checkUsernameIsExist($username);
		$emailExist 		= Companyadminmodel::checkEmailIsExist($email);
		$companyNameExist 	= Companyadminmodel::checkCompanyNameIsExist($companyname);
		
		if($usernameExist == 0 && $emailExist == 0 && $companyNameExist == 0) 
		{
			if(!empty($packageid))
			{
				$companyid		= $this->companyadminmodel->updateCompanyAdmin($datacc,$id);
				$user_password 	= Openfiremodel::generateRandomToken();
				$url 			= ORG_URL."chat/?type=add&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($name)."&group=".urlencode($companyid);
				Openfiremodel::sendDataToServer($url);
				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('companyid', $companyid);	

				/*added by Sunil
				add account number */
				$result = '';
				for($i = 0; $i < 10; $i++) 
				{
					$result .= mt_rand(0, 9);
				}
				$accountNumber 						= '000'.$result;
				$dataComapnyName['account_number']	= $accountNumber;
				/* end */
				$this->companyadminmodel->updateCompanyAdmin($dataComapnyName, $companyid);

				//$bucketname="cuedrive.".$companyid;	
				$bucketname		=	ROOTBUCKET;
				//if($this->s3->putBucket($bucketname,S3::ACL_PRIVATE,"ap-southeast-2")) // $this->s3->putBucket($bucketname,S3::ACL_PRIVATE,"Sydney")
				//{
					/* we can not create file directly.. so creating empty file with required folder.. so amazon will create folder automatically */
					$uploaddir = realpath('./uploads/');
					$dummyfile = $uploaddir ."//".'dummyfile.txt';
					//create company folder on amazon			
					$companyfoldername	= COMPANYFOLDERNAME.$companyid;
					//create public folder for company		
					$file				= fopen($dummyfile,"w");
					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
					{
						//successfully created							
					}			

					$publicfoldername	= "shared";			
					$file				= fopen($dummyfile,"w");
					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$publicfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
					{
						//successfully created
						//update DB
						$datacc 				= array();
						$datacc['foldername']	= $publicfoldername;
						$datacc['companyid']	= $companyid;
						$datacc['fullpath']		= $publicfoldername;
						$datacc['type']			= 'public'; 
						$datacc['createdby']	= $companyid;
						$datacc['createdon']	= date('Y-m-d H:i');				
						$this->foldermodel->insertFolder($datacc);				
					}

					//create user folder for company			
					$publicfoldername	= "user";
					$dummyfile 			= $uploaddir ."//".'dummyfile.txt';
					$file				= fopen($dummyfile,"w");
					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$publicfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
					{
						//successfully created
						//update DB
						$datacc 				= array();
						$datacc['foldername']	= $publicfoldername;
						$datacc['companyid']	= $companyid;
						$datacc['fullpath']		= $publicfoldername;
						$datacc['type']			= 'private'; 
						$datacc['createdby']	= $companyid;
						$datacc['createdon']	= date('Y-m-d H:i');				
						$this->foldermodel->insertFolder($datacc);				
					}
				//}

			//	Email::welcome_Email($username,$email,$password,$createdon,$pkgdefaulttype,$pkgdefaultstorage,$pkgdefaultdevies);
			//	Email::newUserEmail($username,$email,$password,$firstname,$lastname);
				echo 'success';
			}
			else{
				echo 'error';
			}
			
		}else
		{	
		if($emailExist == 1){
				echo 'emailexist'; exit;
			}elseif($usernameExist == 1){
				echo 'usernameexist';exit;
			}elseif($companyNameExist == 1){
				echo 'companynameexist';exit;
			}
			/*if($usernameExist == 0 && $emailExist == 1 && $companyNameExist == 0){
				echo 'emailexist';
			}elseif($usernameExist == 1 && $emailExist == 0 && $companyNameExist == 0){
				echo 'usernameexist';
			}elseif($usernameExist == 0 && $emailExist == 0 && $companyNameExist == 1){
				echo 'companynameexist';
			}*/
		}
	}
	
/* End of Register functions */	
	

/* Folder Management */
	function addAllPermissions($folderid, $permissionflag)
	{
		$flag = false;
		//add to all devices
		if($permissionflag == 'private')	{
			$permissionflag = "alldevice";
			$flag = true;
		}
		
		 $companyid=$this->session->userdata("companyid");
		 $permissions=$this->permissionsmodel->getAllPermissions();
		 
		 if($permissionflag == "alldevice")
		 {		
				$companydevices=$this->companydevicemodel->getDevicesByCompany($companyid);
				for($i=0;$i<count($companydevices);$i++)
				{
					$deviceid=$companydevices[$i]['deviceid'];
					for($j=0;$j<count($permissions);$j++)
					{
						 $datacc = array();
						 $permissionid=$permissions[$j]['permissionid'];
						 if($permissionid!=6 && $permissionid!=7)
						  {
							 $datacc['deviceid']=$deviceid;
							 $datacc['folderid']=$folderid;
							 $datacc['permissionid']=$permissionid;
							 $datacc['createdby']=$companyid;
							 $datacc['updatedby']=$companyid;
							 $datacc['createdon']=date('Y-m-d H:i');	
							 $this->folderdevicesmodel->insertFolderDevice($datacc);
						  }	 
					}
				}
		 }		
		 
		 if($flag)	{
			$permissionflag = "alluser";
		}
		
		 if($permissionflag == "alluser")
		 {
		 //add to all users
				$companyusers=$this->companyusermodel->getUsersByCompany($companyid);
				for($i=0;$i<count($companyusers);$i++)
				{
					$userid=$companyusers[$i]['companyuserid'];
					for($j=0;$j<count($permissions);$j++)
					{
						 $datacc = array();
						 $permissionid=$permissions[$j]['permissionid'];
						 if($permissionid!=6 && $permissionid!=7)
						  {
							 $datacc['companyuserid']=$userid;
							 $datacc['folderid']=$folderid;
							 $datacc['permissionid']=$permissionid;
							 $datacc['createdby']=$companyid;
							 $datacc['updatedby']=$companyid;
							 $datacc['createdon']=date('Y-m-d H:i');	
							 $this->folderusersmodel->insertFolderUser($datacc);
						  }	 
					}
				}
		 }		
	}
	
	function addParentPermissions($parentfolderid,$newfolderid)
	{
		$companyid=$this->session->userdata("companyid");
		$devicepermissions=$this->folderdevicesmodel->getFolderPermissions($parentfolderid);
		for($i=0;$i<count($devicepermissions);$i++)
		{
				 $datacc = array();
				 $permissionid=$devicepermissions[$i]['permissionid'];
				 $datacc['deviceid']=$devicepermissions[$i]['deviceid'];
				 $datacc['folderid']=$newfolderid;
				 $datacc['permissionid']=$permissionid;
				 $datacc['createdby']=$companyid;
				 $datacc['updatedby']=$companyid;
				 $datacc['createdon']=date('Y-m-d H:i');	
				 $this->folderdevicesmodel->insertFolderDevice($datacc);
		}
		
		$userpermissions=$this->folderusersmodel->getUserPermissions($parentfolderid);
		for($i=0;$i<count($userpermissions);$i++)
		{
				 $datacc = array();
				 $permissionid=$userpermissions[$i]['permissionid'];
				 $datacc['companyuserid']=$userpermissions[$i]['companyuserid'];
				 $datacc['folderid']=$newfolderid;
				 $datacc['permissionid']=$permissionid;
				 $datacc['createdby']=$companyid;
				 $datacc['updatedby']=$companyid;
				 $datacc['createdon']=date('Y-m-d H:i');	
				 $this->folderusersmodel->insertFolderUser($datacc);
		}
		
	}
	
	function addAllPermissionsToAllFolders($deviceid)
	{
		$companyid=$this->session->userdata("companyid");
		$permissions=$this->permissionsmodel->getAllPermissions();
		$folders=$this->foldermodel->getAllFolders($companyid);
		for($i=0;$i<count($folders);$i++)
		{
			$folderid=$folders[$i]['folderid'];	
			for($j=0;$j<count($permissions);$j++)
					{
						 $datacc = array();
						 $permissionid=$permissions[$j]['permissionid'];
						 if($permissionid!=6 && $permissionid!=7)	{
							 $datacc['deviceid']=$deviceid;
							 $datacc['folderid']=$folderid;
							 $datacc['permissionid']=$permissionid;
							 $datacc['createdby']=$companyid;
							 $datacc['updatedby']=$companyid;
							 $datacc['createdon']=date('Y-m-d H:i');	
							 $this->folderdevicesmodel->insertFolderDevice($datacc);
						}	 
					}
		}
	}
	
	function folders()
	{
		
		$this->load->view('CompanyAdmin/folderlist');	
	}
	
	function loadFolderTree()
	{
		$companyid=$this->session->userdata("companyid");
		$expandednodesstr=$this->input->post('expandednodesstr');
		$foldertreearr=array();
		//get folders and files
		$folders=$this->foldermodel->getAllFolders($companyid);

                function alpha($a, $b)
                {
                   return strcmp($b['label'], $a['label']);
                }
                
				function custom_sort($a,$b)
				 {
			          return $a['type']>$b['type'];
			     }
			     
			     
				function multicompare($a,$b){
				   $criteria = array(
				       'type' => 'asc',
				       'label' => 'desc'
				   );
				   foreach($criteria as $what => $order){
				       if($a[$what] == $b[$what]){
				           continue;
				       }
				       return (($order == 'desc')?-1:1) * strcmp($a[$what], $b[$what]);
				   }
			       return 0;
			    }
			     
	            
                
                
		for($i=0;$i<count($folders);$i++)
		{
			$folderid=$folders[$i]['folderid'];		
			$parentid=$folders[$i]['parentfolderid'];
			if($parentid==null)
				$parentid=0;
		
			$foldertemparr=array();
			$foldertemparr['id']=$folderid;
			$foldertemparr['label']=$folders[$i]['foldername'];
			$foldertemparr['active_mode']=$folders[$i]['active_mode'];
			$foldertemparr['parentfolderid']=$parentid;
			$foldertemparr['updatedon']=date('d/m/Y g:i a',strtotime($folders[$i]['updatedon']));;
			$foldertemparr['type']='folder';
			$foldertemparr['accesstype']=$folders[$i]['type'];
			$foldertemparr['htmlid']=$folderid;
			
			array_push($foldertreearr, $foldertemparr);
                       
							$files= $this->folderfilemodel->getFiles($folderid);
							for($j=0;$j<count($files);$j++)
							{
								$foldertemparr=array();
								$foldertemparr['id']=$files[$j]['id'];
								$foldertemparr['label']=$files[$j]['filename'];
								$foldertemparr['parentfolderid']=$folderid;
								$foldertemparr['updatedon']=date('d/m/Y g:i a',strtotime($files[$j]['updatedon']));
								$foldertemparr['type']='file';
								$foldertemparr['accesstype']=$folders[$i]['type'];
								array_push($foldertreearr, $foldertemparr);
						}
                 usort($foldertreearr, "multicompare");
		}
	//	 usort($foldertreearr, "custom_sort");  
		$foldertree= $this->buildFolderTree($foldertreearr);
		$data = array();	
		$flatfolderarr=$this->objectArray_flatten($foldertree,"children");
		$flatfolderarr=array_reverse($flatfolderarr);
		$data['foldertreearr']=	$flatfolderarr;	
		$data['expandednodes']=	$expandednodesstr;	
		//echo json_encode($data);
		$this->load->view('CompanyAdmin/foldertreetable',$data);
	}
	
	
	function addFolder()
	{
		$parentfolderId=$this->input->post('parentfolderid');		
		$companyid = $this->session->userdata("companyid");
		if(Permissionsmodel::checkValidFolder($parentfolderId,$companyid) == 1 || $parentfolderId == 0) 
		{		
			$newfoldername=html_purify(trim($this->input->post('newfoldername')));
			$level=trim($this->input->post('level'));
			$permissionflag=$this->input->post('permflag');
			
			if($parentfolderId == 0)
			   {
			   	 $parentfolderId = NULL;
			   }
			//check folder at root level
			 if(!$this->foldermodel->foldernameexistinroot($newfoldername, $companyid, $parentfolderId))
			  {
			  	// check parent folder
				if(!$this->foldermodel->foldernameexist($newfoldername,$parentfolderId))
				 {
				 	
					$companyid=$this->session->userdata("companyid");
					$bucketname=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;
					$parentdetails=$this->foldermodel->getFolderDetails($parentfolderId);
			
			// Create a folder at root level		
			if($parentfolderId == 0)
			{
				$newpath= $newfoldername;
				$parenttype= 'private';
				$parentfolderId = NULL;
				
			}else{	
				
				$path=$parentdetails['fullpath'];
				$parenttype=$parentdetails['type'];
				$newpath=$path."/".$newfoldername;
			}	
		
		
			$uploaddir = realpath('./uploads/');
			$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc 					= array();
				$datacc['foldername']		= $newfoldername;
				$datacc['companyid']		= $companyid;
				$datacc['fullpath']			= $newpath;
				$datacc['type']				= $parenttype; 
				$datacc['parentfolderid']	= $parentfolderId; 
				$datacc['createdby']		= $companyid;
				$datacc['createdon']		= date('Y-m-d H:i');				
				$newfolderid				= $this->foldermodel->insertFolder($datacc);	
//				if(($level+1)<=3)//add default full access to approved user and devices
//				{
//					$this->addAllPermissions($newfolderid); 
//				}
//				else //add parent permissions
//				{
//				 	$this->addParentPermissions($parentfolderId,$newfolderid);
//				}

				if($permissionflag=="parent")
				{
					$this->addParentPermissions($parentfolderId,$newfolderid);
				}
				
				//set permission for root folder
				if($permissionflag == "alluser" || $permissionflag == "alldevice")
				{
					$this->addAllPermissions($newfolderid, $permissionflag); 
				}
				
				echo "1";
			}
			else {
				echo CREATEFOLDERFAIL;
			}
			
		
		
			  
			  
			  	 }else{
					 	echo FOLDERNAMEEXIST;
				   	  }
			  }else{
					echo FOLDERNAMEEXIST;
				   }
		}else{
			echo CREATEFOLDERFAIL;
			}
	}

	/*
		Edit file name
	*/
	function editFileName()
	{
		$selectedid = $this->input->post('selectedid');			
		$companyid = $this->session->userdata("companyid");
		$editfilename = html_purify(trim($this->input->post('editfilename')));
		$companyid=$this->session->userdata("companyid");
		$filepath=$this->folderfilemodel->getFilePath($selectedid);
		$filename = $this->folderfilemodel->getFileName($selectedid);
		//$filenamearr = explode('.', $filename);	
		$filenamearr = pathinfo($filename);
		$fullpatharr = explode($filenamearr['filename'], $filepath);
		$newpath = $fullpatharr[0].$editfilename.'.'.$filenamearr['extension'];
		$newname = $editfilename.'.'.$filenamearr['extension'];
		//$newpath = $fullpatharr[0].$editfilename.'.'.$filenamearr[1];
		//$newname = $editfilename.'.'.$filenamearr[1];
		
		$bucket=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		
		if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$newpath,S3::ACL_PRIVATE))
		{
			//delete the file from sorce folder
			$this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath);
			//update DB
			$datacc=array();
			$datacc['filename']= $newname;
			$datacc['fullpath']= $newpath;
			$datacc['updatedbydevice']=NULL;
			
			$this->folderfilemodel->updateFile($datacc,$selectedid);
			echo '1';
		}
	}//end function
	
	/*
		Edit folder name
	*/
	function editFolderName()
	{
			$selectedid=$this->input->post('selectedid');			
			$companyid = $this->session->userdata("companyid");
			$editfoldername=html_purify(trim($this->input->post('editfoldername')));
			//check folder at root level
			 if(!$this->foldermodel->foldernameexistinroot($editfoldername, $companyid))
			  {
					$companyid=$this->session->userdata("companyid");
					$bucketname=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;
					//$parentdetails=$this->foldermodel->getFolderDetails($parentfolderId);
					$selectedidFolderdetails=$this->foldermodel->getFolderDetails($selectedid);
					$sourcefoldername = $selectedidFolderdetails['foldername'];
					$parenttype=$selectedidFolderdetails['type'];
					$parentfolderId=$selectedidFolderdetails['parentfolderid'];
					$fullpath=$selectedidFolderdetails['fullpath'];
					
					$fullpatharr = explode($sourcefoldername, $fullpath);
					$tempfullpath = $fullpatharr[0].$editfoldername;
					$newpath = $tempfullpath;
					//print_r($fullpatharr);
					
					$uploaddir = realpath('./uploads/');
					$dummyfile = $uploaddir ."//".'dummyfile.txt';
					$file=fopen($dummyfile,"w");
			
					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
					 {
							//create a new folder
							//update same with DB
							$datacc = array();
							$datacc['foldername']=$editfoldername;
							$datacc['companyid']=$companyid;
							$datacc['fullpath']=$newpath;
							$datacc['type']= $parenttype; 
							$datacc['parentfolderid']=$parentfolderId; 
							$datacc['createdby']=$companyid;
							$datacc['createdon']=date('Y-m-d H:i');				
							$newfolderid=$this->foldermodel->insertFolder($datacc);	
							
							$this->addParentPermissions($selectedid,$newfolderid);
							
							//get all folder list
						    $folderList = $this->foldermodel->getAllParentFolders($selectedid);
						    $folderCount = count($folderList);
							
						    if($folderCount > 0)
							 {
							   for($i=0; $i<=$folderCount; $i++)
							    {
								//move folder
								$movefolderid= $folderList[$i]['folderid'];
								$destinationfolderid= $newfolderid;
								$replaceflag=false;
								$folderdetails=$this->foldermodel->getFolderDetails($movefolderid);
								$sourcefoldername=$folderdetails['foldername'];
								if($this->isFolderExist($sourcefoldername, $destinationfolderid) )
								 {
									$existingfolderid=$this->foldermodel->getfolderID($sourcefoldername,$destinationfolderid);
									{
										$uploadflag=false;
									}
								 }else{
										$uploadflag=true;
									  }
										if($uploadflag)
										{
											$sourcefolderpath=$this->foldermodel->getFolderPath($movefolderid);
											$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
											//$bucket="cuedrive.".$companyid;
											$bucket=ROOTBUCKET;
											$companyfoldername=COMPANYFOLDERNAME.$companyid;				
												
											//delete the source folder from amazon
											$deletefolderarr=$this->getChildren($movefolderid);
											array_push($deletefolderarr,$movefolderid);
							
											for($i=0;$i<count($deletefolderarr);$i++)
											{
												$folderid=$deletefolderarr[$i];
												$files= $this->folderfilemodel->getFiles($folderid);
												for($j=0;$j<count($files);$j++)
												{
													$filepath=$files[$j]['fullpath'];
													$fileid=$files[$j]['id'];
									
													$fullpatharr= explode($sourcefoldername, $filepath);
													$tempfullpath= $sourcefoldername.$fullpatharr[1];
													$destinationfilepath= $destinationfolderpath.'/' .$tempfullpath;
													if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
													{
														if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
														{
															//update folderfile
															$datacc=array();
															$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
															$datacc['updatedbydevice']=NULL;
															$this->folderfilemodel->updateFile($datacc,$fileid);
														}
													 }
												}
												//update folder
												$datacc=array();
												$folderpatharr=explode($sourcefoldername, $sourcefolderpath);
												$tempfullpath=$sourcefoldername.$folderpatharr[1];
												$datacc=array();
												if($folderid==$movefolderid) //change parent id
												{
													$datacc['parentfolderid']=$destinationfolderid;
													$datacc['type'] = $this->foldermodel->getFolderType($destinationfolderid);
												 }
												$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
												$datacc['updatedbydevice']=NULL;
												$this->foldermodel->updateFolder($datacc,$folderid);
										 }
										
										}else{
												echo '0';
											 }
								}//end for loop to copy all files and folders
								
						     }	
							 
							 //copy files from the root folder
							 $files= $this->folderfilemodel->getFiles($selectedid);
							 $fileCount = count($files);
							 if($fileCount > 0)
							  {
							 	for($j=0; $j<$fileCount; $j++)
								 {
										$destinationfolderpath=$this->foldermodel->getFolderPath($newfolderid);
										
										$filepath=$files[$j]['fullpath'];
										$fileid=$files[$j]['id'];
										
										//$fullpatharr=explode($sourcefoldername, $filepath);
										$fullpatharr = explode('/', $filepath);
										
										$tempfullpath=$sourcefoldername.$fullpatharr[1];
										$filename = "/".$fullpatharr[1];
										//$destinationfilepath = $destinationfolderpath.'/' .$tempfullpath;
										
										//if($this->s3->copyObject($bucketname, $companyfoldername.'/'.$filepath, $bucketname, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
										if($this->s3->copyObject($bucketname, $companyfoldername.'/'.$filepath, $bucketname, $companyfoldername.'/'.$destinationfolderpath.$filename, S3::ACL_PRIVATE))
										 {
											if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
											{
												//update folderfile
												$datacc=array();
												$datacc['fullpath']= $destinationfolderpath.$filename;
												$datacc['folderid']= $newfolderid;
												$datacc['updatedbydevice']=NULL;
												$this->folderfilemodel->updateFile($datacc,$fileid);
											}
										}
									}
							 }
							//end copy files
							 echo "1";
						}else{//if create new folder
								echo CREATEFOLDERFAIL;
							 }
			  	 
			  }else{
			  
					echo FOLDERNAMEEXIST;
					
				   }
	}//end function
	     
	
	function uploadFile()
	{
		
	$uploadsuccess=false;
	$parentfolderId=$this->input->post('parentfolderid');
    $companyid=$this->session->userdata("companyid");
    
   
    if(Permissionsmodel::checkValidFolder($parentfolderId,$companyid) == 1) {    
    
	//$bucketname="cuedrive.".$companyid;
	$bucketname=ROOTBUCKET;
	$companyfoldername=COMPANYFOLDERNAME.$companyid;
	$companypackagesize=$this->companyadminmodel->getPackageSize($companyid);//in GB
	$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
	//$bucketsize=$this->getBucketSize($bucketname);//in bytes
	$bucketsize=$this->getCompanyFolderSize($companyid);
	foreach($_FILES['selectedfiles']['tmp_name'] as $key => $tmp_name)
	{
	   	$fileSize=$_FILES["selectedfiles"]["size"][$key];
		$bucketsize=$bucketsize+$fileSize;
               // echo 'FILESIZE:'.$fileSize.'<br/>';

	}	
	if($bucketsize<=$companypackagesize)
	{

		foreach($_FILES['selectedfiles']['tmp_name'] as $key => $tmp_name)
		{
			 $fileName = $_FILES['selectedfiles']['name'][$key];	
		     $fileTempName = $_FILES['selectedfiles']['tmp_name'][$key];
		     $fileSize=$_FILES["selectedfiles"]["size"][$key];		
		
				$uploadfolderpath=$this->foldermodel->getFolderPath($parentfolderId);
				$foldername=$uploadfolderpath;
				if($this->s3->putObjectFile($fileTempName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
				{
					//file uploaded successfullly
					//update DB					
				    $datacc=array();
				    $datacc['filename']=$fileName;
				    $datacc['fullpath']=$foldername.'/' .$fileName;
				    $datacc['folderid']=$parentfolderId;
				    $datacc['filesize']=$fileSize;			   	
	
					if($this->input->post('override')=="true" && $this->folderfilemodel->fileNameExist($fileName,$parentfolderId))//replace 
					{
						 $datacc['updatedbycompany']=$companyid;	
						 $replacefileid=$this->folderfilemodel->getfileID($fileName,$parentfolderId);	    
						 /** 25 May 2015 **/ 
						    $datacc['active_mode'] = '0';			
						/** 25 May 2015 **/	 
					     $this->folderfilemodel->updateFile($datacc,$replacefileid);		
					}
					else { //add new
						 $datacc['createdbycompany']=$companyid;
				    	 $datacc['createdon']=date('Y-m-d H:i');			     
				    	$newfileid=$this->folderfilemodel->insertFile($datacc);
					}
				 
				        Classpush::sendFileUploadNotification($parentfolderId,'upload',$fileName);
					$uploadsuccess=true;	
				}	
				else {
						$uploadsuccess=false;
						
				}
				
		}

	}//end of if size validation
	else {
				echo "error:".APIFILESIZEEXCEED;
	}
	
	if($uploadsuccess==true)
	{
		echo "success";
	}
	else {
		echo "error:".APIFILEUPLOADFAIL;
	}
    }
    else {
    	echo "error:".APIFILEUPLOADFAIL;
    }
	
	}
	function checkFileExist()
	{
		 $parentfolderId=$this->input->post('parentfolderid');
		 //$newfilename=$this->input->post('newfilename');
		 $filenamestr=$this->input->post('filenamestr');
		 $filenamearr=explode("@#", $filenamestr);
		 $found="";
		 for($i=0;$i<count($filenamearr);$i++)
		 {
			if(trim($filenamearr[$i])!="")
			{
				 if($this->folderfilemodel->fileNameExist($filenamearr[$i],$parentfolderId))
				 {
					//echo "0";
					if($found=="")
					  $found=$filenamearr[$i];	
					else
					  $found=$found." and ".$filenamearr[$i];
				 }
	
			}
		 }
		 
		 echo $found;
	
	}
	function downloadFile()
	{

		 if($this->session->userdata("companyid")) {
	
		$selectedfileid=$this->input->post('selectedfileid');
			$companyid=$this->session->userdata("companyid");
	
			if(Permissionsmodel::checkValidFile($selectedfileid,$companyid) == 1) {
		
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$filepath=$this->folderfilemodel->getFilePath($selectedfileid);
		//get authenticated url	
		$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepath, DOWNLOADURLEXPIRYTIME);
		$newurl=str_replace('amp;', '', $newurl);
		
				$file_headers = @get_headers($newurl);
				if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
						//redirect('companyadmin/loadFolderTree');
						//echo 'https://'.$_SERVER['SERVER_NAME'] . '/cuedrive/index.php/companyadmin/folders';
						return false;
				}
				else {
						echo $newurl;
				}
			 }
		else {
			echo base_url().'index.php/companyadmin/noAccess';
			 }
		}
		else {
			redirect('login');
		 }
	}
	
	
	function deleteFile()
	{
		$selectedfileid=$this->input->post('selectedid');
		$companyid=$this->session->userdata("companyid");
		 if(Permissionsmodel::checkValidFile($selectedfileid,$companyid) == 1) {
		
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$filepath=$this->folderfilemodel->getFilePath($selectedfileid);
		
				//delete the file
				if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
				{
					//update the database
					$this->folderfilemodel->deleteFile($selectedfileid);
					echo "1";		
				}
				else {
					echo DELETEFAIL;
				}
		 }
		 else {
			echo DELETEFAIL;
		 }
	}
	
	function deleteFolder()
	{
		$selectedfolderid=$this->input->post('selectedid');
		//for testing
		//$selectedfolderid=8;
			$companyid=$this->session->userdata("companyid");
			if(Permissionsmodel::checkValidFolder($selectedfolderid,$companyid) == 1) {
			
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$folderpath=$this->foldermodel->getFolderPath($selectedfolderid);
		$deletefolderarr=$this->getChildren($selectedfolderid);
		//print_r($deletefolderarr);
		array_push($deletefolderarr,$selectedfolderid);
			rsort($deletefolderarr);
			
		for($i=0;$i<count($deletefolderarr);$i++)
		{
			$folderid=$deletefolderarr[$i];
			$files= $this->folderfilemodel->getFiles($folderid);
			for($j=0;$j<count($files);$j++)
			{
				$filepath=$files[$j]['fullpath'];
				$fileid=$files[$j]['id'];
				if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
				{
						//delete from folderfile
					$this->folderfilemodel->deleteFile($fileid);
				}			
			}
						
					//update the database
					
					
					//delete from folderalloweddevices
					$this->folderdevicesmodel->deleteByFoler($folderid);				
					
					//delete from folderallowedusers
					$this->folderusersmodel->deleteByFoler($folderid);	
					
					//delete from folder
					$this->foldermodel->deleteFolder($folderid);
					
						
			
		}
	
			echo "1";	
			}
	}
	
	function copyFolders()
	{
		$newfoldername = '';
		$movefolderid=$this->input->post('selectedid');
		$destinationfolderid=$this->input->post('destinationselectid');
		$sourcefoldername=trim($this->input->post('sourcefoldername'));
		$companyid=$this->session->userdata("companyid");
		
		if(Permissionsmodel::checkValidFolder($movefolderid,$companyid) == 1) 
		{
			$bucketname=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			
			// Copying the data to root folder. Check the folder name is available at root folder 		
			if($destinationfolderid == 0)	{
				if(!$this->foldermodel->foldernameexistinroot($sourcefoldername, $companyid))	{
					$newfoldername = $sourcefoldername;
				}	
				else
				{
					$index = 1; 					
					$newfoldername = $this->checkFolderExistsInRoot($index, $sourcefoldername, $companyid);
				}
			}
			else{
				if(!$this->foldermodel->foldernameexist($sourcefoldername, $destinationfolderid))	{
					$newfoldername = $sourcefoldername;
				}	
				else {
					$index = 1; 					
					$newfoldername = $this->checkFolderExists($index, $sourcefoldername, $destinationfolderid);
				}
			}
			
			if($destinationfolderid == 0)	{
				$newpath = $newfoldername;
				$sourceFolderDetails=$this->foldermodel->getFolderDetails($movefolderid);
				$parenttype = $sourceFolderDetails['type'];
				$parentfolderid = NULL;
			}
			else{
				$destinationfolderpath=$this->foldermodel->getFolderDetails($destinationfolderid);
				$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
				$sourceFolderDetails=$this->foldermodel->getFolderDetails($movefolderid);
				$parenttype = $sourceFolderDetails['type'];
				$parentfolderid = $destinationfolderid; 
			}
			
			$datacc = array();
			$datacc['foldername']=$newfoldername;
			$datacc['companyid']=$companyid;
			$datacc['fullpath']=$newpath;
			$datacc['type']=$parenttype; 
			$datacc['parentfolderid']=$parentfolderid;
			$datacc['createdby']=$companyid;
			$datacc['createdon']=date('Y-m-d H:i');
			
			$returnFlag = $this->addRecursiveFolder($movefolderid,$datacc);
			
			/*if($destinationfolderid > 0)
			{
				$this->addParentPermissions($destinationfolderid,$newfolderid);
			}
			
			//set permission for root folder
			if($parenttype == 'private' && $destinationfolderid == 0)
			{
				$this->addAllPermissions($newfolderid, $parenttype);
			}*/
			
			echo $returnFlag;
		}
	}
	
	function checkFolderExistsInRoot($index, $sourcefoldername, $companyid)
	{
		$sourcefoldernamenew = $sourcefoldername . '('.$index.')';
		$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
		if($result)	{
			$index ++;
			return $this->checkFolderExistsInRoot($index, $sourcefoldername, $companyid);
		}	
		else 
		{
			return $sourcefoldernamenew;
		}
	}
	
	function checkFolderExists($index, $sourcefoldername, $destinationfolderid)
	{
		$sourcefoldernamenew = $sourcefoldername . '('.$index.')';
		$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
		if($result)	{
			$index ++;
			return $this->checkFolderExists($index, $sourcefoldername, $destinationfolderid);
		}	
		else 
		{
			return $sourcefoldernamenew;
		}
	}
	
	function addRecursiveFolder($folderId,$datacc)
	{
		$uploaddir = realpath('./uploads/');
		$dummyfile = $uploaddir ."//".'dummyfile.txt';
		$file=fopen($dummyfile,"w");
		
		$newpath = $datacc['fullpath'];
		$companyid = $datacc['companyid'];
		$parentfolderid = $datacc['parentfolderid'];
		
		$innerTree = array();
		$innerTree = $this->foldermodel->getOneLevel($folderId);
		
		if(count($innerTree) > 0 && is_array($innerTree))	{
			
			if($this->s3->putObjectFile($dummyfile, ROOTBUCKET, COMPANYFOLDERNAME.$companyid.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{ 
				$newparentfolderid=$this->foldermodel->insertFolder($datacc);
				if(!isset($parentfolderid)){
					$this->addParentPermissions($folderId,$newparentfolderid);
				}
				else{
					$this->addParentPermissions($parentfolderid,$newparentfolderid);
				}
				$fileUploadReponse = $this->copyFolderFiles($companyid, $folderId, $newparentfolderid, $newpath);
				if($fileUploadReponse != 1){
					return $fileUploadReponse;
				}
				
				foreach ($innerTree as $key => $innerVal) {
					$folderDetails = $this->foldermodel->getFolderDetails($innerVal);
					$innerFolderPath = $newpath.'/'.$folderDetails['foldername'];
					$folderData = array();
					$folderData['foldername']=$folderDetails['foldername'];
					$folderData['companyid']=$companyid;
					$folderData['fullpath']=$innerFolderPath;
					$folderData['type']=$folderDetails['type']; 
					$folderData['parentfolderid']=$newparentfolderid;
					$folderData['createdby']=$companyid;
					$folderData['createdon']=date('Y-m-d H:i');
					$this->addRecursiveFolder($innerVal,$folderData);
				}
			}
			else{
				return 2;
			}
			
		}
		else{		
			
			if($this->s3->putObjectFile($dummyfile, ROOTBUCKET, COMPANYFOLDERNAME.$companyid.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{ 
				$newfolderid=$this->foldermodel->insertFolder($datacc);
				if(!isset($parentfolderid)){
					$this->addParentPermissions($folderId,$newfolderid);
				}
				else{
					$this->addParentPermissions($parentfolderid,$newfolderid);
				}
				$fileUploadReponse = $this->copyFolderFiles($companyid, $folderId, $newfolderid, $newpath);
				if($fileUploadReponse != 1){
					return $fileUploadReponse;
				}
			}
			else{
				return 2;
			}
		}
		return 1;
	}
	
	function copyFolderFiles($companyid, $movefolderid, $newfolderid, $folderpath)
	{
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$companypackagesize=$this->companyadminmodel->getPackageSize($companyid);//in GB
		$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
		//$bucketsize=$this->getBucketSize($bucketname);//in bytes
		$bucketsize=$this->getCompanyFolderSize($companyid);
		$uploadsuccess=1;
		
		$files= $this->folderfilemodel->getFiles($movefolderid);
		$fileCount = count($files);
		if($fileCount > 0)
		{
			for($j=0; $j < $fileCount; $j++)
			{
				$fileSize=$files[$j]['filesize'];
				$bucketsize=$bucketsize+$fileSize;

				if($bucketsize<=$companypackagesize)
				{
					$fileName = $files[$j]['filename'];	
					$fileTempName = $files[$j]['fullpath'];
					$fileSize=$files[$j]['filesize'];		

					//$uploadfolderpath=$this->foldermodel->getFolderPath($newfolderid);
					//$foldername=$uploadfolderpath;
					$oldfolderpath=$this->foldermodel->getFolderPath($movefolderid);
					$oldfoldername=$oldfolderpath;
					$foldername=$folderpath;
					
					if($this->s3->copyObject($bucketname, $companyfoldername.'/'.$oldfoldername.'/' .$fileName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
					{
						//file uploaded successfullly
						//update DB					
						$insertcc=array();
						$insertcc['filename']=$fileName;
						$insertcc['fullpath']=$foldername.'/' .$fileName;
						$insertcc['folderid']=$newfolderid;
						$insertcc['filesize']=$fileSize;				
						$insertcc['createdbycompany']=$companyid;
						$insertcc['createdon']=date('Y-m-d H:i');			     
						$newfileid=$this->folderfilemodel->insertFile($insertcc);
					 
						//Classpush::sendFileUploadNotification($newfolderid,'upload',$fileName);
						$uploadsuccess=1;	
					}	
					else {
						$uploadsuccess=2;
					}
				}
				else {
					$uploadsuccess=3;
				}
			}
		}
		return $uploadsuccess;
	}
	
	function copyFolder()
	{
	
		$movefolderid=$this->input->post('selectedid');
		$destinationfolderid=$this->input->post('destinationselectid');
		$sourcefoldername=trim($this->input->post('sourcefoldername'));
		
		$companyid=$this->session->userdata("companyid");
		if(Permissionsmodel::checkValidFolder($movefolderid,$companyid) == 1) {
			
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$folderpath=$this->foldermodel->getFolderPath($movefolderid);
		/* $copyfolderarr=$this->getChildren($movefolderid); */
		
		/* Copying the data to root folder. Check the folder name is available at root folder */		
		if($destinationfolderid == 0)	{
			$destinationfolderpath = '';
			$parenttype = 'private';
			
			if(!$this->foldermodel->foldernameexistinroot($sourcefoldername, $companyid))	{
				$newfoldername = $sourcefoldername;
				$newpath = $newfoldername;
/* 				echo '<pre> newfoldername = '; echo $newfoldername; echo '</pre>';
				echo '<pre> newpath = '; echo $newpath; echo '</pre>'; */
			}	else	{
				$index = 1; /* Folder1 */
				$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';				
				$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
				if(!$result)	{
					$newfoldername = $sourcefoldernamenew;
					$newpath = $newfoldername;
				}	else {
					$index ++;	/* Folder2 */
					$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
					$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
					if(!$result)	{
						$newfoldername = $sourcefoldernamenew;
						$newpath = $newfoldername;
					}	else {
						$index ++;	/* Folder3 */
						$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
						$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
						if(!$result)	{
							$newfoldername = $sourcefoldernamenew;
							$newpath = $newfoldername;
						}	else {
							$index ++;	/* Folder4 */
							$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
							$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
							if(!$result)	{
								$newfoldername = $sourcefoldernamenew;
								$newpath = $newfoldername;
							}	else {
								$index ++;	/* Folder5 */
								$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
								$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
								if(!$result)	{
									$newfoldername = $sourcefoldernamenew;
									$newpath = $newfoldername;
								}	else {
									$index ++;	/* Folder6 */
									$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
									$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
									if(!$result)	{
										$newfoldername = $sourcefoldernamenew;
										$newpath = $newfoldername;
									}	else {
										$index ++;	/* Folder7 */
										$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
										$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
										if(!$result)	{
											$newfoldername = $sourcefoldernamenew;
											$newpath = $newfoldername;
										}	else {
											$index ++;	/* Folder8 */
											$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
											$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
											if(!$result)	{
												$newfoldername = $sourcefoldernamenew;
												$newpath = $newfoldername;
											}	else {
												$index ++;	/* Folder9 */
												$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
												$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
											}
										}
									}
								}
							}
						}
					}
				}
			}		
				/* echo '<pre> newfoldername = '; echo $newfoldername; echo '</pre>';
				echo '<pre> newpath = '; echo $newpath; echo '</pre>';			 */
		} 
		
		else {
			
			if(!$this->foldermodel->foldernameexist($sourcefoldername, $destinationfolderid))	{
				$newfoldername = $sourcefoldername;
			}	else {
				$index = 1; /* Folder1 */
				$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
				$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
				if(!$result)
				{
					$newfoldername = $sourcefoldernamenew;
				} 
				else 
				{
					$index++;	/* Folder2 */
					$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
					$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
					if(!$result) 
					{
						$newfoldername = $sourcefoldernamenew;
					}	
					else	
					{
						$index++;	/* Folder3 */
						$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
						$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
						if(!$result)	
						{
							$newfoldername = $sourcefoldernamenew;
						}
						else
						{
							$index++;	/* Folder4 */
							$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
							$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
							if(!$result)
							{
								$newfoldername = $sourcefoldernamenew;
							}
							else
							{
								$index++;	/* Folder5 */
								$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
								$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
								if(!$result)
								{
									$newfoldername = $sourcefoldernamenew;
								}
								else
								{
									$index++;	/* Folder6 */
									$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
									$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
									if(!$result)
									{
										$newfoldername = $sourcefoldernamenew;
									}
									else
									{
										$index++;	/* Folder7 */
										$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
										$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
										if(!$result)
										{
											$newfoldername = $sourcefoldernamenew;
										}
										else
										{
											$index++;	/* Folder8 */
											$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
											$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
											if(!$result)
											{
												$newfoldername = $sourcefoldernamenew;
											}
											else
											{
												$index++;	/* Folder9 */
												$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
												$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
												if(!$result)
												{
													$newfoldername = $sourcefoldernamenew;
												}
												else
												{
													$index++;	/* Folder10 */
													$sourcefoldernamenew = $sourcefoldername . ' ('.$index.')';
													$result = $this->foldermodel->foldernameexist($sourcefoldernamenew, $destinationfolderid);
												}
											}
										}
									}
								}
							}	
						}
					}
				}
			}
			
			$destinationfolderpath=$this->foldermodel->getFolderDetails($destinationfolderid);
			/* $newfoldername = $sourcefoldername; */
			$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
			$parenttype = $destinationfolderpath['type'];
		}
		
		/* echo '<pre> destinationfolderpath : ';
			print_r($destinationfolderpath);
			echo 'newfoldername : ' . $newfoldername;
		echo '</pre>';
		 */
		 
		$tree = array();
		$tree = $this->foldermodel->getOneLevel($movefolderid);
		
		/* echo '<pre> Zero Parent: ' . $movefolderid;
			print_r($tree);
		echo '</pre>'; */
        
		if(count($tree) > 0 && is_array($tree))	{
			$flagroot = false;
			if($destinationfolderid != 0) {
					$destinationfolderpath=$this->foldermodel->getFolderDetails($movefolderid);
				} else {
					$destinationfolderpath=$this->foldermodel->getFolderDetails($destinationfolderid);
					$flagroot = true;
				}	
			foreach ($tree as $key => $val) {
				/* echo "\nVALUE = " . $val; */
				
				//$newfoldername = $sourcefoldername;
				$foldername = $this->foldermodel->getFolderDetails($val);
				$newfoldername = $foldername['foldername'];
				if(!$this->foldermodel->foldernameexistinroot($newfoldername, $companyid))	{					
					$newpath = $newfoldername;
				}	else	{
					$index = 1; /* Folder1 */
					$sourcefoldernamenew = $newfoldername . ' ('.$index.')';				
					$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
					if(!$result) {
						$newfoldername = $sourcefoldernamenew;
					}	else	{
						$index ++;	/* Folder2 */
						$sourcefoldernamenew = $newfoldername . ' ('.$index.')';
						$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
						if(!$result) {
							$newfoldername = $sourcefoldernamenew;
						}	else	{
							$index ++;	/* Folder3 */
							$sourcefoldernamenew = $newfoldername . ' ('.$index.')';
							$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
							if(!$result) {
								$newfoldername = $sourcefoldernamenew;
							}	else	{
								$index ++;	/* Folder4 */
								$sourcefoldernamenew = $newfoldername . ' ('.$index.')';
								$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
								if(!$result) {
									$newfoldername = $sourcefoldernamenew;
								}	else	{
									$index ++;	/* Folder5 */
									$sourcefoldernamenew = $newfoldername . ' ('.$index.')';
									$result = $this->foldermodel->foldernameexistinroot($sourcefoldernamenew, $companyid);
								}
							}
						}
					}
				}	
				
				$newfolderid = $foldername['folderid'];
				$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
				/* $parenttype = $destinationfolderpath['type'];			 */
				$parenttype = 'private';
				$tree = $this->foldermodel->getOneLevel($val);
			
			/* echo '<pre> First Parent: ' . $val;
			print_r($tree);
			echo '</pre>'; */
			
			$uploaddir = realpath('./uploads/');
			$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$newfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$newpath;
				$datacc['type']=$parenttype;
				if($destinationfolderid != 0)	{
					$datacc['parentfolderid']=$movefolderid;
				} else {
					if($flagroot)	{
						$datacc['parentfolderid']	=	$rootparentfolderid; /**/
					}	else {
						$datacc['parentfolderid']	=	NULL;
					}
					
				}				
				/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');
				/* echo '<pre>First'; print_r($datacc); echo '</pre>'; */
				$newfolderid=$this->foldermodel->insertFolder($datacc);	
				$rootparentfolderid = $newfolderid;
				$insertcc = array();
				$files= $this->folderfilemodel->getFiles($val);
				/* echo $this->db->last_query(); */
				$fileCount = count($files);
				
				if($fileCount > 0)
				{
					for($j=0; $j < $fileCount; $j++)
					{
						$fileName=$files[$j]['filename'];
						$fileSize=$files[$j]['filesize'];
						$insertcc['createdbycompany']=$companyid;
						$insertcc['createdon']=date('Y-m-d H:i');
						$insertcc['filename']=$fileName;
						$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
						if($destinationfolderid != 0)	{
							$insertcc['folderid']=$movefolderid;
						}	else	{
							$insertcc['folderid']=$newfolderid;
						}						
						$insertcc['filesize']=$fileSize;	
						
						/* echo '<pre>First'; print_r($insertcc); echo '</pre>'; */
					
						$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);
						//get authenticated url	
						$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
						$newurl=str_replace('amp;', '', $newurl);		
						$file_data = @file_get_contents($newurl);
				
						$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
						$newfileid=$this->folderfilemodel->insertFile($insertcc);
					}
					
				}	
				
				
			}
			
			/* if($val > 0)
			{
				$this->addParentPermissions($val,$newfolderid);
			}
			
			//set permission for root folder
			if($parenttype == 'private' && $val == 0)
			{				
				$this->addAllPermissions($newfolderid, $parenttype); 
			} */
			$parentidval1 = $val;
			foreach ($tree as $key => $val) {
				/* echo "\nVALUE = " . $val; */
				$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval1);
				/* $newfoldername = $sourcefoldername; */
				$foldername = $this->foldermodel->getFolderDetails($val);
				$newfoldername = $foldername['foldername'];
				$newfolderid = $foldername['folderid'];
				$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
				/* $parenttype = $destinationfolderpath['type']; */
				$parenttype = 'private';
				$tree = $this->foldermodel->getOneLevel($val);
				
				/* if(count($tree) > 0 && is_array($tree))	{ */
					/* echo '<pre> Second Parent: ' . $val;
						print_r($tree);
					echo '</pre>'; */
					
					$uploaddir = realpath('./uploads/');
					$dummyfile = $uploaddir ."//".'dummyfile.txt';
					$file=fopen($dummyfile,"w");
					
					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
					{
						//successfully created
						//update DB
						$datacc = array();
						$datacc['foldername']=$newfoldername;
						$datacc['companyid']=$companyid;
						$datacc['fullpath']=$newpath;
						$datacc['type']=$parenttype; 
						$datacc['parentfolderid']=$parentidval1; 
						/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
						$datacc['createdby']=$companyid;
						$datacc['createdon']=date('Y-m-d H:i');				
						/* echo '<pre>Second'; print_r($datacc); echo '</pre>'; */
						$newfolderid=$this->foldermodel->insertFolder($datacc);	
					}
					
					$insertcc = array();
					$files= $this->folderfilemodel->getFiles($val);
					$fileCount = count($files);
					
					if($fileCount > 0)
					{
						for($j=0; $j < $fileCount; $j++)
						{
							$fileName=$files[$j]['filename'];
							$fileSize=$files[$j]['filesize'];
							$insertcc['createdbycompany']=$companyid;
							$insertcc['createdon']=date('Y-m-d H:i');
							$insertcc['filename']=$fileName;
							$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
							/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
							$insertcc['folderid']=$movefolderid;
							$insertcc['filesize']=$fileSize;	
							
							$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);
							//get authenticated url	
							$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
							$newurl=str_replace('amp;', '', $newurl);		
							$file_data = @file_get_contents($newurl);
					
							$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
							
							$newfileid=$this->folderfilemodel->insertFile($insertcc);
						}
					}	
					
					/* if($val > 0)
					{
						$this->addParentPermissions($val,$newfolderid);
					}
					echo 'parenttype = ' . $parenttype; 
					echo 'destinationfolderid = ' . $val;
					//set permission for root folder
					if($parenttype == 'private' && $val == 0)
					{
						echo 'Inside';
						$this->addAllPermissions($newfolderid, $parenttype); 
					} */
					$parentidval2 = $val;
					foreach ($tree as $key => $val) {
						/* echo "\nVALUE = " . $val; */
						$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval2);
						/* $newfoldername = $sourcefoldername; */
						$foldername = $this->foldermodel->getFolderDetails($val);
						$newfoldername = $foldername['foldername'];
						$newfolderid = $foldername['folderid'];
						$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
						/* $parenttype = $destinationfolderpath['type']; */
						$parenttype = 'private';
						$tree = $this->foldermodel->getOneLevel($val);
						
						/* if(count($tree) > 0 && is_array($tree))	{ */
							/* echo '<pre> Third Parent: ' . $val;
								print_r($tree);
							echo '</pre>'; */
							
							$uploaddir = realpath('./uploads/');
							$dummyfile = $uploaddir ."//".'dummyfile.txt';
							$file=fopen($dummyfile,"w");
							
							if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
							{
								//successfully created
								//update DB
								$datacc = array();
								$datacc['foldername']=$newfoldername;
								$datacc['companyid']=$companyid;
								$datacc['fullpath']=$newpath;
								$datacc['type']=$parenttype; 
								$datacc['parentfolderid']=$parentidval2;
/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it								
								$datacc['createdby']=$companyid;
								$datacc['createdon']=date('Y-m-d H:i');			
								/* echo '<pre>Third'; print_r($datacc); echo '</pre>'; */
								$newfolderid=$this->foldermodel->insertFolder($datacc);	
							}
							
							$insertcc = array();
							$files= $this->folderfilemodel->getFiles($val);
							$fileCount = count($files);
							
							if($fileCount > 0)
							{
								for($j=0; $j < $fileCount; $j++)
								{
									$fileName=$files[$j]['filename'];
									$fileSize=$files[$j]['filesize'];
									$insertcc['createdbycompany']=$companyid;
									$insertcc['createdon']=date('Y-m-d H:i');
									$insertcc['filename']=$fileName;
									/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
									$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
									$insertcc['folderid']=$movefolderid;
									$insertcc['filesize']=$fileSize;	
									
									$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
									//get authenticated url	
									$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
									$newurl=str_replace('amp;', '', $newurl);		
									$file_data = @file_get_contents($newurl);
						
									$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
									$newfileid=$this->folderfilemodel->insertFile($insertcc);
								}
								
							}	
							
							$parentidval3 = $val;
							foreach ($tree as $key => $val) {
								/* echo "\nVALUE = " . $val; */
								$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval3);
								/* $newfoldername = $sourcefoldername; */
								$foldername = $this->foldermodel->getFolderDetails($val);
								$newfoldername = $foldername['foldername'];
								$newfolderid = $foldername['folderid'];
								$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
								$parenttype = $destinationfolderpath['type'];
				
								$tree = $this->foldermodel->getOneLevel($val);
								
								/* if(count($tree) > 0  && is_array($tree))	{ */
									/* echo '<pre> Fourth Parent: ' . $val;
										print_r($tree);
									echo '</pre>'; */
									
									$uploaddir = realpath('./uploads/');
									$dummyfile = $uploaddir ."//".'dummyfile.txt';
									$file=fopen($dummyfile,"w");
									
									if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
									{
										//successfully created
										//update DB
										$datacc = array();
										$datacc['foldername']=$newfoldername;
										$datacc['companyid']=$companyid;
										$datacc['fullpath']=$newpath;
										$datacc['type']=$parenttype; 
										$datacc['parentfolderid']=$parentidval3; 
										/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
										$datacc['createdby']=$companyid;
										$datacc['createdon']=date('Y-m-d H:i');				
										/* echo '<pre>Fourth'; print_r($datacc); echo '</pre>'; */
										$newfolderid=$this->foldermodel->insertFolder($datacc);	
									}
									
									$insertcc = array();
									$files= $this->folderfilemodel->getFiles($val);
									$fileCount = count($files);
									
									if($fileCount > 0)
									{
										for($j=0; $j < $fileCount; $j++)
										{
											$fileName=$files[$j]['filename'];
											$fileSize=$files[$j]['filesize'];
											$insertcc['createdbycompany']=$companyid;
											$insertcc['createdon']=date('Y-m-d H:i');
											$insertcc['filename']=$fileName;
											/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
											$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
											$insertcc['folderid']=$movefolderid;
											$insertcc['filesize']=$fileSize;	
											
											$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
											//get authenticated url	
											$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
											$newurl=str_replace('amp;', '', $newurl);		
											$file_data = @file_get_contents($newurl);
								
											$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
											$newfileid=$this->folderfilemodel->insertFile($insertcc);
										}
										/* $newfileid=$this->folderfilemodel->insertFile($insertcc); */
									}	
									$parentidval4 = $val;
									foreach ($tree as $key => $val) {
										/* echo "\nVALUE = " . $val; */
										$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval4);
										/* $newfoldername = $sourcefoldername; */
										$foldername = $this->foldermodel->getFolderDetails($val);
										$newfoldername = $foldername['foldername'];
										$newfolderid = $foldername['folderid'];
										$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
										$parenttype = $destinationfolderpath['type'];
				
										$tree = $this->foldermodel->getOneLevel($val);
										
										/* if(count($tree) > 0 && is_array($tree))	{ */
											/* echo '<pre> Fifth Parent: '. $val;
												print_r($tree);
											echo '</pre>'; */
											
											$uploaddir = realpath('./uploads/');
											$dummyfile = $uploaddir ."//".'dummyfile.txt';
											$file=fopen($dummyfile,"w");
											
											if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
											{
												//successfully created
												//update DB
												$datacc = array();
												$datacc['foldername']=$newfoldername;
												$datacc['companyid']=$companyid;
												$datacc['fullpath']=$newpath;
												$datacc['type']=$parenttype; 
												$datacc['parentfolderid']=$parentidval4; 
												/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
												$datacc['createdby']=$companyid;
												$datacc['createdon']=date('Y-m-d H:i');
												/* echo '<pre>Fifth'; print_r($datacc); echo '</pre>'; */
												$newfolderid=$this->foldermodel->insertFolder($datacc);	
											}
											
											$insertcc = array();
											$files= $this->folderfilemodel->getFiles($val);
											$fileCount = count($files);
											
											if($fileCount > 0)
											{
												for($j=0; $j < $fileCount; $j++)
												{
													$fileName=$files[$j]['filename'];
													$fileSize=$files[$j]['filesize'];
													$insertcc['createdbycompany']=$companyid;
													$insertcc['createdon']=date('Y-m-d H:i');
													$insertcc['filename']=$fileName;
													/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
													$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
													$insertcc['folderid']=$movefolderid;
													$insertcc['filesize']=$fileSize;	
												
													$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
													//get authenticated url	
													$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
													$newurl=str_replace('amp;', '', $newurl);		
													$file_data = @file_get_contents($newurl);
										
													$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
													$newfileid=$this->folderfilemodel->insertFile($insertcc);
													
												}
											}	
											
											$parentidval5 = $val;
											foreach ($tree as $key => $val) {
												/* echo "\nVALUE = " . $val; */
												$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval5);
												/* $newfoldername = $sourcefoldername; */
												$foldername = $this->foldermodel->getFolderDetails($val);
												$newfoldername = $foldername['foldername'];
												$newfolderid = $foldername['folderid'];
												$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
												$parenttype = $destinationfolderpath['type'];
												
												$tree = $this->foldermodel->getOneLevel($val);
												
												/* if(count($tree) > 0 && is_array($tree))	{ */
													/* echo '<pre> Sixth Parent: '. $val;
														print_r($tree);
													echo '</pre>'; */
													
													$uploaddir = realpath('./uploads/');
													$dummyfile = $uploaddir ."//".'dummyfile.txt';
													$file=fopen($dummyfile,"w");
													
													if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
													{
														//successfully created
														//update DB
														$datacc = array();
														$datacc['foldername']=$newfoldername;
														$datacc['companyid']=$companyid;
														$datacc['fullpath']=$newpath;
														$datacc['type']=$parenttype; 
														$datacc['parentfolderid']=$parentidval5; 
														/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
														$datacc['createdby']=$companyid;
														$datacc['createdon']=date('Y-m-d H:i');				
														$newfolderid=$this->foldermodel->insertFolder($datacc);	
													}
													
													
													$insertcc = array();
													$files= $this->folderfilemodel->getFiles($val);
													$fileCount = count($files);
													
													if($fileCount > 0)
													{
														for($j=0; $j < $fileCount; $j++)
														{
															$fileName=$files[$j]['filename'];
															$fileSize=$files[$j]['filesize'];
															$insertcc['createdbycompany']=$companyid;
															$insertcc['createdon']=date('Y-m-d H:i');
															$insertcc['filename']=$fileName;
															/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
															$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
															$insertcc['folderid']=$movefolderid;
															$insertcc['filesize']=$fileSize;	
														
															$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);	
															//get authenticated url	
															$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
															$newurl=str_replace('amp;', '', $newurl);		
															$file_data = @file_get_contents($newurl);
												
															$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
															$newfileid=$this->folderfilemodel->insertFile($insertcc);
														}
													}	
													
													$parentidval6 = $val;
													foreach ($tree as $key => $val) {
														
														$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval6);
														/* $newfoldername = $sourcefoldername; */
														$foldername = $this->foldermodel->getFolderDetails($val);
														$newfoldername = $foldername['foldername'];
														$newfolderid = $foldername['folderid'];
														$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
														$parenttype = $destinationfolderpath['type'];
																
														$tree = $this->foldermodel->getOneLevel($val);
														
														/* if(count($tree) > 0 && is_array($tree))	{ */
															/* echo '<pre> Seventh Parent: '. $val;
																print_r($tree);
															echo '</pre>'; */
															
															$uploaddir = realpath('./uploads/');
															$dummyfile = $uploaddir ."//".'dummyfile.txt';
															$file=fopen($dummyfile,"w");
															
															/* if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
															{ */
																//successfully created
																//update DB
																$datacc = array();
																$datacc['foldername']=$newfoldername;
																$datacc['companyid']=$companyid;
																$datacc['fullpath']=$newpath;
																$datacc['type']=$parenttype; 
																$datacc['parentfolderid']=$parentidval6; 
																/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
																$datacc['createdby']=$companyid;
																$datacc['createdon']=date('Y-m-d H:i');				
																$newfolderid=$this->foldermodel->insertFolder($datacc);	
															/* } */
															
															$insertcc = array();
															$files= $this->folderfilemodel->getFiles($val);
															$fileCount = count($files);
															
															if($fileCount > 0)
															{
																for($j=0; $j < $fileCount; $j++)
																{
																	$fileName=$files[$j]['filename'];
																	$fileSize=$files[$j]['filesize'];
																	$insertcc['createdbycompany']=$companyid;
																	$insertcc['createdon']=date('Y-m-d H:i');
																	$insertcc['filename']=$fileName;
																	/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
																	$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
																	$insertcc['folderid']=$movefolderid;
																	$insertcc['filesize']=$fileSize;	
																
																	$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
																	//get authenticated url	
																	$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
																	$newurl=str_replace('amp;', '', $newurl);		
																	$file_data = @file_get_contents($newurl);
																	$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
																	$newfileid=$this->folderfilemodel->insertFile($insertcc);
																}
															}	
															
															$parentidval7 = $val;
															foreach ($tree as $key => $val) {
																$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval7);
																/* $newfoldername = $sourcefoldername; */
																$foldername = $this->foldermodel->getFolderDetails($val);
				$newfoldername = $foldername['foldername'];
				$newfolderid = $foldername['folderid'];
																$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
																$parenttype = $destinationfolderpath['type'];
																
																$tree = $this->foldermodel->getOneLevel($val);
																
																/* if(count($tree) > 0 && is_array($tree))	{ */
																	/* echo '<pre> Eighth Parent: '. $val;
																		print_r($tree);
																	echo '</pre>'; */
																	
																	$uploaddir = realpath('./uploads/');
																	$dummyfile = $uploaddir ."//".'dummyfile.txt';
																	$file=fopen($dummyfile,"w");
																	
																	if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
																	{
																		//successfully created
																		//update DB
																		$datacc = array();
																		$datacc['foldername']=$newfoldername;
																		$datacc['companyid']=$companyid;
																		$datacc['fullpath']=$newpath;
																		$datacc['type']=$parenttype; 
																		$datacc['parentfolderid']=$parentidval7; 
																		/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
																		$datacc['createdby']=$companyid;
																		$datacc['createdon']=date('Y-m-d H:i');				
																		$newfolderid=$this->foldermodel->insertFolder($datacc);	
																	}
																	
																	
																	$insertcc = array();
																	$files= $this->folderfilemodel->getFiles($val);
																	$fileCount = count($files);
																	
																	if($fileCount > 0)
																	{
																		for($j=0; $j < $fileCount; $j++)
																		{
																			$fileName=$files[$j]['filename'];
																			$fileSize=$files[$j]['filesize'];
																			$insertcc['createdbycompany']=$companyid;
																			$insertcc['createdon']=date('Y-m-d H:i');
																			$insertcc['filename']=$fileName;
																			/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
																			$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
																			$insertcc['folderid']=$movefolderid;
																			$insertcc['filesize']=$fileSize;	
																		
																			$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
																			//get authenticated url	
																			$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
																			$newurl=str_replace('amp;', '', $newurl);		
																			$file_data = @file_get_contents($newurl);
																
																			$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
																			$newfileid=$this->folderfilemodel->insertFile($insertcc);
																		}
																	}	
																	
																	$parentidval8 = $val;
																	foreach ($tree as $key => $val) {
																		$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval8);
																		/* $newfoldername = $sourcefoldername; */
																		$foldername = $this->foldermodel->getFolderDetails($val);
				$newfoldername = $foldername['foldername'];
				$newfolderid = $foldername['folderid'];
																		$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
																		$parenttype = $destinationfolderpath['type'];
																		
																		$tree = $this->foldermodel->getOneLevel($val);
																		
																		/* if(count($tree) > 0 && is_array($tree))	{ */
																			/* echo '<pre> Ninth Parent: '. $val;
																				print_r($tree);
																			echo '</pre>'; */
																			
																			$uploaddir = realpath('./uploads/');
																			$dummyfile = $uploaddir ."//".'dummyfile.txt';
																			$file=fopen($dummyfile,"w");
																			
																			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
																			{
																				//successfully created
																				//update DB
																				$datacc = array();
																				$datacc['foldername']=$newfoldername;
																				$datacc['companyid']=$companyid;
																				$datacc['fullpath']=$newpath;
																				$datacc['type']=$parenttype; 
																				$datacc['parentfolderid']=$parentidval8; 
																				/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
																				$datacc['createdby']=$companyid;
																				$datacc['createdon']=date('Y-m-d H:i');				
																				$newfolderid=$this->foldermodel->insertFolder($datacc);	
																			}
																			
																			
																			$insertcc = array();
																			$files= $this->folderfilemodel->getFiles($val);
																			$fileCount = count($files);
																			
																			if($fileCount > 0)
																			{
																				for($j=0; $j < $fileCount; $j++)
																				{
																					$fileName=$files[$j]['filename'];
																					$fileSize=$files[$j]['filesize'];
																					$insertcc['createdbycompany']=$companyid;
																					$insertcc['createdon']=date('Y-m-d H:i');
																					$insertcc['filename']=$fileName;
																					/* $insertcc['fullpath']=$newpath.'/' .$fileName; */
																					$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
																					$insertcc['folderid']=$movefolderid;
																					$insertcc['filesize']=$fileSize;	
																				
																					
																					$filepathnew=$this->folderfilemodel->getFilePath($files[$j]['id']);								
																			//get authenticated url	
																			$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepathnew, DOWNLOADURLEXPIRYTIME);
																			$newurl=str_replace('amp;', '', $newurl);		
																			$file_data = @file_get_contents($newurl);
																
																			$this->s3->putObjectFile($file_data, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE);
																			$newfileid=$this->folderfilemodel->insertFile($insertcc);
																				}
																			}	
																			
																			$parentidval9 = $val;
																			foreach ($tree as $key => $val) {
																				
																				$destinationfolderpath=$this->foldermodel->getFolderDetails($parentidval9);
																				/* $newfoldername = $sourcefoldername; */
																				$foldername = $this->foldermodel->getFolderDetails($val);
				$newfoldername = $foldername['foldername'];
				$newfolderid = $foldername['folderid'];
																				$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
																				$parenttype = $destinationfolderpath['type'];
				
																				$tree = $this->foldermodel->getOneLevel($val);
																				
																				/* if(count($tree) > 0 && is_array($tree))	{ */
																					/* echo '<pre> Tenth Parent: '. $val;
																						print_r($tree);
																					echo '</pre>'; */
																					
																					$uploaddir = realpath('./uploads/');
																					$dummyfile = $uploaddir ."//".'dummyfile.txt';
																					$file=fopen($dummyfile,"w");
																					
																					if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
																					{
																						//successfully created
																						//update DB
																						$datacc = array();
																						$datacc['foldername']=$newfoldername;
																						$datacc['companyid']=$companyid;
																						$datacc['fullpath']=$newpath;
																						$datacc['type']=$parenttype; 
																						$datacc['parentfolderid']=$parentidval9; 
																						/* $datacc['newfolderid']=$newfolderid; */	//Should Comment it
																						$datacc['createdby']=$companyid;
																						$datacc['createdon']=date('Y-m-d H:i');				
																						$newfolderid=$this->foldermodel->insertFolder($datacc);	
																					}
																				/* } */
																			}
																		/* } */
																	}
																/* } */
															}
														/* } */
													}
												/* } */
											}
										/* } */
									}
								/* } */
							}
						/* } */
					}
				}
		/* 	} */
			
			}
		}	
		else {
			
			/* echo '<pre> First Parent: ' . $movefolderid;
			print_r($tree);
			echo '\nbucketname = ' . $bucketname;
			echo '\ncompanyfoldername = ' . $companyfoldername;						
			echo '\ndestinationfolderid = ' . $destinationfolderid;			
			echo '</pre>'; */
			
			$uploaddir = realpath('./uploads/');
			$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			
			/* if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{ */
				//successfully created
				//update DB				
				
				
				$destinationfolderpath=$this->foldermodel->getFolderDetails($movefolderid);
				$newpath = $destinationfolderpath['fullpath']."/". $newfoldername;
				$parenttype = $destinationfolderpath['type'];
				
				$datacc = array();
				$datacc['foldername']=$newfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$newpath;
				$datacc['type']=$parenttype; 
				if($destinationfolderid != 0)	{
					$datacc['parentfolderid']=$destinationfolderid; 
				}				
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');				
				/* echo '<pre>First'; print_r($datacc); echo '</pre>'; */
				$newfolderid=$this->foldermodel->insertFolder($datacc);	
				
				$insertcc = array();
				$files= $this->folderfilemodel->getFiles($movefolderid);
				/* echo $this->db->last_query(); */
				$fileCount = count($files);
				/* echo '<pre>First'; print_r($files); echo '</pre>'; */
				if($fileCount > 0)
				{
					for($j=0; $j < $fileCount; $j++)
					{
						$fileName=$files[$j]['filename'];
						$fileSize=$files[$j]['filesize'];
						$insertcc['createdbycompany']=$companyid;
						$insertcc['createdon']=date('Y-m-d H:i');
						$insertcc['filename']=$fileName;
						/* $insertcc['fullpath']=$newpath.'/' .$fileName;//Folder name */
						$insertcc['fullpath']=$destinationfolderpath['fullpath'].'/' .$fileName;
						$insertcc['folderid']=$newfolderid;
						$insertcc['filesize']=$fileSize;	
					
						/* echo '<pre>First'; print_r($insertcc); echo '</pre>'; */
						$newfileid=$this->folderfilemodel->insertFile($insertcc);
					}
				}	
				
			/* } */
			
			if($destinationfolderid > 0)
			{
				
				$this->addParentPermissions($destinationfolderid,$newfolderid);
				/* echo 'Inside1';	
				echo 'destinationfolderid = ' . $destinationfolderid;
				echo 'newfolderid = ' . $newfolderid; */
			}
			/* echo 'parenttype = ' . $parenttype;  */
			/* echo 'destinationfolderid = ' . $destinationfolderid; */
			//set permission for root folder
			if($parenttype == 'private' && $destinationfolderid == 0)
			{
				
				$this->addAllPermissions($newfolderid, $parenttype); 
				 
				/*echo 'parenttype = ' . $parenttype;
				echo 'newfolderid = ' . $newfolderid; */
				
			}
		}
	
		/* array_push($copyfolderarr,$movefolderid);
		rsort($copyfolderarr);
		 */
		
/* 		
		for($i=0;$i<count($copyfolderarr);$i++)
		{ */
			/* $folderid=$copyfolderarr[$i];
			$files= $this->folderfilemodel->getFiles($folderid);
			for($j=0;$j<count($files);$j++)
			{
				$filepath=$files[$j]['fullpath'];
				$fileid=$files[$j]['id'];	
				 */
			/*
if($this->s3->copyObject($bucketname, $companyfoldername.'/'.$filepath, $bucketname, $companyfoldername.'/'.$destinationfolderpath,S3::ACL_PRIVATE))
				{
					//successfully created
					//update DB
					$datacc = array();
					$datacc['foldername']=$newfoldername;
					$datacc['companyid']=$companyid;
					$datacc['fullpath']=$newpath;
					$datacc['type']=$parenttype; 
					$datacc['parentfolderid']=$parentfolderId; 
					$datacc['createdby']=$companyid;
					$datacc['createdon']=date('Y-m-d H:i');				
					$newfolderid=$this->foldermodel->insertFolder($datacc);	
				}
				*/
			/* } */
					
				//update the database
				
			/*	
				//delete from folderalloweddevices
				$this->folderdevicesmodel->deleteByFoler($folderid);				
				
				//delete from folderallowedusers
				$this->folderusersmodel->deleteByFoler($folderid);	
				
				//delete from folder
				$this->foldermodel->deleteFolder($folderid);
			*/	
		/* } */
	
			echo "1";	
			}
	}	
	
	function permissions($folderid)
	{
		  if($this->session->userdata("companyid")) {
		
			   $companyid = $this->session->userdata("companyid");
			   $folderid = ($folderid);
				 if(Permissionsmodel::checkValidFolder($folderid,$companyid) == 1) {
			
					  $companyid			=$this->session->userdata("companyid");
					  $companydevices		=$this->companydevicemodel->getDevicesByCompany($companyid);
					  $permissions			=$this->permissionsmodel->getAllPermissions();
					  $folderdetails		=$this->foldermodel->getFolderDetails($folderid);
					  
					  $approvedemails 		= $this->foldermodel->fetchapprovedemails($folderid);
					  $numbercue 			= $this->foldermodel->fetchapprovedemailsnumbercue($folderid);
					  
					  $data['folderid']		=	$folderid;
					  $data['foldername']	=	$folderdetails['foldername'];
					  $devicepermissions	=array();
					  for($i=0;$i<count($companydevices);$i++)
						{
							$deviceid						=$companydevices[$i]['deviceid'];
							$temppermissions				=$this->folderdevicesmodel->getPermissions($folderid,$deviceid);
							$devicepermissions[$i]['deviceid']		=$deviceid;
							$devicepermissions[$i]['devicename']	=$companydevices[$i]['name'];
							$devicepermissions[$i]['permissions']	=$temppermissions;	
						}
				
					  $data['allpermissions']		=$permissions;
					  $data['devicepermissions']	=$devicepermissions;
					  
					  $userpermissions				=array();
					  $companyusers					=$this->companyusermodel->getUsersByCompany($companyid);
					  for($i=0;$i<count($companyusers);$i++)
						{
							$companyuserid		=$companyusers[$i]['companyuserid'];
							$temppermissions	=$this->folderusersmodel->getPermissions($folderid,$companyuserid);
							$userpermissions[$i]['userid']		=$companyuserid;
							$userpermissions[$i]['username']	=$companyusers[$i]['username'];
							$userpermissions[$i]['permissions']	=$temppermissions;	
							$userpermissions[$i]['user_type']	=$companyusers[$i]['user_type'];
						}
					 $data['userpermissions']	=$userpermissions;
					 $data['emails']			=$approvedemails;
					 $data['numbercue']			=$numbercue;
					  
					  $this->load->view('CompanyAdmin/managepermissions',$data);
				 } 
				 else {
					$this->load->view('CompanyAdmin/badaccess');	
				 } 
				  
		  }
		  else {
			redirect('login');
		  }	
	}
	
	function saveFolderDevicePermissions()
	{
		$folderid=$this->input->post('folderid');
		$retdevstr=$this->input->post('retdevstr');
		$companyid=$this->session->userdata("companyid");
		$temp=explode("@", $retdevstr);
		
		if($this->input->post('repeatpermision'))
		{
			//Get Children
			$folders = $this->getChildren($folderid);
			$allfolders = array_merge($folders, array($folderid));
			
			foreach($allfolders as $folder):
				//delete all the folder records first
				$this->folderdevicesmodel->deleteByFoler($folder);
				for($i=0;$i<count($temp);$i++)
				{
					if($temp[$i]!="")
					{
						$devicepermsnarr=explode(":", $temp[$i]);
						$datacc = array();
						$datacc['deviceid']=$devicepermsnarr[0];
						$datacc['folderid']=$folder;
						$datacc['permissionid']=$devicepermsnarr[1];
						$datacc['createdby']=$companyid;
						$datacc['updatedby']=$companyid;
						$datacc['createdon']=date('Y-m-d H:i');	
						$this->folderdevicesmodel->insertFolderDevice($datacc);
					}
				}
			endforeach;
			
			echo "1";	
		}
		else 
		{
			//delete all the folder records first
			$this->folderdevicesmodel->deleteByFoler($folderid);
			for($i=0;$i<count($temp);$i++)
			{
				if($temp[$i]!="")
				{
					$devicepermsnarr=explode(":", $temp[$i]);
					$datacc = array();
					$datacc['deviceid']=$devicepermsnarr[0];
					$datacc['folderid']=$folderid;
					$datacc['permissionid']=$devicepermsnarr[1];
					$datacc['createdby']=$companyid;
					$datacc['updatedby']=$companyid;
					$datacc['createdon']=date('Y-m-d H:i');	
					$this->folderdevicesmodel->insertFolderDevice($datacc);
				}
			}
			echo "1";	
		}	
	}
	function saveFolderUserPermissions()
	{
		$folderid=$this->input->post('folderid');
		$retuserstr=$this->input->post('retuserstr');
		$companyid=$this->session->userdata("companyid");
		$temp=explode("@", $retuserstr);
		
		if($this->input->post('repeatpermisionuser')) {
			
			//Get Children
			$folders = $this->getChildren($folderid);
			$allfolders = array_merge($folders, array($folderid));
			
			foreach($allfolders as $folder):
				//delete all the folder records first
				$this->folderusersmodel->deleteByFoler($folder);
				for($i=0;$i<count($temp);$i++)
				{
					if($temp[$i]!="")
					{
						$userpermsnarr=explode(":", $temp[$i]);
						$datacc = array();
						 $datacc['companyuserid']=$userpermsnarr[0];
						 $datacc['folderid']=$folder;
						$datacc['permissionid']=$userpermsnarr[1];
						$datacc['createdby']=$companyid;
						$datacc['updatedby']=$companyid;
						$datacc['createdon']=date('Y-m-d H:i');	
						$this->folderusersmodel->insertFolderUser($datacc);
					}
				}
			endforeach;
			
			echo "1";
		} else {
			//delete all the folder records first
			$this->folderusersmodel->deleteByFoler($folderid);
			for($i=0;$i<count($temp);$i++)
			{
				if($temp[$i]!="")
				{
					$userpermsnarr=explode(":", $temp[$i]);
					$datacc = array();
					 $datacc['companyuserid']=$userpermsnarr[0];
					 $datacc['folderid']=$folderid;
					$datacc['permissionid']=$userpermsnarr[1];
					$datacc['createdby']=$companyid;
					$datacc['updatedby']=$companyid;
					$datacc['createdon']=date('Y-m-d H:i');	
					$this->folderusersmodel->insertFolderUser($datacc);
				}
			}
			echo "1";
		}
			
	}
	/* End of Folder Management */
	
	/*
	 * Device Management
	 */
	function manageDevices(){
		
		if($this->session->userdata("companyid")) {
		$companyid=$this->session->userdata("companyid");
		
		if(isset($_REQUEST['searchtext']))
		{
			$searchtext= $_REQUEST['searchtext'];	
			$searchtext = html_purify($searchtext);
		}
		else
		{
			$searchtext= "";
		}
		
		/* pagignation control */
				$config['base_url'] = base_url().'index.php/companyadmin/manageDevices/';
				$totalDevices=$this->companydevicemodel->getDeviceCount($companyid,$searchtext);
				$config['total_rows'] = $totalDevices;
				$config['per_page'] = PERPAGE;
				$config['full_tag_open'] = '<div class="pagination">';
				$config['full_tag_close'] = '</div><!--pagination-->';
				$config['first_link'] = '&laquo; First';
				$config['first_tag_open'] = '<li class="prev page">';
				$config['first_tag_close'] = '</li>';
				$config['last_link'] = 'Last &raquo;';
				$config['last_tag_open'] = '<li class="next page">';
				$config['last_tag_close'] = '</li>';
				$config['next_link'] = 'Next &rarr;';
				$config['next_tag_open'] = '<li class="next page">';
				$config['next_tag_close'] = '</li>';
				$config['prev_link'] = '&larr; Previous';
				$config['prev_tag_open'] = '<li class="prev page">';
				$config['prev_tag_close'] = '</li>';
				$config['cur_tag_open'] = '<li class="active"><a href="">';
				$config['cur_tag_close'] = '</a></li>';
				$config['num_tag_open'] = '<li class="page">'; 
				$config['num_tag_close'] = '</li>';
				$this->pagination->initialize($config);		
				$page = $this->uri->segment(3);
				if($page < 1)
					$page = 0;				
				
				$data = array();
			//$filefp = fopen('/home/cuedrive/public_html/log.txt', 'wa+');
				$data['companydevices'] = $this->companydevicemodel->getAllDevices($config['per_page'], $page,$companyid,$searchtext);
			//fwrite($filefp, "\ncompanydevices". $this->db->last_query());
				$data['totalDevices']=$totalDevices;
			//fwrite($filefp, "\ntotalDevices". $totalDevices);
				$totaldevicesadded=$this->companydevicemodel->getDeviceCount($companyid,"");
			//fwrite($filefp, "\ntotaldevicesadded". $totaldevicesadded);
				$totaldevicesallowed=$this->companyadminmodel->getAllowedDevices($companyid);		
			//fwrite($filefp, "\ntotaldevicesallowed". $totaldevicesallowed);	
			//fwrite($filefp, "\ntotaldevicesallowed". $this->db->last_query());
				$leftdevices=intval ($totaldevicesallowed)-intval ($totaldevicesadded);			
			//fwrite($filefp, "\nleftdevices". $leftdevices);		
			//fclose($filefp);
				//echo "leftdevices:".$leftdevices;
				$data['devicesleft']=$leftdevices;
				$data['pagination'] =  $this->pagination->create_links();		
				$data['searchtext']=$searchtext;
				$this->load->view('CompanyAdmin/devicelist',$data);
		}
		else {
			redirect('login');
		}
		}
	
	function loadNewDeviceForm()
	{
		if($this->session->userdata("companyid")) {
				$companyid			= $this->session->userdata("companyid");
				$data['deviceid']	= 0;
				$data['devicename']	= "";
				$data['deviceuuid']	= "";
				$data['action']		= "Add";
				
				$this->load->view('CompanyAdmin/deviceform',$data);
		}
		else {
				redirect('login');
		}
	}

	function loadEditDeviceForm($deviceid)
	{
		if($this->session->userdata("companyid")) {
		$companyid=$this->session->userdata("companyid");
		
		if(Permissionsmodel::checkValidDevice($deviceid,$companyid) == 1) {
		
		$devicedata=$this->companydevicemodel->getDeviceDetails($deviceid);
		$data['deviceid']=$deviceid;
		$data['devicename']=$devicedata['name'];
		$data['deviceuuid']=$devicedata['deviceuuid'];
		$data['action']="Edit";
		
		$this->load->view('CompanyAdmin/deviceform',$data);
		}
		else {
		$this->load->view('CompanyAdmin/badaccess');
		}
		}
		else {
			redirect('login');
		}
	}
	function checkDeviceExist()
	{
		$devicename=trim($this->input->post('devname'));
		$device_uuid=trim($this->input->post('devuuid'));
		$deviceid=$this->input->post('devid');
		if($deviceid==0) //new device
			{
				if($this->companydevicemodel->deviceExists($device_uuid))
					echo "0";
				else {
					  if(Alldevicesmodel::serialIDExists($device_uuid) > 0)
					  {
						echo "1";
					  }
					  else
					  {
						echo "2";
					  }
	
					 }
			}
			else { //existing device
				if($this->companydevicemodel->deviceExistsWithID($device_uuid,$deviceid))
					echo "0";
				else 
							 {
					 if(Alldevicesmodel::serialIDExists($device_uuid) > 0)
						  {
							echo "1";
						  }
						  else
						  {
							echo "2";
						  }
	
					}
			}
	}
	
	function checkDeviceNameExists()
	{
  		$devicename = $_REQUEST['devname'];
  		echo Companydevicemodel::deviceNameExists($devicename);

	}
	function updateDevice()
	{
		if($this->session->userdata("companyid")) 
		{
			$companyid				= $this->session->userdata("companyid");
			$deviceid				= $this->input->post('deviceid');
			$devicename				= html_purify(trim($this->input->post('device_name')));
			$device_uuid			= trim($this->input->post('device_uuid'));
			$permissionflag			= $this->input->post('radioperm');
			$totaldevicesadded		= $this->companydevicemodel->getDeviceCount($companyid,"");
			$totaldevicesallowed	= $this->companyadminmodel->getAllowedDevices($companyid);		
			$leftdevices			= intval ($totaldevicesallowed)-intval ($totaldevicesadded);
		if(true)
		{
			$updatedata					= array();
			$updatedata['name']			= $devicename;
			$updatedata['companyid']	= $companyid;
			$updatedata['deviceuuid']	= $device_uuid;
			if($deviceid==0) //new device
			{
				if($leftdevices>0)
				 {
		  
				 }else{
					$this->session->set_flashdata('successmsg', 'Your device limit is over');
					redirect('companyadmin/manageDevices');
					 }          
			}
			else
			{
				$devicename1 		= $devicename."_".$deviceid."-(Device)";
				$devicedetails 		= Companydevicemodel::getDeviceDetails($deviceid);
				$olddeviceuuid 		= $devicedetails['deviceuuid'];
				$device_password 	= Openfiremodel::generateRandomToken();
				$url = ORG_URL."chat/?type=update&oldusername=".urlencode($olddeviceuuid)."&username=".urlencode($device_uuid)."&password=".urlencode($device_password)."&name=".urlencode($devicename1)."&group=".urlencode($companyid);
				Openfiremodel::sendDataToServer($url);  
				Companydevicemodel::expireDeviceToken($deviceid);
			}
			 $newdeviceid	=	$this->companydevicemodel->updateCompanyDevice($updatedata,$deviceid);
			 if($newdeviceid > 0 && $deviceid==0)
			 {
				$devicename1 			= $devicename."_".$newdeviceid."-(Device)";
				$updatedata['createdon']= date('Y-m-d H:i');
				$device_password 		= Openfiremodel::generateRandomToken();
				$url = ORG_URL."chat/?type=add&username=".urlencode($device_uuid)."&password=".urlencode($device_password)."&name=".urlencode($devicename1)."&group=".urlencode($companyid);
				Openfiremodel::sendDataToServer($url);
			 }
			  $parentfolderId= NULL;
			  if($deviceid==0) //new device
				{
					if($permissionflag=="full")
					{
						$this->addAllPermissionsToAllFolders($newdeviceid);
					}
					 //add home folder for device 			
					 $newfoldername		= $devicename;
					 $bucketname		= ROOTBUCKET;
					 $companyfoldername	= COMPANYFOLDERNAME.$companyid;
					 $newpath 			= $newfoldername;
					 $uploaddir 		= realpath('./uploads/');
					 $dummyfile 		= $uploaddir ."//".'dummyfile.txt';
					 $file				= fopen($dummyfile,"w");
					
					 if($this->foldermodel->foldernameexist_device($newfoldername, $companyid) == 'yes')
					 	{
							$rand_num			= time();//rand(100,999);
							$newpath 			= $newfoldername.'_'.$rand_num;
							$newfoldername		= $devicename.'_'.$rand_num;
							//echo 'found';	
						}
					
					//echo $newpath;
					//exit; 
					 
					 if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
						{
								//successfully created
								//update DB
								$datacc 					= array();
								$datacc['foldername']		= $newfoldername;
								$datacc['companyid']		= $companyid;
								$datacc['fullpath']			= $newpath;
								$datacc['type']				= 'private'; 
								$datacc['parentfolderid']	= NULL;  
								$datacc['createdby']		= $companyid;
								$datacc['createdon']		= date('Y-m-d H:i');				
								$newfolderid				= $this->foldermodel->insertFolder($datacc);	
								
								//default full access to this folder for new device
								$permissions				= $this->permissionsmodel->getAllPermissions();
								for($j=0;$j<count($permissions);$j++)
								 {
										$permissionid=$permissions[$j]['permissionid'];
										if($permissionid!=6 && $permissionid!=7)
										 {
											 $datacc 				= array();
											 $datacc['deviceid']	= $newdeviceid; 
											 $datacc['folderid']	= $newfolderid;
											 $datacc['permissionid']= $permissionid;
											 $datacc['createdby']	= $companyid;
											 $datacc['updatedby']	= $companyid;
											 $datacc['createdon']	= date('Y-m-d H:i');	
											 $this->folderdevicesmodel->insertFolderDevice($datacc);
										 }	 
								}
							}
							$this->session->set_flashdata('successmsg', 'New device has been successfully added');
				}else{
							$this->session->set_flashdata('successmsg', 'Device has been successfully updated');
					 }
						redirect('companyadmin/manageDevices');
			}//end of left device validation	
		}
		else {
			redirect('login');
		}
		
	}
	
	function changeDeviceStatus()
	{
		if($this->session->userdata("companyid")) {
		$companyid=$this->session->userdata("companyid");
		$deviceid=$this->input->post('deviceid');
		$this->companydevicemodel->changeStatus($deviceid);
	
			 Companydevicemodel::expireDeviceToken($deviceid);
	
	
		echo 1;
		}
		else {
			redirect('login');
		}
	}
	
	function deleteDevice()
	{
		if($this->session->userdata("companyid")) 
		{
			$deviceid=$this->input->post('deviceid');
			$selradio=$this->input->post('selradio');	    
			$devicedetails = Companydevicemodel::getDeviceDetails($deviceid);
			$deviceuuid = $devicedetails['deviceuuid'];
			$url = ORG_URL."chat/?type=delete&username=".$deviceuuid;
			Openfiremodel::sendDataToServer($url);
			Companydevicemodel::updateAllUpdatedbydevice($deviceid);
		   /* 
		   Committed this code as per 19-2-15 email
		   if($selradio=="deletecontent") //delete device home folder
		    {
			$companyid=$this->session->userdata("companyid");
			//$bucketname="cuedrive.".$companyid;
			$bucketname=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			$devfoldername="device".$deviceid;
			$parentfolderId=$this->foldermodel->getPrivateRootFolder($companyid);
			$selectedfolderid= $this->foldermodel->getfolderID($devfoldername,$parentfolderId);
			//delete folder
			$folderpath=$this->foldermodel->getFolderPath($selectedfolderid);
			$deletefolderarr=$this->getChildren($selectedfolderid);
			array_push($deletefolderarr,$selectedfolderid);
			for($i=0;$i<count($deletefolderarr);$i++)
			{
				$folderid=$deletefolderarr[$i];
				$files= $this->folderfilemodel->getFiles($folderid);
				for($j=0;$j<count($files);$j++)
				{
					$filepath=$files[$j]['fullpath'];
					$fileid=$files[$j]['id'];
					if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
					{
							//delete from folderfile
						$this->folderfilemodel->deleteFile($fileid);
					}			
				}					
					//update the database					
					//delete from folderalloweddevices
					$this->folderdevicesmodel->deleteByFoler($folderid);				
					//delete from folderallowedusers
					$this->folderusersmodel->deleteByFoler($folderid);					
					//delete from folder
					$this->foldermodel->deleteFolder($folderid);
			}
		}*/
	
		//delete from folderalloweddevices
		$this->folderdevicesmodel->deleteByDevice($deviceid);
		$affectedrows=$this->companydevicemodel->deleteDevice($deviceid);
		echo $affectedrows;
		}
		else {
			redirect('login');
		}
		
	}		

/*
 * End of Device Management
 */
/*
 * User Management
 */
function manageUsers(){
	
	if($this->session->userdata("companyid")) {
	
	$companyid=$this->session->userdata("companyid");
	if(isset($_REQUEST['searchtext']))
	{
		$searchtext= $_REQUEST['searchtext'];	
                $searchtext = html_purify($searchtext);
	}
	else
	{
		$searchtext= "";
	}
	
	/* pagignation control */
			$config['base_url'] 		= base_url().'index.php/companyadmin/manageUsers/';
			$totalUsers					= $this->companyusermodel->getUserCount($companyid,$searchtext);
			$config['total_rows'] 		= $totalUsers;
			$config['per_page'] 		= PERPAGE;
			$config['full_tag_open'] 	= '<div class="pagination">';
			$config['full_tag_close'] 	= '</div><!--pagination-->';
			$config['first_link'] 		= '&laquo; First';
			$config['first_tag_open']	= '<li class="prev page">';
			$config['first_tag_close'] 	= '</li>';
			$config['last_link'] 		= 'Last &raquo;';
			$config['last_tag_open'] 	= '<li class="next page">';
			$config['last_tag_close'] 	= '</li>';
			$config['next_link'] 		= 'Next &rarr;';
			$config['next_tag_open'] 	= '<li class="next page">';
			$config['next_tag_close'] 	= '</li>';
			$config['prev_link'] 		= '&larr; Previous';
			$config['prev_tag_open'] 	= '<li class="prev page">';
			$config['prev_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a href="">';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['num_tag_open'] 	= '<li class="page">';
			$config['num_tag_close'] 	= '</li>';
			$this->pagination->initialize($config);		
			$page = $this->uri->segment(3);
			if($page < 1)
				$page = 0;				
			
			$data 					= array();
			$data['companyusers'] 	= $this->companyusermodel->getAllUsers($config['per_page'], $page,$companyid,$searchtext);
			$data['accountNumber'] 	= Companyadminmodel::getCompanyAccountNumber($companyid);
			$data['totalUsers']		= $totalUsers;
			$data['pagination'] 	= $this->pagination->create_links();		
			$data['searchtext']		= $searchtext;
			$this->load->view('CompanyAdmin/userlist',$data);
	}
	else {
		redirect('login');
	}
	}
	
function loadNewUserForm()
{
	if($this->session->userdata("companyid")) {
	$companyid=$this->session->userdata("companyid");
	$data['companyuserid']=0;
	$data['username']="";
	$data['password']="";
	$data['email']="";
	$data['firstname']="";
	$data['lastname']="";
	$data['user_type']="";
	$data['action']="Add";
	
	$this->load->view('CompanyAdmin/userform',$data);
	}
	else {
		redirect('login');
	}
}

function loadEditUserForm($userid)
{
	if($this->session->userdata("companyid")) {
	$companyid=$this->session->userdata("companyid");
	if(Permissionsmodel::checkValidCompanyUser($userid,$companyid) == 1) {
	$userdata=$this->companyusermodel->getUserDetails($userid);
	$data['companyuserid']=$userid;
	$data['username']=$userdata['username'];
	$data['password']=$userdata['password'];
	$data['email']=$userdata['email'];
	$data['firstname']=$userdata['firstname'];
	$data['lastname']=$userdata['lastname'];
	$data['user_type']=$userdata['user_type'];
	$data['action']="Edit";
	
	$this->load->view('CompanyAdmin/userform',$data);
	}
	else {
	$this->load->view('CompanyAdmin/badaccess');	
	}
	}
	else {
		redirect('login');
	}
}
function checkUserExist()
{
	if($this->session->userdata("companyid")) 
	{
	$companyid=$this->session->userdata("companyid");
	$username=trim($this->input->post('username'));
    $email=trim($this->input->post('email'));
	$userid=$this->input->post('userid');
	if($userid==0) //new user
		{
		if($this->companyusermodel->userExists($username))
		 {
		  echo "0";// username exists
		  
		 }else{
				if($this->companyusermodel->userEmailExists($email))
				 {
				   echo "2"; // email exists
				 }
				 else
				 {
				   echo "1";
				 }
             }
		}
		else { //existing user
			if($this->companyusermodel->userExistsWithID($username,$userid))
			{ 
				echo "0";
			}
			else 
               {
			   if($this->companyusermodel->userEmailExistsWithID($email,$userid))
                {
				echo "2";
                }else{
                       echo "1";
                      }
            }
		}
	}
	else {
		redirect('login');
	}
}
function updateUser()
{
	if($this->session->userdata("companyid")) {
		$companyid=$this->session->userdata("companyid");
		$userid			= $this->input->post('userid');
		$username		= html_purify($this->input->post('username'));
		$useremail		= html_purify($this->input->post('email'));
		$firstname		= html_purify($this->input->post('firstname'));
		$lastname		= html_purify($this->input->post('lastname'));
		$user_type		= html_purify($this->input->post('user_type'));
		$permissionflag	= $this->input->post('radioperm');
		$updatedata		= array();
		$updatedata['username']		= strtolower($username);
		$updatedata['companyid']	= $companyid;
		$updatedata['firstname']	= $firstname;
		$updatedata['lastname']		= $lastname;
		$updatedata['email']		= $useremail;
		$updatedata['user_type']	= $user_type;
		if($userid	==	0) //new user chat setting
		{
			$pass=$this->generateRandomPassword();
			$updatedata['password']		= md5($pass);
			$updatedata['createdon']	= date('Y-m-d H:i');
			Email::sendPassword_Email($username,$useremail,$pass,$firstname,$lastname);	

            $user_password 	= Openfiremodel::generateRandomToken();
			$url 			= ORG_URL."chat/?type=add&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($firstname.' '.$lastname)."&group=".urlencode($companyid);
	        Openfiremodel::sendDataToServer($url); 

		}

                else
                {
                        $userdetails 	= Companyusermodel::getUserDetails($userid);
                        $oldusername 	= $userdetails['username'];
                        $user_password 	= Openfiremodel::generateRandomToken();
						$url 			= ORG_URL."chat/?type=update&oldusername=".urlencode($oldusername)."&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($firstname.' '.$lastname)."&group=".urlencode($companyid);
	                Openfiremodel::sendDataToServer($url); 



                }
		
		$newuserid=$this->companyusermodel->updateCompanyUser($updatedata,$userid);
		
		if($userid==0)//new user
		{		
			if($permissionflag=="full")
			{
				
					$this->addAllPermissionsToAllFoldersForUser($newuserid,$user_type);
				
			}
			else { //later
				//for this user  full access to  Public  folder
				$permissions	= $this->permissionsmodel->getAllPermissions();
				$publicfolder	= $this->foldermodel->getPublicFolder($companyid);
						for($j=0;$j<count($permissions)-1;$j++)
							{
								$datacc 					= array();								
								 $permissionid				= $permissions[$j]['permissionid'];
								 $datacc['companyuserid']	= $newuserid;
								 $datacc['folderid']		= $publicfolder[0]['folderid']; //public folder
								 $datacc['permissionid']	= $permissionid;
								 $datacc['createdby']		= $companyid;
								 $datacc['updatedby']		= $companyid;
								 $datacc['createdon']		= date('Y-m-d H:i');	
								
								
								 $this->folderusersmodel->insertFolderUser($datacc);
							}		
			}
			$this->session->set_flashdata('successmsg', 'New User has been added and an email sent to them with their login information');
		}
		else
		{
			$this->session->set_flashdata('successmsg', 'User has been updated');
		}
			redirect('companyadmin/manageUsers');
	}
	else {
		redirect('login');
	}
}

function addAllPermissionsToAllFoldersForUser($userid, $user_type)
	{
		if($this->session->userdata("companyid")) {
				$companyid		=$this->session->userdata("companyid");
				$permissions	=$this->permissionsmodel->getAllPermissions();
				$folders		=$this->foldermodel->getAllFolders($companyid);
				for($i=0;$i<count($folders);$i++)
				{
					$folderid	=$folders[$i]['folderid'];	
					for($j=0;$j<count($permissions);$j++)
							{
								 $datacc 		= array();
								 $permissionid	=$permissions[$j]['permissionid'];
								 if($user_type == 2)
								 {
									 if($permissionid!=6 && $permissionid!=7)	{
										 $datacc['companyuserid']	=$userid;
										 $datacc['folderid']		=$folderid;
										 $datacc['permissionid']	=$permissionid;
										 $datacc['createdby']		=$companyid;
										 $datacc['updatedby']		=$companyid;
										 $datacc['createdon']		=date('Y-m-d H:i');	
										 $this->folderusersmodel->insertFolderUser($datacc);
								 	}
								 }elseif($user_type == 1 && in_array($permissionid, array(1,4))){	 
									
										 $datacc['companyuserid']	=$userid;
										 $datacc['folderid']		=$folderid;
										 $datacc['permissionid']	=$permissionid;
										 $datacc['createdby']		=$companyid;
										 $datacc['updatedby']		=$companyid;
										 $datacc['createdon']		=date('Y-m-d H:i');	
										 $this->folderusersmodel->insertFolderUser($datacc);
								 		
									 
								 }
							}
				}
		}
		else {
			redirect('login');
		}
	}

	function deleteUser()
	{
       if($this->session->userdata("companyid")) {
                $userid=$this->input->post('userid');
                $userdetails = Companyusermodel::getUserDetails($userid);
                $username = $userdetails['username'];
	        $url = ORG_URL."chat/?type=delete&username=".urlencode($username);
	        Openfiremodel::sendDataToServer($url);
        
		Companyusermodel::updateAllUpdatedbyuser($userid);
		//delete from folderallowedusers
		$this->folderusersmodel->deleteByUser($userid);
		$affectedrows=$this->companyusermodel->deleteUser($userid);	
 
                 
  
		echo $affectedrows;
       }
       else {
       	redirect('login');
       }
       
	}
	
	function changeUserStatus()
	{
		if($this->session->userdata("companyid")) {
		$companyid=$this->session->userdata("companyid");
		$userid=$this->input->post('userid');
		$this->companyusermodel->changeStatus($userid);
		echo 1;
		}
		else {
			redirect('login');
		}
	}
/*
 * End of User Management
 */
function getChildren($parent_id) {

    $tree = Array();   
    if (!empty($parent_id)) {
        $tree = $this->foldermodel->getOneLevel($parent_id);
        foreach ($tree as $key => $val) {
            $ids = $this->getChildren($val);
            $tree = array_merge($tree, $ids);
        }
    }
    return $tree;

}
 function objectArray_flatten($array,$childField) {
        $result = array();

        foreach ($array as $node)
        {
           
            if(isset($node[$childField]))
            {
                $result = array_merge($result,$this-> objectArray_flatten($node[$childField],$childField));
                unset($node[$childField]);
            }
			 $result[] = $node;
        }

        return $result;
    }
	
	
function transpose($array) {
	$transposed_array = array();
	if ($array) {
	foreach ($array as $row_key => $row) {
	if (is_array($row) && !empty($row)) { //check to see if there is a second dimension
	foreach ($row as $column_key => $element) {
	$transposed_array[$column_key][$row_key] = $element;
	}
	}
	else {
	$transposed_array[0][$row_key] = $row;
	}
	}
	return $transposed_array;
	}

}

function buildFolderTree(array &$elements, $parentId = 0,$htmlid=0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parentfolderid'] == $parentId) {
        	
        	if($htmlid!=0)
        		$newhtmlid=$htmlid."-".$element['id'];
        	else
        		$newhtmlid=$element['id'];
        	
        	$element['htmlid']=$newhtmlid;
        	//$element['htmlid']=$htmlid."-".$element['id'];
        	$element['htmlparentid']=$htmlid;
        	if($element['type']=="folder")
        	{
        		
	            $children = $this->buildFolderTree($elements, $element['id'],$newhtmlid);
	            if ($children) {
	                $element['children'] = $children;
	            }
        	}
        	
            $branch[$element['id']] = $element;
 
            }
    }
    return $branch;
}




function remove_json_keys($array) {
  foreach ($array as $k => $v) {
    if (is_array($v)) {
        $array[$k] = $this->remove_json_keys($v);
    } //if
  } //foreach
  return $this->sort_numeric_keys($array);

}

function sort_numeric_keys($array) {
    $i=0;
    $rtn=array();
    foreach($array as $k => $v) {
        if(is_int($k)) {
            $rtn[$i] = $v;
            $i++;
        } else {
            $rtn[$k] = $v;
        } //if
    } //foreach
    return $rtn;
}
function getBucketSize($bucket)
	{
		// Load Library
 	 	$this->load->library('s3');
 	 	$resultarr=$this->s3->getBucket($bucket);
 	 	$totalsize=0;
 	 	foreach($resultarr as  $arr){    
   		   	$totalsize=$totalsize+$arr['size'];
 	 	  	}
 	 	  	return $totalsize;	
	}

function getCompanyFolderSize($companyid)
{
	return  Folderfilemodel::getTotalfileStored($companyid);
}





/**
  Added by Debartha
*/

      public function helpdocs()
      {
      	if($this->session->userdata("companyid")) {
        $data = $this->helpdocsPagination();
       
		$this->load->view('CompanyAdmin/helpdocs',$data);

      	}
      	else {
      		redirect('login');
      	}
      }
      
      function helpdocsPagination()
	  {
		$options = array();
		$customUrl = "";
		if(isset($_REQUEST['searchtext']))
		{
            $searchtext = html_purify($_REQUEST['searchtext']);
			$options['like'] = $searchtext;
		}
		$config["base_url"] = base_url() . "index.php/companyadmin/tickets";
		$config["total_rows"] = count(Contentmanagementmodel::GetContentbycompanyid());
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div><!--pagination-->';
			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 1;
	
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
	
		$data["results"] = Contentmanagementmodel::GetContentDetailsbycompanyid($options);
		$data["links"] = $this->pagination->create_links();
	
		return $data;
	}
	
	function createDocument()
	{
	$this->load->view('CompanyAdmin/helpdocnewform');
	}
	
	function addhelpdoc()
	{
	    date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('article', 'article', 'required');
		$this->form_validation->set_rules('companyid', 'companyid', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data = array("errorMsg"=>"true");
			$this->load->view('CompanyAdmin/helpdocnewform');
		}
		else
		{
			$iData = array(
					'articles' => purify_html($_REQUEST["article"]),
					'companyid' => $_REQUEST["companyid"]
			);
			$affected_rows =  Contentmanagementmodel::adddoc($iData);
			$data = $this->helpdocsPagination();
			$data["errorMsg"] ="Document has been successfully added." ;
		    $this->load->view('CompanyAdmin/helpdocs',$data);
		}
	
	}
	function updatehelpdoc()
	{
		$iData = array(
		'articles' => $_REQUEST["article"],
		'companyid' => $_REQUEST["companyid"],
		'contentid' => $_REQUEST["contentid"]
		);
		$affected_rows =  Contentmanagementmodel::updatedoc($iData);
		$data = $this->helpdocsPagination();
		$data["errorMsg"] ="Document has been successfully updated." ;
		$this->load->view('CompanyAdmin/helpdocs',$data);
	}
	
	
	function modifydocument()
	{
	    $data = array ("id"=>$_REQUEST["id"]);
		$this->load->view('CompanyAdmin/helpdoceditform', $data);
	}
	
	
	
	function deleteDoc()
	{
	    $affectedRows = Contentmanagementmodel::deletearticle($_REQUEST);
		echo $affectedRows;
	}

    public function tickets()
	{
		if($this->session->userdata("companyid")) {
		$data = $this->ticketsPagination();
		$this->load->view('CompanyAdmin/tickets',$data);
		}
		else {
			redirect('login');
		}
	}
	
	function ticketsPagination()
	{
		$options = array();
		$customUrl = "";
		if(isset($_REQUEST['searchtext']))
		{
            $searchtext = html_purify($_REQUEST['searchtext']);
			$options['like'] = $searchtext;
            $data['searchtext'] = $searchtext;
		}
		$config["base_url"] = base_url() . "index.php/companyadmin/tickets";
		$config["total_rows"] = count(Contactusmodel::GetTicketsbycompanyid());
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
	
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
	
		$data["results"] = Contactusmodel::GetTicketsDetailsbycompanyid($options);
		$data["links"] = $this->pagination->create_links();
	
		return $data;
	
	}

      public function feedbacks()
	{
		if($this->session->userdata("companyid")) {
		$data = $this->feedbackPagination();
		//echo json_encode($data);
		$this->load->view('CompanyAdmin/feedback',$data);
		}
		else {
			redirect('login');
		}
	}
	
	public function feedbackPagination()
	{

		$options = array();
		$customUrl = "";
		
		$config["base_url"] = base_url() . "index.php/companyadmin/feedbacks";
		$config["total_rows"] = count(Contactusmodel::GetFeedbacksbycompanyid());
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div><!--pagination-->';
			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
		
		$data["results"] = Contactusmodel::GetFeedbacksDetailsbycompanyid($options);
		$data["links"] = $this->pagination->create_links();
		
		return $data;
	}
	
	
	function createTicket()
	{
		$this->load->view('CompanyAdmin/createticket');
	}
	
	function genratePrimaryKey($serverid)
        {
	$primarykey=$serverid.time().mt_rand(1000, 9999);
	if($primarykey>9223372036854775807) //max of 64 bit int
	{
		genratePrimaryKey($serverid);
	}
	return $primarykey;
        }
	
	
	function addticket()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->load->library('form_validation');
// 		$this->form_validation->set_rules('type', 'Type', 'required');
// 		$this->form_validation->set_rules('priority', 'Priority', 'required');
// 		$this->form_validation->set_rules('email', 'Email', 'required');
// 		$this->form_validation->set_rules('comments', 'Issue Description', 'required');
	           
		if (true)
		{
			   $ticketid = "";
                           $temp =0;
		           
                           if(isset($_REQUEST['ticketid'])) { 
		              $ticketid = $_REQUEST['ticketid'];
		           } 
		           else
		           {  
                             $ticketid =$this->genratePrimaryKey(10);
                             $temp =1;
                           }
			$iData = array(
					'type' => $_REQUEST["type"],
					'priority' => $_REQUEST["priority"],
					'email' => $_REQUEST["email"],
					'phone' => $_REQUEST["phone"],
					'comments' => html_purify($_REQUEST["comments"]),
					'companyid' => $_REQUEST["companyid"],
					'createdon' => $date,
					'sent'=>1,
					'ticketid' => $ticketid
		
			);
			$affected_rows =  Contactusmodel::addticket($iData);

                        if($affected_rows > 0 && $temp == 1)
                        {
                            $email = Companyadminmodel::getCompanyEmail($_REQUEST["companyid"]);
                            Email::ticketConfirmation($email,$ticketid);
                        } 
			
			if(isset($_REQUEST['ticketid']))
			{
			redirect('/companyadmin/ticketconversation?id='.$_REQUEST['ticketid']);
			}
			else
			{
			$data = $this->ticketsPagination();
			$data["errorMsg"] ="Record added" ;
		        $this->load->view('CompanyAdmin/tickets',$data);
		     }
		}
	}
	
	
	function replyTicket()
	{
	    $data = array ("id"=>$_REQUEST["ticketid"],"priority"=>$_REQUEST["priority"]);
	    $this->load->view('CompanyAdmin/replyform',$data);
	}
	
	function createFeedback()
	{
		$this->load->view('CompanyAdmin/createfeedback');
	}
	
	function addfeedback()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('type', 'Type', 'required');
		$this->form_validation->set_rules('priority', 'Priority', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('comments', 'Issue Description', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required');
	
		if ($this->form_validation->run() == FALSE)
		{
			$data = array("errorMsg"=>"true");
			$this->load->view('CompanyAdmin/createfeedback',$data);
		}
		else
		{
			$iData = array(
					'type' => $_REQUEST["type"],
					'priority' => $_REQUEST["priority"],
					'email' => $_REQUEST["email"],
					'phone' => $_REQUEST["phone"],
					'comments' => html_purify($_REQUEST["comments"]),
					'companyid' => $_REQUEST["companyid"],
					'createdon' => $date
		
			);
			$affected_rows =  Contactusmodel::addticket($iData);
			
                        $email = Companyadminmodel::getCompanyEmail($_REQUEST["companyid"]);
                        Email::feedbackConfirmation($email,$affected_rows);

			$data = $this->feedbackPagination();
			$data["errorMsg"] ="Record added" ;
		     
		     $this->load->view('CompanyAdmin/feedback',$data);
		}
	}
	/**
	Added by Debartha
	*/	
	function deletefeedback()
	{
		$affectedRows = Contactusmodel::deletefeedback($_REQUEST);
		echo $affectedRows;
	}
	
	function deleteticket()
	{
	$affectedRows = Contactusmodel::deleteticket($_REQUEST);
		echo $affectedRows;
	}	
		
	/**
	Added by Debartha
	*/
	function contentmanagement()
	{
		$data = array ("type"=>$_REQUEST["type"]);
		$this->load->view('CompanyAdmin/terms', $data);
	}
	
	
	
	public function ticketconversation()
	{
		if($this->session->userdata("companyid")) {
			$ticketid = $_REQUEST['id'];
			$companyid = $this->session->userdata("companyid");
			if(Permissionsmodel::checkValidTicket($ticketid,$companyid) == 1) {
			
			 $data = $this->ticketconversationPagination();
			//echo json_encode($data);
			 $this->load->view('CompanyAdmin/ticketconversation',$data);
			}
			else {
			 $this->load->view('CompanyAdmin/badaccess');	
			}
		
		
		}
		else {
			redirect('login');
		}
	}
	
	public function ticketconversationPagination()
	{

		$options = array();
		$customUrl = "";
		
		$config["base_url"] = base_url() . "index.php/companyadmin/feedbacks";
		$config["total_rows"] = count(Contactusmodel::Getticketconversation($_REQUEST['id']));
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
		$options['ticketid']=$_REQUEST['id'];
		$data["results"] = Contactusmodel::Getticketconversationdetails($options);
		$data["links"] = $this->pagination->create_links();
		
		return $data;
	}
	
	
	
	function upgradePackakge()
	{
	    $companyid = 0;
	    if($this->session->userdata("companyid")) {
	         	$companyid 		= $this->session->userdata("companyid");
				$data 			= array();
				$pkgarr			= array();
				$packages 		= $this->packagemodel->getBusinessPackage();
				$activepackage 	= $this->packagemodel->getActivePackageDetails($companyid);
				foreach($packages as $package){
					$pkgarr[]=$package;
				}
				//transpose the array
				$transposedarr		=	$this->transpose($packages);
				$data['results']	=	$transposedarr;
				$data['activepackage']= $activepackage;
				
				//print_r($transposedarr);
				//echo json_encode($data);
				$this->load->view('CompanyAdmin/upgradeform',$data);
			}
			else
			{
				header ("Location:".base_url()."index.php/login");
				exit;
			}
	}
	
	
	function upgradePacakge($flag = '')
	{
	    $companyid = 0;
	    if($this->session->userdata("companyid")) {
	        $companyid 		= $this->session->userdata("companyid");
			$data 			= array();
			$pkgarr			= array();
			$packages 		= $this->packagemodel->getBusinessPackage();
			$activepackage 	= $this->packagemodel->getActivePackageDetails($companyid);
			foreach($packages as $package)
				{
					$pkgarr[]=$package;
				}
			//transpose the array
			$transposedarr	= $this->transpose($packages);
			$data['results']= $transposedarr;
			$data['activepackage']	= $activepackage;
			
			/** Custom code **/
			$data['company_name_mob_user'] = $this->companyadminmodel->getCompanyName($companyid);
			/** Custom code **/
			
			$tokenExist = $this->companyadminmodel->getTokenFromCompanyAdmin($companyid);
			if(!$tokenExist)	{
				$data['Ctoken'] = '';
			} else {
				$data['Ctoken'] 	= $tokenExist;
				$data['token'] 		= $tokenExist;
				require 'recurring/GetCardFromTokenEway.php';
				$token 				= new GetCardFromTokenEway();
				$data['tokenCustomerDetails'] = $token->get_card_from_token_eway($data);			
			}
			
			$data['cexpirydate'] 	= $this->companyadminmodel->returnExpiryDate($companyid);
			
			//print_r($transposedarr);
			//echo json_encode($data);
			if($flag == 1)	{
				$data['paymentMessage'] = 'Your payment has been successful';
			}	else	{
				$data['paymentMessage'] = '';		
			}
			
			$this->load->view('CompanyAdmin/upgradeform',$data);
		}
		else
		{
			header ("Location:".base_url()."index.php/login");
			exit;
		}
	}



        function billinghistory()
        {
		  if($this->session->userdata("companyid"))
		  {
			 $data = $this->billinghistoryPagination();
			 $this->load->view('CompanyAdmin/billinghistory',$data);
			 
		  }else{
				redirect('login');
			   }
        }




      public function billinghistoryPagination()
	   {
		$options = array();
		$customUrl = "";
		$config["base_url"] = base_url() . "index.php/companyadmin/billinghistory";
		$config["total_rows"] = count(Transactionmodel::GetBillingbycompanyid($this->session->userdata("companyid")));
		$config['reuse_query_string'] = TRUE;
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div><!--pagination-->';
		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		
		$options['limit'] = $config["per_page"];
		$options['offset'] = $page;
		$options['companyid']=$this->session->userdata("companyid");
		$data["results"] = Transactionmodel::GetBillingDetailsbycompanyid($options);
		$data["links"] = $this->pagination->create_links();
		$data["creditamount"] = creditamount($this->session->userdata("companyid"));
		return $data;
	}
	
	
	function createInvoice()
	{
         include_once "xero.php";
		 define('XERO_KEY','4RVEQBHQD09RAMCYB5WQERXEGYANYK');
		 define('XERO_SECRET','ZKW6CGPHRM43MLRXXWE6SLDQYO6P6Y');


         $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );
	   
		 $packageid = $_REQUEST['selectedpkgid'];   
		 $noofdevices = $_REQUEST['noofdevices'];
		 $packagedetails = $this->packagemodel->getDetails($packageid);

         $price = $packagedetails['price'];
         $price_per_device = $packagedetails['additionaldeviceprice'];
         $number_of_devices = $noofdevices;
         date_default_timezone_set('Asia/Calcutta');
		 $date = date("Y-m-d H:i:s");
		 $today = strtotime($date);
		   //$due = strtotime('+15 days',$today);
		   // $duedate= date("Y-m-d H:i:s",$due);
			   // "DueDate" => $duedate,
		 $expiry = strtotime('+30 days',$today);
		 $expirydate = date("Y-m-d",$expiry);
		 $new_invoice = array(
			 array(
             "Type"=>"ACCREC",
             "Contact" => array(
             "Name" => "API TEST Contact 1"
                 ),
             "Date" => $date,
						 "DueDate" => $expirydate,
             
             "Status" => "AUTHORISED",
             "LineAmountTypes" => "Exclusive",
             "LineItems"=> array(
                "LineItem" => array(
                    array(
                        "Description" => "Package Amount", 
                        "UnitAmount" => $price,
                        "AccountCode" => "880"
                    ),
                    array(
                        "Description" => "Devices",
                        "Quantity" => $number_of_devices,
                        "UnitAmount" => $price_per_device,
                        "AccountCode" => "880",
                        "TaxType" => "NONE"
                    )
                    
                )
            )
        )
    );

    $invoice_result = $xero->Invoices( $new_invoice );
     //echo json_encode($invoice_result);die;
    $invoiceid = $invoice_result['Invoices']['Invoice']['InvoiceID'];
    $invoicenumber = $invoice_result['Invoices']['Invoice']['InvoiceNumber'];


   // echo $invoiceid;
  // $invoiceid = 'INV-DEMO';
   //$invoicenumber = 'inv12345';
   $iData = array(
				'companyid' => $this->session->userdata("companyid"),
				'transactionid' => '0',
				'amount' => $price,
				'transactiondate' => '-',
				'type' => 'creditcard',
                                'comment'=>'-',
                                'invoiceid'=>$invoiceid,
                                'invoicenumber'=>$invoicenumber,
                                'ispaid'=>0,
				'createdon' => $date
		);
   // $affected_rows = Transactionmodel::addInvoice($iData);


    $pkgdetails=$this->packagemodel->getDetails($packageid);
    $pkgdefaultdevies=$pkgdetails['noofdevices'];
    $packageType = $pkgdetails['type'];
   
    //$noofdevices+=2;
    $companyid = $this->session->userdata("companyid");
    //$sql= "update companyadmin set packageid = {$packageid} ,expirydate ='{$expirydate}',noofdevicesallowed = noofdevicesallowed+{$noofdevices} where companyid = {$companyid}";
	$sql= "update companyadmin set packageid = {$packageid}, type = '{$packageType}', expirydate ='{$expirydate}', noofdevicesallowed = {$noofdevices} where companyid = {$companyid}";
    $this->db->query($sql);
    $pdf_invoice = $xero->Invoices($invoiceid, '', '', '', 'pdf');
		
		//redirect('companyadmin/upgradePackakge');
		//header ("Location:".base_url()."index.php/companyadmin/upgradePacakge");
		 /*echo "<script language=\"javascript\">alert('test');</script>";
		//echo '<script type="text/javascript">window.location.reload()</script>';*/
		
    //header('Content-type: application/pdf');
    //header('Content-Disposition: inline; filename="the.pdf"');    
		//echo ($pdf_invoice);
		//redirect('companyadmin/upgradePacakge');
		echo 'success';   
	}
	

    function openInvoice()
    {
        $invoiceid = $_REQUEST['id'];
 
         include_once "xero.php";
		 define('XERO_KEY','4RVEQBHQD09RAMCYB5WQERXEGYANYK');
		 define('XERO_SECRET','ZKW6CGPHRM43MLRXXWE6SLDQYO6P6Y');


         $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );

         $pdf_invoice = $xero->Invoices($invoiceid, '', '', '', 'pdf');

         header('Content-type: application/pdf');
         header('Content-Disposition: inline; filename="the.pdf"');

         echo ($pdf_invoice);


    }



    function corporate()
    {
        $companyid = 0;
	    if($this->session->userdata("companyid")) 
		{
	    	$companyid = $this->session->userdata("companyid");
			$data = array();
			$userdetails = $this->packagemodel->getUserDetails($companyid);
			$data['userdetails']=$userdetails;
			$this->load->view('CompanyAdmin/corporateform',$data);
		}
		else
		{
			header ("Location:".base_url()."index.php/login");
			exit;
		}
    }
	
	
    function corporaterequest()
    {
       $abn = html_purify($_REQUEST['abn']);
       $acn = html_purify($_REQUEST['acn']);
       $email = html_purify($_REQUEST['email']);
       $companyid = $_REQUEST['companyid'];
	   $username = html_purify($_REQUEST['username']);
	   $phone = html_purify($_REQUEST['phone']);

       
       $price = $_REQUEST['price'];
       $storage = $_REQUEST['storage'];
       $priceperdevice = $_REQUEST['priceperdevice'];
       $maxdevices = $_REQUEST['maxdevices'];
       $emailsupport = $_REQUEST['emailsupport'];
       
       $temp_data=$price.','.$storage.','.$priceperdevice.','.$maxdevices.','.$emailsupport;
       
      // $affected_rows = Companyadminmodel::processCorporateRequest($abn,$acn,$email,$companyid,$temp_data);
	   $affected_rows = Companyadminmodel::processCorporateRequest($abn,$acn,$email,$companyid,$temp_data,$username,$phone);
      
	   Email::sendApplicationCorporateSubmitionMail($email,$username,$phone);
       $data = array("errorMsg" => "Application has been successfully submitted");
	   
       $companyid = $this->session->userdata("companyid");
		$userdetails = $this->packagemodel->getUserDetails($companyid);
		$data['userdetails']=$userdetails;
		redirect('companyadmin/upgradePacakge');
		//$this->load->view('CompanyAdmin/corporateform',$data);
    }
    
    
    function generateInvoice($packageid)
    {

    	include_once "xero.php";
		define('XERO_KEY','4RVEQBHQD09RAMCYB5WQERXEGYANYK');
		define('XERO_SECRET','ZKW6CGPHRM43MLRXXWE6SLDQYO6P6Y');
    	
    	$xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );
    	
    	$packageid = $packageid;
    	$packagedetails = $this->packagemodel->getDetails($packageid);
    	
    	$price = $packagedetails['price'];
    	$price = ($price * 5) / 6;
    	
    	$price_per_device = $packagedetails['additionaldeviceprice'];
    	$number_of_devices = $packagedetails['noofdevices'];
    	 
    	date_default_timezone_set('Asia/Calcutta');
    	$date = date("Y-m-d");
    	$today = strtotime($date);
    	$due = strtotime('+15 days',$today);
    	$duedate= date("Y-m-d",$due);
    	
    	$new_invoice = array(
    			array(
    					"Type"=>"ACCREC",
    					"Contact" => array(
    							"Name" => "API TEST Contact 1"
    					),
    					"Date" => $date,
    					"DueDate" => $duedate,
    					"Status" => "AUTHORISED",
    					"LineAmountTypes" => "Exclusive",
    					"LineItems"=> array(
    							"LineItem" => array(
    									array(
    											"Description" => "Package Amount",
    	
    											"UnitAmount" => $price,
    											"AccountCode" => "880"
    									),
    									array(
    											"Description" => "Devices",
    											"Quantity" => $number_of_devices,
    											"UnitAmount" => $price_per_device,
    											"AccountCode" => "880",
    											"TaxType" => "NONE"
    									)
    	
    							)
    					)
    			)
    	);
    	
    	 
    	$invoice_result = $xero->Invoices( $new_invoice );
    	// echo json_encode($invoice_result);
    	$invoiceid = $invoice_result['Invoices']['Invoice']['InvoiceID'];
    	$invoicenumber = $invoice_result['Invoices']['Invoice']['InvoiceNumber'];
    	
    	
    	// echo $invoiceid;
    	
    	
    	$iData = array(
    			'companyid' => $this->session->userdata("companyid"),
    			'transactionid' => '0',
    			'amount' => $price,
    			'transactiondate' => '-',
    			'type' => 'credicard',
    			'comment'=>'-',
    			'invoiceid'=>$invoiceid,
    			'invoicenumber'=>$invoicenumber,
    			'ispaid'=>0,
    			'createdon' => $date
    	);
    	//$affected_rows = Transactionmodel::addInvoice($iData);
    	
    	
    	
    	
    	
    	$pdf_invoice = $xero->Invoices($invoiceid, '', '', '', 'pdf');
    	
    	header('Content-type: application/pdf');
    	header('Content-Disposition: inline; filename="the.pdf"');
    	
    	echo ($pdf_invoice);
    	
    	
    	
    	
    }
       
       
   public function generateRandomPassword()
	{
		$length = 6;
		$pin = "";
		$possible = "012346789ABCDFGHJKLMNPQRTVWXYZ";
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
		// check for length overflow and truncate if necessary
		if ($length > $maxlength)
		{
			$length = $maxlength;
		}
		// set up a counter for how many characters are in the password so far
		$i = 0;
		// add random characters to $password until $length is reached
		while ($i < $length)
		{
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, $maxlength-1), 1);
			// have we already used this character in $password?
			if (!strstr($pin, $char)) {
				// no, so it's OK to add it onto the end of whatever we've already got...
				$pin .= $char;
				// ... and increase the counter by one
				$i++;
			}
		}
		return "$".$pin."#";
	}
	
	
	public function resetPassword()
	{
	   	  $email = html_purify($_REQUEST['email']);
          $id = Companyadminmodel::userIsExists($email);
      if($id > 0){
		   $userIsActive = Companyadminmodel::userIsActive($email);
		   if($userIsActive > 0)
		   {
			   $generatedPassword = $this->generateRandomPassword();
			   $password = md5($generatedPassword);
			   $updateid=Companyadminmodel::resetPassword($email,$password);
			   if($updateid > 0)
			   {
				  $username = Companyadminmodel::getUsername($email);
				  $userDetails = Companyadminmodel::getUserDetails($email);
				  $fullname = $userDetails->firstname.' '.$userDetails->lastname;
				  Email::sendUpdatedPassword_Email($fullname,$email,$generatedPassword,$username);
				  echo '1';
			   }
			   
		   }else{
		   		  echo 'Sorry, your account is deactivated';
		   		}
		   
	   }else{
               $idd = Companyadminmodel::userIsExistsInCompanyUser($email);
               if($idd > 0)
               {
                 
				   $companyUserIsActive = Companyadminmodel::companyUserIsActive($email);
				   if($companyUserIsActive > 0)
				   {
					  $generatedPassword = $this->generateRandomPassword();
					  $password = md5($generatedPassword);
					  $updateid=Companyadminmodel::resetPasswordCompanyUser($email,$password);
					  if($updateid > 0)
					  {
					   $username = Companyadminmodel::getCompanyUsername1($email);
					   Email::sendUpdatedPassword_Email($username,$email,$generatedPassword);
					   echo '1';
					  }
				    }else{
		   		  			echo 'Sorry, your account is deactivated';
		   				 }
               }
               else
               {
                 echo 'User does not exist in cuedrive';
               }
          }
	
	}
	
	
	public function updatepassword()
        {
           	$companyid 			= $this->session->userdata("companyid");
			$data 				= array();
			$userdetails 		= $this->packagemodel->getUserDetails($companyid);
			$data['userdetails']=$userdetails;
			$this->load->view('CompanyAdmin/updatepassword', $data);
        }
		
	public function updatepasswordAu()
        {
           	$useridd 			= $this->session->userdata("userid");
			$data 				= array();
			$userdetails 		= Companyusermodel::getUserDetails($useridd);
			$data['userdetails']=$userdetails;
			$this->load->view('CompanyAdmin/updatepassword_adminuser', $data);
        }
	

        public function updateCompanyPassword()
        {
        	if($this->session->userdata("companyid")) 
			{
              $username 	= html_purify($_POST['username']);
              $newpass 		= md5(html_purify($_POST['newpass']));
			  $email 		= html_purify($_POST['email']);
              $companyid 	= 0;
              if($this->session->userdata("companyid")) 
              {
                $companyid = $this->session->userdata("companyid");
              }
				      $userdetails = $this->packagemodel->getUserDetails($companyid);
					  if(Companyadminmodel::userExistsWithUsername($username,$companyid) == 0)
	 			 		{
						 if(Companyadminmodel::userExistsWithEmail($email,$companyid) == 0 )
						 {
						  $updateid 	= Companyadminmodel::updateCompanyPassword($username,$newpass,$email,$companyid);
						  $data 		= array('errMsg' => 'Profile has been updated successfully', 'userdetails' => $userdetails);
						  $this->load->view('CompanyAdmin/updatepassword',$data);
						 
						 }else{
							  $data 	= array('errorMsg' => 'This email already exists, please use another email address', 'userdetails' => $userdetails);
							  $this->load->view('CompanyAdmin/updatepassword',$data);
						      }
						 }else{
							  $data 	= array('errorMsg' => 'User name already exists, please select another username', 'userdetails' => $userdetails);
						  	  $this->load->view('CompanyAdmin/updatepassword',$data);
						 	  }
        	 }else {
        			redirect('login');
        		  }
        }
		
		
	public function updateCompanyPasswordAu()
        {
        	if($this->session->userdata("userid")) 
			{
              $username 	= html_purify($_POST['username']);
              $password		= md5(html_purify($_POST['newpass']));
			  $email 		= html_purify($_POST['email']);
              $userid 		= $this->session->userdata("userid");
			  
			  if(($email && $username && $password)!='')
				   { 
					  $userdetails = Companyusermodel::getUserDetails($userid);
					  if(Companyusermodel::userExistsWithUsername($username,$userid) == 0)
					  {
						 if(Companyusermodel::userExistsWithEmail($email,$userid) == 0 )
						 {
							
							$oldusername = $userdetails['username'];
							$sql = "update companyuser set email = '{$email}',username = '{$username}',password = '$password' where companyuserid={$userid}";
							$this->db->query($sql);
							$firstname = $userdetails['firstname'];
							$companyid = $userdetails['companyid'];
							$user_password = Openfiremodel::generateRandomToken();
							$url = ORG_URL."chat/?type=update&oldusername=".urlencode($oldusername)."&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($firstname)."&group=".urlencode($companyid);
							Openfiremodel::sendDataToServer($url); 
							$data 		= array('errMsg' => 'Profile has been updated successfully', 'userdetails' => $userdetails);
						  $this->load->view('CompanyAdmin/updatepassword_adminuser',$data);
						 }
						 else
						 {
						   $data 	= array('errorMsg' => 'This email already exists, please use another email address', 'userdetails' => $userdetails);
							  $this->load->view('CompanyAdmin/updatepassword_adminuser',$data);
						 }
					  
					  
					}else{
							 $data 	= array('errorMsg' => 'User name already exists, please select another username', 'userdetails' => $userdetails);
						  	  $this->load->view('CompanyAdmin/updatepassword_adminuser',$data);
						  }
						  
					}else{
							echo "Please complete the required fields";
						 }
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
             /* $userdetails 	= $this->packagemodel->getUserDetails($companyid);
			  if(Companyadminmodel::userExistsWithUsername($username,$companyid) == 0)
	 			{
				 if(Companyadminmodel::userExistsWithEmail($email,$companyid) == 0 )
					 {
						  $updateid 	= Companyadminmodel::updateCompanyPassword($username,$newpass,$email,$companyid);
						  $data 		= array('errMsg' => 'Profile has been updated successfully', 'userdetails' => $userdetails);
						  $this->load->view('CompanyAdmin/updatepassword',$data);
						 
						 }else{
							  $data 	= array('errorMsg' => 'This email already exists, please use another email address', 'userdetails' => $userdetails);
							  $this->load->view('CompanyAdmin/updatepassword',$data);
						      }
						 }else{
							  $data 	= array('errorMsg' => 'User name already exists, please select another username', 'userdetails' => $userdetails);
						  	  $this->load->view('CompanyAdmin/updatepassword',$data);
						 	  }*/
        	 }else {
        			redirect('login');
        		  }
        }





        function saveFolderEmailPermissions()
        {
        $folderid = $_REQUEST['folderid'];
        $emails = html_purify($_REQUEST['emails']);
        $numbercue = $_REQUEST['numbercue'];
        $this->foldermodel->savefolderemailpermission($folderid,$emails,$numbercue);
        echo "1";
        }
        



    public function auditloglist()
	{
		if($this->session->userdata("companyid")) {
		$data = $this->auditloglistPagination();
		$this->load->view('CompanyAdmin/loglist',$data);
		}
		else {
			redirect('login');
		}
	}
	
	function auditloglistPagination()
	{

	if(!$this->session->userdata("companyid")) 
         {
	  header ("Location:".base_url()."index.php/login");
	  exit;
         }
	
	else {
		$options = array();
		$customUrl = "";
		if(isset($_REQUEST['searchtext']))
		{
                        $searchtext = html_purify($_REQUEST['searchtext']);
			$options['like'] = $searchtext;
			
			$data["search"] = $searchtext;
		}
                else if(isset($_REQUEST['from']) && isset($_REQUEST['to']))
                {
                   $todate = date('Y-m-d',strtotime($_REQUEST['to']."+1 days"));
                   
                   $fromdate = date('Y-m-d',strtotime($_REQUEST['from']));
                   

                   $options['to'] = $todate;
                   $options['from'] = $fromdate;



                }
                else  {
			$config["base_url"] = base_url() . "index.php/companyadmin/auditloglist";
			$config["total_rows"] = count(Auditlogmodel::getAuditlogDetails());
			$config['reuse_query_string'] = TRUE;
			$config["per_page"] = 20;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div><!--pagination-->';
			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
			$options['limit'] = $config["per_page"];
			$options['offset'] = $page;
			}
			$data["results"] = Auditlogmodel::getAuditlogDetails($options);
			$data["links"] = $this->pagination->create_links();
			return $data;
	     }
	}

function moveFile(){

	// Load Library
	$this->load->library('s3');
	$movefileid=$_REQUEST['movefileid'];
	$destinationfolderid=$_REQUEST['destinationfolderid'];
	$companyid=$_REQUEST['companyid'];
	
	 { //device authenticated
		
		 {
			//move file
				
			$replaceflag=false;
			$fileName=$this->folderfilemodel->getFileName($movefileid);
				
			if($this->isFileExist($fileName, $destinationfolderid) )
			{
				$existingfileid=$this->folderfilemodel->getfileID($fileName,$destinationfolderid);
			
					$uploadflag=false;
				
			}
			else {
				$uploadflag=true;
			}

			if($uploadflag)
			{
				$sourcefilepath=$this->folderfilemodel->getFilePath($movefileid);
				$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
				$destinationfilepath=$destinationfolderpath."/".$fileName;
				//$bucket="cuedrive.".$companyid;
				$bucket=ROOTBUCKET;
				$companyfoldername=COMPANYFOLDERNAME.$companyid;
				
				if($this->s3->copyObject($bucket, $companyfoldername.'/'.$sourcefilepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
				{
						
                   /**
					$activitydetails = 'moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					if($username!="")
					{
						$activitydetails = $username.' '.'has moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					}
					$logcc['deviceid'] = $deviceid;
					$logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['action'] = 'move';
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfilepath;
					$logcc['from']=$sourcefilepath;
						
					*/	
						
						
						
					//delete the file from sorce folder
					$this->s3->deleteObject($bucket, $companyfoldername.'/'.$sourcefilepath);
					//update DB
					$datacc=array();
					$datacc['fullpath']=$destinationfolderpath.'/' .$fileName;
					$datacc['folderid']=$destinationfolderid;
					$datacc['updatedbydevice']=NULL;
					
				  /**
					if($userauth)
					{
						$companyuserid=$usrauthresult['companyuserid'];
						$datacc['updatedbyuser']=$companyuserid;
						$logcc['companyuserid'] = $companyuserid;
					}
					*/
					
					$this->folderfilemodel->updateFile($datacc,$movefileid);
						
					if($replaceflag)
					{
						//delete the existing file from destination folder
						$this->folderfilemodel->deleteFile($existingfileid);
							
					}
					
					$activitydetails = 'moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					$logcc=array();
					$logcc['companyuserid'] = 0;
					$logcc['action'] = 'move';
					$logcc['deviceid'] = 0;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfilepath;
					$logcc['from']=$sourcefilepath;
					$logcc['companyid']= $companyid;
					
					
					$this->auditlogmodel->updateAuditLog($logcc);
					//$this->response(APIFILEMOVESUCCESS,200);
					echo '1';

				}
				else {
					echo '0';
				}
			}//duplicate file validation
			else {
				echo 'fileExists'; exit;
			}
		}
	}
}

function isFileExist($newfilename,$parentfolderid)
{
	 if($this->folderfilemodel->fileNameExist($newfilename,$parentfolderid))
	 {
	 	return true;
	 }
	 else {
	 	return false;
	 }
}



function moveFolder()
{
	// Load Library
	$this->load->library('s3');
	$movefolderid=$_REQUEST['movefolderid'];
	$destinationfolderid=$_REQUEST['destinationfolderid'];
	$companyid=$_REQUEST['companyid'];
    {
		{
			
				//move folder
				$replaceflag=false;
				$folderdetails=$this->foldermodel->getFolderDetails($movefolderid);
				$sourcefoldername=$folderdetails['foldername'];
				if($this->isFolderExist($sourcefoldername, $destinationfolderid) )
				{
					$existingfolderid=$this->foldermodel->getfolderID($sourcefoldername,$destinationfolderid);
					
					{
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
					
				if($uploadflag)
				{
					$sourcefolderpath=$this->foldermodel->getFolderPath($movefolderid);
					$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
					//$bucket="cuedrive.".$companyid;
					$bucket=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;				
						
					//delete the source folder from amazon
					$deletefolderarr=$this->getChildren($movefolderid);
					array_push($deletefolderarr,$movefolderid);
	
					for($i=0;$i<count($deletefolderarr);$i++)
					{
					$folderid=$deletefolderarr[$i];
					$files= $this->folderfilemodel->getFiles($folderid);
					for($j=0;$j<count($files);$j++)
					{
					$filepath=$files[$j]['fullpath'];
					$fileid=$files[$j]['id'];
	
					$fullpatharr=explode($sourcefoldername, $filepath);
					$tempfullpath=$sourcefoldername.$fullpatharr[1];
					$destinationfilepath=$destinationfolderpath.'/' .$tempfullpath;
					if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
					{
					if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
					{
						//update folderfile
						$datacc=array();
							
						$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
						$datacc['updatedbydevice']=NULL;
		
						$this->folderfilemodel->updateFile($datacc,$fileid);
					}
					}
					}
					//update folder
					$datacc=array();
					$folderpatharr=explode($sourcefoldername, $sourcefolderpath);
					$tempfullpath=$sourcefoldername.$folderpatharr[1];
					$datacc=array();
					if($folderid==$movefolderid) //change parent id
					{
					$datacc['parentfolderid']=$destinationfolderid;
					$datacc['type'] = $this->foldermodel->getFolderType($destinationfolderid);
			}
			$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
			$datacc['updatedbydevice']=NULL;
	
			$this->foldermodel->updateFolder($datacc,$folderid);
		}
			$activitydetails = 'moved a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
			$logcc=array();
			$logcc['companyuserid'] = 0;
			$logcc['action'] = 'move';
			$logcc['deviceid'] = 0;
			$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$logcc['activitydetails'] = $activitydetails;
			$logcc['to']=$destinationfolderpath;
			$logcc['from']=$sourcefolderpath;
			$logcc['companyid']= $companyid;
			
			$this->auditlogmodel->updateAuditLog($logcc);
			echo '1';
	
		}//duplicate file validation
	else {
		echo '0';
	}
	
		}
   }
}


function isFolderExist($newfoldername,$parentfolderid)
{
	if($this->foldermodel->folderNameExist($newfoldername,$parentfolderid))
	{
		return true;
	}
	else {
		return false;
	}
}


function downloadLogFile()
{
  $this->load->helper('download');
  $filename = $this->encrypt->decode($_POST['filename']);
  $companyid = $this->encrypt->decode($_POST['companyid']);

  if(file_exists ( 'log/'.$filename )) {
  if($companyid == $this->session->userdata("companyid")) {
  $data = file_get_contents("/home/cuedrive/www/cuedrive/log/".$filename); // Read the file's contents
  $name = 'Auditlog.txt';

  force_download($name, $data); 
  }
}
 else
 {
        $data = $this->auditloglistPagination();
		$this->load->view('CompanyAdmin/loglist',$data);

 }
}

function noAccess()
{
	$this->load->view('CompanyAdmin/badaccess');
}


function getPackageById() 
{

  $packageid = $_REQUEST['packageid'];
  $data = Packagemodel::getDetails($packageid);
  echo json_encode($data);

}

function autoPayment()
{
     $companyid = $this->session->userdata("companyid");
     $data = array('autopayment' => 'true');
     $this->db->where('companyid',$companyid);
     $this->db->update('companyadmin',$data);
     echo 'success';
}

function cancelautoPayment()
{
     $companyid = $this->session->userdata("companyid");
     $data = array('autopayment' => 'false');
     $this->db->where('companyid',$companyid);
     $this->db->update('companyadmin',$data);
    // Email::cancelAutoPayment();
     echo 'success';
}
function fetchPaymentOption()
{
        $companyid = $this->session->userdata("companyid");
        $query = $this->db->get_where('companyadmin',array('companyid' => $companyid));
        $data = $query->row_array();
        $autopayment = $data['autopayment'];
        $devices = $data['noofdevicesallowed'];
        echo trim($autopayment.":".$devices);
}

	
	function createDir()
	{
		
		$parentfolderId=$this->input->post('parentfolderid');		
		$companyid = $this->session->userdata("companyid");
		if(Permissionsmodel::checkValidFolder($parentfolderId,$companyid) == 1) {		
		$newfoldername=html_purify(trim($this->input->post('newfoldername')));
		$level=trim($this->input->post('level'));
		$permissionflag=$this->input->post('permflag');
		if(!$this->foldermodel->foldernameexist($newfoldername,$parentfolderId))
		{
		$companyid=$this->session->userdata("companyid");
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$companyfolder=
		$parentdetails=$this->foldermodel->getFolderDetails($parentfolderId);
		$path=$parentdetails['fullpath'];
		$parenttype=$parentdetails['type'];
		$newpath=$path."/".$newfoldername;
		$uploaddir = realpath('./uploads/');
		$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$newfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$newpath;
				$datacc['type']=$parenttype; 
				$datacc['parentfolderid']=$parentfolderId; 
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');				
				$newfolderid=$this->foldermodel->insertFolder($datacc);	
				if($permissionflag=="parent")
				{
					$this->addParentPermissions($parentfolderId,$newfolderid);
				}
				
				echo $newfolderid;
			}
			else {
				echo "createfolderfail";
			}
			
		}
		else
		{
			echo "foldernameexists";
		}
		}
		else {
			echo "createfolderfail";
		}
		
	}

	function checkTokenExists()
	{
		$companyid = $this->session->userdata("companyid");
		$query = $this->db->get_where('companyadmin',array('companyid' => $companyid));
		$data = $query->row_array();
		$token= $data['customerToken'];
		if($token != 0)
		{
			  echo 'success';
		}
		else
		{
			  echo 'error';
		}
	}
	
	function upgradeDevice()
		{
			$companyid = $this->session->userdata("companyid");	
			$tokenExist = $this->companyadminmodel->getTokenFromCompanyAdmin($companyid);
			$data['Ctoken'] = '';
			/*if(!$tokenExist)	{
				$data['Ctoken'] = '';
			} else {
				$data['Ctoken'] = $tokenExist;
				$data['token'] = $tokenExist;
				require 'recurring/GetCardFromTokenEway.php';
				$token = new GetCardFromTokenEway();
				$data['tokenCustomerDetails'] = $token->get_card_from_token_eway($data);			
			}*/
			$this->load->view('CompanyAdmin/deviceupgrade', $data);
		}

	
	function updateDetails()
		{
			$data = array ("id"=>$this->session->userdata("companyid"));
			$this->load->view('CompanyAdmin/updateDetails', $data);
		}


	/** code start ***/
	
		function getAddressDetails()	{
		
			$data['noofdevices'] = '';
			$data['pkgid'] = '';
			$data['country'] = array();
			
			if($_POST['userid'] != '')	{
				$userId = $_POST['userid'];
				$data['noofdevices'] = $_POST['noofdevices'];
				$data['pkgid'] = $_POST['pkgid'];
				$data['country'] = $this->companyadminmodel->GetCountryList();
			} else	{
				$userId = '';
			}
			
			 $this->load->view('CompanyAdmin/addressDetails', $data);
			
		}
		
		function updateCompanyMbDetails()
	{
			
			$companyid = $_POST["companyid"];
			$companyname = $_POST["company_name"];
			if(Companyadminmodel::companyNameExists($companyname,$companyid) == FALSE)
			{
				$userData = array(	'name' => html_purify($_POST["company_name"]),
									'firstname' => html_purify($_POST["firstname"]),
									'lastname' => html_purify($_POST["lastname"]),
									'address' => html_purify($_POST["address"]),
									'phone' => html_purify($_POST["phone"]),
									'city' => html_purify($_POST["city"]),
									'country' => html_purify($_POST["country"]),
									'zipcode' => html_purify($_POST["zipcode"]),									
									'companyid' => html_purify($_POST["companyid"])
								);
								
				$affected_rows = Companyadminmodel::UpdateCompany($userData);
				echo 'success';
			} else	{
				echo 'error';
			}	
				//$data = array ("id"=>$this->session->userdata("companyid"), "sucessMsg" =>"Record updated");
			
	}
		
		/**  code end  ***/		
		
	function updateAccountDetails()
	{ 
			$companyid = $this->session->userdata("companyid");
			$companyname = $_REQUEST["name"];
			if(Companyadminmodel::companyNameExists($companyname,$companyid) == FALSE)
			{ 
				$userData = array(	'name' => $companyname,
									'address' => $_REQUEST["address"],
									'phone' => html_purify($_REQUEST["phone"]),
									'city' => html_purify($_REQUEST["city"]),
									'country' => html_purify($_REQUEST["country"]),
									'zipcode' => html_purify($_REQUEST["zipcode"]),
									'abn' => html_purify($_REQUEST["abn"]),
									'acn' => html_purify($_REQUEST["acn"]),
									'companyid' => $companyid
								);
								
				$affected_rows = Companyadminmodel::UpdateCompany($userData);
				$data = array ("id"=>$this->session->userdata("companyid"), "sucessMsg" =>"Record updated");
			}else{
				$data = array ("id"=>$this->session->userdata("companyid"), "errorMsg" =>COMPANYNAMEEXIST);
			     }
			$this->load->view('CompanyAdmin/updateDetails',$data);
	}
	
	function amendAccount($companyid = '')	{		
		
		if($this->input->post('acctpass') != '')	{
			if(Companyadminmodel::getPassword($this->session->userdata('companyid'), $this->input->post('acctpass')))	{
				
				$this->load->view('CompanyAdmin/amendacctconfirm');
				
			} else {
				$this->load->view('CompanyAdmin/loadpasswordform', array('message' => "The password entered does not match."));
			}
		} else {
			$this->load->view('CompanyAdmin/loadpasswordform', array('message' => "Please enter password."));
		}
		
	}
	
	function loadpasswordfrm()	{
		
		$this->load->view('CompanyAdmin/loadpasswordform', array('message'=>''));
	}
	
	function cancelAccount()	{
		
		if(Companyadminmodel::checkAccountType($this->session->userdata('companyid')))	{
		//For Corporate accounts
			Companyadminmodel::cancelAccount($this->session->userdata('companyid'));			
			
			//Send email
			$this->load->library('email');
			$config = array (
						  'mailtype' => 'html',
						  'charset'  => 'utf-8',
						  'priority' => '1'
						   );
				$this->email->initialize($config);
				$this->email->from('admin@cuedrive.com', 'cuedrive');
				$this->email->to('sunil.game@aressindia.net');
			   
				
				$this->email->subject('Cuedrive - Amend Account');
			   
				
				$message = "Hi Accounts,<br><br><br>".$this->session->userdata('username')." has amended his account. Please send him final invoice.
							
							<br/>Kind Regards,<br>
							Cuedrive team";
			 
				$this->email->message($message);

				$this->email->send();
				
				echo '<script type="text/javascript">parent.$.magnificPopup.close()</script>';
			
		}	else {
			//For regular accounts
			if(Companyadminmodel::cancelAccount($this->session->userdata('companyid')))	{
				
				echo '<script type="text/javascript">parent.$.magnificPopup.close()</script>';
				
				//Generate Invoice
				
				//Write your code here
				
				
			}
		}
	}
	
	function cardupdatesuccess()	{
		
		//$data['token'] = $tokenCustomer;
		$data['token'] = Companyadminmodel::getTokenFromCompanyAdmin($this->session->userdata('companyid'));
		$compemail = Companyadminmodel::getCompanyEmail($this->session->userdata('companyid'));
		require 'recurring/GetCardFromTokenEway.php';
		$tokenNew = new GetCardFromTokenEway();
		$tokenCustomerDetails = $tokenNew->get_card_from_token_eway($data);			
		
		//Send email
		$this->load->library('email');
		$config = array (
		  'mailtype' => 'html',
		  'charset'  => 'utf-8',
		  'priority' => '1'
		);
		$this->email->initialize($config);
		$this->email->from('admin@cuedrive.com', 'cuedrive');
		//$this->email->to('sunil.game@aressindia.net');
		$this->email->to($compemail);
		$this->email->subject('Cuedrive - Card is changed'); 
		
		$message = "Hi ".$this->session->userdata('username').",<br><br>This is a courtesy email to inform you that your credit card details have changed.<br/><br/>Your new card number contains the following numbers: ". $tokenCustomerDetails['ccnumber'] ."<br/><br/>If you did not initiate	this change, please	contact	Cuedrive support.<br/><br/>Kind Regards,<br>Cuedrive team";
	 
		$this->email->message($message);
		$this->email->send();
/*//echo '<script type="text/javascript">parent.document.getElementById("ccnumberdetails").innerHTML = "'. $tokenCustomerDetails["ccnumber"] .'";</script>';*/
		echo $tokenCustomerDetails["ccnumber"];
		
		//$this->load->view('CompanyAdmin/cardupdatesuccess'); 
	}
	
	function updatecard($token = '')	{ 
		
		if($this->input->post())	{ 
			
		}	else	{
			
			$data['token'] = Companyadminmodel::getTokenFromCompanyAdmin($this->session->userdata('companyid'));
			require 'recurring/GetCardFromTokenEway.php';
			$token = new GetCardFromTokenEway();
	        $data['tokenCustomerDetails'] = $token->get_card_from_token_eway($data);			
			$this->load->view('CompanyAdmin/updatecard', $data);			
			
		}
	}
}