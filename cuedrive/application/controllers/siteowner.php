<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Siteowner extends CI_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->database();

		$this->load->library('session'); 

		$this->load->library('pagination');

		$this->load->helper(array('form','url','cookie'));

		$this->load->model('siteownermodel');

		$this->load->model('companyadminmodel');

		$this->load->model('companydevicemodel');

		$this->load->model('packagemodel');

		$this->load->model('contentmanagementmodel');

		$this->load->model('contactusmodel');

		$this->load->model('folderfilemodel');

		$this->load->model('email');

		$this->load->model('transactionmodel');

		$this->load->model('auditlogmodel');

		$this->load->model('companyusermodel');
		$this->load->model('saveorderofhelparticles');

		$this->load->helper('htmlpurifier');

		

	}



	function index()

	{

		$this->load->view('siteowner/login');

	}

	

	function login()

	{

		$username= $this->input->post('username');

		$password= md5($this->input->post('userpass'));

		$rememberPassword= $this->input->post('userpass');

		$rememberMe = $this->input->post('rememberme');

		

		$ismanager=$this->siteownermodel->checkIsManager($username, $password);

		$isMasterAdmin=$this->siteownermodel->checkIsMasterAdmin($username, $password);

		$retval=$this->siteownermodel->adminauth($username, $password);

		

		if($retval==0) //authentication failed

			{

				$this->session->set_flashdata('loginmsg', 'Invalid Username or Password');

				redirect('siteowner');



			}

			else { //authenticated

				$adminid=$retval;

				if((int) $rememberMe == 1)

				{

					$cookie = array(

                   	'name'   => 'UserCredential',

                   	'value'  => "$username :$rememberPassword",

                   	'expire' => 60*60*24*365,

              	 	);

					set_cookie($cookie); 

					//echo "set cookie";

				} else {

					delete_cookie('UserCredential');

				}

				

				$this->session->set_userdata('usernam', $username);

				$this->session->set_userdata('adminid', $adminid);		

				$this->session->set_userdata('ismanager', $ismanager);

				$this->session->set_userdata('masteradmin', $isMasterAdmin);

				

				//redirect('siteowner/updateuser?id='.$this->session->userdata("adminid"));

				redirect('siteowner/dashboard');

				

			}

	}

	

	public function businesspackagelist()

	{

		$data = $this->businessPackagelistPagination();

		$this->load->view('siteowner/packagelist',$data);

	}

	

	function businessPackagelistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

		    $searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

		$config["base_url"] = base_url() . "index.php/siteowner/businesspackagelist";

		$config["total_rows"] = count(Packagemodel::getBusinessPackages());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Packagemodel::getBusinessPackagesDetails($options);

		$data["links"] = $this->pagination->create_links();

	

		return $data;

	

	}

	

	

	public function corporatepackagelist()

	{

		$data = $this->corporatePackagelistPagination();

		$this->load->view('siteowner/packagelist',$data);

	}

	

	function corporatePackagelistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			

            $searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

		$config["base_url"] = base_url() . "index.php/siteowner/packagelist";

		$config["total_rows"] = count(Packagemodel::getCorporatePackages());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Packagemodel::getCorporatePackagesDetails($options);

		$data["links"] = $this->pagination->create_links();

	

		return $data;

	

	}

	

	public function feedbacks()

	{

		$data = $this->feedbackPagination();

		$this->load->view('siteowner/feedback',$data);

	}

	

	public function feedbackPagination()

	{



		$options = array();

		$customUrl = "";

		

		$config["base_url"] = base_url() . "index.php/siteowner/feedbacks";

		$config["total_rows"] = count(Contactusmodel::GetFeedbacks());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

		

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

		

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

		

		$data["results"] = Contactusmodel::GetFeedbacksDetails($options);

		$data["links"] = $this->pagination->create_links();

		

		return $data;

		

		

		

	}

	

	public function tickets()

	{

		$data = $this->ticketsPagination();

		$this->load->view('siteowner/tickets',$data);

	}

	

	function ticketsPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			 $searchtext= $_REQUEST['searchtext'];	

                        $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;



		}

		$config["base_url"] = base_url() . "index.php/siteowner/tickets";

		$config["total_rows"] = count(Contactusmodel::GetTicketsDetails($options));

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Contactusmodel::GetTicketsDetails($options);

		$data["links"] = $this->pagination->create_links();

	       

		return $data;

	

	}

	

	function replyTicket()

	{

		$data = array ("id"=>$_REQUEST["ticketid"],"companyid"=>$_REQUEST["companyid"],"priority"=>$_REQUEST["priority"]);

		$this->load->view('siteowner/replyform',$data);

	}

	

	function addticket()

	{

		date_default_timezone_set('Asia/Calcutta');

		$date = date("Y-m-d H:i:s");

		$this->load->library('form_validation');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('priority', 'Priority', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required');

		$this->form_validation->set_rules('comments', 'Issue Description', 'required');

	

		if ($this->form_validation->run() == FALSE)

		{ 

			//$data = array("errorMsg"=>"true");

			$data = array ("id"=>$_REQUEST["ticketid"],"companyid"=>$_REQUEST["companyid"]);

			$this->load->view('CompanyAdmin/createticket',$data);

		}

		else

		{ 

			$ticketid = (isset($_REQUEST['ticketid'])) ? $_REQUEST['ticketid'] : $this->genratePrimaryKey(10);

			$iData = array(

					'type' => $_REQUEST["type"],

					'priority' => $_REQUEST["priority"],

					'email' => $_REQUEST["email"],

					'phone' => $_REQUEST["phone"],

					'comments' => html_purify($_REQUEST["comments"]),

					'companyid' => $_REQUEST["companyid"],

					'companyuserid' => $_REQUEST["companyuserid"],

					'createdon' => $date,

					'sent'=>0,

					'ticketid' => $ticketid

	

			);

			$affected_rows =  Contactusmodel::addticket($iData);

				

			if(isset($_REQUEST['ticketid']))

			{

				redirect('/siteowner/ticketconversation?id='.$_REQUEST['ticketid']);

			}

			else

			{

				$data = $this->ticketsPagination();

				$data["errorMsg"] ="Record added" ;

				$this->load->view('siteowner/tickets',$data);

			}

		}

	}

	

    public function pendingApprovals()

    {

    	$data = $this->pendingApprovalsPagination();

    	$this->load->view('siteowner/pendingapprovals',$data);

    }

    

    

    function pendingApprovalsPagination()

    {

    	$options = array();

    	$customUrl = "";

    	if(isset($_REQUEST['searchtext']))

    	{

    		$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

    	}

    	$config["base_url"] = base_url() . "index.php/siteowner/pendingapprovals";

    	$config["total_rows"] = count(Companyadminmodel::GetPendingApprovals());

    	$config['reuse_query_string'] = TRUE;

    	$config["per_page"] = 20;

    	$config["uri_segment"] = 3;

    	$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

    

    	$this->pagination->initialize($config);

    	$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

    

    	$options['limit'] = $config["per_page"];

    	$options['offset'] = $page;

    

    	$data["results"] = Companyadminmodel::GetPendingApprovalsDetails($options);

    	$data["links"] = $this->pagination->create_links();

    

    	return $data;

    

    }

	function dashboard()

	{

		$data = $this->dashboardlistPagination();

		$this->load->view('siteowner/dashboard',$data);

	}

	

	function dashboardlistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

		$config["base_url"] = base_url() . "index.php/siteowner/dashboard";

		$config["total_rows"] = count(Companyadminmodel::GetCompanyAdmins());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Companyadminmodel::GetCompanyAdminDetails($options, 'dashboard');

		$data["links"] = $this->pagination->create_links();

		

		return $data;

	

	}

	

	

	function billing()

	{

		$data = $this->companylistPagination();

		$this->load->view('siteowner/currentbilling',$data);

	}

	

	

	function userlist()

	{

	  $data = $this->userlistPagination();

	  $this->load->view('siteowner/userlist',$data);

	}

	function userlistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

		$config["base_url"] = base_url() . "index.php/siteowner/companylist";

		$config["total_rows"] = count(Siteownermodel::GetSiteownerAdmins());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Siteownermodel::GetSiteownerAdminsDetails($options);

		$data["links"] = $this->pagination->create_links();

		

		return $data;

	

	}

	

	public function archiveaccounts()	{

		$data = $this->archiveaccountsPagination();

		$this->load->view('siteowner/archiveaccounts',$data);

	}

	

	function archiveaccountsPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{ 	

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			

			if(isset($_REQUEST['searchtext']))

			 {

				$options['like'] = $_REQUEST['searchtext'];

				$this->session->set_userdata('searchby', $searchtext);



			}else{	

			

				$options['like'] = $this->session->userdata('searchby');

				

				}

				//$options['like'] = $searchtext;

				//$data["search"] = $searchtext;

				$data["search"] = $options['like'];

		}else{ 

			  $this->session->unset_userdata('searchby');	

			 }

		

		$config["base_url"] = base_url() . "index.php/siteowner/archiveaccounts";

		

		if(isset($_REQUEST['searchtext']))

		 {

			$config['suffix'] = '?searchtext='.$_REQUEST['searchtext'];

		 }	

			

		$config["total_rows"] = count(Companyadminmodel::GetCompanyAdmins($options, 'archive'));

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Companyadminmodel::GetCompanyAdminDetails($options, 'archive');

		$data["links"] = $this->pagination->create_links();

		

		return $data;

	

	}

	

	public function companylist()

	{

		$data = $this->companylistPagination();

		$this->load->view('siteowner/companylist',$data);

	}

	

	function companylistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{ 	

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			

			if(isset($_REQUEST['searchtext']))

			 {

				$options['like'] = $_REQUEST['searchtext'];

				$this->session->set_userdata('searchby', $searchtext);



			}else{	

			

				$options['like'] = $this->session->userdata('searchby');

				

				}

				//$options['like'] = $searchtext;

				//$data["search"] = $searchtext;

				$data["search"] = $options['like'];

		}else{ 

			  $this->session->unset_userdata('searchby');	

			 }

		

		$config["base_url"] = base_url() . "index.php/siteowner/companylist";

		

		if(isset($_REQUEST['searchtext']))

		 {

			$config['suffix'] = '?searchtext='.$_REQUEST['searchtext'];

		 }	

			

		$config["total_rows"] = count(Companyadminmodel::GetCompanyAdmins($options, 'isactiveyes'));

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Companyadminmodel::GetCompanyAdminDetails($options, 'isactiveyes');

		$data["links"] = $this->pagination->create_links();

		

		return $data;

	

	}

	

	//Added by sunil archive list

	public function archivelist()

	{

		$data = $this->archivelistPagination();

		$this->load->view('siteowner/archivelist',$data);

	}

	

	function archivelistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

			$config["base_url"] = base_url() . "index.php/siteowner/archivelist";

			$config["total_rows"] = count(Companyadminmodel::GetArchiveList());

			$config['reuse_query_string'] = TRUE;

			$config["per_page"] = 20;

			$config["uri_segment"] = 3;

			$config['full_tag_open'] = '<div class="pagination">';

			$config['full_tag_close'] = '</div><!--pagination-->';

			$config['first_link'] = '&laquo; First';

			$config['first_tag_open'] = '<li class="prev page">';

			$config['first_tag_close'] = '</li>';

			$config['last_link'] = 'Last &raquo;';

			$config['last_tag_open'] = '<li class="next page">';

			$config['last_tag_close'] = '</li>';

			$config['next_link'] = 'Next &rarr;';

			$config['next_tag_open'] = '<li class="next page">';

			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '&larr; Previous';

			$config['prev_tag_open'] = '<li class="prev page">';

			$config['prev_tag_close'] = '</li>';

			$config['cur_tag_open'] = '<li class="active"><a href="">';

			$config['cur_tag_close'] = '</a></li>';

			$config['num_tag_open'] = '<li class="page">';

			$config['num_tag_close'] = '</li>';

	

			$this->pagination->initialize($config);

			$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

		

			$options['limit'] = $config["per_page"];

			$options['offset'] = $page;

		

			$data["results"] = Companyadminmodel::GetArchiveAccountDetails($options);

			$data["links"] = $this->pagination->create_links();

			

			return $data;

	}

	

	

	function addcorporatepackage()

	{

	  $data = array ("id"=>$_REQUEST["id"]);

	  $this->load->view('siteowner/addcorporatepackage', $data);

	}

	

	

	function insertcorporatepackage()

	{

	    date_default_timezone_set('Asia/Calcutta');

		$str_date=date("Y-m-d");

		$str_time =date("H:i:s");

		$companyid = $_REQUEST["companyid"];

		$creditlimit = (isset($_REQUEST["creditlimit"])) ? $_REQUEST["creditlimit"] : 0;

		$idata = array(

		              'storagespace' => $_REQUEST["storage"],

					  'noofdevices' => $_REQUEST["noofdevices"],

					  'price' => $_REQUEST["fee"],

					  'additionaldeviceprice' => $_REQUEST['priceperdevice'],

					  'type'=>'Corporate',

					  'accounttype'=>'corporate',

					  'mobile'=>'free',

					  'forduration'=>'1Month',

					  'creditlimit' => $creditlimit,

					  'createdon'=>$str_date." ".$str_time,

					 'emailsupport' => $_REQUEST["emailsupport"]

		

		);

		$this->load->library('form_validation');		

		$this->form_validation->set_rules('username', 'Name', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required');

		$this->form_validation->set_rules('name', 'Company Name', 'required');

		$this->form_validation->set_rules('address', 'Address', 'required');

		$this->form_validation->set_rules('phone', 'Phone', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('abn', 'ABN', 'required');		

		if ($this->form_validation->run() == FALSE)

		{

			$data = array("errorMsg"=>"true","id"=>$_REQUEST["companyid"]);

			$this->load->view('siteowner/modifycompanyadmin', $data);

		}

		else

		{

		        if($_REQUEST['temp'] == '0' && $_REQUEST["status"] != 'waiting') {

		        $packageid = Packagemodel::addPackage($idata);

		

		        }

		        else {

		        	$packageid = $_REQUEST['packageid'];

		        }

		        if($_REQUEST['temp'] == '1') {

		       

		        $idata['packageid'] = $_REQUEST['packageid'];

		        $packageid = Packagemodel::UpdatePackage($idata);

		        $packageid = $_REQUEST['packageid'];

		        }

		

		        if($packageid > 0) {

		

		        $pkgdetails=$this->packagemodel->getDetails($packageid);

		        //print_r($pkgdetails);

                        $pkgdefaultdevies=$pkgdetails['noofdevices'];

		        $storagespace = $pkgdetails['storagespace'];

		

		        $date1= date("Y-m-d H:i:s");

	                $today = strtotime($date1);

		        $expiry = strtotime('+30 days',$today);

		        $expirydate = date("Y-m-d H:i:s",$expiry);

			

			$email = html_purify($_REQUEST["email"]);

			

		        $userData = array(

				'username' => html_purify($_REQUEST["username"]),

				'email' => html_purify($_REQUEST["email"]),

				'name' => html_purify($_REQUEST["name"]),

				'address' => html_purify($_REQUEST["address"]),

				'phone' => html_purify($_REQUEST["phone"]),

				

				'packageid' => $packageid,

				'expirydate' => $expirydate,

				'balanceamount' => $_REQUEST["balanceamount"],

				'status' => 'approved',

				'type' => $_REQUEST["type"],

				'companyid' => $companyid,

				'acn' => html_purify($_REQUEST["acn"]),

				'abn' => html_purify($_REQUEST["abn"]),

				'noofdevicesallowed' => $pkgdefaultdevies

		);	

		

			Email::corporateApproveEmail($email,$storagespace,$pkgdefaultdevies);

			$affected_rows = Companyadminmodel::UpdateCompany($userData);

			$data = $this->pendingApprovalsPagination();

			$data["errorMsg"] = "Record updated" ;

    	    $this->load->view('siteowner/pendingapprovals',$data);

		}

	  }

	}

	

		

	function updateuser()

	{

	    $data = array ("id"=>$_REQUEST["id"]);

		$this->load->view('siteowner/updateuser', $data);

	}

	

	

	function ModifyUser()

	{

	    $this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Name', 'required');

		$this->form_validation->set_rules('password', 'Password', 'required');

		

		if ($this->form_validation->run() == FALSE)

		{

			$data = array ("id"=>$_REQUEST["adminid"]);

		        $this->load->view('siteowner/updateuser', $data);

		}

		else

		{

		$manager = 0;

		if(isset($_REQUEST['ismanager']))

		{

		$manager = 1;

		}

			$userData = array(

					'username' => html_purify($_REQUEST["username"]),

					'password' => md5(html_purify($_REQUEST["password"])),

					'firstname' => html_purify($_REQUEST["firstname"]),

					'lastname' => html_purify($_REQUEST["lastname"]),

					'email' => html_purify($_REQUEST["email"]),

					'ismanager' => $manager,

					'adminid'=>$_REQUEST["adminid"]

					

			);

			$affected_rows = Siteownermodel::updateUser($userData);

			$data = $this->userlistPagination();

			$data["errorMsg"] ="Record updated" ;

	        $this->load->view('siteowner/userlist',$data);

		}

	

	}

	

	

	function modifycompanyadmin()

	{

		$data = array ("id"=>$_REQUEST["id"]);

		$this->load->view('siteowner/modifycompanyadmin', $data);

		

	}

	

	function updatecompany()

	{

		$companyid = $_REQUEST["companyid"];

		if($companyid == '')

		 {

			$data = $this->companylistPagination();

			$this->load->view('siteowner/companylist',$data);

			

		 }else{

							

			if(Companyadminmodel::userExistsWithUsername($_REQUEST["username"],$companyid) == 0)

			 {

			   if(Companyadminmodel::userExistsWithEmail($_REQUEST["email"],$companyid) == 0 )

				{

					

				

					$userData = array(

								'username' => html_purify($_REQUEST["username"]),

								'email' => html_purify($_REQUEST["email"]),

								//'name' => html_purify($_REQUEST["name"]),

								'name' => $_REQUEST["name"],

								'address' => html_purify($_REQUEST["address"]),

								'phone' => html_purify($_REQUEST["phone"]),

								'city' => html_purify($_REQUEST["city"]),

								'country' => html_purify($_REQUEST["country"]),

								'zipcode' => html_purify($_REQUEST["zipcode"]),

								'firstname' => html_purify($_REQUEST["firstname"]),

								'lastname' => html_purify($_REQUEST["lastname"]),

								//'packageid' => $_REQUEST["packageid"],

								'expirydate' => date ("Y-m-d", strtotime($_REQUEST["expirydate"])),

								'balanceamount' => $_REQUEST["balanceamount"],

								'status' => $_REQUEST["status"],

								'type' => $_REQUEST["type"],

								'abn' => html_purify($_REQUEST["abn"]),

								'acn' => html_purify($_REQUEST["acn"]),

								'companyid' => $companyid,

								'noofdevicesallowed' => $_REQUEST["devices"]

							);	

			

						$this->load->library('form_validation');		

						$this->form_validation->set_rules('username', 'Name', 'required');

						$this->form_validation->set_rules('email', 'Email', 'required');

						$this->form_validation->set_rules('name', 'Company Name', 'required');

						$this->form_validation->set_rules('address', 'Address', 'required');

						$this->form_validation->set_rules('phone', 'Phone', 'required');

						$this->form_validation->set_rules('type', 'Type', 'required');

								

						if ($this->form_validation->run() == FALSE)

						{

							$data = array("errorMsg"=>"true","id"=>$_REQUEST["companyid"]);

							$this->load->view('siteowner/modifycompanyadmin', $data);

						}

						else

						{

						

							  if($_REQUEST['type'] == 'corporate')

							  {

							  $packageid = $_REQUEST["packageid"];

							  $price = $_REQUEST['price'];

							  $Storage = $_REQUEST['Storage'];

							  $sql = "update package set price='{$price}' , storagespace='{$Storage}' where packageid = {$packageid}";

							  $this->db->query($sql);

							  

							  }

							

							$affected_rows = Companyadminmodel::UpdateCompany($userData);

							$data = $this->companylistPagination();

							$data["errorMsg"] ="Record updated" ;

							$this->load->view('siteowner/companylist',$data);

						}

		   

						

						

				}else{

				

					  $data["errorMsg"] = "This email already exists, please use another email address";

					  $data["id"] = $companyid;

					  $this->load->view('siteowner/modifycompanyadmin', $data);

					

					 }

					 

				}else{

				

					  $data["errorMsg"] ="User name already exists, please select another username";

					  $data["id"] = $companyid;

					  $this->load->view('siteowner/modifycompanyadmin', $data);

					

					 }

			  }	

	}

	

	function updatepackage()

	{

		

		$packageid = $_REQUEST["packageid"];

		$userData = array(

				'accounttype' => html_purify($_REQUEST["accounttype"]),

				'price' => html_purify($_REQUEST["price"]),

				'forduration' => html_purify($_REQUEST["forduration"]),

				//'type' => html_purify($_REQUEST["type"]),

				'type' => $_REQUEST["type"],

				'storagespace' => html_purify($_REQUEST["storagespace"]),

				'additionaldeviceprice' => html_purify($_REQUEST["additionaldeviceprice"]),

				'noofdevices' => html_purify($_REQUEST["noofdevices"]),

				'emailsupport' => html_purify($_REQUEST['emailsupport']),

				'packageid' => $packageid

		);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('accounttype', 'Name', 'required');

		$this->form_validation->set_rules('price', 'Fee', 'required');

		$this->form_validation->set_rules('forduration', 'Duration', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('storagespace', 'Storage', 'required');

		$this->form_validation->set_rules('additionaldeviceprice', 'Additional Device', 'required');

		$this->form_validation->set_rules('noofdevices', 'No of Device', 'required');

		

		if ($this->form_validation->run() == FALSE)

		{

			$data = array("errorMsg"=>"true","id"=>$_REQUEST["packageid"]);

			$this->load->view('siteowner/modifypackage', $data);

		}

		else

		{

			$affected_rows = Packagemodel::UpdatePackage($userData);

			if($_REQUEST["accounttype"] == 'business') {

			$data = $this->businessPackagelistPagination();

			}

			else

			{

			$data = $this->corporatePackagelistPagination();

			}

			$data["errorMsg"] ="Record updated" ;

			$this->load->view('siteowner/packagelist',$data);

		}

		

	}

	function addNewPackage()

	{

		$this->load->view('siteowner/addpackage');

	

	}

	

	function addpackage()

	{

		date_default_timezone_set('Asia/Calcutta');

		$date = date("Y-m-d H:i:s");

		$this->load->library('form_validation');

		$this->form_validation->set_rules('accounttype', 'Name', 'required');

		$this->form_validation->set_rules('price', 'Fee', 'required');

		$this->form_validation->set_rules('forduration', 'Duration', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('storagespace', 'Storage', 'required|max_length[6]');

		$this->form_validation->set_rules('additionaldeviceprice', 'Additional Device', 'required');

		$this->form_validation->set_rules('noofdevices', 'No of Device', 'required');

	

		if ($this->form_validation->run() == FALSE)

		{

			$data = array("errorMsg"=>"true");

			$this->load->view('siteowner/addpackage',$data);

		}

		else

		{

			$userData = array(

				'accounttype' => html_purify($_REQUEST["accounttype"]),

				'price' => html_purify($_REQUEST["price"]),

				'forduration' => html_purify($_REQUEST["forduration"]),

				//'type' => html_purify($_REQUEST["type"]),

				'type' => $_REQUEST["type"],

				'storagespace' => html_purify($_REQUEST["storagespace"]),

				'additionaldeviceprice' => html_purify($_REQUEST["additionaldeviceprice"]),

				'noofdevices' => html_purify($_REQUEST["noofdevices"]),

				'mobile' => 'free',

				'emailsupport' => $_REQUEST["emailsupport"],

				'createdon' => $date

				

		);

			$affected_rows =  Packagemodel::addPackage($userData);

			if($_REQUEST["accounttype"] == 'business') {

			$data = $this->businessPackagelistPagination();

			}

			else

			{

			$data = $this->corporatePackagelistPagination();

			}

			$data["errorMsg"] ="Record added" ;

			$this->load->view('siteowner/packagelist',$data);

		}

	

	}

	

	function addNewUser()

	{

	      $this->load->view('siteowner/adduser');

	}

	

	function addUser()

	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Name', 'required');

		

		$password = $this->generateRandomPassword();

		

		if ($this->form_validation->run() == FALSE)

		{

			$data = array("errorMsg"=>"true");

			$this->load->view('siteowner/adduser',$data);

		}

		else

		{

		$manager = 0;

		if(isset($_REQUEST['ismanager']))

		{

		$manager = 1;

		}

			$userData = array(

					'username' => html_purify($_REQUEST["username"]),

					'password' => md5($password),

					'firstname' => html_purify($_REQUEST["firstname"]),

					'lastname' => html_purify($_REQUEST["lastname"]),

					'email' => html_purify($_REQUEST["email"]),

					'ismanager' => $manager

					

			);

			$affected_rows = Siteownermodel::addUser($userData);

			Email::sendPassword_Email($_REQUEST["username"],$_REQUEST["email"],$password,$_REQUEST["firstname"],$_REQUEST["lastname"] );

			$data = $this->userlistPagination();

			$data["errorMsg"] ="New Admin has been successfully added" ;

	                $this->load->view('siteowner/userlist',$data);

			

		}

		

	}

	

	function addNewCompany()

	{

		$this->load->view('siteowner/addcompanyadmin');

	

	}

	

	function addCompany()

	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Name', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required');

		$this->form_validation->set_rules('name', 'Company Name', 'required');

		$this->form_validation->set_rules('address', 'Address', 'required');

		$this->form_validation->set_rules('phone', 'Phone', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('password', 'Password', 'required');

		

		if ($this->form_validation->run() == FALSE)

		{

			$data = array("errorMsg"=>"true");

			$this->load->view('siteowner/addcompanyadmin',$data);

		}

		else

		{

			$userData = array(

					'username' => $_REQUEST["username"],

					'email' => $_REQUEST["email"],

					'name' => $_REQUEST["name"],

					'address' => $_REQUEST["address"],

					'phone' => $_REQUEST["phone"],

					'type' => $_REQUEST["type"],

					'password' => $_REQUEST["password"],

					'packageid' => 1

			);

			$affected_rows = Companyadminmodel::addCompany($userData);

			$data = $this->companylistPagination();

			$data["errorMsg"] ="Record added" ;

			$this->load->view('siteowner/companylist',$data);

		}

		

	}

	

	

	function bannedCompany()

	{

		$affectedRows = Companyadminmodel::bannedCompany($_REQUEST);

		echo $affectedRows;

	}

	

	function approveCompany()

	{

		$affectedRows = Companyadminmodel::approveCompany($_REQUEST);

		echo $affectedRows;

	}

	

	function deletepackage()

	{

		$affectedRows = Packagemodel::deletepackage($_REQUEST);

		echo $affectedRows;

	}

	

	function deleteCompany()

	{

	       $affectedRows = Companyadminmodel::deleteCompany($_REQUEST);

	       echo $affectedRows;

	}

	

	function deleteUser()

	{

	       $affectedRows = Siteownermodel::deleteUser($_REQUEST);

	       echo $affectedRows;

	}

	function deletefeedback()

	{

		$affectedRows = Contactusmodel::deletefeedback($_REQUEST);

		echo $affectedRows;

	}

	function deletearticle()

	{

	        $affectedRows = Contactusmodel::deletearticle($_REQUEST);

		echo $affectedRows;

	}

	function modifypackage()

	{

		$data = array ("id"=>$_REQUEST["id"]);

		$this->load->view('siteowner/modifypackage', $data);

	

	}

	

	function contentmanagement()

	{

		$data = array ("type"=>$_REQUEST["type"],"id"=>"0");

		$this->load->view('siteowner/terms', $data);

	}

	

	function addarticles()

	{

		$this->load->view('siteowner/addarticles');

	}

	function articles()

	{

		$data = array ("type"=>$_REQUEST["type"],"id"=>$_REQUEST["id"]);

		$this->load->view('siteowner/articles', $data);

	}

	

	function updatecontent()

	{

	    $this->load->library('form_validation');

		$this->form_validation->set_rules('terms', 'Text', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('contentid', 'Text', 'required');

		

		if ($this->form_validation->run() == FALSE)

		{

		    $data = $this->articlelistPagination();

			$this->load->view('siteowner/articlelist',$data);

		}

		else{

		$content = $_REQUEST['terms'];

		$type = $_REQUEST['type'];

		$contentid = $_REQUEST['contentid'];
		$cat_id = $_REQUEST['Category'];

		$title = (isset($_REQUEST['doctitle'])) ? html_purify($_REQUEST['doctitle']) : "";

		if($type == 'article')

		{

			$affected_rows = Contentmanagementmodel::UpdateArticle($content,$contentid,$title,$cat_id);

			$data = $this->articlelistPagination();

			$data["errorMsg"] ="Record updated" ;

			$data["type"] =$type;

			$this->load->view('siteowner/articlelist',$data);

		}

		

		else

		{

			$affected_rows = Contentmanagementmodel::UpdateContent($content,$type,$contentid);

			$data["type"] =$type;

			$data["errorMsg"] ="Record updated" ;

			$this->load->view('siteowner/terms',$data);

		}

	  }

	}

	

	

	function addcontent()

	{

	    $this->load->library('form_validation');

		$this->form_validation->set_rules('terms', 'Text', 'required');

		if ($this->form_validation->run() == FALSE)

		{

		       $this->load->view('siteowner/addarticles');

		}

		else{

		$content = html_purify($_REQUEST['terms']);

		$title = html_purify($_REQUEST['doctitle']);
		$cat_id = html_purify($_REQUEST['Category']);

		{

			$affected_rows = Contentmanagementmodel::AddArticle($content, $title, $cat_id);

			$data["type"] ='article';

			$data["errorMsg"] ="Record added" ;

			$data = $this->articlelistPagination();

			$data["errorMsg"] ="Record added" ;

			$this->load->view('siteowner/articlelist',$data);

		 }

	   }

	}

	

	

	

	function logout()

	{

		$this->session->sess_destroy();

		//$this->load->view('siteowner/login');

		redirect('https://cuedrive.com/');

	}

	

	function GetTicketsConversation()

	{

		$companyid = $_REQUEST['companyid'];

		$data = Contactusmodel::GetTicketsConversation($companyid,$type='support');

		echo json_encode($data);

	}

	

	function sendReply()

	{

		date_default_timezone_set('Asia/Calcutta');

		$date = date("Y-m-d H:i:s");

		$companyid = $_REQUEST['companyid'];

		$reply = html_purify($_REQUEST['reply']);

		$iData = array(

				'companyid' => $companyid,

				'comments' => $reply,

				'email' => '-',

				'phone' => '-',

				'type' => 'support',

				'createdon' => $date,

				'sent'=>0

		);

		$affected_rows = Contactusmodel::sendReply($iData);

		$data = $this->ticketsPagination();

		$data["errorMsg"] ="Message has been successfully sent." ;

		$this->load->view('siteowner/tickets',$data);

		

	}

	

	

	function helpdoc()

	{

	        $data = array();

		$this->load->view('siteowner/helpdoc',$data);

	}

	

	function test()

	{

	$this->load->view('siteowner/test');

	}

	

	public function articlelist()

	{

		$data = $this->articlelistPagination();

		$this->load->view('siteowner/articlelist',$data);

	}

	

	function articlelistPagination()

	{

		$options = array();

		$customUrl = "";

		if(isset($_REQUEST['searchtext']))

		{

			$searchtext= $_REQUEST['searchtext'];	

            $searchtext = html_purify($searchtext);

			$options['like'] = $searchtext;

			$data["search"] = $searchtext;

		}

		$config["base_url"] = base_url() . "index.php/siteowner/articlelist";

		$config["total_rows"] = count(Contactusmodel::GetArticleList());

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 1;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

	

		$data["results"] = Contactusmodel::GetArticleListDetails($options);

		$data["links"] = $this->pagination->create_links();

		

		return $data;

	

	}

	

	public function ticketconversation()

	{

		$data = $this->ticketconversationPagination();

		//echo json_encode($data);

		$this->load->view('siteowner/coversation',$data);

	}

	

	public function ticketconversationPagination()

	{

	

		$options = array();

		$customUrl = "";

	

		$config["base_url"] = base_url() . "index.php/siteowner/ticketconversation";

		$config["total_rows"] = count(Contactusmodel::Getticketconversation($_REQUEST['id']));

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

	

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

		$options['ticketid']=$_REQUEST['id'];

		$data["results"] = Contactusmodel::Getticketconversationdetails($options);

		$data["links"] = $this->pagination->create_links();

	

		return $data;

	}

	

	function replyFeedback()

        {

          $emailto = $_REQUEST['email_to'];

          $message = html_purify($_REQUEST['message']);

          $fid = $_REQUEST['fid'];

          

          Email::replyFeedback($emailto,$message,$fid);

          $data = $this->feedbackPagination();

	  $this->load->view('siteowner/feedback',$data);        



        }

        

        public function generateRandomPassword()

	{

		

		$length = 6;

		$pin = "";

		$possible = "012346789ABCDFGHJKLMNPQRTVWXYZ";

		// we refer to the length of $possible a few times, so let's grab it now

		$maxlength = strlen($possible);

		// check for length overflow and truncate if necessary

		if ($length > $maxlength)

		{

			$length = $maxlength;

		}

		// set up a counter for how many characters are in the password so far

		$i = 0;

		// add random characters to $password until $length is reached

		while ($i < $length)

		{

		

			// pick a random character from the possible ones

			$char = substr($possible, mt_rand(0, $maxlength-1), 1);

				

			// have we already used this character in $password?

			if (!strstr($pin, $char)) {

				// no, so it's OK to add it onto the end of whatever we've already got...

				$pin .= $char;

				// ... and increase the counter by one

				$i++;

			}

		

		}

		

		return $pin;

	}

	

    public function resetPassword()

	{

	   	   $email = $_REQUEST['email'];

           $id = Siteownermodel::userIsExists($email);



           if($id > 0) {

           

		   $generatedPassword = $this->generateRandomPassword();

		   $password = md5($generatedPassword);	

		   

           $updateid=Siteownermodel::resetPassword($email,$password);

           

		   if($updateid > 0)

           {

                $username = Siteownermodel::getUsername($email);

                Email::sendUpdatedPassword_Email($username,$email,$generatedPassword);

                echo '1';

           }

	  }

          else

          {

               echo 'User does not exist in CueDrive';

          }

	}

	

	public function expiryCronjobs()

	{

	   Email::accountExpiryCronJobs();

	}

	

	function archiveinvoices($companyid = '')

	{

	  

		 $data = $this->billinghistoryPagination($companyid);		 

		 $this->load->view('siteowner/billinghistory',$data);

		 

	  

	}

	

	public function billinghistoryPagination($companyid = '')

	{

		$options = array();

		$customUrl = "";

		$config["base_url"] = base_url() . "index.php/siteowner/archiveinvoices";

		$config["total_rows"] = count(Transactionmodel::GetBillingForSiteowner($companyid));

		$config['reuse_query_string'] = TRUE;

		$config["per_page"] = 20;

		$config["uri_segment"] = 3;

		$config['full_tag_open'] = '<div class="pagination">';

		$config['full_tag_close'] = '</div><!--pagination-->';

		$config['first_link'] = '&laquo; First';

		$config['first_tag_open'] = '<li class="prev page">';

		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';

		$config['last_tag_open'] = '<li class="next page">';

		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';

		$config['next_tag_open'] = '<li class="next page">';

		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';

		$config['prev_tag_open'] = '<li class="prev page">';

		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';

		$config['num_tag_close'] = '</li>';

		

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

		

		$options['limit'] = $config["per_page"];

		$options['offset'] = $page;

		$options['companyid']=$companyid;

		$data["results"] = Transactionmodel::GetBillingDetailsForSiteowner($options);

		$data["links"] = $this->pagination->create_links();

		//$data["creditamount"] = creditamount($this->session->userdata("companyid"));

		return $data;

	}

	

	function deactivateaccount($companyid = '')	{

		if($this->input->post())	{

			

			$this->load->library('form_validation');

			$this->load->model('openfiremodel');

			$this->load->model('folderusersmodel');

			$this->load->model('folderdevicesmodel');

			$this->load->model('companydevicemodel');

			$this->load->model('companyusermodel');

			

			$this->form_validation->set_rules('emailcheck', 'Email', 'required|valid_email');

			

				if ($this->form_validation->run() == FALSE)	{

					$this->load->view('siteowner/enteremailaddress', array('message'=>'Please enter valid email address.'));

				}	else	{

					

					$companyid = $this->input->post('companyid');

					$flag = '';

					$flagset = false;

					

					// Delete Device

					$deviceDetails = $this->companydevicemodel->getDevicesByCompany($companyid);						

					

					if(count($deviceDetails) > 0)	{

						foreach($deviceDetails as $detail):

							

							$deviceid = $detail['deviceid'];

							$deviceuuid = $detail['deviceuuid'];

							

							$url = base_url()."team/chat/?type=delete&username=".$deviceuuid;

							

							Openfiremodel::sendDataToServer($url);					

							

							Companydevicemodel::updateAllUpdatedbydevice($deviceid);

							

							//delete from folderalloweddevices

							$this->folderdevicesmodel->deleteByDevice($deviceid);

							

							$affectedrows = Companydevicemodel::deleteDeviceFromSiteOwner($deviceid, $companyid);

							

						endforeach;

						$flag .= '<center><font style="font-family: arial; font-size: 14px;">All devices associated with company admin are removed.</font></center>';

						$flag .= '<br/>';

						$flagset = true;

					}

					

					// Delete User								

					$users = Companyadminmodel::getcompanuuserdetailsfromcomapnyadmin($companyid);

					

					if(count($users) > 0 && is_array($users))	{

						foreach($users as $user):

							

							$username = $user['username'];

							$userid = $user['companyuserid'];



							//$userdetails = Companyusermodel::getUserDetails($userid);						

							$url = base_url()."team/chat/?type=delete&username=".urlencode($username);

							Openfiremodel::sendDataToServer($url);

							

							Companyusermodel::updateAllUpdatedbyuser($userid);

							//delete from folderallowedusers

							$this->folderusersmodel->deleteByUser($userid);

							$affectedrows = Companyusermodel::deleteUserFromSiteOwnerUser($userid, $companyid);								

							

						endforeach;

						$flag .= '<center><font style="font-family: arial; font-size: 14px;">All users associated with company admin are removed.</font></center>';

						$flag .= '<br/>';

						$flagset = true;

					} 

					

					if(!$flagset)	{

						

						Companyadminmodel::cancelAccount($companyid);

						echo '<br/><center><font style="font-family: arial; font-size: 14px;">The account is already deactivated.</font><br/><br/><br/><input onclick="window.parent.location.href=\''.base_url("index.php/siteowner/companylist/").'\'" style="background-color: #3c8dbc; border-color: #367fa9; padding: 6px 12px;color: #ffffff;border-radius: 4px;cursor: pointer; line-height: 1.42857; float: right;" type="button" id="submit" name="submit" value="Close" /></center>';	

					}

										

					if($flagset) {

						

						$this->db->set('isactive' , 'no');

						$this->db->where('companyid', $companyid);

						$this->db->update('companyadmin');

						

						$emailcheck = $this->input->post('emailcheck');						

						$userdata = array('company_id' => $companyid, 'email_adddress' => $emailcheck);

						$this->db->insert('userdeactivate', $userdata);

						

						echo $flag .= '<input onclick="parent.$.magnificPopup.close(); window.location.reload();" style="background-color: #3c8dbc; border-color: #367fa9; padding: 6px 12px;color: #ffffff;border-radius: 4px;cursor: pointer; line-height: 1.42857; float: right;" type="button" id="submit" name="submit" value="Close" />';	

					}

					

				}						

			//

		} else	{



			$data = array('companyid'=> $companyid, 'message' => '');

			$this->load->view('siteowner/enteremailaddress', $data);

		}		

	}

	

	function deletebucketdata($companyid = '', $flag = '')	{

		

		if($this->input->post()) {

			

			$this->load->library('s3');

			$this->load->model('folderfilemodel');

			$this->load->model('permissionsmodel');

			

			if($this->input->post('companyid') == '')	{

				$companyid = '';

			}	else	{

				$companyid = $this->input->post('companyid');

			}	

			

			if($companyid != '')	{

				

				if(Companyadminmodel::verifycompanyexistance($companyid)) {				

					

					$bucket=ROOTBUCKET;					

					

					$fileids = $this->folderfilemodel->getFilesByCompany($companyid);

					if(count($fileids) > 0)	{

						echo '<strong style="font-family:Source Sans Pro,sans-serif">Please download following files and click on "delete bucket" button to delete the data from s3 storage.</strong>';

					}	else	{

						Companyadminmodel::updatebucketdataflag($companyid, '1');

						

						echo '<br/><center><p style="font-family: arial; text-align: center; padding: 20px; font-size: 14px;">All the data is deleted.</p><input onclick="parent.$.magnificPopup.close(); 

						window.location.href=\''.base_url("index.php/siteowner/archiveaccounts").'\'" style="background-color: #3c8dbc; border-color: #367fa9; padding: 6px 12px;color: #ffffff;border-radius: 4px;cursor: pointer; line-height: 1.42857; float: right;" type="button" id="submit" name="submit" value="Close" /></center>';	

					}

						$flag = false;

					

						foreach($fileids as $fileid) :

						

							$selectedfileid = $fileid['id'];

						

							if(Permissionsmodel::checkValidFile($selectedfileid,$companyid) == 1) {

							

							$companyfoldername=COMPANYFOLDERNAME.$companyid;

							$filepath=$this->folderfilemodel->getFilePath($selectedfileid);

							

							//get authenticated url	

							$newurl=$this->s3->getAuthenticatedURL($bucket, $companyfoldername.'/'.$filepath, DOWNLOADURLEXPIRYTIME);

							$newurl=str_replace('amp;', '', $newurl);

						

								$file_headers = @get_headers($newurl);

								

								if($file_headers[0] == 'HTTP/1.1 404 Not Found') {

										return false;

								}	else {

										echo '<ul>';

										echo '<a target="_blank" href="'.$newurl.'" ><li style="font-family:arial; font-size:12px;">'.$fileid["filename"].'</li></a>';

										echo '</ul>';

										$flag = true;

								}

							}

							

						endforeach;

					

						if($flag)	{

						echo '<br/>';

							$form .= '';

							$form .= '<form method="post" action="'.base_url("index.php/siteowner/removebucketdata/".$companyid).'">';

							$form .= '<input type="hidden" id="companyid" name="companyid" value="'.$companyid.'" />';	

							$form .= '<input style="background-color: #3c8dbc; border-color: #367fa9; padding: 6px 12px;color: #ffffff;border-radius: 4px;cursor: pointer; line-height: 1.42857; float: right;" type="submit" id="submit" name="submit" value="Delete Bucket" />';

							$form .= '</form>';

							echo $form;

						}

						

				}

			}

		} else {

			$data['companyid'] = $companyid;

			$this->load->view('siteowner/deletes3data', $data);

		}

		

	}

	

	function invoicepayment($token = '', $amount = '', $invref = '', $id = '')	{

		if($this->input->post())	{

			if($this->input->post('token') != 0 || $this->input->post('token') != '')	{

				

				$data['customerid'] = $this->input->post('token');

				$data['amount'] = $this->input->post('amount')*100;

				$data['invref'] = $this->input->post('invref');

				$id = $this->input->post('id');

				require 'recurring/CreateProcessPayment.php';

				$token = new CreateProcessPayment();

				$data['tokenCustomerDetails'] = $token->create_payment_eway($data);

			

				$userData = array('ispaid' => 1);			

				$this->db->where('id', $id);

				$this->db->update('transaction', $userData);			

				$this->load->view('CompanyAdmin/cardupdatesuccess', $data);

				

			}	else	{

				

				$this->load->view('siteowner/carderror');

				

			}	

			

		} else {

			$data['token'] = $token;	

			$data['amount'] = $amount;

			$data['invref'] = $invref;

			$data['id'] = $id;

			if($token != 0)	{

				$this->load->view('siteowner/paymentconfirm', $data);	

			}	else	{

				$this->load->view('siteowner/carderror');	

			}

			

		}

	}

	

	function removebucketdata()	{

		

			

		if($this->input->post()) {

			

			$this->load->library('s3');

			$this->load->model('folderfilemodel');

			$this->load->model('permissionsmodel');

			$this->load->model('classpush');

			

			if($this->input->post('companyid') == '')	{

				$companyid = '';

			}	else	{

				$companyid = $this->input->post('companyid');

			}	

			

			if($companyid != '')	{

				

				if(Companyadminmodel::verifycompanyexistance($companyid)) {				

					

					$bucket=ROOTBUCKET;					

					

					$fileids = $this->folderfilemodel->getFilesByCompany($companyid);

					

					

					foreach($fileids as $fileid) :

					

						$selectedfileid = $fileid['id'];

						if(Permissionsmodel::checkValidFile($selectedfileid,$companyid) == 1) {

							

							$companyfoldername=COMPANYFOLDERNAME.$companyid;

							$filepath=$this->folderfilemodel->getFilePath($selectedfileid);

							

							//delete the file

							

							if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))

							{

								//update the database

								$this->folderfilemodel->deleteFile($selectedfileid);

								Companyadminmodel::updatebucketdataflag($companyid, '1');

								

								//Classpush::sendFileUploadNotification($fileid['folderid'],'delete',$fileid['fileName']);

								//$this->response(array('success' => DELETESUCCESS), 200);									 

							}

							else {

								  $this->response(array('error' => DELETEFAIL), 400);

							}	

						}	

					endforeach;

					//$this->s3->deleteBucket($bucket);

				}

				$this->load->view('siteowner/loadwindow');

			}

		} else {	

			$data['companyid'] = $companyid;			

			$this->load->view('siteowner/deletes3data', $data);

		}

		

		

	}

	function orderarticallist()
		{
			 $id = $_REQUEST['orders'];
			 //@file_put_contents('test_log.txt',$id);	
			 Saveorderofhelparticles::setOrder();
			// @file_put_contents('test_log_vishwa.txt',$retunrValue);
			
		}

}