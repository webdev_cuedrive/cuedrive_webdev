<?php
require(APPPATH.'/libraries/REST_Controller.php');  
class Api extends REST_Controller {

	/*
	 * purpose:  device login - which will generate new sessiontoken
	 */
function deviceLogin_get() {
        //$result = mail("hussulinux@gmail.com", "Device Login", $retresult );

	//load model
	$this->load->model('companydevicemodel');	
	//request parameters
	$deviceuuid=$this->get('deviceuuid');
        //$deviceuuid = $_REQUEST['deviceuuid'];
	
	// check expiry
        
        {
	   $retresult= $this->companydevicemodel->deviceLogin($deviceuuid);
	//$result = mail("hussulinux@gmail.com", "Subject  $deviceuuid", $retresult );
	
	if($retresult=="notfound")
        {

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "deviceLogin (notfound) called at -".date('Y-m-d H:i')." - from deviceuuid :".$deviceuuid."\n";
                 file_put_contents($file, $current);
        	     $this->response(array('error' => APIDEVNTFND), 400);         
        }
       else if($retresult=="notactive")
        {

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "deviceLogin (notactive) called at -".date('Y-m-d H:i')." - from deviceuuid :".$deviceuuid."\n";
                 file_put_contents($file, $current);

        	     $this->response(array('error' => APIDEVDSBL), 404);   
        }
   		else //authenticated
        {

                // fetch openfire credentials for chat login




                 if(Companydevicemodel::checkDeviceExpiryWithDeviceUUID($deviceuuid))	
                 {
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "deviceLogin (checkDeviceExpiryWithDeviceUUID) called at -".date('Y-m-d H:i')." - from deviceuuid :".$deviceuuid."\n";
                 file_put_contents($file, $current);
   	             $this->response(array('error' => APIDEVDSBL), 404);
                 }

                 else {

                
                $service_url = ORG_URL."chat/?type=fetch&username=".$deviceuuid;
                $curl = curl_init($service_url);
	            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	           $curl_response = curl_exec($curl);
	           if ($curl_response === false) {
		       $info = curl_getinfo($curl);
		       curl_close($curl);
		       die('error occured during curl exec. Additioanl info: ' . var_export($info));
	           }
	           curl_close($curl);
               $response = json_decode($curl_response);
               $jidpassword = $response[0]->{'plainPassword'};
               $jid = $response[0]->{'username'};

				$deviceid=$retresult['deviceid'];
				//update sessiontoken        		
				$sessiontoken= md5(microtime().rand());
				$datacc=array();
				$datacc['sessiontoken']=$sessiontoken;
				$datacc['lastloggedon']=date('Y-m-d H:i');
				$this->companydevicemodel->updateCompanyDevice($datacc,$deviceid);
				$ouputarr=array();
				$ouputarr['deviceid']=$deviceid;
				$ouputarr['sessiontoken']=$sessiontoken;    
				$ouputarr['jid']=$jid."@cuedrive.com.au";    
				//$ouputarr['jidpassword']= 'cuedrive123#$';    
				$ouputarr['jidpassword']= $jidpassword;    
				$file = 'apierrorlog.txt';
				$current = file_get_contents($file);
				$current .= "deviceLogin (success) called at -".date('Y-m-d H:i')." - from deviceuuid :".$deviceuuid."\n";
				file_put_contents($file, $current);
				$this->response($ouputarr, 200);

           } // 200 being the HTTP response code
        }
      
	}
	}
	/*
	 * purpose: get file list for the device and/or user
	 */




function filesList_get(){
	
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');

	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
		
      
	$userauth=false;
	$adminauth=false;
	
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "filesList (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {

	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
			else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$companyid = $this->companyusermodel->fetchAdminID($username,md5($password));
				$foldersforadmin=$this->folderusersmodel->getFoldersForAdmin($companyid);//get folders for admin
			}
			else {//user authenticated
				$userauth=true;
				$companyuserid=$usrauthresult['companyuserid'];
				$foldersforuser=$this->folderusersmodel->getFoldersForUser($companyuserid);//get folders for user
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				//get public folder
				$companyid=$devauthresult['companyid'];
				$publicfolder=$this->foldermodel->getPublicFolder($companyid); //assumed each company will have one public folder
				if($userauth)//user provided and authenticated .. show user  folders
				{

					$folders=$foldersforuser;
				}
				else if($adminauth)
				{			

					$folders=$foldersforadmin;
				}
				else { //get folders for device

					$folders=$this->folderdevicesmodel->getFoldersForDevice($this->get('deviceid')); 	
				}
				
				/*echo '<pre>';
				print_r($this->db->last_query());
				echo '</pre>';*/
				
				$outputjson=array();
			 	if($folders)
			        {
			        	$folders=array_merge($folders,$publicfolder);
			 	        $folders = array_unique_multidimensional($folders);//remvove duplicate folderids
			          
			           	$foldertreearr=array();
			           	$outputjson['deviceID']= $deviceid;
			           	for($i=0;$i<count($folders);$i++)
						{
							$folderid=$folders[$i]['folderid'];
							
							/*permission set per folder*/
							$activeMode = 0;
							$foldertreearr[$i]['active_mode']	=	$activeMode;
							$permissions=$this->folderdevicesmodel->getPermissions($folderid,$deviceid);
							$countPermission = count($permissions);
							if($countPermission > 0)	{
								foreach($permissions as $perm):				
									if($perm['permissionid'] == 7)	{
										$foldertreearr[$i]['active_mode']	=	1;
										$activeMode = 1;
									}	
								endforeach;
							}	else	{
								$foldertreearr[$i]['active_mode']	=	0;
								$activeMode = 0;
							}
							/*permission set per folder*/
		
							$folderdetails=$this->foldermodel->getFolderDetails($folderid);
							$parentid=$folderdetails['parentfolderid'];
							if($parentid==null)
								$parentid=0;
							$foldertreearr[$i]['id']=$folderid;
							$foldertreearr[$i]['name']=$folderdetails['foldername'];
							
							$foldertreearr[$i]['folderpath']=$folderdetails['fullpath'];
							$foldertreearr[$i]['createdon']=date('d/m/Y g:i a',strtotime($folderdetails['createdon']));
							$foldertreearr[$i]['updatedon']=date('d/m/Y g:i a',strtotime($folderdetails['updatedon']));
							$foldertreearr[$i]['formattedupdatedon']=date('d M Y',strtotime($folderdetails['updatedon']));
							$foldertreearr[$i]['parentfolderid']=$parentid;
							
							if(in_array_r($folderid, $publicfolder))
							{
								
								//$permissions=array();
								//$permissions['name']="All";
								$permissions=$this->permissionsmodel->getAllPermissions();
							}
							else
							{
								if($userauth)//user provided and authenticated.. so user level permission
								{
									$permissions=$this->folderusersmodel->getPermissions($folderid,$companyuserid);
								}
								else if($adminauth)
								{
									$permissions=$this->permissionsmodel->getAllPermissions();
								}
					        	else { // device level permission
					        		$permissions=$this->folderdevicesmodel->getPermissions($folderid,$deviceid);
					        	}		
							}
				        	$foldertreearr[$i]['permissions']=$permissions;	
							
				        	$outputfiles=array();
							$files= $this->folderfilemodel->getFiles($folderid);
							for($j=0;$j<count($files);$j++)
							{
								$outputfiles[$j]['fileID']=$files[$j]['id'];
								$outputfiles[$j]['name']=$files[$j]['filename'];
								$outputfiles[$j]['active_mode']=$activeMode;
								$outputfiles[$j]['is_downloaded']=$files[$j]['active_mode'];
								$outputfiles[$j]['filePath']=$files[$j]['fullpath'];
								$outputfiles[$j]['fileSize']=$files[$j]['filesize']."bytes";
								$outputfiles[$j]['createdon']=date('d/m/Y g:i a',strtotime($files[$j]['createdon']));
								$outputfiles[$j]['updatedon']=date('d/m/Y g:i a',strtotime($files[$j]['updatedon']));
								$outputfiles[$j]['formattedupdatedon']=date('d M Y',strtotime($files[$j]['updatedon']));
							}
							$foldertreearr[$i]['files']=$outputfiles;
						}
						//print_r($foldertreearr);
			          $foldertree= $this->buildFolderTree($foldertreearr);
			        $foldertree = $this->remove_json_keys($foldertree);	
			        	$outputjson['folder']=$foldertree;
			            $this->response($outputjson, 200); // 200 being the HTTP response code
			        }
			
			        else
			        {
			            $this->response(array('error' => APINOFILE), 404);
			        }
		}
	
	} //end of if device authenticated
	}
	}
	
	
	
/**
	*	Update downloaded file
	*/
	function updateDownloadedFile_get(){
	
	error_reporting('E_ALL');
	
//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$folderid=$this->get('folderid');
	$sessiontoken=$this->get('sessiontoken');
	$fileid=$this->get('fileID');
	
	
	//optional request parameters
	 $username=($this->get('username'))?$this->get('username'):"";
	 $password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;

	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "updateDownloadedFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
	
				$createdon=date('Y-m-d H:i:s');
				   		 $updatedon=date('d/m/Y g:i a',strtotime($createdon));	
		$dataFlag = array('active_mode' => '1')	;
		$this->folderfilemodel->updateFile( $dataFlag,  $fileid );		
			
		/*$permissions=$this->folderdevicesmodel->getPermissions($folderid,$deviceid);

		 $countPermission = count($permissions);
		*/
		/*if($countPermission > 0)	{
			
			foreach($permissions as $perm):				
				if($perm['permissionid'] == 7)	{
					$this->response(array('success' => 'Auto Download is on.','deviceid' => $deviceid), 200);
				}	
			endforeach;
		}	else	{
			$this->response(array('error' => 'Auto Download is off.','deviceid' => $deviceid), 200);
		}*/
			/*$this->response(array('error' => 'Auto Download is off.','deviceid' => $deviceid), 200);*/
		}
	}
	}
	}
	
	
	/**
	*	Get all Notification
	*/
	function getAllNotifications_get(){
	
	error_reporting('E_ALL');
	
//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$devicetoken=$this->get('devicetoken');
	$folderid=$this->get('folderid');
	$sessiontoken=$this->get('sessiontoken');
	$fileid=$this->get('fileID');
	
	
	//optional request parameters
	 $username=($this->get('username'))?$this->get('username'):"";
	 $password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;

	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "updateDownloadedFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
	
				/**Enter Code Here**/
				$result = $this->folderdevicesmodel->getAllNotifications($devicetoken);
				if(!$result)	{
					$this->response(array('error' => 'There are no notifications','deviceid' => $deviceid), 200);	
				}	else	{
					$this->response(array('success' => 'Success', 'notification' =>  $result,'deviceid' => $deviceid), 200);	
				}	
				/**Enter Code Here**/
		}
	}
	}
	}
	
	
		/**
	*	Get all Notification
	*/
	function updateNotifications_get(){
	
	error_reporting('E_ALL');
	
//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$devicetoken=$this->get('devicetoken');
	$notificationId=$this->get('notification_id');
	$sessiontoken=$this->get('sessiontoken');
	$notificationFlag=$this->get('read_status');
	
	
	//optional request parameters
	 $username=($this->get('username'))?$this->get('username'):"";
	 $password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;

	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "updateDownloadedFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
	
				/**Enter Code Here**/
				$data = array('notification_read_flag' => 1);
				$this->folderdevicesmodel->updateTheNotificationStatus($notificationId, $data);
				$this->response(array('success' => 'Success', 'notification' =>  'Status is updated','deviceid' => $deviceid), 200);		
				/**Enter Code Here**/
		}
	}
	}
	}
	
	
	/*
	 * purpose: Upload the file to amazon
	 */
	function uploadFile_post()
	{
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
        $this->load->model('classpush');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->post('deviceid');
	$sessiontoken=$this->post('sessiontoken');
	$uploadfolderid=$this->post('uploadfolderid');
	
	$fileName = $_FILES['uploadedfile']['name'];
    $fileTempName = $_FILES['uploadedfile']['tmp_name'];
	
    
	//optional request parameters
	$username=($this->post('username'))?$this->post('username'):"";
	$password=($this->post('password'))?$this->post('password'):"";
	$userauth=false;
	$adminauth=false;
	$permission=false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                  $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "uploadFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);

		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated


                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($uploadfolderid,$deviceid);
		if($destinationPermission == 1)
		{
			$permission = true;
		}



		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
				$userauth=true;

                                $companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			        $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($uploadfolderid,$companyuserid);
		       
		               if($destinationPermission == 1)
		              {
			          $permission = true;
		              }
		              else 
		              {
		        	  $permission = false;
		              }




				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			if($permission) {
			$companyid=$devauthresult['companyid'];
			//$bucketname="cuedrive.".$companyid;
			$bucketname=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			//check bucket size validation
			//$bucketsize=$this->getBucketSize($bucketname)+$_FILES["uploadedfile"]["size"]; //in bytes
			$bucketsize=$this->getCompanyFolderSize($companyid);//in bytes
			$companypackagesize=$this->Companyadminmodel->getPackageSize($companyid);//in GB
			$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
			//$companypackagesize=1024*(str_replace("KB","",$companypackagesize)); //in bytes for testing
			if($bucketsize<=$companypackagesize)
			{
				$replaceflag=false;
				if($this->isFileExist($fileName, $uploadfolderid) )
				{
					if(($this->post('override')) )
					{
						if($this->post('override')=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=false;
						}
					}
					else {
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
				
				if($uploadflag)
				{
			
				$uploadfolderpath=$this->foldermodel->getFolderPath($uploadfolderid);
				$foldername=$uploadfolderpath;
				if($this->s3->putObjectFile($fileTempName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
				{
					//file uploaded successfullly
					//update DB
					
				    $datacc=array();
				    $datacc['filename']=$fileName;
				    $datacc['fullpath']=$foldername.'/' .$fileName;
				    $datacc['folderid']=$uploadfolderid;
				    $datacc['filesize']=$_FILES["uploadedfile"]["size"];
				    
				    
				    
				    $activitydetails = 'uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
				    if($username!="")
				    {
				     $activitydetails = $username.' '.'has uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
				    }
				    
				    
				    
				    
				    $logcc['deviceid'] = $deviceid;
                                    $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				    $logcc['action'] = 'upload';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']  = '-';
				    $logcc['from']  = '-';
					   
					   
					 
				    
				    $newfileid = 0;
				    
				    
				    
				  
				    if($replaceflag)//update existing
				    {	
				    	 $datacc['updatedbydevice']=$deviceid;
					     if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['updatedbyuser']=$companyuserid;
					    }	

                                        		// $createdon=date('Y-m-d H:i:s');
				   		// $updatedon=date('d/m/Y g:i a',strtotime($createdon));	
				   		// $datacc['createdon'] = $createdon;
  
				    	$replacefileid=$this->folderfilemodel->getfileID($fileName,$uploadfolderid);
                                        	 	     
				    	$this->folderfilemodel->updateFile($datacc,$replacefileid);	

                                        $updatedon=date('d/m/Y g:i a',strtotime($this->folderfilemodel->getFileUpdatedTimeStamp($replacefileid)));
                                        $newfileid=$replacefileid;
				    }
				    else //add new
				    {
				    	$datacc['createdbydevice']=$deviceid;
					    	  if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['createdbyuser']=$companyuserid;
					    	 $logcc['companyuserid'] = $companyuserid;
					    }				   
				   		 $createdon=date('Y-m-d H:i:s');
				   		 $updatedon=date('d/m/Y g:i a',strtotime($createdon));	
				   		 $datacc['createdon'] = $createdon;
				    	$newfileid=$this->folderfilemodel->insertFile($datacc);	
				    }
                                          Classpush::sendFileUploadNotification($uploadfolderid,'upload',$fileName,$deviceid);
				    	  $this->auditlogmodel->updateAuditLog($logcc);	
                                          	    				
					$this->response(array('success' => APIFILEUPLOADSUCCESS,'fileID' => $newfileid,'updatedon' => $updatedon), 200);
	
				}	
				else {
					$this->response(array('error' => APIFILEUPLOADFAIL), 400);
				}	
			}//duplicate file validation
			else {
				$this->response(array('error' => APIFILEEXIST), 400);
			}
		}//end of if size validation
		else {
			//$this->response(APIFILESIZEEXCEED,400);
			$this->response(array('error' => APIFILESIZEEXCEED), 400);
		}
		}
		else
		{
		
		$this->response(array('error' => 'Permission denied'), 400);
		
		
		}
		}
	}
	}
	}




        function registerCompanyWithDevice_post() {
		
    // load needed models

		$this->load->library('s3');//amazon lib
		$this->load->helper('url');
		$this->load->model('companyadminmodel');
		$this->load->model('packagemodel');
		$this->load->model('foldermodel');
		$this->load->model('folderfilemodel');
		$this->load->model('folderdevicesmodel');
		$this->load->model('folderusersmodel');	
		$this->load->model('companydevicemodel');
		$this->load->model('companyusermodel');	
		$this->load->model('permissionsmodel');
		$this->load->model('contactusmodel');
		$this->load->model('contentmanagementmodel');
        $this->load->model('transactionmodel');
        $this->load->model('email');
        $this->load->model('auditlogmodel');
        $this->load->model('openfiremodel');
        $this->load->model('classpush');
        $this->load->model('alldevicesmodel');
        $this->load->helper('htmlpurifier');
        $this->load->library('encrypt');
	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
		
		
	
		$id=0;
		$datacc = array(); 
		$ouput = array();
		$device_uuid=trim($this->post('device_uuid'));
		$devicename=html_purify(trim($this->post('device_name')));
		
        $name = html_purify($this->post('company_name'));
		$datacc['name']= $this->post('company_name');
		$datacc['email']= html_purify($this->post('email'));
		$datacc['address']= html_purify($this->post('address'));
		
		$datacc['firstname']= html_purify($this->post('firstname'));
		$datacc['lastname']= html_purify($this->post('lastname'));
		$datacc['city']= html_purify($this->post('city'));
		$datacc['zipcode']= html_purify($this->post('zipcode'));
		$datacc['country']= html_purify($this->post('country'));
		$datacc['phone']= html_purify($this->post('phone'));
		$username= html_purify(strtolower($this->post('username')));
		$datacc['username']=$username;
		$datacc['password']= md5(html_purify($this->post('password')));
		$datacc['passwordhint']= 123;
		$datacc['isactive']='yes';
		$datacc['type']='business';
		$datacc['type'] = 'business';
		$createdon = date('Y-m-d H:i:s');
        $datacc['createdon']=$createdon;
		$today = strtotime($createdon);
		$expiry = strtotime('+30 days',$today);
		$expirydate= date("Y-m-d",$expiry);
		$datacc['expirydate']=$expirydate;
		$datacc['status']='approved';
		$extradevices = 0;
		$packageid=1;
		$datacc['packageid']=$packageid;
		$pkgdetails=$this->packagemodel->getDetails($packageid);
		$pkgdefaultdevies=$pkgdetails['noofdevices'];
		$datacc['noofdevicesallowed']=$extradevices+$pkgdefaultdevies;
		$email = html_purify($this->post('email'));
		//$password =  html_purify($this->post('password'));
		
		$usernameExist = Companyadminmodel::checkUsernameIsExist($username);
		$emailExist = Companyadminmodel::checkEmailIsExist($email);
	       // $deviceNameExists = Companydevicemodel::deviceNameExists($devicename);
	          $deviceNameExists = 0;
        if(Alldevicesmodel::serialIDExists($device_uuid) > 0) {	
	
		if($packageid == 1 && $usernameExist == 0 && $emailExist == 0 && $deviceNameExists == 0) 
		{
		 
		 $companyid=$this->companyadminmodel->updateCompanyAdmin($datacc,$id);
		 $user_password = Openfiremodel::generateRandomToken();
		 $url = ORG_URL."chat/?type=add&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($name)."&group=".urlencode($companyid);
	     Openfiremodel::sendDataToServer($url);
	
		
		/** added by Sunil add account number */
		
	     $result = '';
		 for($i = 0; $i < 10; $i++) 
		 {
		   $result .= mt_rand(0, 9);
		 }
		 $accountNumber = '000'.$result;
		 $dataComapnyName['account_number']= $accountNumber;
		 $this->companyadminmodel->updateCompanyAdmin($dataComapnyName, $companyid);
		
		/** end */
		
		$bucketname=ROOTBUCKET;
     	/**  we can not create file directly.. so creating empty file with required folder.. so amazon will create folder automatically */
			
		    $uploaddir = realpath('./uploads/');
			$dummyfile = $uploaddir ."//".'dummyfile.txt';
			//create company folder on amazon			
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			//create public folder for company		
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created							
			}			
			
			$publicfoldername="shared";			
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$publicfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$publicfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$publicfoldername;
				$datacc['type']='public'; 
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');				
				$this->foldermodel->insertFolder($datacc);				
			}
			
		//create user folder for company			
			$publicfoldername="user";
			$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$publicfoldername.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$publicfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$publicfoldername;
				$datacc['type']='private'; 
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');				
				$this->foldermodel->insertFolder($datacc);
				
				$updatedata['name']=$devicename;
				$updatedata['companyid']=$companyid;
				$updatedata['deviceuuid']=$device_uuid;
				$deviceid=$this->companydevicemodel->updateCompanyDevice($updatedata,$deviceid);

                //$ouput = Companydevicemodel::registerDevice($devicename,$device_uuid,$companyid);	
				
				$this->setAllPermissionsToAllFolders($deviceid, $companyid);
			}
		//}
				
			Email::welcome_Email($username,$email,$password,$createdon,'Free','2 GB','2');
			
			$ouputarr = array();
			//$ouput = Companydevicemodel::registerDevice($devicename,$device_uuid,$companyid);
			
			//$deviceid=$ouput['deviceid'];
        	//update sessiontoken        		
        	$sessiontoken= md5(microtime().rand());
        	$datacc1=array();
        	$datacc1['sessiontoken']=$sessiontoken;
        	$datacc1['lastloggedon']=date('Y-m-d H:i');
        	//$this->companydevicemodel->updateCompanyDevice($datacc1,$deviceid);
        	
        	$ouputarr['deviceid']=$deviceid;
        	//$ouputarr['sessiontoken']=$sessiontoken;    

            //$ouputarr['jid']=$ouput['jid']."@cuedrive.com.au";    
           // $ouputarr['jidpassword']=$ouput['jidpassword']; 
           // $ouputarr['error']=$ouput['error'];      
          
        	$this->response($ouputarr, 200);
	
		}
		else {
			
			if($usernameExist == 0 && $emailExist == 1 && $deviceNameExists == 0)
			{
               $this->response(array('error' => 'Email already exists.'), 400);
			   
			} 
			else if($usernameExist == 1 && $emailExist == 0 && $deviceNameExists == 0)
			{
              $this->response(array('error' => 'Username already exists.'), 400);
			}
                        else if($usernameExist == 1 && $emailExist == 1 && $deviceNameExists == 0)
			{
              $this->response(array('error' => 'Username and Email already exists.'), 400);
			}
			else 
			{
                   $this->response(array('error' => 'Something went wrong.'), 400);
            }
		}
	}
	else
	{
	   $this->response(array('error' => 'Device is not authenticated!'), 400);
	}
  }


function setAllPermissionsToAllFolders($deviceid, $companyid)
	{
		$permissions=$this->permissionsmodel->getAllPermissions();
		$folders=$this->foldermodel->getAllFolders($companyid);
		for($i=0;$i<count($folders);$i++)
		{
			$folderid=$folders[$i]['folderid'];	
			for($j=0;$j<count($permissions);$j++)
					{
						 $datacc = array();
						 $permissionid=$permissions[$j]['permissionid'];
						 if($permissionid!=6)
						 {
							 $datacc['deviceid']=$deviceid;
							 $datacc['folderid']=$folderid;
							 $datacc['permissionid']=$permissionid;
							 $datacc['createdby']=$companyid;
							 $datacc['updatedby']=$companyid;
							 $datacc['createdon']=date('Y-m-d H:i');	
							 $this->folderdevicesmodel->insertFolderDevice($datacc);
						} 	 
					}
		}
	}

	/*
	 * Check if same file exist in specified folder
	 */
function isFileExist($newfilename,$parentfolderid)
{
	 if($this->folderfilemodel->fileNameExist($newfilename,$parentfolderid))
	 {
	 	return true;
	 }
	 else {
	 	return false;
	 }
}
	/*
	 * purpose: get authenticated and expiring  url for file download from amazon
	 */
	function getfileDownloadUrl_get(){
		//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$fileid=$this->get('fileid');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "getfileDownloadUrl (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			$companyid=$devauthresult['companyid'];
			$bucket=ROOTBUCKET;
			//$companyfolder="cuedrive.".$companyid;
			$companyfolder=COMPANYFOLDERNAME.$companyid;
			$filepath=$this->folderfilemodel->getFilePath($fileid);
			 //get authenticated url
                       
                         if($filepath == '0') {
			 
                          $this->response('file is deleted',400);
                         }
                         else
                         {
                         
                         $newurl=$this->s3->getAuthenticatedURL($bucket, $companyfolder.'/'.$filepath, DOWNLOADURLEXPIRYTIME);
			 echo $newurl;
			 //$this->response($newurl,200);

                         }
			
			
		}
	}
	}
	}
	
	
	/*
	 * purpose: Updated the uploaded file to amazon
	 */
	function updateFile_post()
	{
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
        $this->load->model('classpush');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->post('deviceid');
	$sessiontoken=$this->post('sessiontoken');
	$updatefileid=$this->post('updatefileid');
	$destinationfolderid=$this->folderfilemodel->getFolderId($updatefileid);
	$fileName = $_FILES['uploadedfile']['name'];
    $fileTempName = $_FILES['uploadedfile']['tmp_name'];
	
	//optional request parameters
	$username=($this->post('username'))?$this->post('username'):"";
	$password=($this->post('password'))?$this->post('password'):"";
	$userauth=false;
	$adminauth=false;
	$permission = false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "updateFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
	
	        $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($destinationfolderid,$deviceid);
                 
                if($destinationPermission == 1)
		{
			$permission = true;
		}
               
	
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
				$userauth=true;
				$companyuserid = Companyusermodel::fetchUserID($username , md5($password));

                                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($destinationfolderid,$companyuserid);
		       
		                if($destinationPermission == 1)
		                {
			            $permission = true;
		                }
		                else 
		                {
		        	    $permission = false;
		                }
			


			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			
			if($permission) {
			$companyid=$devauthresult['companyid'];
			//$bucketname="cuedrive.".$companyid;
			$bucketname=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			//check bucket size validation
			$oldfilesize=$this->folderfilemodel->getFileSize($updatefileid);
			$bucketsize=$this->getCompanyFolderSize($companyid)-$oldfilesize+$_FILES["uploadedfile"]["size"]; //in bytes
			$companypackagesize=$this->Companyadminmodel->getPackageSize($companyid);//in GB
			$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
			if($bucketsize<=$companypackagesize)
			{
				$uploadfolderid=$this->folderfilemodel->getFolderId($updatefileid);
				$uploadfolderpath=$this->foldermodel->getFolderPath($uploadfolderid);
				$foldername=$uploadfolderpath;
				//replace the existing file
				if($this->s3->putObjectFile($fileTempName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
				{
					//file uploaded successfullly
					//update DB
				    $datacc=array();
				    $datacc['filename']=$fileName;
				    $datacc['fullpath']=$foldername.'/' .$fileName;
				    $datacc['folderid']=$uploadfolderid;
				    $datacc['filesize']=$_FILES["uploadedfile"]["size"];
				    $datacc['updatedbydevice']=$deviceid;
				     if($userauth)
				    {
				    	$companyuserid=$usrauthresult['companyuserid'];
				    	 $datacc['updatedbyuser']=$companyuserid;
				    	  $logcc['companyuserid'] = $companyuserid;
				    }	
				    
				    


                                    $activitydetails =  'updated a file'.' '.'cuedrive://'.$foldername.'/' .$fileName;
                                    if($username!="")
				    {
				     $activitydetails = $username.' '.'has updated a file'.' '.'cuedrive://'.$foldername.'/' .$fileName;
				    }
				    $logcc['deviceid'] = $deviceid;
                                    $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		                    $logcc['action'] = 'update';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']='-';  
				    $logcc['from']='-'; 
				   
				     Classpush::sendFileUploadNotification($uploadfolderid,'update',$fileName,$deviceid);   
				     $this->auditlogmodel->updateAuditLog($logcc);			     
				     $this->folderfilemodel->updateFile($datacc,$updatefileid);
				     $updatedon=date('d/m/Y g:i a',strtotime($this->folderfilemodel->getFileUpdatedTimeStamp($updatefileid)));			
					
                                     $this->response(array('success' => APIFILEUPDATESUCCESS,'fileID' => $updatefileid,'updatedon' => $updatedon), 200);					
					
				}	
				else {
					$this->response(array('error' => APIFILEUPDATEFAIL), 400);
				}	
			}//end of if size validation
		else {
			$this->response(array('error' => APIFILESIZEEXCEED), 400);
		}
		}
		else
		{
		
		 $this->response(array('error' => 'Permission denied'), 400);
		}
		}
	}
	}
	}
	
		/*
	 * purpose: drag and drop file from one folder to another in amazon
	 */
	function moveFile_get(){
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$movefileid=$this->get('movefileid');
	$destinationfolderid=$this->get('destinationfolderid');
	$folderid = Folderfilemodel::getFolderid($movefileid);

    $permission = false;

	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "moveFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		
		$destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($destinationfolderid,$deviceid);
		$sourcePermission = Folderfilemodel::checkWritePermissionOnDevice($folderid,$deviceid);
		if($destinationPermission == 1 && $sourcePermission == 1)
		{
			$permission = true;
		}
		
		
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$permission = true;
			}
			else {//user authenticated
				
				
				$userauth=true;		

				$companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			    $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($destinationfolderid,$companyuserid);
		        $sourcePermission = Folderfilemodel::checkWritePermissionOnUser($folderid,$companyuserid);
		        if($destinationPermission == 1 && $sourcePermission == 1)
		        {
			       $permission = true;
		        }
		        else 
		        {
		        	$permission = false;
		        }
				
				
				
				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			//move file
			
			if($permission) {
			
			$replaceflag=false;
			$fileName=$this->folderfilemodel->getFileName($movefileid);			
			
				if($this->isFileExist($fileName, $destinationfolderid) )
				{
				$existingfileid=$this->folderfilemodel->getfileID($fileName,$destinationfolderid);
					if(($this->get('override')) )
					{
						if($this->get('override')=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=false;
						}
					}
					else {
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}

				if($uploadflag)
				{
					$sourcefilepath=$this->folderfilemodel->getFilePath($movefileid);

                                       
					$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
					$destinationfilepath=$destinationfolderpath."/".$fileName;
					$companyid=$devauthresult['companyid'];
					//$bucket="cuedrive.".$companyid;
					$bucket=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;
					
					//$retvalue=$this->s3->copyObject($bucket, $sourcefilepath, $bucket, $destinationfilepath,S3::ACL_PRIVATE);
					//print_r ($retvalue);
					if($this->s3->copyObject($bucket, $companyfoldername.'/'.$sourcefilepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
					{
					

                                          $activitydetails = 'moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
                                          if($username!="")
				          {
				            $activitydetails = $username.' '.'has moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
				          }
					  $logcc['deviceid'] = $deviceid;
                                          $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					  $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					  $logcc['action'] = 'move';
					  $logcc['activitydetails'] = $activitydetails;
					  $logcc['to']=$destinationfilepath;
					  $logcc['from']=$sourcefilepath;
						//delete the file from sorce folder
						$this->s3->deleteObject($bucket, $companyfoldername.'/'.$sourcefilepath);
						//update DB
						 $datacc=array();
						    $datacc['fullpath']=$destinationfolderpath.'/' .$fileName;
						    $datacc['folderid']=$destinationfolderid;
						    $datacc['updatedbydevice']=$deviceid;
						     if($userauth)
						    {
						    	$companyuserid=$usrauthresult['companyuserid'];
						    	 $datacc['updatedbyuser']=$companyuserid;
						    	  $logcc['companyuserid'] = $companyuserid;
						    }				     
						   $this->folderfilemodel->updateFile($datacc,$movefileid);
						   
						   if($replaceflag)
						   {
						   	 //delete the existing file from destination folder
						   	 $this->folderfilemodel->deleteFile($existingfileid);
						   	 
						   }
						   $this->auditlogmodel->updateAuditLog($logcc);
						//$this->response(APIFILEMOVESUCCESS,200);
						$this->response(array('success' => APIFILEMOVESUCCESS), 200);				
						
					}
					else {					
						//$this->response(APIFILEMOVEFAIL,400);
						$this->response(array('error' => APIFILEMOVEFAIL), 400);
					}
		}//duplicate file validation
			else {
				$this->response(array('error' => APIFILEEXIST), 400);
			}
			}
			else 
			{
				$this->response(array('error' => 'Permission denied'), 400);
			}
			
		}
	}
	}
	}
	/*
	 * purpose: delete file from amazon
	 */
	function deleteFile_get(){
	
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
        $this->load->model('classpush');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$fileid=$this->get('fileid');
    $fileName = Folderfilemodel::getFileName($fileid);
	$folderid = Folderfilemodel::getFolderid($fileid);
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	$permission = false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "deleteFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		
		$sourcePermission = Folderfilemodel::checkDeletePermissionOnDevice($folderid,$deviceid);
		if($sourcePermission == 1)
		{
			$permission = true;
		}
		
		
		
		
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$permission = true;
			}
			else {//user authenticated
				$userauth=true;	
				$logcc['companyuserid'] = $usrauthresult['companyuserid'];		

				
				$companyuserid = Companyusermodel::fetchUserID($username , md5($password));
		        $sourcePermission = Folderfilemodel::checkDeletePermissionOnUser($folderid,$companyuserid);
		        if($sourcePermission == 1)
		        {
			       $permission = true;
		        }
		        else 
		        {
		        	$permission = false;
		        }
				
				
				
				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			
			if($permission) {
			
			$companyid=$devauthresult['companyid'];
			//$bucket="cuedrive.".$companyid;
			$bucket=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			
			$filepath=$this->folderfilemodel->getFilePath($fileid);
			
			
			$activitydetails = 'deleted a file'.' '.'cuedrive://'.$filepath;
                        if($username!="")
		        {
			  $activitydetails = $username.' '.'has deleted a file'.' '.'cuedrive://'.$filepath;
			}
			
			
			$logcc['deviceid'] = $deviceid;
                        $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid);
			$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$logcc['action'] = 'delete';
			$logcc['activitydetails'] = $activitydetails;
			$logcc['from']=	$filepath;	  
			
					   
			$this->auditlogmodel->updateAuditLog($logcc);
			

			
			//delete the file
			if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
			{
				//update the database
				$this->folderfilemodel->deleteFile($fileid);
                                Classpush::sendFileUploadNotification($folderid,'delete',$fileName,$deviceid);   
				$this->response(array('success' => DELETESUCCESS), 200);
				 
			}
			else {
				  $this->response(array('error' => DELETEFAIL), 400);
			}
			
			
		}
		else 
	        {
		  $this->response(array('error' => 'Permission denied'), 400);
	        }
	}
	
	}
	}
	}
	
	/*
	 * Delete folder from amazon
	 */
		function deleteFolder_get()
	{
		//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$selectedfolderid=$this->get('folderid');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
        $permission = false;

        if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
        {

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "deleteFolder (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
        	$this->response(array('error' => APIDEVDSBL), 404);
        }
        
        else {

	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated

                $sourcePermission = Folderfilemodel::checkDeletePermissionOnDevice($selectedfolderid,$deviceid);
		if($sourcePermission == 1)
		{
			$permission = true;
		}



		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$permission = true;
			}
			else {//user authenticated
				$userauth=true;	
				$logcc['companyuserid'] = $usrauthresult['companyuserid'];
                               
                                $companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			        
		                $sourcePermission = Folderfilemodel::checkDeletePermissionOnUser($selectedfolderid,$companyuserid);
		                if($sourcePermission == 1)
		                 {
			            $permission = true;
		                 }
		                else 
		                 {
		        	    $permission = false;
		                 }





			
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	

                        if($permission) {
			
			$companyid=$devauthresult['companyid'];
			//$bucket="cuedrive.".$companyid;
			$bucket=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			$folderpath=$this->foldermodel->getFolderPath($selectedfolderid);
			

                        $activitydetails = 'deleted a folder'.' '.'cuedrive://'.$folderpath;
                        if($username!="")
		        {
			  $activitydetails = $username.' '.'has deleted a folder'.' '.'cuedrive://'.$folderpath;
			}


			
			$logcc['deviceid'] = $deviceid;
                        $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
			$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$logcc['action'] = 'delete';
			$logcc['activitydetails'] = $activitydetails;
			$logcc['from']=	$folderpath;	  
			
					   
			$this->auditlogmodel->updateAuditLog($logcc);
			
			
			
			$this->removeFolder($selectedfolderid,$bucket,$companyfoldername);
			$this->response(array('success' => DELETESUCCESSFOLDER), 200);
                        }
                        else {
                                $this->response(array('error' => 'Permission denied'), 400);
                             }
	}
	}
    }
	}
	
	function removeFolder($selectedfolderid,$bucketname,$companyfoldername)
	{
			$deletefolderarr=$this->getChildren($selectedfolderid);
			array_push($deletefolderarr,$selectedfolderid);
			rsort($deletefolderarr);
			
			//	print_r($deletefolderarr);
				
			for($i=0;$i<count($deletefolderarr);$i++)
			{
				$folderid=$deletefolderarr[$i];
				$files= $this->folderfilemodel->getFiles($folderid);
				for($j=0;$j<count($files);$j++)
				{
					$filepath=$files[$j]['fullpath'];
					$fileid=$files[$j]['id'];
					if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
					{
							//delete from folderfile
						$this->folderfilemodel->deleteFile($fileid);
					}			
				}							
						//update the database										
						//delete from folderalloweddevices
						$this->folderdevicesmodel->deleteByFoler($folderid);				
						
						//delete from folderallowedusers
						$this->folderusersmodel->deleteByFoler($folderid);						
						//delete from folder
						$this->foldermodel->deleteFolder($folderid);
					
					
		}//end of for
	}
	
	function userLogin_get()
	{
		$this->load->model('companydevicemodel');
		$this->load->model('companyusermodel');	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$username=$this->get('username');
	$password=$this->get('password');
			
	$userauth=false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "userLogin (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$this->response($usrauthresult,400);
			}
			else {//user authenticated
				


                               // fetch openfire credentials for chat login
                
                              $service_url = ORG_URL."chat/?type=fetch&username=".$username;
                              $curl = curl_init($service_url);
	                      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	                      $curl_response = curl_exec($curl);
	                      if ($curl_response === false) {
		              $info = curl_getinfo($curl);
		              curl_close($curl);
		              die('error occured during curl exec.Additioanl info: ' . var_export($info));
	                      }
	                      curl_close($curl);
                              $response = json_decode($curl_response);
                              $jidpassword = $response[0]->{'plainPassword'};
                              $jid = $response[0]->{'username'};






		              if(array_key_exists("isadmin", $usrauthresult))
			      {
				$companyuserid = 0;
			      }
                              else {
				$companyuserid=$usrauthresult['companyuserid'];
                               }
				$ouputarr=array();
        		$ouputarr['userid']=$companyuserid;
                        $ouputarr['jid']=$jid."@cuedrive.com.au";
                        $ouputarr['jidpassword']=$jidpassword;
           		$this->response($ouputarr, 200); // 200 being the HTTP response code
			}
		}
	}
	}
	}
	
	
	
	/* 
	 * API for sturctured list of children for folder
	 */
	function getChildrenList_get()
	{
		//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$selectedfolderid=$this->get('folderid');

	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
			
	$userauth=false;
	$adminauth=false;

	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "getChildrenList (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$companyid = $this->companyusermodel->fetchAdminID($username,md5($password));
				$foldersforadmin=$this->folderusersmodel->getFoldersForAdmin($companyid);//get folders for admin
			}
			else {//user authenticated
				$userauth=true;
                                $companyuserid=$usrauthresult['companyuserid'];
				$foldersforuser=$this->folderusersmodel->getFoldersForUser($companyuserid);//get folders for user
				
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				$folders=$this->getChildren($selectedfolderid);
					array_push($folders,$selectedfolderid);				
					if($userauth)//user provided and authenticated .. show user  folders
				{
					$foldersfilter=$foldersforuser;
				}
                                else if($adminauth)
				{
					$foldersfilter=$foldersforadmin;
				}
				else { //get folders for device
					$foldersfilter=$this->folderdevicesmodel->getFoldersForDevice($this->get('deviceid')); 	
				}
				$foldersfiltertemp=array();
					
				for($i=0;$i<count($foldersfilter);$i++)
				{
					array_push($foldersfiltertemp,$foldersfilter[$i]['folderid']);	
				}
					
			                
			                //  print_r($foldersfilter);
			            $folders= array_uintersect($folders, $foldersfiltertemp,  array($this,'compareDeepValue'));
			             $folders= array_values($folders);
			             
								
						$outputjson=array();			 	
			      
			           	$foldertreearr=array();
			         
			           	for($i=0;$i<count($folders);$i++)
					{
							$folderid=$folders[$i];
							$folderdetails=$this->foldermodel->getFolderDetails($folderid);
							$parentid=$folderdetails['parentfolderid'];
							if($parentid==null)
								$parentid=0;
							if($folderid==$selectedfolderid)//for making it main parent in response array
								$parentid=0;
							$foldertreearr[$i]['id']=$folderid;
							$foldertreearr[$i]['name']=$folderdetails['foldername'];
							$foldertreearr[$i]['folderpath']=$folderdetails['fullpath'];
							$foldertreearr[$i]['createdon']=date('d/m/Y g:i a',strtotime($folderdetails['createdon']));
							$foldertreearr[$i]['updatedon']=date('d/m/Y  g:i a',strtotime($folderdetails['updatedon']));
							$foldertreearr[$i]['formattedupdatedon']=date('d M Y',strtotime($folderdetails['updatedon']));
							$foldertreearr[$i]['parentfolderid']=$parentid;
						
								if($userauth)//user provided and authenticated.. so user level permission
								{
									$permissions=$this->folderusersmodel->getPermissions($folderid,$companyuserid);
								}
                                        else if($adminauth)
								{
									$permissions=$this->permissionsmodel->getAllPermissions();
								}
					        	else { // device level permission
					        		$permissions=$this->folderdevicesmodel->getPermissions($folderid,$deviceid);
					        	}		
							
				        	$foldertreearr[$i]['permissions']=$permissions;	
							
				        	$outputfiles=array();
							$files= $this->folderfilemodel->getFiles($folderid);
							for($j=0;$j<count($files);$j++)
							{
								$outputfiles[$j]['fileID']=$files[$j]['id'];
								$outputfiles[$j]['name']=$files[$j]['filename'];
								$outputfiles[$j]['filePath']=$files[$j]['fullpath'];
								$outputfiles[$j]['fileSize']=$files[$j]['filesize']."bytes";
								$outputfiles[$j]['createdon']=date('d/m/Y g:i a',strtotime($files[$j]['createdon']));
								$outputfiles[$j]['updatedon']=date('d/m/Y g:i a',strtotime($files[$j]['updatedon']));
								$outputfiles[$j]['formattedupdatedon']=date('d M Y',strtotime($files[$j]['updatedon']));
							}
							$foldertreearr[$i]['files']=$outputfiles;
						}
						//print_r($foldertreearr);
			         	 $foldertree= $this->buildFolderTree($foldertreearr);
			        	$foldertree = $this->remove_json_keys($foldertree);					    
			        	
			        	$outputjson['folder']=$foldertree;
                                    if(count($foldertree) > 0) {
			            $this->response($outputjson, 200); // 200 being the HTTP response code
                                    }
                                    else
                                    {
                                      $this->response(array("error" => "error"), 400);
                                    }
			       
		}
	
	} //end of if device authenticated
	}
	}
	
	function compareDeepValue($val1, $val2)
{
   return strcmp($val1, $val2);
}
	/* 
	 * API for sturctured list of children files for folder
	 */
	function getChildrenFileList_get()
	{
		//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$selectedfolderid=$this->get('folderid');

	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
			
	$userauth=false;
        $adminauth=false;
	 $outp = array();
	 
	 if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	 {
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "getChildrenFileList (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
	 	$this->response(array('error' => APIDEVDSBL), 404);
	 }
	 else {
	 
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				$folders=$this->getChildren($selectedfolderid);
				array_push($folders,$selectedfolderid);				
				
				
						$outputjson=array();			 	
			                $sum =0 ;
			           	$foldertreearr=array();
			                $filess = array();
			           	for($i=0;$i<count($folders);$i++)
					{
							$folderid=$folders[$i];
							$folderdetails=$this->foldermodel->getFolderDetails($folderid);
							$parentid=$folderdetails['parentfolderid'];
							if($parentid==null)
								$parentid=0;
							if($folderid==$selectedfolderid)//for making it main parent in response array
								$parentid=0;
			
				        	$outputfiles=array();
							$files = $this->folderfilemodel->getFilesarray($folderid);
                                        
                                        if(count($files) > 0) {

                                       
                                        $filess = $files;
                                       
					foreach($filess as $filesss)
							{
                                     
								$outputfiles['fileID']=$filesss['id'];
								$outputfiles['name']=$filesss['filename'];
								$outputfiles['filePath']=$filesss['fullpath'];
								$outputfiles['fileSize']=$filesss['filesize']."bytes";
								$outputfiles['createdon']=date('d/m/Y g:i a',strtotime($filesss['createdon']));
								$outputfiles['updatedon']=date('d/m/Y g:i a',strtotime($filesss['updatedon']));
								$outputfiles['formattedupdatedon']=date('d M Y',strtotime($filesss['updatedon']));
                                        
                                        $outp[] = $outputfiles;
							}

                                        
					}
                                      
                                        
						}
				      $outputjson['files'] = $outp;
 
			            $this->response($outputjson, 200); // 200 being the HTTP response code
			       
		}
	
	} //end of if device authenticated
	 }
	}
	
	/*
	 * purpose: drag and drop folder from one folder to another in amazon
	 */
function moveFolder_get(){
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$movefolderid=$this->get('movefolderid');
	$destinationfolderid=$this->get('destinationfolderid');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	$permission = false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "moveFolder (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		
		
	    $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($destinationfolderid,$deviceid);
		$sourcePermission = Folderfilemodel::checkWritePermissionOnDevice($movefolderid,$deviceid);
		if($destinationPermission == 1 && $sourcePermission == 1)
		{
			$permission = true;
		}
		
		
		
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				$permission = true;
			}
			else {//user authenticated
			
			    $logcc['companyuserid'] = $usrauthresult['companyuserid'];
				$userauth=true;		

				
			    $companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			    $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($destinationfolderid,$companyuserid);
		        $sourcePermission = Folderfilemodel::checkWritePermissionOnUser($movefolderid,$companyuserid);
		        if($destinationPermission == 1 && $sourcePermission == 1)
		        {
			       $permission = true;
		        }
		        else 
		        {
		        	$permission = false;
		        }
				
				
				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			//move folder	

			if($permission) {
			$replaceflag=false;
			$folderdetails=$this->foldermodel->getFolderDetails($movefolderid);
			$sourcefoldername=$folderdetails['foldername'];
			if($this->isFolderExist($sourcefoldername, $destinationfolderid) )
				{
					$existingfolderid=$this->foldermodel->getfolderID($sourcefoldername,$destinationfolderid);
					if(($this->get('override')) )
					{
						if($this->get('override')=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=false;
						}
					}
					else {
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
			
				if($uploadflag)
				{
					$sourcefolderpath=$this->foldermodel->getFolderPath($movefolderid);					
					$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
					$companyid=$devauthresult['companyid'];
					//$bucket="cuedrive.".$companyid;
					$bucket=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;
					
                                        $activitydetails = 'moved a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
                                        if($username!="")
		                        {
			                  $activitydetails = $username.' '.'has moved a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
			                }					



					$logcc['deviceid'] = $deviceid;
                                        $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['action'] = 'move';
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfolderpath;
					$logcc['from']=$sourcefolderpath;
					
					
					
					
					 if($replaceflag)
						  {
						   	 //first delete the existing folder from destination folder
						   		$this->removeFolder($existingfolderid,$bucket,$companyfoldername);						   	 
						   }					
					
					//delete the source folder from amazon								
						$deletefolderarr=$this->getChildren($movefolderid);
						array_push($deletefolderarr,$movefolderid);
						
						for($i=0;$i<count($deletefolderarr);$i++)
						{
							$folderid=$deletefolderarr[$i];
							$files= $this->folderfilemodel->getFiles($folderid);
							for($j=0;$j<count($files);$j++)
							{			
								$filepath=$files[$j]['fullpath'];
								$fileid=$files[$j]['id'];
								
								$fullpatharr=explode($sourcefoldername, $filepath);
								$tempfullpath=$sourcefoldername.$fullpatharr[1];
								$destinationfilepath=$destinationfolderpath.'/' .$tempfullpath;
								if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
								{								
									if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
									{
										//update folderfile
									 	$datacc=array();
									
									    $datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
									    $datacc['updatedbydevice']=$deviceid;
									     if($userauth)
									    {
									    	$companyuserid=$usrauthresult['companyuserid'];
									    	$datacc['updatedbyuser']=$companyuserid;
									    }				     
									   $this->folderfilemodel->updateFile($datacc,$fileid);
									}			
							}
							}
							//update folder
								 $datacc=array();
								 $folderpatharr=explode($sourcefoldername, $sourcefolderpath);
								 $tempfullpath=$sourcefoldername.$folderpatharr[1];
								 $datacc=array();
						 		 if($folderid==$movefolderid) //change parent id
								   {
								   	  $datacc['parentfolderid']=$destinationfolderid;
                                                                          $datacc['type'] = $this->foldermodel->getFolderType($destinationfolderid);
								   }
								    $datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
								    $datacc['updatedbydevice']=$deviceid;
								     if($userauth)
								    {
								    	$companyuserid=$usrauthresult['companyuserid'];
								    	 $datacc['updatedbyuser']=$companyuserid;
								    }				     
								   $this->foldermodel->updateFolder($datacc,$folderid);															  
						}			
												   
						 
						$this->auditlogmodel->updateAuditLog($logcc);
						$this->response(array('success' => APIFOLDERMOVESUCCESS), 200);				
						
//					}
//					else {					
//						$this->response(array('error' => APIFOLDERMOVEFAIL), 400);
//					}
		}//duplicate file validation		
			else {
				$this->response(array('error' => APIFOLDEREXIST), 400);
			}
			
		}
		else {
			$this->response(array('error' => 'Permission denied'), 400);
		}
		}
	}
	}
	}
	
/*
	 * Check if same file exist in specified folder
	 */
function isFolderExist($newfoldername,$parentfolderid)
{
	 if($this->foldermodel->folderNameExist($newfoldername,$parentfolderid))
	 {
	 	return true;
	 }
	 else {
	 	return false;
	 }
}
	
	/*
	 * Purpose: Common Functions
	 */
	 
	 function getChildren($parent_id) {

    $tree = Array();   
    if (!empty($parent_id)) {
        $tree = $this->foldermodel->getOneLevel($parent_id);
        foreach ($tree as $key => $val) {
            $ids = $this->getChildren($val);
            $tree = array_merge($tree, $ids);
        }
    }
    return $tree;

}
	function deviceAuth($deviceid,$sessiontoken)
	{
		$retarr=array();
		//authenticate the session token and check whether device is active or not	
		$authresult= $this->companydevicemodel->deviceAuth($deviceid,$sessiontoken);	
		if($authresult=="notactive")
		{		
                        
			$retarr['error'] = APIDEVDSBL; 
		}
		else if($authresult=="expired")
		{
			 $retarr['error'] =APISESSEXP; 
		}
		else
		{
			$retarr=$authresult;
		}
		return $retarr;
	}
	
	function userAuth($username,$password,$deviceid)
	{
		$retarr=array();
		//authenticate user login and check whether he is having access to device or not
		$userauthresult=$this->companyusermodel->userAuth($username, md5($password),$deviceid);
		if($userauthresult=="invalidlogin")
		{
			$retarr['error'] = APIUSRLOGNFAIL; 
		} 
		else if($userauthresult=="notactive")
		{		
			$retarr['error'] = APIUSRNOACTIVE; 
		}
		else if($userauthresult=="noaccesstodevice")
		{
			$retarr['error'] = APIUSRNOACCESS; 
		}
		else if($userauthresult=="companyadmin")
		{
			$retarr['isadmin'] = 'companyadmin'; 
		}
		else //authenticated
		{
			$retarr=$userauthresult;
		}
		return $retarr;
	}
	
	function buildFolderTree(array &$elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parentfolderid'] == $parentId) {
        	
            $children = $this->buildFolderTree($elements, $element['id']);
            if ($children) {
                $element['subfolder'] = $children;
            }
           
            $branch[$element['id']] = $element;
           // $branch['folder'] = $element;
            }
    }
    return $branch;
}
	
	function getBucketSize($bucket)
	{
		// Load Library
 	 	$this->load->library('s3');
 	 	$resultarr=$this->s3->getBucket($bucket);
 	 	$totalsize=0;
 	 	foreach($resultarr as  $arr){    
   		   	$totalsize=$totalsize+$arr['size'];
 	 	  	}
 	 	  	return $totalsize;	
	}
	
function getCompanyFolderSize($companyid)
{
	return  Folderfilemodel::getTotalfileStored($companyid);
}
	function remove_json_keys($array) {
  foreach ($array as $k => $v) {
    if (is_array($v)) {
        $array[$k] = $this->remove_json_keys($v);
    } //if
  } //foreach
  return $this->sort_numeric_keys($array);

}

function sort_numeric_keys($array) {
    $i=0;
    $rtn=array();
    foreach($array as $k => $v) {
        if(is_int($k)) {
            $rtn[$i] = $v;
            $i++;
        } else {
            $rtn[$k] = $v;
        } //if
    } //foreach
    return $rtn;
}
	/*
	 * End of Common Functions
	 */
		
		function getBucket_get()
	{
		$bucket="cuedrive.1";
		// Load Library
 	 	$this->load->library('s3');
 	 	//print_r($this->s3->getBucket($bucket));
 	 	//$this->response($this->s3->getBucket($bucket),200);
 	 	$resultarr=$this->s3->getBucket($bucket);
 	 	print_r($resultarr);
 	 	foreach($resultarr as  $arr){    
   		   	 echo $arr['size'];
    		}
		
		
 	 	
	}
	
	function deleteTest_get()
	{
		$bucket="cuedrive.3";
		// Load Library
 	 	$this->load->library('s3');
 	 	$this->s3->deleteObject($bucket, "testdel");
	}
	
	
	
	function releaseLock_get() {

	$this->load->model('lockfilemodel');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	$this->load->model('companyusermodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	
	$fileid=$this->get('fileid');
	 $folderid = Folderfilemodel::getFolderid($fileid);
	$sessiontoken=$this->get('sessiontoken');
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $$adminauth=false;
	$companyuserid = 0;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "releaseLock (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			
			$response = Lockfilemodel::releaseLock($fileid,$folderid,$deviceid,$companyuserid);
			if($response > 0) {
			$this->response(array('success' => 'File is unlocked successfully!'), 200);
			}
			else
			{
			$this->response(array('error' => 'Database error!'), 404); 
			}
		}
	
	} //end of if device authenticated
	 
	}
	}
	
	/**
	  Webservice to lock a file
	*/
	function checkStatus_get()
	{
	$this->load->model('lockfilemodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');
	$this->load->model('folderfilemodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	
	$fileid=$this->get('fileid');
	 $folderid = Folderfilemodel::getFolderid($fileid);
	$sessiontoken=$this->get('sessiontoken');
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{

                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "checkStatus (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose      
			$ouputarr = Lockfilemodel::fetchLastLockStatus($fileid,$folderid);
			if($ouputarr['status'] == 'no')
			$this->response($ouputarr, 400);
			else
			$this->response($ouputarr, 200);
		}
	
	} //end of if device authenticated
	}
	 }
	 
	 
	 
	function lockFile_get()
	{
	$this->load->model('lockfilemodel');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	$this->load->model('companyusermodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	
	$fileid=$this->get('fileid');
        $folderid = Folderfilemodel::getFolderid($fileid);
	$sessiontoken=$this->get('sessiontoken');
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
	$companyuserid = 0;
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "lockFile (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			$lockid =   Lockfilemodel::lockExists($fileid,$folderid); 
			$response = Lockfilemodel::lockfile($fileid,$folderid,$deviceid,$companyuserid,$lockid);
			if($response > 0) {
			$this->response(array('success' => 'File is locked successfully!'), 200);
			}
			else
			{
			$this->response(array('error' => 'Database error!'), 404); 
			}
		}
	
	} //end of if device authenticated
	 
	}
	 
	 }
	 
	 
	 
	 	
	function createFolder_get()
	{

	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('auditlogmodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	 // Load Library
 	 $this->load->library('s3');
 	 
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$parentfolderId=$this->get('parentfolderid');
	$newfoldername=trim($this->get('newfoldername'));

	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
			
	$userauth=false;
	$adminauth=false;
	$permission=false;
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                 $file = 'apierrorlog.txt';
                 $current = file_get_contents($file);
                 $current .= "createFolder (checkDeviceExpiryWithDeviceID) called at -".date('Y-m-d H:i')." - from deviceid :".$deviceid."\n";
                 file_put_contents($file, $current);
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated


                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($parentfolderId,$deviceid);
		
		if($destinationPermission == 1)
		{
			$permission = true;
		}
		
		$permission = true;


		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
				$userauth=true;
				$companyuserid=$usrauthresult['companyuserid'];
			        $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($parentfolderId,$companyuserid);
		        
		                if($destinationPermission == 1)
		                {
			            $permission = true;
		                }
		               else 
		               {
		        	    $permission = false;
		               }
                               

				 $logcc['companyuserid'] = $companyuserid;
			     }
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	

                 if($permission) {
			//add folder
			if(!$this->foldermodel->foldernameexist($newfoldername,$parentfolderId))
		{
		$companyid=$devauthresult['companyid'];
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$parentdetails=$this->foldermodel->getFolderDetails($parentfolderId);
		$path=$parentdetails['fullpath'];
		
		$parenttype=$parentdetails['type'];
		
		if($parentfolderId == 0)
		{
			$parenttype= 'private';
			$parentfolderId = NULL;
			
			$result = $this->foldermodel->foldernameexist($newfoldername,$parentfolderId);
			if($result)		{	
				$this->response(array('error' => FOLDERNAMEEXIST), 400);
			}
			
		}	
			
		if($parenttype == '')	{
			$parenttype = 'private';
		}
		
		$newpath=$path."/".$newfoldername;
		
		
                $activitydetails = 'created a folder'.' '.'cuedrive://'.$newpath;
                if($username!="")
		{
		$activitydetails = $username.' '.'has created a folder'.' '.'cuedrive://'.$newpath;
                }

		
		$logcc['deviceid'] = $deviceid;
                $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
		$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
	        $logcc['action'] = 'create';
		$logcc['activitydetails'] = $activitydetails;
		$logcc['to'] =$newpath; 
		
		
		
		
		
			
		
		$uploaddir = realpath('./uploads/');
		$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$newfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$newpath;
				$datacc['type']=$parenttype; 
				$datacc['parentfolderid']=$parentfolderId; 
				//$datacc['createdby']=$companyid;
				
				 $datacc['createdbydevice']=$deviceid;
					     if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['createdbyuser']=$companyuserid;
					    }	
				$datacc['createdon']=date('Y-m-d H:i');				
				$newfolderid=$this->foldermodel->insertFolder($datacc);
				
					
/*				if(($level+1)<=3)//add default full access to approved user and devices
				{
					$this->addAllPermissions($newfolderid); 
				}
				else //add parent permissions
				{
				 	$this->addParentPermissions($parentfolderId,$newfolderid);
				}
*/
					if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    }
					    else
					    {
					    	$companyuserid="";
					    }
							
							
				if($parentfolderId == '' || $parentfolderId == 0)				{
					$this->addAllPermissions($newfolderid,$companyid,$deviceid,$companyuserid);
				}	else	{
					$this->addParentPermissions($parentfolderId,$newfolderid,$companyid,$deviceid,$companyuserid);												
				}
				
				//$this->response(array('success' => APIFOLDERCREATESUCCESS), 200);
				$folderdetails=$this->foldermodel->getFolderDetails($newfolderid);
				$outputarr['id']=$newfolderid;
				$outputarr['name']=$folderdetails['foldername'];
				$outputarr['folderpath']=$folderdetails['fullpath'];
				$outputarr['createdon']=date('d/m/Y g:i a',strtotime($folderdetails['createdon']));
				$outputarr['updatedon']=date('d/m/Y  g:i a',strtotime($folderdetails['updatedon']));
				$outputarr['formattedupdatedon']=date('d M Y',strtotime($folderdetails['updatedon']));
				$outputarr['parentfolderid']=$folderdetails['parentfolderid'];
				
				
				
				
			$this->auditlogmodel->updateAuditLog($logcc);
                          $this->response($outputarr, 200);
			}
			else {
				//echo CREATEFOLDERFAIL;
				$this->response(array('error' => CREATEFOLDERFAIL), 400);
			}
			
		}
		else
		{
			//echo FOLDERNAMEEXIST;
			$this->response(array('error' => FOLDERNAMEEXIST), 400);
		}
                }
                else
                {
                   $this->response(array('error' => 'Permission denied'), 400);
                }
		}
	}//end of if device authenticated
	}
	}
	
	
	function addAllPermissions($folderid,$companyid,$deviceid,$companyuserid)
	{
		//add to all devices
		 
		 $permissions=$this->permissionsmodel->getAllPermissions();
		 	
				$companydevices=$this->companydevicemodel->getDevicesByCompany($companyid);
				for($i=0;$i<count($companydevices);$i++)
				{
					$deviceid=$companydevices[$i]['deviceid'];
					for($j=0;$j<count($permissions);$j++)
					{
						 $datacc = array();
						 $permissionid=$permissions[$j]['permissionid'];
						 if($permissionid!=6)
						  {
							 $datacc['deviceid']=$deviceid;
							 $datacc['folderid']=$folderid;
							 $datacc['permissionid']=$permissionid;
							 $datacc['createdby']=$companyid;
							 $datacc['updatedby']=$companyid;
							 $datacc['createdon']=date('Y-m-d H:i');	
							 $this->folderdevicesmodel->insertFolderDevice($datacc);
							  
						  }	 
					}
				}
		 
	}
	
	function addParentPermissions($parentfolderid,$newfolderid,$companyid,$deviceid,$companyuserid)
	{
		$devicepermissions=$this->folderdevicesmodel->getFolderPermissions($parentfolderid);

		for($i=0;$i<count($devicepermissions);$i++)
		{
					 	$datacc = array();
					  	 $permissionid=$devicepermissions[$i]['permissionid'];
					     $datacc['deviceid']=$devicepermissions[$i]['deviceid'];
						 $datacc['folderid']=$newfolderid;
						 $datacc['permissionid']=$permissionid;
						 $datacc['updatedbydevice']=$deviceid;
					     if($companyuserid!="")
					    {
					    	 $datacc['updatedbyuser']=$companyuserid;
					    }	
						 $datacc['createdon']=date('Y-m-d H:i');	
						 $this->folderdevicesmodel->insertFolderDevice($datacc);
						 
		}
		
		$userpermissions=$this->folderusersmodel->getUserPermissions($parentfolderid);
		for($i=0;$i<count($userpermissions);$i++)
		{
					 	$datacc = array();
					  	 $permissionid=$userpermissions[$i]['permissionid'];
					     $datacc['companyuserid']=$userpermissions[$i]['companyuserid'];
						 $datacc['folderid']=$newfolderid;
						 $datacc['permissionid']=$permissionid;
						$datacc['updatedbydevice']=$deviceid;
					     if($companyuserid!="")
					    {
					    	 $datacc['updatedbyuser']=$companyuserid;
					    }	
						 $datacc['createdon']=date('Y-m-d H:i');	
						 $this->folderusersmodel->insertFolderUser($datacc);
		}
		
	}
	
	
	function fetchApprovedEmails_get()
	{
	$this->load->model('companyusermodel');
	$this->load->library('s3');
	$this->load->model('foldermodel');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
        $this->load->model('email');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
        $fileid=$this->get('fileid');
        $folderid = Folderfilemodel::getFolderid($fileid);
	$sessiontoken=$this->get('sessiontoken');
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";

        $isok=($this->get('isok'))?$this->get('isok'):1;

	$userauth=false;
        $adminauth=false;
	$companyuserid = 0;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
                
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	else {
	
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			$response = Foldermodel::fetchPermitedEmails($folderid,$fileid);
			if($response == 1) {

                             if($isok == 1) {
                             
                                $companyid=$devauthresult['companyid'];
				//$bucket="cuedrive.".$companyid;
				$bucket=ROOTBUCKET;
				$companyfoldername=COMPANYFOLDERNAME.$companyid;
				$filepath=$this->folderfilemodel->getFilePath($fileid);
                                $filename = $this->folderfilemodel->getFileName($fileid);
				//get authenticated url 
				if($filepath == '0') {
				
					$newurl = "";
				}
				else
				{
					 
					$newurl=$this->s3->getAuthenticatedURL($bucket, $companyfoldername.'/'.$filepath, 300);
				
				}
					
				
				Email::sendFileAttachment($newurl,$folderid,$filename,$fileid);

			        $this->response(array('success' => 'true'), 200);

                              }
                              else if($isok == 2)
                              {

                                 $emails = Email::sendFolderAllowedEmails($folderid);
                                 $this->response(array('emails' => $emails), 200);

                              }
			}
			else if($response == 2)
			{
			 $this->response(array('success' => 'false'), 404);
			}
			else
			{
			$this->response(array('success' => 'false'), 400);
			}
		}
	
	} //end of if device authenticated
	 
	}
	
	}
	
	
	
	
	/*
	
	Purpose: Register device for push notification
	
	*/
	
	function registerDevice_get()
	{
	    // load the needed models
	    $this->load->model('classpush');
	    
	    
            // load the needed helpers
	    $this->load->helper(array('cuedrive_common'));
	       
                $resultArray = array(); 
		$errorFound = false;
			
	
		if((!($this->get('is_iphone')))  && (!($this->get('is_android')) ))
		{
			$errorFound = true;
			$resultArray["error"][] = "Device type is missing either iphone or andriod";
		}	
		if(($this->get('is_iphone')) )
		{
			if(!($this->get('device_token')) )
			{
				$errorFound = true;
				$resultArray["error"][] = "device token is missing";
			}

		}
		if(($this->get('is_android')) )
		{
			if((!($this->get('reg_id')) ))
			{
				$errorFound = true;
				$resultArray["error"][] = "Reg id is missing";
			}
		}
		
		if($errorFound)
		{
			//return $resultArray;	
                        $this->response($resultArray, 400);
		}
		else
		{
			
			$operating_system = 0;
			$device_id = ($this->get('device_id'))?$this->get('device_id'):"";
                        $device_uuid = $this->get('device_uuid');
			$iphonepushopt = 0;
			$androidpushopt = 0;
			$device_token = '';
			$reg_id = '';
			$pushbadge = 'disabled';
			$pushalert = 'disabled';
			$pushsound = 'disabled';
			if(($this->get('is_iphone')))
			{
				$iphonepushopt = $this->get('is_iphone');
				$device_token = $this->get('device_token');
				$pushbadge = 'enabled';
				$pushalert = 'enabled';
				$pushsound = 'enabled';
			}
			if(($this->get('is_android')))
			{
				$androidpushopt = $this->get('is_android');
				$reg_id = $this->get('reg_id');
			}
			
			$result = Classpush::registerDevice($operating_system,$device_id,$iphonepushopt,$androidpushopt,$device_token,$reg_id,$pushbadge,$pushalert,$pushsound,$device_uuid);
			$this->response($result, 200);
		}	
	
	}
	
	
	
	
	function fileLockNotification_get()
	{
	
	// load needed models
	$this->load->model('foldermodel');
        $this->load->model('classpush');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	$this->load->model('companyusermodel');
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
        $fileid=$this->get('fileid');
	$sessiontoken=$this->get('sessiontoken');
	$lockedbydeviceid = $this->get('lockedbydeviceid');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
	
        if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
        {
        	$this->response(array('error' => APIDEVDSBL), 404);
        }
        else {    
    
        //authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                         else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			$response = Classpush::sendFileLockNotification($deviceid,$fileid,$lockedbydeviceid,$username);
			echo $response;
                        if($response == 1) {
			$this->response(array('success' => 'true'), 200);
			}
			else
			{
			$this->response(array('success' => 'false'), 400);
			}
		}
	
	} //end of if device authenticated
	 
	
	
    }
	
	
	}
	
	
	
	
	
	function chatNotification_get()
	{
	
	// load needed models
	$this->load->model('foldermodel');
        $this->load->model('classpush');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	$this->load->model('companyusermodel');
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$todeviceid = $this->get('todeviceid');
        $message = $this->get('message');
        $time = ($this->get('time')) ? $this->get('time') : "";
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
	
       
	 { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                          else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			$response = Classpush::sendChatNotification($deviceid,$todeviceid,$message,$time,$username);
			
                        if($response == 1) {
			$this->response(array('success' => 'true'), 200);
			}
			else
			{
			$this->response(array('success' => 'false'), 400);
			}
		}
	
	} //end of if device authenticated
	 
	
	
	
	
	
	}
	
	
	
	function removeLockIsAcceptNotification_get()
	{
	
	// load needed models
	$this->load->model('foldermodel');
        $this->load->model('classpush');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	$this->load->model('companyusermodel');
        // load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
        $fileid=$this->get('fileid');
	$sessiontoken=$this->get('sessiontoken');
	$openfordeviceid = $this->get('openfordeviceid');
        $isaccept = $this->get('isaccept');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
	
        
        if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
        {
        	$this->response(array('error' => APIDEVDSBL), 404);
        }
        else {
        //authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                          else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
				
			// send respose   
			$response = Classpush::sendRemoveLockIsAcceptNotification($deviceid,$fileid,$openfordeviceid,$username,$isaccept);
			
                        if($response == 1) {
			$this->response(array('success' => 'true'), 200);
			}
			else
			{
			$this->response(array('success' => 'false'), 400);
			}
		}
	
	} //end of if device authenticated
	 
	
	
	
        }
	
	}
	
	
        function resetNotificationBadge_get()
        {
           $this->load->helper(array('cuedrive_common'));
           $this->load->model('classpush');
           $deviceid=$this->get('deviceid');
           $response = Classpush::resetNotificationBadgeCount($deviceid);
           $this->response(array('success' => 'true'), 200);
        }


	
	
		
	/*
	 * purpose: Upload the file to amazon
	 */
	function updateFileOnOffline_post()
	{
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
        $this->load->model('folderfilemodel');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->post('deviceid');
	$sessiontoken=$this->post('sessiontoken');
	$uploadfileid=$this->post('uploadfileid');

        $uploadfolderid = Folderfilemodel::getFolderid($fileid);

        
	$fileName = $_FILES['uploadedfile']['name'];

         $i = 1;
         while(Folderfilemodel::fileNameExist($fileName,$uploadfolderid))
         {           
               $fileName = (string)$fileName.$i;
               $i++;
         }


        $fileTempName = $_FILES['uploadedfile']['tmp_name'];
	
    
	//optional request parameters
	$username=($this->post('username'))?$this->post('username'):"";
	$password=($this->post('password'))?$this->post('password'):"";
	$userauth=false;
	$adminauth=false;
        $permission = false;

	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated

                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($uploadfolderid,$deviceid);
		
		if($destinationPermission == 1)
		{
			$permission = true;
		}


		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                         else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
				$userauth=true;
 
                                $companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			        $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($uploadfolderid,$companyuserid);
		       
		                if($destinationPermission == 1 && $sourcePermission == 1)
		                {
			           $permission = true;
		                }
		               else 
		                {
		        	   $permission = false;
		                }
				
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			if($permission) {
			$companyid=$devauthresult['companyid'];
			//$bucketname="cuedrive.".$companyid;
			$bucketname=ROOTBUCKET;
			$companyfoldername=COMPANYFOLDERNAME.$companyid;
			//check bucket size validation
			//$bucketsize=$this->getBucketSize($bucketname)+$_FILES["uploadedfile"]["size"]; //in bytes
			$bucketsize=$this->getCompanyFolderSize($companyid)+$_FILES["uploadedfile"]["size"]; //in bytes
			$companypackagesize=$this->Companyadminmodel->getPackageSize($companyid);//in GB
			$companypackagesize=1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
			//$companypackagesize=1024*(str_replace("KB","",$companypackagesize)); //in bytes for testing
			if($bucketsize<=$companypackagesize)
			{
				$replaceflag=false;
				if($this->isFileExist($fileName, $uploadfolderid) )
				{
					if(($this->post('override')) )
					{
						if($this->post('override')=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=false;
						}
					}
					else {
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
				
				if($uploadflag)
				{
			
				$uploadfolderpath=$this->foldermodel->getFolderPath($uploadfolderid);
				$foldername=$uploadfolderpath;
				if($this->s3->putObjectFile($fileTempName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
				{
					//file uploaded successfullly
					//update DB
					
				    $datacc=array();
				    $datacc['filename']=$fileName;
				    $datacc['fullpath']=$foldername.'/' .$fileName;
				    $datacc['folderid']=$uploadfolderid;
				    $datacc['filesize']=$_FILES["uploadedfile"]["size"];
				    
				    
                                    $activitydetails = 'uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
                                    if($username!="")
		                    {
		                      $activitydetails = $username.' '.'has uploaded a file'.' '.$fileName.' '.'into'.' '.'cuedrive://'.$foldername;
                                    }



				    $logcc['deviceid'] = $deviceid;
                                    $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				    $logcc['action'] = 'upload';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']  = '-';
				    $logcc['from']  = '-';
					   
					   
					 
				    
				    
				    
				    
				    
				  
				    if($replaceflag)//update existing
				    {	
				    	 $datacc['updatedbydevice']=$deviceid;
					     if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['updatedbyuser']=$companyuserid;
					    }		
				    	$replacefileid=$this->folderfilemodel->getfileID($fileName,$uploadfolderid);	     
				    	$this->folderfilemodel->updateFile($datacc,$replacefileid);	
				    }
				    else //add new
				    {
				    	$datacc['createdbydevice']=$deviceid;
					    	  if($userauth)
					    {
					    	$companyuserid=$usrauthresult['companyuserid'];
					    	 $datacc['createdbyuser']=$companyuserid;
					    	 $logcc['companyuserid'] = $companyuserid;
					    }				   
				   		 $createdon=date('Y-m-d H:i:s');
				   		 $updatedon=date('d/m/Y g:i a',strtotime($createdon));	
				   		 $datacc['createdon'] = $createdon;
				    	$newfileid=$this->folderfilemodel->insertFile($datacc);	
				    }
				    	  $this->auditlogmodel->updateAuditLog($logcc);		    				
					$this->response(array('success' => APIFILEUPLOADSUCCESS,'fileID' => $newfileid,'updatedon' => $updatedon), 200);
	
				}	
				else {
					$this->response(array('error' => APIFILEUPLOADFAIL), 400);
				}	
			}//duplicate file validation
			else {
				$this->response(array('error' => APIFILEEXIST), 400);
			}
		}//end of if size validation
		else {
			//$this->response(APIFILESIZEEXCEED,400);
			$this->response(array('error' => APIFILESIZEEXCEED), 400);
		}
		}
		else
		{
		$this->response(array('error' => 'Permission denied'), 400);
		
		}
		}
		
	}
	}
	
	
	
	
	
	
	function renameFile_get(){
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');
	$this->load->model('folderusersmodel');
	$this->load->model('Companyadminmodel');
	$this->load->model('auditlogmodel');
	// Load Library
	$this->load->library('s3');

	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$fileid=$this->get('fileid');
	$newfilename = $this->get('filename');
        $destinationfolderid = Folderfilemodel::getFolderid($fileid);
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
        $permission = false;
        
        if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
        {
        	$this->response(array('error' => APIDEVDSBL), 404);
        }
        else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		$this->response($devauthresult,400);
	}
	else { //device authenticated


                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($destinationfolderid,$deviceid);
	
		if($destinationPermission == 1)
		{
			$permission = true;
		}
		



		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                       else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
				$userauth=true;

                                $companyuserid = Companyusermodel::fetchUserID($username , md5($password));
			        $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($destinationfolderid,$companyuserid);
		        
		               if($destinationPermission == 1)
		               {
			            $permission = true;
		               }
		               else 
		               {
		        	    $permission = false;
		               }




			}
		}

		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {
			//move file
			if($permission) {	
			$replaceflag=false;
			$oldfileName=$this->folderfilemodel->getFileName($fileid);
			$folderid = Folderfilemodel::getFolderid($fileid);
			if($this->isFileExist($newfilename, $folderid) )
			{
				$existingfileid=$this->folderfilemodel->getfileID($newfilename,$folderid);
				if(($this->get('override')) )
				{
					if($this->get('override')=="true")
					{
						$uploadflag=true;
						$replaceflag=true;
					}
					else {
						$uploadflag=true;
					}
				}
				else {
					$uploadflag=true;
				}
			}
			else {
				$uploadflag=true;
			}

			if($uploadflag)
			{
				$sourcefilepath=$this->folderfilemodel->getFilePath($fileid);

                               
				$destinationfolderpath=$this->foldermodel->getFolderPath($folderid);
				$destinationfilepath=$destinationfolderpath."/".$newfilename;
				$companyid=$devauthresult['companyid'];
				//$bucket="cuedrive.".$companyid;
				$bucket=ROOTBUCKET;
				$companyfoldername=COMPANYFOLDERNAME.$companyid;
					
				//$retvalue=$this->s3->copyObject($bucket, $sourcefilepath, $bucket, $destinationfilepath,S3::ACL_PRIVATE);
				//print_r ($retvalue);
				if($this->s3->copyObject($bucket, $companyfoldername.'/'.$sourcefilepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
				{
						

					$activitydetails = 'rename a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					if($username!="")
					{
						$activitydetails = $username.' '.'has rename a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					}
					$logcc['deviceid'] = $deviceid;
					$logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['action'] = 'rename';
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfilepath;
					$logcc['from']=$sourcefilepath;
						
						
						
						
						
					//delete the file from sorce folder
					$this->s3->deleteObject($bucket, $companyfoldername.'/'.$sourcefilepath);
					//update DB
					$datacc=array();
					$datacc['fullpath']=$destinationfolderpath.'/' .$newfilename;
					$datacc['filename']=$newfilename;
					$datacc['folderid']=$folderid;
					$datacc['updatedbydevice']=$deviceid;
					if($userauth)
					{
						$companyuserid=$usrauthresult['companyuserid'];
						$datacc['updatedbyuser']=$companyuserid;
						$logcc['companyuserid'] = $companyuserid;
					}
					$this->folderfilemodel->updateFile($datacc,$fileid);
						
					if($replaceflag)
					{
						//delete the existing file from destination folder
						$this->folderfilemodel->deleteFile($existingfileid);
							
					}
					$this->auditlogmodel->updateAuditLog($logcc);
					//$this->response(APIFILEMOVESUCCESS,200);
					$this->response(array('success' => 'Successfully renamed the file!'), 200);

				}
				else {
					//$this->response(APIFILEMOVEFAIL,400);
					$this->response(array('error' => 'Something went wrong while renaming your file!'), 400);
				}
			}//duplicate file validation
			else {
				$this->response(array('error' => APIFILEEXIST), 400);
			}
                       }
                       else
                      {
                            $this->response(array('error' => 'Permission denied'), 400);

                      }
		}
	}
        }
}

	
	
	
	
	function renameFolder_get(){
	//load models
	$this->load->model('folderfilemodel');
	$this->load->model('folderdevicesmodel');
	$this->load->model('foldermodel');
	$this->load->model('permissionsmodel');
	$this->load->model('companydevicemodel');
	$this->load->model('companyusermodel');	
	$this->load->model('folderusersmodel');	
	$this->load->model('Companyadminmodel');	
	$this->load->model('auditlogmodel');
	 // Load Library
 	 $this->load->library('s3');
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	$movefolderid=$this->get('folderid');
	$destinationfolderid=Foldermodel::getParentID($movefolderid);
	$newfoldername = $this->get('foldername');
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
	$adminauth=false;
	$permission = false;
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	
	else {
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated

                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnDevice($destinationfolderid,$deviceid);
		
		if($destinationPermission == 1)
		{
			$permission = true;
		}


		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
                                $permission = true;
				
			}
			else {//user authenticated
			
			        $logcc['companyuserid'] = $usrauthresult['companyuserid'];
				$userauth=true;	
                                $destinationPermission = Folderfilemodel::checkEditSavePermissionOnUser($usrauthresult['companyuserid'],$companyuserid);
		       
		                if($destinationPermission == 1)
		                {
			             $permission = true;
		                }
		               else 
		               {
		        	    $permission = false;
		               }
				

   

			
			}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			//move folder
                         if($permission) {
			
			$replaceflag=false;
			$folderdetails=$this->foldermodel->getFolderDetails($movefolderid);	
			$sourcefoldername=$folderdetails['foldername'];
			if($this->isFolderExist($newfoldername, $destinationfolderid) )
				{
					$existingfolderid=$this->foldermodel->getfolderID($newfoldername,$destinationfolderid);
					if(($this->get('override')) )
					{
						if($this->get('override')=="true")
						{
							$uploadflag=true;
							$replaceflag=true;
						}
						else {
							$uploadflag=true;
						}
					}
					else {
						$uploadflag=true;
					}
				}
				else {
					$uploadflag=true;
				}
			
				if($uploadflag)
				{
					$sourcefolderpath=$this->foldermodel->getFolderPath($movefolderid);	
 
                   // $sourcefolderarray = explode('/', $sourcefolderpath);
                   // $countsourcefolderarray = count($sourcefolderarray);
                   // $sourcefolderarray[$countsourcefolderarray - 1] = $newfoldername;
                   // $sourcefolderpath= implode('/', $sourcefolderarray);
                    
					$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
					$companyid=$devauthresult['companyid'];
					//$bucket="cuedrive.".$companyid;
					$bucket=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;
					
                                        $activitydetails = 'renamed a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
                                        if($username!="")
		                        {
			                  $activitydetails = $username.' '.'has renamed a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
			                }					



					$logcc['deviceid'] = $deviceid;
                    $logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['action'] = 'move';
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfolderpath;
					$logcc['from']=$sourcefolderpath;
					
					
					
					
					 if($replaceflag)
						  {
						   	 //first delete the existing folder from destination folder
						   		$this->removeFolder($existingfolderid,$bucket,$companyfoldername);						   	 
						   }					
					
					//delete the source folder from amazon								
						$deletefolderarr=$this->getChildren($movefolderid);
						array_push($deletefolderarr,$movefolderid);
						
						for($i=0;$i<count($deletefolderarr);$i++)
						{
							$folderid=$deletefolderarr[$i];
							$files= $this->folderfilemodel->getFiles($folderid);
							for($j=0;$j<count($files);$j++)
							{			
								$filepath=$files[$j]['fullpath'];
								$fileid=$files[$j]['id'];
								
								//$fullpatharr=explode($sourcefoldername, $filepath);
								//$tempfullpath=$sourcefoldername.$fullpatharr[1];
								$destinationfilepath=str_replace("/".$sourcefoldername."/","/".$newfoldername."/",$filepath);
								
								//$destinationfilepath=$destinationfolderpath.'/' .$tempfullpath;
								if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
								{								
									if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
									{
										//update folderfile
									 	$datacc=array();									
									   // $datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
									    $datacc['fullpath']=$destinationfilepath;
									    $datacc['updatedbydevice']=$deviceid;
									     if($userauth)
									    {
									    	$companyuserid=$usrauthresult['companyuserid'];
									    	$datacc['updatedbyuser']=$companyuserid;
									    }	
									    			     
									   $this->folderfilemodel->updateFile($datacc,$fileid);
									}			
							}
							}
							//update folder
								 $datacc=array();
								
								// $folderpatharr=explode($sourcefoldername, $sourcefolderpath);
								// $tempfullpath=$sourcefoldername.$folderpatharr[1];
								//$tempfullpath=$sourcefolderpath;
								
								$oldpath=$this->foldermodel->getFolderPath($folderid);
								$newpath=str_replace($sourcefoldername,$newfoldername,$oldpath);
								 $datacc=array();
						 		 if($folderid==$movefolderid) //change name
								   {
									  $search="/".$sourcefoldername;
									if( ( $pos = strrpos( $oldpath , $search ) ) !== false ) { 
									$search_length  = strlen($search );
									$newpath    = substr_replace( $oldpath , "/".$newfoldername , $pos , $search_length );
									}
								   	  $datacc['foldername']=$newfoldername;
									}
									else
							{
									 $newpath=str_replace("/".$sourcefoldername."/","/".$newfoldername."/",$oldpath);
							}
							
									$datacc['fullpath']=$newpath;
								    $datacc['updatedbydevice']=$deviceid;
								     if($userauth)
								    {
								    	$companyuserid=$usrauthresult['companyuserid'];
								    	 $datacc['updatedbyuser']=$companyuserid;
								    }				     
								   $this->foldermodel->updateFolder($datacc,$folderid);															  
						}			
												   
						 
						$this->auditlogmodel->updateAuditLog($logcc);
						$this->response(array('success' => APIFOLDERMOVESUCCESS), 200);				
					
		}//duplicate file validation		
			else {
				$this->response(array('error' => APIFOLDEREXIST), 400);
			}

                       }
                       else
                       {
$this->response(array('error' => 'Permission denied'), 400);
                       }
		
		}
	}
	}
	}
		
		
	
	function fetchTermsAndConditions_get()
        {
          $this->load->model('contentmanagementmodel');
          $data = Contentmanagementmodel::getTerms();

          $this->response(array('data' => $data), 200);	
        }

        function fetchPrivacyPolicy_get()
        {
          $this->load->model('contentmanagementmodel');
          $data = Contentmanagementmodel::getPrivacy();
          $this->response(array('data' => stripslashes($data)), 200);	
        }

       function fetchHelpDocuments_get()
       {
  
          $this->load->model('contentmanagementmodel');
          $data = Contentmanagementmodel::getHelpDocuments();
          $this->response(array('data' => $data), 200);	

       }



       function createTicket_get()
       {

           
	
	// load needed models
	$this->load->model('contactusmodel');
	$this->load->model('companyusermodel');
        $this->load->model('companydevicemodel');
	
    // load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
  
	$comments = $this->get('comments');
	$type = 'support';
	$priority = $this->get('priority');
	$sent = 1;
	$status = 'open';
	$ticketid = $this->get('ticketid');
	
	if($ticketid == 0)
	{
	  $ticketid = Companyusermodel::genratePrimaryKey('20');
	}
	
	
	
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;

	
        //authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}

                        else if(array_key_exists("isadmin", $usrauthresult))
			{
                       
                                $userauth=true;
				$adminauth=1;
                                $companyid = Companyusermodel::fetchAdminID($username,md5($password));
                                $companyuserid = null;
				
			}


			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
                                $companyid = Companyusermodel::getCompanyidFromCompanyUserId($companyuserid);
				}
		}
		
		if(($username!="" || $password!="") && !$userauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	
			
                      // date_default_timezone_set('Asia/Calcutta');
		       
	
			$datacc = array();
			$datacc['comments'] = $comments;
			$datacc['type'] = $type;
			$datacc['priority'] = $priority;
			$datacc['sent'] = $sent;
			$datacc['status'] = $status;
			$datacc['ticketid'] = $ticketid;
			$datacc['companyid'] = $companyid;
			$datacc['companyuserid'] = $companyuserid;
			$datacc['email'] = '-';
			$datacc['phone'] = '-';
                        $datacc['createdon'] = date("Y-m-d H:i:s");;
			
			$insertid = Contactusmodel::createTicketFromApp($datacc);
			if($insertid > 0)
			{
				$this->response(array('success' => 'true'), 200);
			}
			else
			{
				$this->response(array('success' => 'false'), 400);
			}
			
		}
	
	} //end of if device authenticated
   }


   function fetchTicket_get()
	{
	
	// load needed models
	$this->load->model('contactusmodel');

	$this->load->model('companyusermodel');

        $this->load->model('companydevicemodel');
	

        // load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	
	$sessiontoken=$this->get('sessiontoken');

	
        //optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=0;
	
        //authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
                       
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
       
                       else if(array_key_exists("isadmin", $usrauthresult))
			{
                       
                                $userauth=true;
				$adminauth=1;
                                $companyuserid = Companyusermodel::fetchAdminID($username,md5($password));
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth)
		{
			$this->response($usrauthresult,400);
		}
		else {	

			
			$data= Contactusmodel::fetchTicketApp($companyuserid,$adminauth);
			
			
			$this->response($data, 200);
			
			
		}
	
	} //end of if device authenticated
	 
}
   
function getSerialUUID_get()
   {
      $this->load->model('alldevicesmodel');
      $deviceuuid=$this->get('deviceuuid');
      $devicetype=$this->get('devicetype');
      $serialID = Alldevicesmodel::getDeviceUUID($deviceuuid,$devicetype);
      
      $this->response(array('serialid' => $serialID), 200);
   }
   


   function getSerialUUID_post()
   {
      $this->load->model('alldevicesmodel');
      $deviceuuid=$this->post('deviceuuid');
      $devicetype=$this->post('devicetype');
      $serialID = Alldevicesmodel::getDeviceUUID($deviceuuid,$devicetype);
      
      $this->response(array('serialid' => $serialID), 200);
   }
   
   
   function releaseAllLock_get()
   {
	$this->load->model('lockfilemodel');
	$this->load->model('companydevicemodel');
	$this->load->model('folderfilemodel');
	$this->load->model('companyusermodel');
	// load the needed helpers
	$this->load->helper(array('cuedrive_common'));
	//mandatory request parameters
	$deviceid=$this->get('deviceid');
	$sessiontoken=$this->get('sessiontoken');
	//optional request parameters
	$username=($this->get('username'))?$this->get('username'):"";
	$password=($this->get('password'))?$this->get('password'):"";
	$userauth=false;
        $adminauth=false;
	$companyuserid = 0;
	
	if(Companydevicemodel::checkDeviceExpiryWithDeviceID($deviceid))
	{
		$this->response(array('error' => APIDEVDSBL), 404);
	}
	
	else {
	
	//authenticate device
	$devauthresult=$this->deviceAuth($deviceid,$sessiontoken);
	if(array_key_exists("error", $devauthresult))
	{
		 $this->response($devauthresult,400);
	}
	else { //device authenticated
		if($username!="" || $password!="")
		{
			//authenticate user
			$usrauthresult=$this->userAuth($username, $password, $deviceid);
			if(array_key_exists("error", $usrauthresult))
			{
				$userauth=false;
			}
                        else if(array_key_exists("isadmin", $usrauthresult))
			{
				$adminauth=true;
				
			}
			else {//user authenticated
				$userauth=true;
				
				$companyuserid=$usrauthresult['companyuserid'];
				}
		}
		
		if(($username!="" || $password!="") && !$userauth && !$adminauth)
		{
			$this->response($usrauthresult,400);
		}
		else {			
			// send respose   	
			$response = Lockfilemodel::releaseAllLocks($deviceid,$companyuserid);
			if($response > 0) {
			$this->response(array('success' => 'Files are unlocked successfully!'), 200);
			}
			else
			{
			$this->response(array('error' => 'Database error!'), 404); 
			}
		}
	
	} //end of if device authenticated
	}
   }
function getServerCurrentTimestamp_get()
{

$this->load->model('Companyadminmodel');
$current_timestamp = $this->Companyadminmodel->getCurretServerTimestamp();
$this->response($current_timestamp,200);

}

//****************************************************************//
//**************CRON JOB FOR NOTIFICATIONS************************//
//****************************************************************//
function cronformakingoldnotificationasread_get() {

$this->load->model('classpush');
Classpush::cronformakingoldnotificationasread();
}







//****************************************************************//
//*****************TESTING FUNCTION*******************************//
//****************************************************************//
function sendtestpush_get()
	{
	    // load the needed models

           $this->load->model('classpush');
	   $this->response( Classpush::SendTestPush());

	}
	
}
?>