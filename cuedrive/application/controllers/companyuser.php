<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companyuser extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session'); 
		$this->load->library('s3');//amazon lib
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('companyadminmodel');
		$this->load->model('packagemodel');
		$this->load->model('foldermodel');
		$this->load->model('folderfilemodel');
		$this->load->model('folderdevicesmodel');
		$this->load->model('folderusersmodel');	
		$this->load->model('companydevicemodel');
		$this->load->model('companyusermodel');	
		$this->load->model('permissionsmodel');
		$this->load->model('contactusmodel');	
		$this->load->model('auditlogmodel');
		$this->load->model('openfiremodel');	
		$this->load->model('classpush');
		$this->load->model('email');
		
		if($this->session->userdata("username")!= "")
		 {
			$username = $this->session->userdata("username");
			$isActiveAccount = Companyusermodel::validateAccountActive($username);
			if($isActiveAccount == FALSE)
			 {
				$this->session->sess_destroy();

			 }
		 }	
	}

	function index()
	{
		
		$this->load->view('CompanyUser/folderlist');
	}
	function loadFolderTree()
	{
		$companyid=$this->session->userdata("companyid");
		$companyuserid=$this->session->userdata("userid");
		$expandednodesstr=$this->input->post('expandednodesstr');
		$foldertreearr=array();
		//get folders and files for user
		$folders=$this->folderusersmodel->getFoldersForUser($companyuserid);//get folders for user
		
		
		       function multicompare($a,$b){
				   $criteria = array(
				       'type' => 'asc',
				       'label' => 'desc'
				   );
				   foreach($criteria as $what => $order){
				       if($a[$what] == $b[$what]){
				           continue;
				       }
				       return (($order == 'desc')?-1:1) * strcmp($a[$what], $b[$what]);
				   }
			       return 0;
			    }
			     
	
		for($i=0;$i<count($folders);$i++)
		{
			$folderid=$folders[$i]['folderid'];
			$folderdetails=$this->foldermodel->getFolderDetails($folderid);
			$parentid=$folderdetails['parentfolderid'];
			if($parentid==null)
				$parentid=0;
		
			$foldertemparr=array();
			$foldertemparr['id']=$folderid;
			$foldertemparr['label']=$folderdetails['foldername'];
			$foldertemparr['parentfolderid']=$parentid;
			$foldertemparr['updatedon']=date('d/m/Y g:i a',strtotime($folderdetails['updatedon']));;
			$foldertemparr['type']='folder';
			$foldertemparr['accesstype']=$folderdetails['type'];
			$foldertemparr['htmlid']=$folderid;
			$permissions=$this->folderusersmodel->getPermissions($folderid,$companyuserid);
			
			$foldertemparr['permissions']=$permissions;	
			array_push($foldertreearr, $foldertemparr);
			
			$files= $this->folderfilemodel->getFiles($folderid);
			for($j=0;$j<count($files);$j++)
			{
				$foldertemparr=array();
				$foldertemparr['id']=$files[$j]['id'];
				$foldertemparr['label']=$files[$j]['filename'];
				$foldertemparr['parentfolderid']=$folderid;
				$foldertemparr['updatedon']=date('d/m/Y g:i a',strtotime($files[$j]['updatedon']));
				$foldertemparr['type']='file';
				$foldertemparr['accesstype']=$folderdetails['type'];
				$foldertemparr['permissions']=$permissions;	
				array_push($foldertreearr, $foldertemparr);								
				}
	
			 usort($foldertreearr, "multicompare");
		}
		
		
		//print_r($foldertreearr);
		$foldertree= $this->buildFolderTree($foldertreearr);
		$data = array();	
	//print_r($foldertree);
	$flatfolderarr=$this->objectArray_flatten($foldertree,"children");
	$flatfolderarr=array_reverse($flatfolderarr);
	$data['foldertreearr']=	$flatfolderarr;		
	$data['expandednodes']=	$expandednodesstr;	
	//echo json_encode($data);
	
        $this->load->view('CompanyUser/foldertreetable',$data);	
	}

	
function addFolder()
	{
		$parentfolderId		=$this->input->post('parentfolderid');
		$newfoldername		=trim($this->input->post('newfoldername'));
		$level				=trim($this->input->post('level'));
		if(!$this->foldermodel->foldernameexist($newfoldername,$parentfolderId))
			{
				$companyid			= $this->session->userdata("companyid");
				//$bucketname="cuedrive.".$companyid;
				$bucketname			= ROOTBUCKET;
				$companyfoldername	= COMPANYFOLDERNAME.$companyid;		
				$parentdetails		= $this->foldermodel->getFolderDetails($parentfolderId);
				$path				= $parentdetails['fullpath'];
				$parenttype			= $parentdetails['type'];
				$newpath			= $path."/".$newfoldername;
				$uploaddir 			= realpath('./uploads/');
				$dummyfile 			= $uploaddir ."//".'dummyfile.txt';
				$file				= fopen($dummyfile,"w");
				if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
					{
						//successfully created
						//update DB
						$datacc 				= array();
						$datacc['foldername']	= $newfoldername;
						$datacc['companyid']	= $companyid;
						$datacc['fullpath']		= $newpath;
						$datacc['type']			= $parenttype; 
						$datacc['parentfolderid']= $parentfolderId; 
						$datacc['createdby']	= $companyid;
						$datacc['createdon']	= date('Y-m-d H:i');				
						$newfolderid			= $this->foldermodel->insertFolder($datacc);	
						$this->addParentPermissions($parentfolderId,$newfolderid);						   
						$username 		= ($this->session->userdata("username")) ? $this->session->userdata("username") : NULL;
						$companyuserid 	= ($this->session->userdata("userid")) ? $this->session->userdata("userid") : 0;							
						$activitydetails= $username.' '.'has created a folder'.' '.'cuedrive://'.$newpath;
						$logcc['deviceid'] 			= 0;
						$logcc['ipaddress'] 		= $_SERVER['REMOTE_ADDR'];
						$logcc['action'] 			= 'create';
						$logcc['activitydetails'] 	= $activitydetails;
						$logcc['companyuserid'] 	= $companyuserid;
						$logcc['companyid'] 		= Companyusermodel::getCompanyidFromCompanyUserId($companyuserid);
						$logcc['to']  				= '-';
						$logcc['from']  			= '-';							
						$this->auditlogmodel->updateAuditLog($logcc);
						echo "1";
				}else {
					echo CREATEFOLDERFAIL;
				}
			
		}
		else
		{
			echo FOLDERNAMEEXIST;
		}
			
	}
	
function addParentPermissions($parentfolderid,$newfolderid)
	{
		$companyid=$this->session->userdata("companyid");
		$devicepermissions=$this->folderdevicesmodel->getFolderPermissions($parentfolderid);
		for($i=0;$i<count($devicepermissions);$i++)
		{
					 	$datacc = array();
					  	 $permissionid=$devicepermissions[$i]['permissionid'];
					     $datacc['deviceid']=$devicepermissions[$i]['deviceid'];
						 $datacc['folderid']=$newfolderid;
						 $datacc['permissionid']=$permissionid;
						 $datacc['createdby']=$companyid;
						 $datacc['updatedby']=$companyid;
						 $datacc['createdon']=date('Y-m-d H:i');	
						 $this->folderdevicesmodel->insertFolderDevice($datacc);
		}
		
		$userpermissions=$this->folderusersmodel->getUserPermissions($parentfolderid);
		for($i=0;$i<count($userpermissions);$i++)
		{
					 	$datacc = array();
					  	 $permissionid=$userpermissions[$i]['permissionid'];
					     $datacc['companyuserid']=$userpermissions[$i]['companyuserid'];
						 $datacc['folderid']=$newfolderid;
						 $datacc['permissionid']=$permissionid;
						 $datacc['createdby']=$companyid;
						 $datacc['updatedby']=$companyid;
						 $datacc['createdon']=date('Y-m-d H:i');	
						 $this->folderusersmodel->insertFolderUser($datacc);
		}
		
	}
function uploadFile()
	{
		$uploadsuccess		=	false;
		$parentfolderId		=	$this->input->post('parentfolderid');
		$companyid			=	$this->session->userdata("companyid");
		//$bucketname="cuedrive.".$companyid;
		$bucketname			=	ROOTBUCKET;
		$companyfoldername	=	COMPANYFOLDERNAME.$companyid;
		$companypackagesize	=	$this->companyadminmodel->getPackageSize($companyid);//in GB
		$companypackagesize	=	1073741824*(str_replace("GB","",$companypackagesize)); //in bytes
		//$bucketsize=$this->getBucketSize($bucketname);//in bytes
		$bucketsize			=	$this->getCompanyFolderSize($companyid);//in bytes
		foreach($_FILES['selectedfiles']['tmp_name'] as $key => $tmp_name)
		{
			$fileSize	=	$_FILES["selectedfiles"]["size"][$key];
			$bucketsize	=	$bucketsize+$fileSize;
		}	
		if($bucketsize<=$companypackagesize)
		{
			foreach($_FILES['selectedfiles']['tmp_name'] as $key => $tmp_name)
			{
				 $fileName 		= $_FILES['selectedfiles']['name'][$key];	
				 $fileTempName 	= $_FILES['selectedfiles']['tmp_name'][$key];
				 $fileSize		= $_FILES["selectedfiles"]["size"][$key];		
			
				$uploadfolderpath=$this->foldermodel->getFolderPath($parentfolderId);
				$foldername		= $uploadfolderpath;
				if($this->s3->putObjectFile($fileTempName, $bucketname, $companyfoldername.'/'.$foldername.'/' .$fileName, S3::ACL_PRIVATE))
					{
						//file uploaded successfullly
						//update DB					
						$datacc				= array();
						$datacc['filename']	= $fileName;
						$datacc['fullpath']	= $foldername.'/' .$fileName;
						$datacc['folderid']	= $parentfolderId;
						$datacc['filesize']	= $fileSize;			   	
		
						if($this->input->post('override')=="true" && $this->folderfilemodel->fileNameExist($fileName,$parentfolderId))//replace 
						{
							 $datacc['updatedbycompany']	= $companyid;	
							 $replacefileid					= $this->folderfilemodel->getfileID($fileName,$parentfolderId);     
							 $this->folderfilemodel->updateFile($datacc,$replacefileid);		
						}
						else { //add new
							 	$datacc['createdbycompany']	= $companyid;
							 	$datacc['createdon']		= date('Y-m-d H:i');			     
								$newfileid					= $this->folderfilemodel->insertFile($datacc);
						}					
						
						$username 		= ($this->session->userdata("username")) ? $this->session->userdata("username") : NULL;
						$companyuserid 	= ($this->session->userdata("userid")) ? $this->session->userdata("userid") : 0;
						$logcc['companyuserid'] = $companyuserid;
						$logcc['companyid'] 	= Companyusermodel::getCompanyidFromCompanyUserId($companyuserid);
						$activitydetails 		= $username.' '.'has uploaded a file into'.' '.'cuedrive://'.$foldername.'/' .$fileName;
						$logcc['deviceid'] 			= 0;
						$logcc['ipaddress'] 		= $_SERVER['REMOTE_ADDR'];
						$logcc['action'] 			= 'upload';
						$logcc['activitydetails'] 	= $activitydetails;
						$logcc['to']  				= '-';
						$logcc['from']  			= '-';
						
						$this->auditlogmodel->updateAuditLog($logcc);
						
						Classpush::sendFileUploadNotification($parentfolderId,'upload',$fileName);				 
						$uploadsuccess	=	true;	
					}	
					else {
							$uploadsuccess	=	false;
							
					}				
			}
		}//end of if size validation
		else {
					echo "error:".APIFILESIZEEXCEED;
		}
		
		if($uploadsuccess==true)
		{
			echo "success";
		}
		else {
			echo "error:".APIFILEUPLOADFAIL;
		}
		
		
	}
function checkFileExist()
{
	 $parentfolderId=$this->input->post('parentfolderid');
	 //$newfilename=$this->input->post('newfilename');
	 $filenamestr=$this->input->post('filenamestr');
	 $filenamearr=explode("@#", $filenamestr);
	 $found="";
	 for($i=0;$i<count($filenamearr);$i++)
	 {
	 	if(trim($filenamearr[$i])!="")
	 	{
			 if($this->folderfilemodel->fileNameExist($filenamearr[$i],$parentfolderId))
			 {
			 	//echo "0";
			 	if($found=="")
		    	  $found=$filenamearr[$i];	
		      	else
		      	  $found=$found." and ".$filenamearr[$i];
			 }

	 	}
	 }
	 
	 echo $found;

}


function downloadFile()
{
	$selectedfileid=$this->input->post('selectedfileid');
    $companyid=$this->session->userdata("companyid");
	//$bucketname="cuedrive.".$companyid;
	$bucketname=ROOTBUCKET;
	$companyfoldername=COMPANYFOLDERNAME.$companyid;

	$filepath=$this->folderfilemodel->getFilePath($selectedfileid);
	 //get authenticated url	
	$newurl=$this->s3->getAuthenticatedURL($bucketname, $companyfoldername.'/'.$filepath, DOWNLOADURLEXPIRYTIME);
	 $newurl=str_replace('amp;', '', $newurl);
	echo $newurl;
}

function deleteFile()
{
	$selectedfileid=$this->input->post('selectedid');
    $companyid=$this->session->userdata("companyid");
	//$bucketname="cuedrive.".$companyid;
	$bucketname=ROOTBUCKET;
	$companyfoldername=COMPANYFOLDERNAME.$companyid;
	$filepath=$this->folderfilemodel->getFilePath($selectedfileid);
	
			//delete the file
			if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
			{
				//update the database
				$this->folderfilemodel->deleteFile($selectedfileid);
				
				    $username = ($this->session->userdata("username")) ? $this->session->userdata("username") : NULL;
				    $companyuserid = ($this->session->userdata("userid")) ? $this->session->userdata("userid") : 0;
				    $logcc['companyuserid'] = $companyuserid;
				    $logcc['companyid'] = Companyusermodel::getCompanyidFromCompanyUserId($companyuserid);
				    $activitydetails = $username.' '.'has deleted a file'.' '.'cuedrive://'.$filepath;
				    $logcc['deviceid'] = 0;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				    $logcc['action'] = 'delete';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']  = '-';
				    $logcc['from']  = '-';
				    
				    $this->auditlogmodel->updateAuditLog($logcc);
				
				
				echo "1";		
			}
			else {
				echo DELETEFAIL;
			}
}

function deleteFolder()
{
	$selectedfolderid=$this->input->post('selectedid');
	//for testing
	//$selectedfolderid=8;
    $companyid=$this->session->userdata("companyid");
	//$bucketname="cuedrive.".$companyid;
	$bucketname=ROOTBUCKET;
	$companyfoldername=COMPANYFOLDERNAME.$companyid;
	$folderpath=$this->foldermodel->getFolderPath($selectedfolderid);
	$deletefolderarr=$this->getChildren($selectedfolderid);
	array_push($deletefolderarr,$selectedfolderid);
	 rsort($deletefolderarr);
	//print_r($deletefolderarr);
	for($i=0;$i<count($deletefolderarr);$i++)
	{
		$folderid=$deletefolderarr[$i];
		$files= $this->folderfilemodel->getFiles($folderid);
		for($j=0;$j<count($files);$j++)
		{
			$filepath=$files[$j]['fullpath'];
			$fileid=$files[$j]['id'];
			if($this->s3->deleteObject($bucketname, $companyfoldername.'/'.$filepath))
			{
					//delete from folderfile
				$this->folderfilemodel->deleteFile($fileid);
			}			
		}
					
				//update the database				
				
				//delete from folderalloweddevices
				$this->folderdevicesmodel->deleteByFoler($folderid);				
				
				//delete from folderallowedusers
				$this->folderusersmodel->deleteByFoler($folderid);	
				
				//delete from folder
				$this->foldermodel->deleteFolder($folderid);
				
				
				
				
				
				    $username = ($this->session->userdata("username")) ? $this->session->userdata("username") : NULL;
				    $companyuserid = ($this->session->userdata("userid")) ? $this->session->userdata("userid") : 0;
				    $logcc['companyuserid'] = $companyuserid;
				    $logcc['companyid'] = Companyusermodel::getCompanyidFromCompanyUserId($companyuserid);
				    $activitydetails = $username.' '.'has deleted a folder'.' '.'cuedrive://'.$folderpath;
				    $logcc['deviceid'] = 0;
				    $logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				    $logcc['action'] = 'delete';
				    $logcc['activitydetails'] = $activitydetails;
				    $logcc['to']  = '-';
				    $logcc['from']  = '-';
				    
				    $this->auditlogmodel->updateAuditLog($logcc);
				
				
				
				
				
						
		
	}
	echo "1";
}
function buildFolderTree(array &$elements, $parentId = 0,$htmlid=0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parentfolderid'] == $parentId) {
        	
        	if($htmlid!=0)
        		$newhtmlid=$htmlid."-".$element['id'];
        	else
        		$newhtmlid=$element['id'];
        	
        	$element['htmlid']=$newhtmlid;
        	//$element['htmlid']=$htmlid."-".$element['id'];
        	$element['htmlparentid']=$htmlid;
        	if($element['type']=="folder")
        	{
        		
	            $children = $this->buildFolderTree($elements, $element['id'],$newhtmlid);
	            if ($children) {
	                $element['children'] = $children;
	            }
        	}
        	
            $branch[$element['id']] = $element;
 
            }
    }
    return $branch;
}


function getChildren($parent_id) {

    $tree = Array();   
    if (!empty($parent_id)) {
        $tree = $this->foldermodel->getOneLevel($parent_id);
        foreach ($tree as $key => $val) {
            $ids = $this->getChildren($val);
            $tree = array_merge($tree, $ids);
        }
    }
    return $tree;

}

function remove_json_keys($array) {
  foreach ($array as $k => $v) {
    if (is_array($v)) {
        $array[$k] = $this->remove_json_keys($v);
    } //if
  } //foreach
  return $this->sort_numeric_keys($array);

}

function sort_numeric_keys($array) {
    $i=0;
    $rtn=array();
    foreach($array as $k => $v) {
        if(is_int($k)) {
            $rtn[$i] = $v;
            $i++;
        } else {
            $rtn[$k] = $v;
        } //if
    } //foreach
    return $rtn;
}

function objectArray_flatten($array,$childField) {
        $result = array();

        foreach ($array as $node)
        {
           
            if(isset($node[$childField]))
            {
                $result = array_merge($result,$this-> objectArray_flatten($node[$childField],$childField));
                unset($node[$childField]);
            }
			 $result[] = $node;
        }
        return $result;
    }
	
function getBucketSize($bucket)
	{
		// Load Library
 	 	$this->load->library('s3');
 	 	$resultarr=$this->s3->getBucket($bucket);
 	 	$totalsize=0;
 	 	foreach($resultarr as  $arr){    
   		   	$totalsize=$totalsize+$arr['size'];
 	 	  	}
 	 	  	return $totalsize;	
	}
	
function getCompanyFolderSize($companyid)
{
	return  Folderfilemodel::getTotalfileStored($companyid);
}
function loadChat()
{
  $this->load->view('CompanyUser/chat');
}




function updateuserprofile()
{
  $userid = $_REQUEST['userid'];
  $username = $_REQUEST['username'];
  $email = $_REQUEST['email'];
  $password = $_REQUEST['password'];
  
  if(($email && $username && $password)!='')
   { 
	  
	  if(Companyusermodel::userExistsWithUsername($username,$userid) == 0)
	  {
		 if(Companyusermodel::userExistsWithEmail($email,$userid) == 0 )
		 {
			$userdetails = Companyusermodel::getUserDetails($userid);
			$oldusername = $userdetails['username'];
			$sql = "update companyuser set email = '{$email}',username = '{$username}',password = '".md5($password)."' where companyuserid={$userid}";
			$this->db->query($sql);
			$firstname = $userdetails['firstname'];
			$companyid = $userdetails['companyid'];
			$user_password = Openfiremodel::generateRandomToken();
			$url = ORG_URL."chat/?type=update&oldusername=".urlencode($oldusername)."&username=".urlencode($username)."&password=".urlencode($user_password)."&name=".urlencode($firstname)."&group=".urlencode($companyid);
			Openfiremodel::sendDataToServer($url); 
			echo "1";
		 }
		 else
		 {
		   echo "This email already exists";
		 }
	  
	  
	}else{
			 echo "This username already exists";
		  }
		  
    }else{
   			echo "Please complete the required fields";
    	 }
}


function postmax()
{
 Email::sendTestEmail('debartha','debartha.nandi@teks.co.in','password');
}



function createDir()
	{
		
		$parentfolderId=$this->input->post('parentfolderid');		
		$companyid = $this->session->userdata("companyid");
		if(Permissionsmodel::checkValidFolder($parentfolderId,$companyid) == 1) {		
		$newfoldername=(trim($this->input->post('newfoldername')));
		$level=trim($this->input->post('level'));
		$permissionflag=$this->input->post('permflag');
		if(!$this->foldermodel->foldernameexist($newfoldername,$parentfolderId))
		{
		$companyid=$this->session->userdata("companyid");
		//$bucketname="cuedrive.".$companyid;
		$bucketname=ROOTBUCKET;
		$companyfoldername=COMPANYFOLDERNAME.$companyid;
		$companyfolder=
		$parentdetails=$this->foldermodel->getFolderDetails($parentfolderId);
		$path=$parentdetails['fullpath'];
		$parenttype=$parentdetails['type'];
		$newpath=$path."/".$newfoldername;
		$uploaddir = realpath('./uploads/');
		$dummyfile = $uploaddir ."//".'dummyfile.txt';
			$file=fopen($dummyfile,"w");
			if($this->s3->putObjectFile($dummyfile, $bucketname, $companyfoldername.'/'.$newpath.'/dummyfile.txt', S3::ACL_PRIVATE))
			{
				//successfully created
				//update DB
				$datacc = array();
				$datacc['foldername']=$newfoldername;
				$datacc['companyid']=$companyid;
				$datacc['fullpath']=$newpath;
				$datacc['type']=$parenttype; 
				$datacc['parentfolderid']=$parentfolderId; 
				$datacc['createdby']=$companyid;
				$datacc['createdon']=date('Y-m-d H:i');				
				$newfolderid=$this->foldermodel->insertFolder($datacc);	
				if($permissionflag=="parent")
				{
					$this->addParentPermissions($parentfolderId,$newfolderid);
				}
				
				echo $newfolderid;
			}
			else {
				echo "createfolderfail";
			}
			
		}
		else
		{
			echo "foldernameexists";
		}
		}
		else {
			echo "createfolderfail";
		}
		
	}


function getrandonm()
{
	echo Openfiremodel::generateRandomToken();
}

function getFolderPermission()
{
	$mainFloderId=$this->input->post('mainFloderId');
	$query = $this->db->get_where('folderallowedusers',array('folderid' => $mainFloderId));
	//$data = $query->row_array();
	//$permissionid = $data['permissionid'];
	$perResult = 2;
    foreach($query->result_array() as $row)
	{
    	$permissionid = $row['permissionid'];
		if($permissionid != 4)
		{
			$perResult = 1;
		}
	}
	echo $perResult;
}

function moveFile(){

	// Load Library
	$this->load->library('s3');
	$movefileid=$_REQUEST['movefileid'];
	$destinationfolderid=$_REQUEST['destinationfolderid'];
	$companyid=$_REQUEST['companyid'];
	
	 { //device authenticated
		
		 {
			//move file
				
			$replaceflag=false;
			$fileName=$this->folderfilemodel->getFileName($movefileid);
				
			if($this->isFileExist($fileName, $destinationfolderid) )
			{
				$existingfileid=$this->folderfilemodel->getfileID($fileName,$destinationfolderid);
			
					$uploadflag=false;
				
			}
			else {
				$uploadflag=true;
			}

			if($uploadflag)
			{
				$sourcefilepath=$this->folderfilemodel->getFilePath($movefileid);
				$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
				$destinationfilepath=$destinationfolderpath."/".$fileName;
				//$bucket="cuedrive.".$companyid;
				$bucket=ROOTBUCKET;
				$companyfoldername=COMPANYFOLDERNAME.$companyid;
				
				if($this->s3->copyObject($bucket, $companyfoldername.'/'.$sourcefilepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
				{
						
                   /**
					$activitydetails = 'moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					if($username!="")
					{
						$activitydetails = $username.' '.'has moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					}
					$logcc['deviceid'] = $deviceid;
					$logcc['companyid']=Companydevicemodel::getCompanyidFromDeviceId($deviceid)	;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['action'] = 'move';
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfilepath;
					$logcc['from']=$sourcefilepath;
						
					*/	
						
						
						
					//delete the file from sorce folder
					$this->s3->deleteObject($bucket, $companyfoldername.'/'.$sourcefilepath);
					//update DB
					$datacc=array();
					$datacc['fullpath']=$destinationfolderpath.'/' .$fileName;
					$datacc['folderid']=$destinationfolderid;
					$datacc['updatedbydevice']=NULL;
					
				  /**
					if($userauth)
					{
						$companyuserid=$usrauthresult['companyuserid'];
						$datacc['updatedbyuser']=$companyuserid;
						$logcc['companyuserid'] = $companyuserid;
					}
					*/
					
					$this->folderfilemodel->updateFile($datacc,$movefileid);
						
					if($replaceflag)
					{
						//delete the existing file from destination folder
						$this->folderfilemodel->deleteFile($existingfileid);
							
					}
					
					$activitydetails = 'moved a file'.' '.'cuedrive://'.$sourcefilepath.' '.'to'.' '.'cuedrive://'.$destinationfilepath;
					$logcc=array();
					$logcc['companyuserid'] = 0;
					$logcc['action'] = 'move';
					$logcc['deviceid'] = 0;
					$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$logcc['activitydetails'] = $activitydetails;
					$logcc['to']=$destinationfilepath;
					$logcc['from']=$sourcefilepath;
					$logcc['companyid']= $companyid;
					
					
					$this->auditlogmodel->updateAuditLog($logcc);
					//$this->response(APIFILEMOVESUCCESS,200);
					echo '1';

				}
				else {
					echo '0';
				}
			}//duplicate file validation
			else {
				echo 'fileExists'; exit;
			}
		}
	}
}

function isFileExist($newfilename,$parentfolderid)
{
	 if($this->folderfilemodel->fileNameExist($newfilename,$parentfolderid))
	 {
	 	return true;
	 }
	 else {
	 	return false;
	 }
}



function moveFolder()
{
	// Load Library
	$this->load->library('s3');
	$movefolderid=$_REQUEST['movefolderid'];
	$destinationfolderid=$_REQUEST['destinationfolderid'];
	$companyid=$_REQUEST['companyid'];
    {
		{
			
				//move folder
				$replaceflag=false;
				$folderdetails=$this->foldermodel->getFolderDetails($movefolderid);
				$sourcefoldername=$folderdetails['foldername'];
				if($this->isFolderExist($sourcefoldername, $destinationfolderid) )
				{
					$existingfolderid=$this->foldermodel->getfolderID($sourcefoldername,$destinationfolderid);
					
					{
						$uploadflag=false;
					}
				}
				else {
					$uploadflag=true;
				}
					
				if($uploadflag)
				{
					$sourcefolderpath=$this->foldermodel->getFolderPath($movefolderid);
					$destinationfolderpath=$this->foldermodel->getFolderPath($destinationfolderid);
					//$bucket="cuedrive.".$companyid;
					$bucket=ROOTBUCKET;
					$companyfoldername=COMPANYFOLDERNAME.$companyid;				
						
					//delete the source folder from amazon
					$deletefolderarr=$this->getChildren($movefolderid);
					array_push($deletefolderarr,$movefolderid);
	
					for($i=0;$i<count($deletefolderarr);$i++)
					{
					$folderid=$deletefolderarr[$i];
					$files= $this->folderfilemodel->getFiles($folderid);
					for($j=0;$j<count($files);$j++)
					{
					$filepath=$files[$j]['fullpath'];
					$fileid=$files[$j]['id'];
	
					$fullpatharr=explode($sourcefoldername, $filepath);
					$tempfullpath=$sourcefoldername.$fullpatharr[1];
					$destinationfilepath=$destinationfolderpath.'/' .$tempfullpath;
					if($this->s3->copyObject($bucket, $companyfoldername.'/'.$filepath, $bucket, $companyfoldername.'/'.$destinationfilepath,S3::ACL_PRIVATE))
					{
					if($this->s3->deleteObject($bucket, $companyfoldername.'/'.$filepath))
					{
						//update folderfile
						$datacc=array();
							
						$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
						$datacc['updatedbydevice']=NULL;
		
						$this->folderfilemodel->updateFile($datacc,$fileid);
					}
					}
					}
					//update folder
					$datacc=array();
					$folderpatharr=explode($sourcefoldername, $sourcefolderpath);
					$tempfullpath=$sourcefoldername.$folderpatharr[1];
					$datacc=array();
					if($folderid==$movefolderid) //change parent id
					{
					$datacc['parentfolderid']=$destinationfolderid;
					$datacc['type'] = $this->foldermodel->getFolderType($destinationfolderid);
			}
			$datacc['fullpath']=$destinationfolderpath.'/' .$tempfullpath;
			$datacc['updatedbydevice']=NULL;
	
			$this->foldermodel->updateFolder($datacc,$folderid);
		}
			$activitydetails = 'moved a folder'.' '.'cuedrive://'.$sourcefolderpath.' '.'to'.' '.'cuedrive://'.$destinationfolderpath;
			$logcc=array();
			$logcc['companyuserid'] = 0;
			$logcc['action'] = 'move';
			$logcc['deviceid'] = 0;
			$logcc['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$logcc['activitydetails'] = $activitydetails;
			$logcc['to']=$destinationfolderpath;
			$logcc['from']=$sourcefolderpath;
			$logcc['companyid']= $companyid;
			
			$this->auditlogmodel->updateAuditLog($logcc);
			echo '1';
	
		}//duplicate file validation
	else {
		echo '0';
	}
	
		}
   }
}
	
}
	?>