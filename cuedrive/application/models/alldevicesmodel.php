<?php
class Alldevicesmodel extends CI_Model {

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	// -------------------------------------------------------------------------------------------------	
	function genratePrimaryKey($serverid)
    {
		$primarykey=$serverid.time().mt_rand(1000, 9999);
		
		if($primarykey > 9223372036854775807) //max of 64 bit int
		{
			genratePrimaryKey($serverid);
		}
		return $primarykey;
	}
	// -------------------------------------------------------------------------------------------------        
	public function generateRandomPin()
    {
   		$length   = 6;
        $pin      = "";
        $possible = "ABCDEFGHIJKLMNOPQRSTWXYZ";
        
		// we refer to the length of $possible a few times, so let's grab it now
        $maxlength = strlen($possible);
        
		// check for length overflow and truncate if necessary
        if ($length > $maxlength)
        {
            $length = $maxlength;
        }
        
		// set up a counter for how many characters are in the password so far
        $i = 0;
        
		// add random characters to $password until $length is reached
        while ($i < $length)
        {
       		// pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);
               
            // have we already used this character in $password?
            if (!strstr($pin, $char)) 
			{
            	// no, so it's OK to add it onto the end of whatever we've already got...
                $pin .= $char;
                
				// ... and increase the counter by one
                $i++;
            }
        }
		return $pin;
    }
	// -------------------------------------------------------------------------------------------------    
    function deviceExistsInAlldevices($deviceuuid)
    {
    	$query = $this->db->get_where('alldevices', array('deviceuuid' => $deviceuuid));
     	$data = $query->row_array();
     
	 	return $data;
    }
	// -------------------------------------------------------------------------------------------------    
    function addDeviceIntoAlldevices($deviceuuid)
    {
    	$query = $this->db->get_where('alldevices', array('deviceuuid' => $deviceuuid));
     	$data = $query->row_array();
     
	 	return $data;
    }
	// -------------------------------------------------------------------------------------------------    
    function getDeviceUUID($deviceuuid,$devicetype)
    {
    	$info = Alldevicesmodel::deviceExistsInAlldevices($deviceuuid);
     	if(count($info) > 0)
     	{
        	$serialuuid = $info['serialuuid'];
         	return $serialuuid;
     	}
     	else
     	{
			$rand_1 = Alldevicesmodel::generateRandomPin();
        	$rand_2 = Alldevicesmodel::genratePrimaryKey(60);
        	$serialuuid = $rand_2.''.$rand_1;
        	
			$data = array(
	   			'deviceuuid' => $deviceuuid ,
	   			'serialuuid' => $serialuuid ,
	   			'devicetype' => $devicetype);

			$this->db->insert('alldevices', $data); 
			$id = $this->db->insert_id();
			if($id > 0)
			{
	  			return $serialuuid;
			} 
     	}
    }
	// -------------------------------------------------------------------------------------------------    
    function serialIDExists($deviceuuid)
    {
     	$query = $this->db->get_where('alldevices', array('serialuuid' => $deviceuuid));
     
	 	return $query->num_rows();
    }
 }
?>