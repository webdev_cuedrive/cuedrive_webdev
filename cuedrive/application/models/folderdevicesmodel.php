<?php
class Folderdevicesmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function getFoldersForDevice($deviceid)
	{
		$this->db->select('folderid');
		$this->db->where('deviceid', $deviceid);
		$query = $this->db->get('folderalloweddevices');
		return $query->result_array();
	}
	
	function getPermissions($folderid,$deviceid)
	{
		$this->db->select('folderalloweddevices.permissionid,permissions.name');
		$this->db->from('folderalloweddevices');
		$this->db->join('permissions', 'folderalloweddevices.permissionid= permissions.permissionid');
		$this->db->where('folderid', $folderid);	
		$this->db->where('deviceid', $deviceid);
		$query = $this->db->get();
		return $query->result_array();
	}
	function deleteByFoler($folderid)
	{
			$this->db->where('folderid', $folderid);;
      		$query = $this->db->delete('folderalloweddevices');
      		return $this->db->affected_rows();
	}
	function insertFolderDevice($datacc)
	{
		 $this->db->insert('folderalloweddevices', $datacc);
		return $this->db->affected_rows();
	}
	
	function getFolderPermissions($folderid)
	{
		$this->db->select('*');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folderalloweddevices');
		return $query->result_array();
	}
	
	function deleteByDevice($deviceid)
	{
			$this->db->where('deviceid', $deviceid);
      		$query = $this->db->delete('folderalloweddevices');
      		return $this->db->affected_rows();
	}
	
	function getAllNotifications($deviceToken)	{
		$this->db->where('device_token', $deviceToken);
		$query = $this->db->get('notificationlogs');
			
		if($query->num_rows() > 0)	{
			return $query->result_array();
		}	else	{
			return false;
		}		
	}
	
	function updateTheNotificationStatus($id, $data)	{

		$this->db->where('notification_logid', $id);
		$query = $this->db->update('notificationlogs', $data);			
	}
	
}
?>