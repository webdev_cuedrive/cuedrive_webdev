<?php

class Companydevicemodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
		$this->load->model('permissionsmodel');
	}

function deviceLogin($deviceuuid) {	
			if ( $this->deviceExists($deviceuuid) == TRUE )
			{
				if ( $this->deviceActive($deviceuuid) == TRUE )
				{
					$this->db->select("*");
					$this->db->where('deviceuuid', $deviceuuid);
					$query = $this->db->get('companydevice');	
					return $query->row_array();
				}
				else {
					return "notactive";
				}
			}
			else
			{
				return "notfound";
			}
		}
		
function deviceExists($deviceuuid){		
		$this->db->select('deviceuuid');
		$this->db->where('deviceuuid', $deviceuuid);
		$result =  $this->db->get('companydevice');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
function deviceActive($deviceuuid)
{
		$this->db->select('deviceuuid');
		$this->db->where('deviceuuid', $deviceuuid);
		$this->db->where('isactive', 'yes');
		$result =  $this->db->get('companydevice');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
}

function deviceActiveByID($deviceid)
{
		$this->db->select('deviceid');
		$this->db->where('deviceid', $deviceid);
		$this->db->where('isactive', 'yes');
		$result =  $this->db->get('companydevice');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
}

function deviceAuth($deviceid,$sessiontoken)
{	
	if ( $this->deviceActiveByID($deviceid) == TRUE )
	{
			$cd=date('Y-m-d H:i');
			$this->db->select('*');
			$this->db->where('TIMESTAMPDIFF(HOUR,lastloggedon,\''.$cd.'\')<=',24);	
			$this->db->where('deviceid', $deviceid);
			$this->db->where('sessiontoken', $sessiontoken);	
			$result =  $this->db->get('companydevice');
			return ($result->num_rows() == 1) ? $result->row_array() : "expired";
	}
	else
	{
		return "notactive";
	}
	
}
function updateCompanyDevice($datacc,$deviceid)
	{			
		if ($deviceid != 0 ) { //update
			
				$this->db->where('deviceid', $deviceid);
				$this->db->update('companydevice', $datacc);
			
			
			
			
		}
		else // insert
		{
		    $this->db->insert('companydevice', $datacc);
		     $deviceid = $this->db->insert_id();
		}
		return $deviceid;
		//return $this->db->affected_rows();
	} 
	
function getSessionToken($deviceid)
{
		$this->db->select('sessiontoken');
		$this->db->where('deviceid', $deviceid);
		$query = $this->db->get('companydevice');
		$query = $query->row();
		return $query->sessiontoken;
}
	
function getDevicesByCompany($companyid)
{
		$this->db->select('*');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companydevice');
		return $query->result_array();
}
	
function getAllDevices($num, $offset,$companyid,$searchtext)
{
		$this->db->select('*');
		$this->db->like('deviceuuid', $searchtext);
		$this->db->where('companyid', $companyid);
		$this->db->limit($num, $offset);
		$query = $this->db->get('companydevice');
		return $query->result_array();
}
function getDeviceCount($companyid,$searchtext)
	{
		$this->db->like('deviceuuid', $searchtext);
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companydevice');
		return $query->num_rows();		
	}	

function getDeviceDetails($deviceid)
	{
		$this->db->select('*');
		$this->db->where('deviceid', $deviceid);
		$result = $this->db->get('companydevice');
		return $result->row_array();
	}
	
function deleteDevice($deviceid)
	{
		
		$companyid = $this->session->userdata("companyid");
		if(Permissionsmodel::checkValidDevice($deviceid,$companyid) == 1) {
		
		$this->db->where('deviceid', $deviceid);
      	$this->db->limit(1,0);
      		$query = $this->db->delete('companydevice');
      		return $this->db->affected_rows();
		}
	}
	
	function deleteDeviceFromSiteOwner($deviceid, $companyid)
	{
		if(Permissionsmodel::checkValidDevice($deviceid,$companyid) == 1) {
		
		$this->db->where('deviceid', $deviceid);
      	$this->db->limit(1,0);
      		$query = $this->db->delete('companydevice');
      		return $this->db->affected_rows();
		}
	}
	
	function deviceExistsWithID($deviceuuid,$deviceid){		
		$this->db->select('deviceuuid');
		$this->db->where('deviceuuid', $deviceuuid);
		$this->db->where('deviceid !=', $deviceid);
		$result =  $this->db->get('companydevice');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function changeStatus($deviceid)
	{
		$companyid = $this->session->userdata("companyid");
		if(Permissionsmodel::checkValidDevice($deviceid,$companyid) == 1) {
		$this->db->query("UPDATE companydevice SET isactive=(SELECT CASE isactive WHEN 'yes' THEN 'no' ELSE 'yes' END) WHERE deviceid='$deviceid'"); 
		}
	}
	
	
	/**
	 * Added by Debartha
	 * @param unknown_type $companyid
	 */
	public function fetchDeviceCount($companyid)
	{
		$sql = "select * from companydevice where companyid = {$this->db->escape($companyid)}";
		$query = $this->db->query($sql);
		return $query->num_rows();
	
	}
	
	
	public function getCompanyidFromDeviceId($deviceid)
	{
	  $sql = "select companyid from companydevice where deviceid = {$this->db->escape($deviceid)}";
          $query = $this->db->query($sql);
          $query = $query->row();
          return $query->companyid;
	
	}
	
	
	
	function updateAllUpdatedbydevice($deviceid)
	{
	   $sql = "update folder set createdbydevice = NULL,updatedbydevice = NULL where createdbydevice = {$deviceid} or updatedbydevice = {$deviceid}";
	   $sql1 ="update folderfile set createdbydevice = NULL,updatedbydevice = NULL where createdbydevice = {$deviceid} or updatedbydevice = {$deviceid}";
	   $sql2 = "update folderalloweddevices set updatedbydevice = NULL where updatedbydevice = {$deviceid}";
	   $sql3 = "update folderallowedusers set updatedbydevice = NULL where updatedbydevice = {$deviceid}";   
	   $sql4 = "delete from lockfile where deviceid = {$this->db->escape($deviceid)}";
	   $this->db->query($sql);
	   $this->db->query($sql1);
	   $this->db->query($sql2);
	   $this->db->query($sql3);
	   $this->db->query($sql4);
	
	}
	
	
	function expireDeviceToken($deviceid)
	{
        	//update sessiontoken        		
        	$sessiontoken= md5(microtime().rand());
        	$datacc=array();
        	$datacc['sessiontoken']=$sessiontoken;
        	$datacc['lastloggedon']=date('Y-m-d H:i');
        	Companydevicemodel::updateCompanyDevice($datacc,$deviceid);
	
	}
	
	function checkDeviceExpiryWithDeviceID($deviceid)
	{
		$this->db->select('companyadmin.expirydate,companyadmin.companyid');
		$this->db->join('companyadmin','companyadmin.companyid = companydevice.companyid');
		$this->db->where('companydevice.deviceid', $deviceid);
		$result = $this->db->get('companydevice');
		if($result->num_rows() > 0) {
		  $data = $result->row();
		  if(strtotime($data->expirydate) >= strtotime(date('Y-m-d H:i:s'))) {
		  	return FALSE;
		  }
		  else {
		  	return TRUE;
		  }
		}
		else
		{
		  return TRUE;
		}
	}
	
	function checkDeviceExpiryWithDeviceUUID($deviceid)
	{
		$this->db->select('companyadmin.expirydate,companyadmin.companyid');
		$this->db->join('companyadmin','companyadmin.companyid = companydevice.companyid');
		$this->db->where('companydevice.deviceuuid', $deviceid);
		$result = $this->db->get('companydevice');
		if($result->num_rows() > 0) {
			$data = $result->row();
			if(strtotime($data->expirydate) >= strtotime(date('Y-m-d H:i:s'))) {
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
		else
		{
			return TRUE;
		}
	}
	
	function devicenameexist($newdevicername, $companyid)
	{
		$this->db->select('deviceid');
		$this->db->where('name', $newdevicername);
		$this->db->where ('companyid', $companyid);
		$result = $this->db->get('companydevice');
		$query = $result->row();
		return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
}
?>