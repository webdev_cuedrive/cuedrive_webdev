<?php
class Lockfilemodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	
	/**
	   Purpose : To get time diffrence between two MICROTIME  
	 */
	function timeDifference($time_start)
	{
	   $time_end = microtime(true);
           $duration = $time_end-$time_start;
           $hours = (int)($duration/60/60);
           $minutes = (int)($duration/60)-$hours*60;
           $seconds = (int)$duration-$hours*60*60-$minutes*60;
           $totalsec = $hours*60*60 + $minutes*60 + $seconds;
           return $totalsec;

	}
	
	
	/**
	   Purpose : To check wheather the file is in the defined folder
	 */
	function checkFileWithinFolder($fileid , $folderid)
	{
	
	        $this->db->select('*');
		$this->db->where('id', $fileid);
		$this->db->where('folderid', $folderid);
		$result =  $this->db->get('folderfile');
		return ($result->num_rows() >= 1) ? 1 : 0;
	
	}
	
	/**
	   Purpose : To check Folder permission wrt deviceid or userid
	 */
	function checkFolderPermission($folderid , $userid , $deviceid)
	{
	  $sql_device = "SELECT * FROM folderalloweddevices WHERE deviceid = {$deviceid} AND folderid = {$folderid} AND (permissionid = 2 OR permissionid = 4)"; // 2 = write permission ; 4 = editsave
	  $sql_user = "SELECT * FROM folderallowedusers WHERE companyuserid = {$userid} AND folderid = {$folderid} AND (permissionid = 2 OR permissionid = 4)"; // 2 = write permission ; 4 = editsave   
	    
	     $query_device =  $this->db->query($sql_device);
		
		if($query_device->num_rows() > 0) 
		{
		    if($userid != 0) 
	            {
		       $query_user =  $this->db->query($sql_user);
		       if($query_user->num_rows() > 0)
		       {
		          return "USERPERMISSION";
		       }
		       else
		       {
		          return "NOUSERPERMISSION";
		       }
		    
		    }
		    else
		    {
		       return "DEVICEPERMISSION";
		    }
		  
		}
		else
		{
		   return "NODEVICEPERMISSION";
		}
	
	}
	
	/**
	   Purpose : To get last lock time difference
	 */
	function fetchLastLockStatus($fileid,$folderid)
	{
	    $jid = "";
	   $sql = "select * from lockfile where fileidfk = {$fileid} and folderidfk = {$folderid}";
	   $query = $this->db->query($sql);
	   if($query->num_rows() > 0)
	   {
	       $rs = $query->row();
	       $lockstatus = $rs->islocked;
	       if($lockstatus == 1)
	       {
	           // file is locked
	           $lockedbyname = "";
	          
	          
	          $final_array['lockedbyuserid'] = $rs->lockedby;
	          $devicedetails = Lockfilemodel::getCompanyDevice($rs->deviceid);
	          $final_array['devicename'] = $devicedetails->name;
	          $final_array['deviceuuid'] = $devicedetails->deviceuuid;
	          
	          
	          
	          
	          $service_url = ORG_URL."chat/?type=fetch&username=".$devicedetails->deviceuuid;
                   $curl = curl_init($service_url);
	           curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	           $curl_response = curl_exec($curl);
		        if ($curl_response === false) {
			    $info = curl_getinfo($curl);
			    curl_close($curl);
			    die('error occured during curl exec. Additioanl info: ' . var_export($info));
		        }
		   curl_close($curl);
	           $response = json_decode($curl_response);
	           
	           $jid = (isset($response[0]->{'username'})) ? $response[0]->{'username'} : 'N/A';
	          
	          
	          
	          
	          
	          if($rs->lockedby != 0) {
	           $lockedbydetails = Lockfilemodel::getCompanyUsername($rs->lockedby);
	           $lockedbyname = $lockedbydetails->name;
	           $lockedbyusername = $lockedbydetails->username;
	           
	           
	           $service_url = ORG_URL."chat/?type=fetch&username=".$lockedbyusername;
                   $curl = curl_init($service_url);
	           curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	           $curl_response = curl_exec($curl);
		        if ($curl_response === false) {
			    $info = curl_getinfo($curl);
			    curl_close($curl);
			    die('error occured during curl exec. Additioanl info: ' . var_export($info));
		        }
		   curl_close($curl);
	           $response = json_decode($curl_response);
	           
	           $jid = (isset($response[0]->{'username'})) ? $response[0]->{'username'} : 'N/A';
		           
	           
	           
	          }
	          
	          
	          
	          
	          
	          
	          
	          
	          $final_array['jid'] = $jid.'@cuedrive.com.au';
	          
	          $final_array['deviceid'] = $rs->deviceid;
	          $final_array['lockedtime'] = $rs->lockdatetime;
	          $final_array['status']  = "yes";
	          $final_array['lockedbyusername']  = $lockedbyname;
	          
	          $filedetails = Lockfilemodel::fetchFileDetails($fileid,$folderid);
	          $final_array['fileID']=$filedetails['id'];
		  $final_array['name']=$filedetails['filename'];
	          $final_array['filePath']=$filedetails['fullpath'];
		  $final_array['fileSize']=$filedetails['filesize']."bytes";
		  $final_array['createdon']=date('d/m/Y g:i a',strtotime($filedetails['createdon']));
		  $final_array['updatedon']=date('d/m/Y g:i a',strtotime($filedetails['updatedon']));
		  $final_array['formattedupdatedon']=date('d M Y',strtotime($filedetails['updatedon']));
		  
		  
	          
	          
	       }
	       else
	       {
	          $final_array['status']  = "no";
	          
	       } 
	   }
	   else
	   {
	       $final_array['status']  = "no";
	      
	   }
	
	
	return $final_array;
	}
	
	function getCompanyUsername($userid)
	{
	   $sql = "select CONCAT(firstname,' ',lastname) as name,username from companyuser where companyuserid = {$userid}";
	   $query = $this->db->query($sql);
	   $res = $query->row();
	   return $res;
	}
	
	function getCompanyDevice($deviceid)
	{
	   $sql = "select name,deviceuuid from companydevice where deviceid = {$deviceid}";
	   $query = $this->db->query($sql);
	   return $query->row();
	}
	
	function fetchFileDetails($fileid,$folderid)
	{
	
	        $this->db->select('*');
		$this->db->where('id', $fileid);
		$this->db->where('folderid', $folderid);
		$query =  $this->db->get('folderfile');
		return $query->row_array();
	
	
	/*
	
	
	*/
	
	}
	
	
	function lockfile($fileid,$folderid,$deviceid,$companyuserid,$lockid)
	{
	   
	   if($lockid != 0)
	   {
	     // update
	     
	     $sql = "UPDATE `lockfile` SET `islocked`=1,`lockedby`={$companyuserid},`deviceid`={$deviceid} WHERE fileidfk = {$fileid} AND folderidfk = {$folderid}";
	     $this->db->query($sql);
	    return 1;
	     
	   }
	   else
	   {
	     // insert
	     
	     $sql = "INSERT INTO `lockfile`(`fileidfk`, `islocked`, `lockedby`, `folderidfk`, `deviceid`) VALUES ({$fileid},1,{$companyuserid},{$folderid},{$deviceid})";
	     $this->db->query($sql);
	     return $this->db->affected_rows();
	   }
	   
	
	}
	
	function releaseLock($fileid,$folderid,$deviceid,$companyuserid)
	{
	
	$sql = "UPDATE `lockfile` SET `islocked`=0 WHERE fileidfk = {$fileid} AND folderidfk = {$folderid}";
	$this->db->query($sql);
	return $this->db->affected_rows();
	    
	
	}
	
	function releaseAllLocks($deviceid,$companyuserid)
	{
		$sql = "DELETE FROM `lockfile` WHERE deviceid = {$deviceid}";
	    $this->db->query($sql);
	    return $this->db->affected_rows();
	}
	
	function lockExists($fileid,$folderid)
	{
	  	$this->db->select('*');
		$this->db->where('fileidfk', $fileid);
		$this->db->where('folderidfk', $folderid);
		$result =  $this->db->get('lockfile');
		return ($result->num_rows() == 1) ? 1 : 0;
	
	
	}
	
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
?>