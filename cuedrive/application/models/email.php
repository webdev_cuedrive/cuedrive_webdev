<?php
class Email extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function newUserEmail($username,$email,$password,$firstname,$lastname)
	{
		$this->load->library('email');
		$config = array (
			'mailtype' => 'html',
			'charset'  => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->from('admin@cuedrive.com', 'cuedrive');
		$this->email->to($email);

		$data = array();
		$data['username'] = $username;
		$data['password'] = $password;
		$data['fullname'] = $firstname.' '.$lastname;

		$this->email->subject('Cuedrive account has been created for you');
		$message=$this->load->view('email/newuseremail',$data,TRUE);
		$this->email->message($message);

		$this->email->send();
	}
	
	function sendPassword_Email($username,$email,$password,$firstname,$lastname)
	{
		$this->load->library('email');
		$config = array (
		'mailtype' => 'html',
		'charset'  => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->from('admin@cuedrive.com', 'cuedrive');
		$this->email->to($email);

		$data = array();
		$data['username'] = $username;
		$data['password'] = $password;
		$data['fullname'] = $firstname.' '.$lastname;

		$this->email->subject('Cuedrive account has been created for you');
		$message=$this->load->view('email/teammembermail',$data,TRUE);

		/* $message = "Hi,<br><br>
		Admin has created an account for you. You can log in with the following parameters:<br>
		Username: 	".$username." <br>
		Password: 	".$password." <br><br><br>
		Regards,<br>
		cuedrive Team";
		*/
		$this->email->message($message);

		$this->email->send();

	}
     
     
     
     
     
     	
    function welcome_Email($username,$email,$password,$createdon,$accounttype,$storage,$devices)
    {
		$this->load->library('email');
		$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        
        $data = array();
        $data['date'] = $createdon;
        $data['accounttype'] = $accounttype;
        $data['storage'] = $storage;
        $data['devices'] = $devices;
        $data['username'] = $username;
        $this->email->subject('Welcome to cuedrive');
        $message=$this->load->view('email/newadminemail',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Welcome to cuedrive . An account has been created successfully for you.<br>
                    <br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
    }
     
     
    function replyFeedback($email,$message,$fid)
    {
		$this->load->library('email');
		$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['fullname'] = $email;
        $data['id'] = 100000+$fid;
        $data['reply'] = $message;
        $this->email->subject('Cuedrive reply- feedback');
        $message1=$this->load->view('email/replyfeedbackemail',$data,TRUE);
        
      /*  $message1 = "Hi,<br><br>".
                     $message
                   ."<br><br> Regards,<br>
                    cuedrive Team";
       */ 
        $this->email->message($message1);

        $this->email->send();
        
             
        
		$sql = "UPDATE `contactus` SET `sent`= 1 WHERE `id` = {$fid}";
		$this->db->query($sql);

    }
     
    function feedbackConfirmation($email,$id)
    {
		$this->load->library('email');
		$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['fullname']=$email;
        $data['id']=100000+$id;
        $data['title'] = "Feedback ID";
        $data['body']="Thank you for leaving your feedback. We have received your email and will respond to you as soon as possible";
        $this->email->subject('Cuedrive feedback confirmation');
        $message=$this->load->view('email/supportemail',$data,TRUE);
        
       /* $message = "Hi,<br>
                    Your feedback has been sent successfully.<br>
                   <br><br> Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
    }
	
	
    function ticketConfirmation($email,$ticketid)
    {
		$this->load->library('email');
		$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['fullname']=$email;
        $data['id']=$ticketid;
        $data['title'] = "Follow-Up Ticket";
        $data['body']="Thank you for contacting cuedrive Support. We have received your email and will respond to you as soon as possible.";
        $this->email->subject('Cuedrive ticket confirmation');
        $message=$this->load->view('email/supportemail',$data,TRUE);
        /*
        $message = "Hi,
                    Your ticket has been created successfully.<br>
                    Ticket ID : ".$ticketid."
                    <br><br> Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
    }
     
     
    function sendUpdatedPassword_Email($fullname,$email,$password,$username)
    {
		$this->load->library('email');
		$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
		$data['fullname'] =$fullname;
        $data['username'] =$username;
        $data['password'] = $password;
        $this->email->subject('Cuedrive password has been updated');
        $message=$this->load->view('email/forgotpasswordemail',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
    }
         
    public function sendFolderAllowedEmails($folderid)
    {
        $to = "";
	    $sql = "select * from  folderallowedemails where folderid = {$folderid}";
	    $query = $this->db->query($sql);
	    $query = $query->row();
	    $emails = $query->emails;
     
	    $email_array = explode(',',$emails);

	     for($i=0;$i<5;$i++)
	     {
	           if(($email_array[$i] != "") && strpos($email_array[$i],'@') > 0)
	           {
	             $to.=$email_array[$i].",";
	           }
	        
	     }
             $to = substr($to,0,-1);
             return $to;

    }
     

     
     function sendFileAttachment($newurl,$folderid,$filename,$fileid)
     {
     $to = "";
     $sql = "select * from  folderallowedemails where folderid = {$folderid}";
     $query = $this->db->query($sql);
     $query = $query->row();
     $emails = $query->emails;
     
      $ext =  end(explode('.', $filename));
      $numcue = $query->numbercue;
     
      if($numcue == 1)
      {
        $filename = $fileid.'#'.date("H:i:s d-m-Y").".".$ext;
      }
     
     
     
        $email_array = explode(',',$emails);
       
        
        for($i=0;$i<5;$i++)
        {
           if(($email_array[$i] != "") && strpos($email_array[$i],'@') > 0)
           {
             $to.=$email_array[$i].",";
           }
        
        }
      $to = substr($to,0,-1);
     
     
     $htmlbody = " Please find the attachment below.";

	
	
	$subject = "cuedrive attachment";//"Test email with attachment"; //Email Subject
	
	$from="admin@cuedrive.com";
	
	$headers = "From: $from\r\nReply-To: $from";
	
	$random_hash = md5(date('r', time()));
	
	$headers .= "\r\nContent-Type: multipart/mixed;boundary=\"PHP-mixed-".$random_hash."\"";
	//echo "<br>".$newurl;
	
	
	// Set your file path here
	
	$attachment = chunk_split(base64_encode(file_get_contents(str_replace("&amp;","&",$newurl))));
	
	//define the body of the message.
	
	$message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative;boundary=\'PHP-alt-$random_hash\'\r\n\r\n";
	
	$message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/plain;charset=\'iso-8859-1\'\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
	
	//Insert the html message.
	
	$message .= $htmlbody;
	
	$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
	
	//include attachment
	
	$message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/zip;name=\'".$filename."\'\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";

	$message .= $attachment;
	
	$message .= "/r/n--PHP-mixed-$random_hash--";
	
	//send the email
	
	$mail = mail( $to, $subject , $message, $headers );
     
     
     }
     
     
     
     
     function sendTestEmail($username,$email,$password)
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        
        $this->email->subject('');
        $data=array();
        $message=$this->load->view('map_mail_format',$data,TRUE);
       
        
        $this->email->message($message);

        $this->email->send();
   
     }
     
     
     
     
     function sendExpiryWarning_Email($fullname,$email,$days,$username)
     {
		$this->load->library('email');
		$config = array (
					  'mailtype' => 'html',
					  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data 				= array();
        $data['fullname'] 	= $fullname;
		$data['username'] 	= $username;
        $data['days'] 		= $days;
        $this->email->subject('Cuedrive account expiry');
        $message=$this->load->view('email/expirywarning',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
     }     
     
     
    function sendExpiry_Email($fullname,$email,$username)
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['fullname'] =$fullname;
		$data['username'] =$username;
       
        $this->email->subject('Cuedrive account expired');
        $message=$this->load->view('email/expirymail',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
     }
     
    function sendFreeExpiry_Email($fullname,$email,$username)
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['fullname'] =$fullname;
		$data['username'] =$username;
       
        $this->email->subject('Cuedrive free account expired');
        $message=$this->load->view('email/freeexpiry',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
     }     
         
    function sendApplicationCorporateSubmitionMail($email,$username,$phone)
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['email'] =$email;
       
        $this->email->subject('Request to Corporate account');
        $message=$this->load->view('email/corporateapplicationsubmit',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
     }
     
    function corporateApproveEmail($email,$storage,$devices)
     {
	$this->load->library('email');
	$config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8'
                   );
        $this->email->initialize($config);
        $this->email->from('admin@cuedrive.com', 'cuedrive');
        $this->email->to($email);
        $data = array();
        $data['email'] =$email;
        $data['storage'] =$storage;
        $data['devices'] =$devices;
        $this->email->subject('Your corporate account has been approved');
        $message=$this->load->view('email/corporateapplicationapprove',$data,TRUE);
        /*
        $message = "Hi,<br><br>
                    Your password has been successfully updated . You can log in with the following parameters:<br>
                    Username: 	".$username." <br>
                    Password: 	".$password." <br><br><br>
                    Regards,<br>
                    cuedrive Team";
        */
        $this->email->message($message);

        $this->email->send();
   
     }          
     
    public function accountExpiryCronJobs()
     {
        //$query = $this->db->get('companyadmin');
		$query = $this->db->get_where('companyadmin',array('autopayment'=>'false'));
        $query = $query->result_array();
        $today = date('Y-m-d H:i:s');
        
        foreach($query as $result)
        {
              $email 		= $result['email'];
              $expirydate 	= $result['expirydate'];
              $packageid 	= $result['packageid'];
              //$name = $result['name'];
			  $fullname 	= $result['firstname'].' '.$result['lastname'];
			  $username 	= $result['username'];
              
              $datetime1 	= new DateTime($today);
              $datetime2 	= new DateTime($expirydate);
              
              $interval 	= $datetime1->diff($datetime2);
              $diff = $interval->format('%R%a');
              
              if($diff == 7)
              {
                 //  7 days warning
                 Email::sendExpiryWarning_Email($fullname,$email,7,$username);
              }
              else if($diff == 3)
              {
                 //  3 days warning
                 Email::sendExpiryWarning_Email($fullname,$email,3,$username);
              }
              else if($diff == -1 && $packageid != 1)
              {
                 // account expiry
                  Email::sendExpiry_Email($fullname,$email,$username);
              }
              else if($diff == -1 && $packageid == 1)
              {
                 // free account expiry
                 Email::sendFreeExpiry_Email($fullname,$email,$username);
              }
               else if($diff == -7 && $packageid != 1)
              {
                 // account expiry warning after 7 days
                  Email::sendExpiry_Email($fullname,$email,$username);
              }
          
        }

     }
 }
?>