<?php
class Classpush extends CI_Model
{
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	// -------------------------------------------------------------------------------------------------  		
	public function registerDevice($operating_system, $device_id, $iphonepushopt, $androidpushopt, $device_token, $reg_id, $pushbadge, $pushalert, $pushsound, $device_uuid)
	{
		// Register Device for Push Notification
		$device_id = 0;
        $sql = "SELECT `deviceid` FROM `companydevice` WHERE `deviceuuid` = '{$device_uuid}'";
        $query100 = $this->db->query($sql);
        if($query100->num_rows() > 0)
        {
        	$query100 = $query100->row();
            $device_id = $query100->deviceid;
        }

		$query = "INSERT INTO pushnotifications 
			(operating_system, device_id, iphonepushopt, androidpushopt, device_token, reg_id, is_active, pushbadge, pushalert, pushsound, deviceuuid) 
			VALUES 
				 ('{$operating_system}','{$device_id}','{$iphonepushopt}','{$androidpushopt}','{$device_token}','{$reg_id}',1,'{$pushbadge}','{$pushalert}','{$pushsound}','{$device_uuid}')				 
					 ON DUPLICATE KEY UPDATE 
					 operating_system =  '{$operating_system}', 
					  device_id = '{$device_id}',
					 iphonepushopt =  '{$iphonepushopt}', 
					 androidpushopt =  '{$androidpushopt}', 
					 device_token =  '{$device_token}', 
					 reg_id =  '{$reg_id}',
					 pushbadge = '{$pushbadge}',
					 pushalert = '{$pushalert}',
					 pushsound = '{$pushsound}',
					 deviceuuid = '{$device_uuid}'
					";
			
		$this->db->query($query);
		$resultArray=array('errMsg' => 'success');
			
		return $resultArray;
	}
	
	// -------------------------------------------------------------------------------------------------  			
	public function SendTestPush()
	{

$device = '03efa0a62de27b9f83aee0376c96958e24c04b575919d759a13c05087f0becff';
		$payload['aps'] = array('alert' => 'hello', 'badge' => 1, 'sound' => 'default' );
			
			
			$payload = json_encode($payload);
			// CuedriveDevdev.pem , CuedriveDev.pem
			/*$options = array('ssl' => array(
			  'local_cert' => '/home/cuedrive/www/cuedrive/CuedriveDev.pem',
			  'passphrase' => 'teks1234'));*/
			
			//live
			$options = array('ssl' => array('local_cert' => '/home/cuedrive/www/cuedrive/server_certificates_bundle_production.pem'));
			//sandbox
			//$options = array('ssl' => array('local_cert' => '/home/cuedrive/www/cuedrive/server_certificates_bundle_sandbox.pem'));

			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, $options);
			
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			//$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
			//fwrite($apns, $apnsMessage);  

			//Live Url
			$apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			
			//Sandbox Url
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			
			$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
			fwrite($apns, $apnsMessage);   
			//socket_close($apns);
			fclose($apns);
echo $apnsMessage;
	}

	// -------------------------------------------------------------------------------------------------  		
	public function sendPushMessage($pushmessage, $device_token, $reg_id, $deviceType, $param1, $param2, $param3, $type, $deviceid, $jid)
	{
		if($deviceType == 'IPHONE')
		{
			//update badge count 
			$query = "SELECT badgecount FROM pushnotifications WHERE device_token = '{$device_token}'";
			$query = $this->db->query($query);
			$row = $query->row_array();
			$updatequery = "update pushnotifications set badgecount=badgecount+1 WHERE device_token ='{$device_token}'";
			$updatequery = $this->db->query($updatequery);
			$device = $device_token;//'0cdf707f497569038122fd193d5854392ed29bf436ee400418fb5a25abfcd6b8';
			if($type == 'FILELOCK') 
			{
			  $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'name' => $param1 , 'filename' => $param2 , 'fileid' => $param3, 'type' => '1','openfordeviceid' => $deviceid,'jid' => $jid.'@cuedrive.com.au');
			}
			
			if($type == 'CHAT') 
			{
			  $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'name' => $param1 , 'time' => $param2 , 'type' => '4','jid' => $jid.'@cuedrive.com.au');
			}
			
			if($type == '1') 
			{
			  $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'name' => $param1 , 'filename' => $param2 , 'fileid' => $param3, 'type' => '2', 'isaccept' => $type,'jid' => $jid.'@cuedrive.com.au');
			}
			
			if($type == '0') 
			{
			  $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'name' => $param1 , 'filename' => $param2 , 'fileid' => $param3, 'type' => '3', 'isaccept' => $type,'jid' => $jid.'@cuedrive.com.au');
			}
			if($type == 'UPLOAD')
			{
			 $payload['aps'] = array('alert' => $pushmessage, 'badge' => $row["badgecount"]+1, 'sound' => 'default', 'name' => $param1 , 'filename' => $param2 , 'fileid' => $param3, 'type' => '5', 'isaccept' => $type,'jid' => $jid.'@cuedrive.com.au');
			
			}


			$payload = json_encode($payload);
			// CuedriveDevdev.pem , CuedriveDev.pem
			/*$options = array('ssl' => array(
			  'local_cert' => '/home/cuedrive/www/cuedrive/CuedriveDev.pem',
			  'passphrase' => 'teks1234'));*/
			
			//live
			$options = array('ssl' => array('local_cert' => '/home/cuedrive/www/cuedrive/server_certificates_bundle_production.pem'));
			//sandbox
			//$options = array('ssl' => array('local_cert' => '/home/cuedrive/www/cuedrive/server_certificates_bundle_sandbox.pem'));

			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, $options);
			
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			//$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
			//fwrite($apns, $apnsMessage);  

			//Live Url
			$apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			
			//Sandbox Url
			//$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
			
			$apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
			fwrite($apns, $apnsMessage);   
			//socket_close($apns);
			fclose($apns);
			
			//echo $errorString.'//'.$payload['aps'].$apnsMessage."ERROR>>".$error."<<>>>".$device;
		}
		
		if($deviceType == 'ANDRIOD')
		{	
			#error_reporting(E_ALL);
			$regId =$reg_id;
			$message = $pushmessage;
			include_once '/home/cuedrive/www/cuedrive/GCM.php';		
			$gcm = new GCM();
			$pushid= 1;
			$registatoin_ids = array($regId);

			if($type == 'FILELOCK') 
			{
				$message = array("message" => $pushmessage,"id"=>$pushid,"name"=>$param1,"filename"=>$param2,"fileid"=>$param3,"type"=>'1',"openfordeviceid"=>$deviceid,"jid"=>$jid);
			 
			}
			if($type == 'CHAT') 
			{
			  	$message = array("message" => $pushmessage,"id"=>$pushid,"name"=>$param1,"time"=>$param2,"type"=>'4',"jid"=>$jid);
			}
			if($type == '1') 
			{
				$message = array("message" => $pushmessage,"id"=>$pushid,"name"=>$param1,"filename"=>$param2,"fileid"=>$param3,"type"=>'2',"isaccept"=>$type,"jid"=>$jid);
			}
			if($type == '0') 
			{
				$message = array("message" => $pushmessage,"id"=>$pushid,"name"=>$param1,"filename"=>$param2,"fileid"=>$param3,"type"=>'3',"isaccept"=>$type,"jid"=>$jid);
			}
			if($type == 'UPLOAD')
			{
			 	$message = array("message" => $pushmessage,"id"=>$pushid,"name"=>$param1,"filename"=>$param2,"fileid"=>$param3,"type"=>'5',"isaccept"=>$type,"jid"=>$jid);
			}

			$result = $gcm->send_notification($registatoin_ids, $message);
			
			$return_array['success'] = "true";
			$return_array['a_message'] = "android notification sent";
		
			#print_r($result);exit;
		}
	}
	// -------------------------------------------------------------------------------------------------  	        
	function sendFileLockNotification($deviceid,$fileid,$lockedbydeviceid,$username)
    {

    	$devicedetails = Companydevicemodel::getDeviceDetails($deviceid);
        $devicename = $devicedetails['name'];
           
        $jid = Classpush::getDeviceUUIDfromDeviceid($deviceid);
           
        $filename = substr(Folderfilemodel::getFileName($fileid),0,5);
        $name = $devicename;
        if($username != "")
        {
        	$name = substr($devicename.'/'.$username,0,5);
        }
       
        $pushmessage = substr($name,0,5).' '.'is attempting to access'. $filename; 
        $query = "SELECT operating_system, device_id, iphonepushopt, androidpushopt, device_token, reg_id, is_active 
		     FROM pushnotifications 
		     WHERE is_active = 1 AND device_id = '{$lockedbydeviceid}';
		    ";
		$query = $this->db->query($query);
	   	$row = $query->row();	
				
	   	if($query->num_rows() > 0)
	    {
			if(!empty($row->device_token))
			{
				$device_token = $row->device_token;
		    	Classpush::sendPushMessage($pushmessage,$row->device_token,0,'IPHONE',$name,$filename,$fileid,'FILELOCK',$deviceid,$jid);
			}
			if(!empty($row->reg_id))
			{
				$device_token = 0;
		    	Classpush::sendPushMessage($pushmessage,0,$row->reg_id,'ANDRIOD',$name,$filename,$fileid,'FILELOCK',$deviceid,$jid);
			}
			/* Insert to Notification Log Start */	
		 	//$type = 'FILELOCK';
			//$folderid = Folderfilemodel::getFolderid($fileid);
			//$query = "INSERT INTO notificationlogs (notification_type, device_id, device_token, file_id, parent_folder_id, notification_message, notification_device_name) VALUES ('{$type}', '{$deviceid}', '{$device_token}' , '{$fileid}', '{$folderid}', '{$pushmessage}', '{$devicename}')";
			//$this->db->query($query);
			/* Insert to Notification Log End */
		
			return 1;	
		}
	    else
	    {
	       	// device is not registered
	    	return 0;
	    }	
	}   
	// -------------------------------------------------------------------------------------------------  	       
    function sendRemoveLockIsAcceptNotification($deviceid,$fileid,$openfordeviceid,$username,$isaccept)
    {
    	$devicedetails = Companydevicemodel::getDeviceDetails($deviceid);
        $devicename = $devicedetails['name'];
        $jid = Classpush::getDeviceUUIDfromDeviceid($deviceid);
           
        $filename = Folderfilemodel::getFileName($fileid);
        $name = $devicename;
        if($username != "")
        {
        	$name = $devicename.'/'.$username;
        }
        $pushmessage = $name;  
        if($isaccept == 1) 
		{
        	$pushmessage = "accept";  
        }
           
        $query = "SELECT operating_system, device_id, iphonepushopt, androidpushopt, device_token, reg_id, is_active 
		     FROM pushnotifications 
		     WHERE is_active = 1 AND device_id = '{$openfordeviceid}';
		    ";
		$query = $this->db->query($query);
	   	$row = $query->row();	
				
	   	if($query->num_rows() > 0)
	    {
			if(!empty($row->device_token))
			{
		    	$device_token = $row->device_token;
				Classpush::sendPushMessage($pushmessage,$row->device_token,0,'IPHONE',$name,$filename,$fileid,$isaccept,'',$jid);
			}
			if(!empty($row->reg_id))
			{
				$device_token = '';
		    	Classpush::sendPushMessage($pushmessage,0,$row->reg_id,'ANDRIOD',$name,$filename,$fileid,$isaccept,'',$jid);
			}
			/* Insert to Notification Log Start */
			//	$type = 'REMOVELOCK';
			//$type = $isaccept;
			//$folderid = Folderfilemodel::getFolderid($fileid);
			
			//$query = "INSERT INTO notificationlogs (notification_type, device_id, device_token, file_id, parent_folder_id, notification_message, notification_device_name) VALUES ('{$type}', '{$deviceid}', '{$device_token}' , '{$fileid}', '{$folderid}', '{$pushmessage}', '{$devicename}')";
			//$this->db->query($query);
			/* Insert to Notification Log End */
					
			return 1;	
		}
	    else
	    {
	    	// device is not registered
	       	return 0;
	    }
	}
	// -------------------------------------------------------------------------------------------------  	       
	function sendFileUploadNotification($folderid,$type,$fileName,$deviceid)
    {
    	$folderdetails = Foldermodel::getFolderDetails($folderid);
        $foldername = $folderdetails['foldername'];
          
        $sql = "select deviceid from folderalloweddevices where folderid = {$folderid} and permissionid = 6";
        $query1 = $this->db->query($sql);
        $res = $query1->result();
          
        if($type == 'upload')
        {
        	$pushmessage = $fileName.' file is uploaded on'.' '.$foldername.' '.'folder';
        }
        else if($type == 'delete')
        {
        	$pushmessage = $fileName.' file is deleted from'.' '.$foldername.' '.'folder';
        }
        else
        {
        	$pushmessage = $fileName.' file is updated on'.' '.$foldername.' '.'folder';
        }
					
		if($query1->num_rows() > 0)
	    {
			foreach($res as $row1)
			{
				$device=$row1->deviceid;

if($deviceid!=$device) {

				$query = "SELECT operating_system,device_id,iphonepushopt,androidpushopt,device_token,reg_id,is_active 
		                               FROM pushnotifications 
		                               WHERE is_active = 1 AND device_id = '{$device}';";
				
				$query = 	$this->db->query($query);
				$row = $query->row();
				if($query->num_rows() > 0)
				{	
					if(!empty($row->device_token))
				    {
				    	$device_token = $row->device_token;
						Classpush::sendPushMessage($pushmessage,$row->device_token,0,'IPHONE','','','','UPLOAD','','N/A');
				    }
				    if(!empty($row->reg_id))
				    {
						$device_token = 0;
				        Classpush::sendPushMessage($pushmessage,0,$row->reg_id,'ANDRIOD','','','','UPLOAD','','N/A');
				    }
				     
					/* Insert to Notification Log Start */
					if($device_token != 'na') 
					{
						$fileId = Folderfilemodel::getfileID($fileName, $folderid);
						$deviceDetails = Companydevicemodel::getDeviceDetails($device);
						$notificationDeviceName = $deviceDetails['name'];
					
						$query = "INSERT INTO notificationlogs (notification_type, device_id, device_token, file_id, parent_folder_id, notification_message, notification_device_name) VALUES ('{$type}', '{$device}', '{$device_token}' , '{$fileId}', '{$folderid}', '{$pushmessage}', '{$notificationDeviceName}')";
						$this->db->query($query);
					}
					/* Insert to Notification Log End */
				}
}
			}
		}
	}   
	// -------------------------------------------------------------------------------------------------  	
    function sendChatNotification($deviceid,$todeviceid,$message,$time,$username)
    {
    	//$devicedetails = Companydevicemodel::getDeviceDetails($deviceid);
        //$devicename = $devicedetails['name'];
        // $name = $devicename;
        //if($username != "")
        // {
        //   $name = $devicename.'/'.$username;
        // }
          
        $jid = Classpush::getDeviceUUIDfromDeviceuuid($deviceid);
        $name = "";
        $pushmessage = $message;
        $query = "SELECT operating_system,device_id,iphonepushopt,androidpushopt,device_token,reg_id,is_active 
		     FROM pushnotifications 
		     WHERE is_active = 1 AND deviceuuid = '{$todeviceid}';
		    ";
		$query = $this->db->query($query);
	   	$row = $query->row();	
				
	   	if($query->num_rows() > 0)
	    {
			if(!empty($row->device_token))
			{
				$device_token = $row->device_token;
		    	Classpush::sendPushMessage($pushmessage,$row->device_token,0,'IPHONE',$name,$time,'-','CHAT','',$jid);
			}
			if(!empty($row->reg_id))
			{
				$device_token = 0;
		    	Classpush::sendPushMessage($pushmessage,0,$row->reg_id,'ANDRIOD',$name,$time,'-','CHAT','',$jid);
			}
			/* Insert to Notification Log Start */
			//$fileId = '';
			//$folderid = '';
			//$type = 'chat';
			//$deviceDetails = Companydevicemodel::getDeviceDetails($deviceid);
			//$notificationDeviceName = $deviceDetails['name'];
					
			//$query = "INSERT INTO notificationlogs (notification_type, device_id, device_token, file_id, parent_folder_id, notification_message, notification_device_name) VALUES ('{$type}', '{$deviceid}', '{$device_token}' , '{$fileId}', '{$folderid}', '{$pushmessage}', '{$notificationDeviceName}')";
			//$this->db->query($query);
			/* Insert to Notification Log End */
					
			return 1;	
		}
	    else
	    {
	    	// device is not registered
	       	return 0;
		}	
    }
	// -------------------------------------------------------------------------------------------------  	        
	public function resetNotificationBadgeCount($deviceid)
    {
    	$updatequery = "update pushnotifications set badgecount=0 WHERE device_id ='{$deviceid}'";
		$updatequery = $this->db->query($updatequery);
	     
	    return 1;
	}
	// -------------------------------------------------------------------------------------------------  		
	public function sendPushNotification($deviceid,$option)
	{
		if($option == "FILEUPDATE")
		{
			$query1 = "";
			$query1 = $this->db->query($query1);
			$res = $query1->result();
		    if($query1->num_rows() > 0)
			{
         		foreach($res as $row1)
			   	{
			   		$device=$row1->deviceid;
			   	  
				    $query = "SELECT userid_fk,operating_system,device_id,iphonepushopt,androidpushopt,device_token,reg_id,is_active
				               FROM pushnotifications
				               WHERE is_active = 1 AND device_id = '{$device}';";
				
				    $query = 	$this->db->query($query);
				    $row = $query->row();
				    if($query->num_rows() > 0)
				    {
						if(!empty($row->device_token))
				        {
				        	Classpush::sendPushMessage($pushmessage,$row->device_token,0,'IPHONE');
				        }
				        if(!empty($row->reg_id))
				        {
				        	Classpush::sendPushMessage($pushmessage,0,$row->reg_id,'ANDRIOD');
				        }
					}
				}
			}
		} 		
	}
	// -------------------------------------------------------------------------------------------------  	             
	public function getDeviceUUIDfromDeviceid($deviceid)
    {
    	$query = "SELECT deviceuuid FROM companydevice WHERE deviceid = {$deviceid}";
               
		$query = $this->db->query($query);
		$query = $query->row_array();
		$deviceUUID = $query['deviceuuid'];
		
		$service_url = ORG_URL."chat/?type=fetch&username=".$deviceUUID;
        $curl = curl_init($service_url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $curl_response = curl_exec($curl);
	    
		if ($curl_response === false) 
		{
			$info = curl_getinfo($curl);
		    curl_close($curl);
		    die('error occured during curl exec. Additioanl info: ' . var_export($info));
	    }
	    curl_close($curl);
        $response = json_decode($curl_response);
           
        $jid = (isset($response[0]->{'username'})) ? $response[0]->{'username'} : 'N/A';
        return $jid;
	}
	// -------------------------------------------------------------------------------------------------  	                          
	public function getDeviceUUIDfromDeviceuuid($deviceid)
    {
    	$service_url = ORG_URL."chat/?type=fetch&username=".$deviceid;
        $curl = curl_init($service_url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $curl_response = curl_exec($curl);
	    
		if ($curl_response === false) 
		{
			$info = curl_getinfo($curl);
		    curl_close($curl);
		    
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
	    }
	    curl_close($curl);
        $response = json_decode($curl_response);
        $jid = (isset($response[0]->{'username'})) ? $response[0]->{'username'} : 'N/A';
        
		return $jid;
	}


//****************************************************************//
//*************************CRON JOB*******************************//
//****************************************************************//
public function cronformakingoldnotificationasread() {
$query = "UPDATE `notificationlogs` SET `notification_read_flag` = '1' WHERE `notification_date` < adddate( now( ) , -3 ) AND `notification_read_flag` = '0'";
$this->db->query($query);
}

}