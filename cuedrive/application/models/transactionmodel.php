<?php
class Transactionmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	
	function addInvoice($options = array())
	{
		$qualificationArray = array('companyid','transactionid','amount','transactiondate','type','comment','invoiceid','invoicenumber','ispaid','createdon');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('transaction');
		return $this->db->insert_id();
	}
	
	
	
	
	function GetBillingbycompanyid($id)
	{
		$sql = "select * from transaction
			where companyid = {$id} and ispaid != 2";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	function GetBillingForSiteowner($companyid = '')
	{		
		if($companyid == '')	{
			$sql = "select * from transaction";
		}	else	{
			$sql = "select * from transaction where companyid = {$companyid}";
		}
		
		$query = $this->db->query($sql);
		
		if($query->num_rows() <= 0) {
			return false;
		}	else	{
			return $query->result_array();
		}
		
	}
	
	function GetBillingDetailsForSiteowner($options = array())
	{
		$this->db->select('transaction.*, companyadmin.customerToken');
		if($options['companyid'] != '')	{
			$this->db->where('transaction.companyid', $options['companyid']);
		}
		//$this->db->where('ispaid !=', 2);
		//$this->db->where('comment =', 'package');
		$this->db->group_by("transaction.invoicenumber");
		
		if(isset($options['limit']) && isset($options['offset'])) {
			if($options['offset'] == $options['companyid']) {
				$options['offset'] = 0;
			}
			$this->db->limit($options['limit'], $options['offset']);
			
		}	else if(isset($options['limit'])) {
		
			$this->db->limit($options['limit']);
		}
		
		$this->db->order_by("transaction.id","desc");
		$this->db->from('transaction');
		$this->db->join('companyadmin', 'companyadmin.companyid = transaction.companyid');
		$query = $this->db->get();
		//echo '<pre>'.$this->db->last_query().'</pre>';
		if($query->num_rows() <= 0) {
			
			return false;
			
		}	else	{
			
			return $query->result();
			
		}
	}
	
	
	
	function GetBillingDetailsbycompanyid($options = array())
	{
		$this->db->select('*');
		$this->db->where('companyid', $options['companyid']);
		$this->db->where('ispaid !=', 2);
		//$this->db->where('comment =', 'package');
		$this->db->group_by("invoicenumber");
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		
		$this->db->order_by("id","desc");
		$query = $this->db->get('transaction');
		
		if($query->num_rows() == 0) return false;
	
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	public function getThisMonthBill($companyid = 0)
	{
	
	     $sql = "SELECT DATE_SUB(LAST_DAY(DATE_ADD(NOW(), INTERVAL 0 MONTH)), 
                     INTERVAL DAY(LAST_DAY(DATE_ADD(NOW(), INTERVAL 0 MONTH)))-1 DAY) AS firstOfCurrentMonth,
                     LAST_DAY(DATE_ADD(NOW(), 
                     INTERVAL 0 MONTH)) AS lastOfCurrentMonth";
                     
             $query = $this->db->query($sql);
             $query = $query->row();
             $fisrtdate = $query->firstOfCurrentMonth;
             $lastdate = $query->lastOfCurrentMonth;
             
             
             $total = 0;
             if($companyid == 0) {
               $sql1 = "SELECT amount FROM transaction WHERE timestamp(createdon) BETWEEN timestamp('{$fisrtdate}') AND timestamp('{$lastdate}')";
             }
             else
             {
               $sql1 = "SELECT amount FROM transaction WHERE timestamp(createdon) BETWEEN timestamp('{$fisrtdate}') AND timestamp('{$lastdate}') AND companyid = {$companyid}";
             }
             $query1 = $this->db->query($sql1);
             $result = $query1->result();
             if($query1->num_rows() > 0)
             {
             
              foreach($result as $row)
              {
                 $total+=$row->amount;
              }
             
             }
	
	     return $total;
	
	}
	
}
?>