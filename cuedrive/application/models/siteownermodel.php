<?php
class Siteownermodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function adminauth($username, $password)
	{
			$this->db->select('adminid');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$result = $this->db->get('siteowner');
			$query = $result->row();
			return ($result->num_rows() == 1) ? $query->adminid : 0;	
	}
	
	function checkIsManager($username, $password)
	{
	
	        $this->db->select('ismanager');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$result = $this->db->get('siteowner');
			$query = $result->row();
			if($result->num_rows() == 1) 
			       return $query->ismanager;
	
	}
	
	
	function checkIsMasterAdmin($username, $password)
	{
	
	        $this->db->select('masteradmin');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$result = $this->db->get('siteowner');
			$query = $result->row();
			if($result->num_rows() == 1) 
			       return $query->masteradmin;
	
	}

	
	function GetSiteownerAdmins()
	{
	        $this->db->select('*');
		$query = $this->db->get('siteowner');
		if($query->num_rows() == 0) return false;
		if(isset($options['adminid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	function GetSiteownerAdmin($id)
	{
	        $this->db->select('*');
	        $this->db->where('adminid',$id);
		$query = $this->db->get('siteowner');
		if($query->num_rows() == 0) return false;
		if(isset($options['adminid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->row_array();
		}
	}
	
	
	function GetSiteownerAdminsDetails($options = array())
	{
		$this->db->select('*');
		$qualificationArray = array('adminid', 'username','like');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
				if($qualifier == 'adminid')
				{
					$this->db->where('siteowner.adminid', $options[$qualifier]);
				}
				
				else if($qualifier == 'like' )
				{
					$this->db->like('siteowner.username', $options[$qualifier]);
					$this->db->or_like('siteowner.firstname', $options[$qualifier]);
					$this->db->or_like('siteowner.lastname', $options[$qualifier]);
					$this->db->or_like('siteowner.email', $options[$qualifier]);
					
				}
				else{
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		
		
		$query = $this->db->get('siteowner');
		
		if($query->num_rows() == 0) return false;
	
		if(isset($options['adminid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	function addUser($options = array())
	{	
		$qualificationArray = array('username','password','firstname','lastname','email','ismanager');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('siteowner');
		return $this->db->insert_id();
	
	}
	
	function updateUser($options = array())
	{
	    $qualificationArray = array('username','password','firstname','lastname','email','ismanager','adminid');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->where('adminid', $options['adminid']);
		$this->db->update('siteowner');
		return $this->db->affected_rows();
	}
	
	function deleteUser()
	{
		$id = $_REQUEST['adminid'];
		$this->db->where('adminid', $id);
		$this->db->delete('siteowner');
		return $this->db->affected_rows();
	}
	
    function resetPassword($email,$password)
      {
      $sql = "UPDATE `siteowner` SET  `password`='{$password}' WHERE `email`='{$email}'";
      $this->db->query($sql);
      return $this->db->affected_rows();
      }

function getUsername($email)
{
        $sql = "select * from siteowner where `email`='{$email}'";
	$query = $this->db->query($sql);
	$res = $query->row();
	return $res->username;

}

function userIsExists($email)
{
    $sql = "select * from siteowner where `email`='{$email}'";
	$query = $this->db->query($sql);
	
	return $query->num_rows();

}

}
?>