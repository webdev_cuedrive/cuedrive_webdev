<?php
class Folderfilemodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	
	function getFiles($folderid)
	{
		$this->db->select('*');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folderfile');
		return $query->result_array();
	}
	
	
	
	function getFilesarray($folderid)
	{
		$this->db->select('*');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folderfile');
		if($query->num_rows() > 0) {
		return $query->result_array();
		}
	}
	
	
	
	
	
		function insertFile($datacc)
	{
		 $this->db->insert('folderfile', $datacc);
		 $fileId = $this->db->insert_id();
		 return $fileId;
	}
		function updateFile($datacc,$updatefileid)
	{
		$this->db->where('id', $updatefileid);
		$this->db->update('folderfile', $datacc);
	}
	function getFilePath($fileid){
		
		$this->db->select('fullpath');
		$this->db->where('id', $fileid);
		$query = $this->db->get('folderfile');
		if($query->num_rows() > 0) {
		$query = $query->row();
		return $query->fullpath;
		}
		else
		{
		 return 0;
		}
	}
	function getFolderid($fileid)
	{
		$this->db->select('folderid');
		$this->db->where('id', $fileid);
		$query = $this->db->get('folderfile');
		if($query->num_rows() > 0) {
		$query = $query->row();
		return $query->folderid;
		}
		else
		{
		 return 0;
		}
	}
	function getFileName($fileid)
	{
		$this->db->select('filename');
		$this->db->where('id', $fileid);
		$query = $this->db->get('folderfile');
		$query = $query->row();
		return $query->filename;
	}
	function getFileSize($fileid)
	{
		$this->db->select('filesize');
		$this->db->where('id', $fileid);
		$query = $this->db->get('folderfile');
		$query = $query->row();
		return $query->filesize;
	}
		function deleteFile($fileid)
	{
		$this->db->where('id', $fileid);;
      	$this->db->limit(1,0);
      		$query = $this->db->delete('folderfile');
      		return $this->db->affected_rows();
	}
	function getFilesByCompany($companyid)
	{
		$this->db->select('folderfile.*');
			$this->db->from('folderfile');
			$this->db->join('folder', 'folder.folderid= folderfile.folderid');
			$this->db->where('folder.companyid', $companyid);	
			$query = $this->db->get();
		return $query->result_array();
	}
	function fileNameExist($newfilename,$parentfolderId)
	{
			$this->db->select('id');
			$this->db->where('filename', $newfilename);
			$this->db->where ('folderid', $parentfolderId);
			$result = $this->db->get('folderfile');
			$query = $result->row();
			return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
	
	function getfileID($filename,$folderid)
	{
			$this->db->select('id');
			$this->db->where('filename', $filename);
			$this->db->where('folderid', $folderid);
			$query = $this->db->get('folderfile');
			$query = $query->row();
			return $query->id;
	}
	
	
	/**
	 * Added by Debartha
	 */
	
	function getTotalfileStored($companyid)
	{
		$sql = "SELECT sum(filesize) as GB  FROM `folder`
		join folderfile file on file.folderid = folder.folderid
		where folder.createdby = {$companyid}";
		$query = $this->db->query($sql);
		$res = $query->row();
		if($query->num_rows() > 0)
		{
			return $res->GB;
		}
		else
		{
			return 0;
		}
	
	
	}
	
	function getAllTotalfileStored()
	{
		$sql = "SELECT sum(filesize) as GB  FROM `folderfile`
				";
		$query = $this->db->query($sql);
        $res = $query->row();
        if($query->num_rows() > 0)
	      {
	        return $res->GB;
		  }
		else
		 {
			return 0;
		 }
	}
	
	function formatBytes($size, $precision = 2)
	{
		$n=0;
		$base = log($size) / log(1024);
		$suffixes = array('', 'kb', 'Mb', 'GB', 'TB');
		if(!is_nan(round(pow(1024, $base - floor($base)), $precision)))
		{
			$n = round(pow(1024, $base - floor($base)), $precision)." ".$suffixes[floor($base)];
		}
		return $n ;
	}
	
	
	function checkWritePermissionOnDevice($folderid,$deviceid)
	{
		$ret = 0;
		$sql = "select * from folderalloweddevices where folderid = {$folderid} and deviceid = {$deviceid} and permissionid = 2";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
	
	function checkEditSavePermissionOnDevice($folderid,$deviceid)
	{
		$ret = 0;
		$sql = "select * from folderalloweddevices where folderid = {$folderid} and deviceid = {$deviceid} and permissionid = 4";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
	
	function checkEditSavePermissionOnUser($folderid,$userid)
	{
		$ret = 0;
		$sql = "select * from folderallowedusers where folderid = {$folderid} and companyuserid = {$userid} and permissionid = 4";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
	
	
	
	
	
      function checkWritePermissionOnUser($folderid,$userid)
	{
		$ret = 0;
		$sql = "select * from folderallowedusers where folderid = {$folderid} and companyuserid = {$userid} and permissionid = 2";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
	
	
	
	
	
	
	function checkDeletePermissionOnDevice($folderid,$deviceid)
	{
		$ret = 0;
		$sql = "select * from folderalloweddevices where folderid = {$folderid} and deviceid = {$deviceid} and permissionid = 5";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
       function checkDeletePermissionOnUser($folderid,$userid)
	{
		$ret = 0;
		$sql = "select * from folderallowedusers where folderid = {$folderid} and companyuserid = {$userid} and permissionid = 5";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$ret = 1;		 
		}
		return $ret;
		
	}
	
	function getFileUpdatedTimeStamp($fileid)
	{
	
	                $this->db->select('updatedon');
			$this->db->where('id', $fileid);
			$query = $this->db->get('folderfile');
			$query = $query->row();
			return $query->updatedon;
	
	}
	

}
?>