<?php
class Folderusersmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function getFoldersForUser($companyuserid)
	{
		$this->db->select('folderid');
		$this->db->where('companyuserid', $companyuserid);
		$query = $this->db->get('folderallowedusers');
		return $query->result_array();
	}
	
	
	function getFoldersForAdmin($companyid)
	{
	
	        $this->db->select('folderid');
		$this->db->where('companyid', $companyid);
		//$this->db->where('parentfolderid !=', NULL);
		$query = $this->db->get('folder');
		return $query->result_array();
	
	}
	
	
	function getPermissions($folderid,$companyuserid)
	{
		$this->db->select('folderallowedusers.permissionid,permissions.name');
		$this->db->from('folderallowedusers');
		$this->db->join('permissions', 'folderallowedusers.permissionid= permissions.permissionid');
		$this->db->where('folderid', $folderid);	
		$this->db->where('companyuserid', $companyuserid);	
		$query = $this->db->get();
		return $query->result_array();
	}
	function deleteByFoler($folderid)
	{
			$this->db->where('folderid', $folderid);;
      		$query = $this->db->delete('folderallowedusers');
      		return $this->db->affected_rows();
	}
	function insertFolderUser($datacc)
	{
		 $this->db->insert('folderallowedusers', $datacc);
		return $this->db->affected_rows();
	}
	
	function getUserPermissions($folderid)
	{
		$this->db->select('*');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folderallowedusers');
		return $query->result_array();
	}
	function deleteByUser($userid)
	{
			$this->db->where('companyuserid', $userid);	
      		$query = $this->db->delete('folderallowedusers');
      		return $this->db->affected_rows();
	}
}
?>