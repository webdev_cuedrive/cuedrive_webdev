<?php
class Openfiremodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	
	function sendDataToServer($service_url)
	{
	
	$curl = curl_init($service_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	if ($curl_response === false) {
		$info = curl_getinfo($curl);
		curl_close($curl);
		die('error occured during curl exec. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	
	}
	
	
	
	public function generateRandomToken()
	{
		
		$length = 8;
		$pin = "";
		$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
		// check for length overflow and truncate if necessary
		if ($length > $maxlength)
		{
			$length = $maxlength;
		}
		// set up a counter for how many characters are in the password so far
		$i = 0;
		// add random characters to $password until $length is reached
		while ($i < $length)
		{
		
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, $maxlength-1), 1);
				
			// have we already used this character in $password?
			if (!strstr($pin, $char)) {
				// no, so it's OK to add it onto the end of whatever we've already got...
				$pin .= $char;
				// ... and increase the counter by one
				$i++;
			}
		
		}
		
		return $pin;
	}
	
	
	
}	
	
	
?>