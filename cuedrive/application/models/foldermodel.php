<?php
class Foldermodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function getFolderDetails($folderid)
	{
		$this->db->select('*');
		$this->db->where('folderid', $folderid);
		$result = $this->db->get('folder');
		return $result->row_array();
	}
	
	
	
	function getFolderType($folderid)
	{
		$this->db->select('type');
		$this->db->where('folderid', $folderid);
		$result = $this->db->get('folder');
		$result = $result->row_array();
		return $result['type'];
	}
	
	
	
	
	function getPublicFolder($companyid)
	{
		$this->db->select('folderid');
		$this->db->where('companyid', $companyid);
		$this->db->where ('parentfolderid', null);
		$this->db->where('type', 'public');
		$query = $this->db->get('folder');
		return $query->result_array();
	}
	
	function getFolderPath($folderid)
	{
		
		$this->db->select('fullpath');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folder');
		$query = $query->row();
		return $query->fullpath;
	}
	
	function insertFolder($datacc)
	{
		$this->db->insert('folder', $datacc);
		return $this->db->insert_id();
	}
	
	function getParentID($folderid)
	{
		
		$this->db->select('parentfolderid');
		$this->db->where('folderid', $folderid);
		$query = $this->db->get('folder');
		$query = $query->row();
		return $query->parentfolderid;
	}
	

	
	function getAllFolders($companyid)
	{
		$this->db->select('*');
		$this->db->where('companyid', $companyid);
		$this->db->order_by('foldername','asc');
		$query = $this->db->get('folder');
		return $query->result_array();
	}
	
	function getOneLevel($folderid)
	{

		$query=mysql_query("SELECT folderid FROM folder WHERE parentfolderid='".$folderid."'");
		$folder_id=array();
		if(mysql_num_rows($query)>0){
			while($result=mysql_fetch_assoc($query)){
				$folder_id[]=$result['folderid'];
			}
		}   
			return $folder_id;
	}
	
	
	function deleteFolder($folderid)
	{
		$this->db->where('folderid', $folderid);;
      	$this->db->limit(1,0);
      	$query = $this->db->delete('folder');
      	return $this->db->affected_rows();
	}
	
	function foldernameexist($newfoldername,$parentfolderId)
	{
		$this->db->select('folderid');
		$this->db->where('foldername', $newfoldername);
		if($parentfolderId == NULL)	{
			$this->db->where ('fullpath', "/".$newfoldername);
		}	
		if($parentfolderId != NULL)	{
			$this->db->where ('parentfolderid', $parentfolderId);
		}	
		$result = $this->db->get('folder');
		//echo $this->db->last_query();
		$query = $result->row();
		return ($result->num_rows() > 0) ? TRUE : FALSE;	
	}
	
	
	
	function foldernameexist_device($newfoldername, $companyid)
	{
		$this->db->select('folderid');
		$this->db->where('foldername', $newfoldername);	
		$this->db->where ('fullpath', $newfoldername);
		$this->db->where('companyid', $companyid);			
		$result = $this->db->get('folder');
		//echo $this->db->last_query();
		$query = $result->row();
		return ($result->num_rows() > 0) ? "yes" : "no";	
	}
	
	
	function updateFolder($datacc,$updatefolderid)
	{
		$this->db->where('folderid', $updatefolderid);
		$this->db->update('folder', $datacc);
	}
	
	
	
	function getfolderID($foldername,$parentfolderId)
	{
		$this->db->select('folderid');
		$this->db->where('foldername', $foldername);
		$this->db->where ('parentfolderid', $parentfolderId);
		$query = $this->db->get('folder');
		$query = $query->row();
		return $query->folderid;
	}
	function getPrivateRootFolder($companyid)
	{
		$this->db->select('folderid');
		$this->db->where('companyid', $companyid);
		$this->db->where ('parentfolderid', null);
		$this->db->where('type', 'private');
		$query = $this->db->get('folder');
		$query = $query->row();
		return $query->folderid;
	}
	
	
	
	function fetchapprovedemails($folderid)
	{
	  $data = "";
	  $sql = "select emails from folderallowedemails where folderid = {$folderid}";
	  $query = $this->db->query($sql);
	  if($query->num_rows() > 0)
	  {
	    $res=$query->row();
	    $data = $res->emails;
	  }
	  
	   return $data;
	
	}
	
	function fetchapprovedemailsnumbercue($folderid)
	{
	  $data = 0;
	  $sql = "select numbercue from folderallowedemails where folderid = {$folderid}";
	  $query = $this->db->query($sql);
	  if($query->num_rows() > 0)
	  {
	    $res=$query->row();
	    $data = $res->numbercue;
	  }
	  
	   return $data;
	
	}
	
	function savefolderemailpermission($folderid,$emails,$numbercue)
	{
	  $sql = "delete from folderallowedemails where folderid = {$folderid}";
	  $this->db->query($sql);
	  $sql1 = "insert into folderallowedemails (folderid,emails,numbercue) values ({$folderid},'{$emails}',{$numbercue})";
	  $this->db->query($sql1);
	
	}
	
	
	function fetchPermitedEmails($folderid=0,$fileid)
	{
	  $folderid = Folderfilemodel::getFolderid($fileid);
	   $ret = 0;
	   $data = Foldermodel::fetchapprovedemails($folderid);
	   if($data != "")
	   {
	        // send file to particular email addresses
	       if(strpos($data,'@') > 0)
	         {
	   
	          $ret = 1;
	         }
	         else
	         {
	         $ret = 0;
	         $retid = Foldermodel::fetchapprovedemailsnumbercue($folderid);
	   	 if($retid == 1)
	   	 {
	   	 	$ret = 2;
	   	 }
	          
	         }
	   }
	   else {
	   	 
	   	 $retid = Foldermodel::fetchapprovedemailsnumbercue($folderid);
	   	 if($retid == 1)
	   	 {
	   	 	$ret = 2;
	   	 }
	   }
	   return $ret;
	}
	
	
	function foldernameexistinroot($newfoldername, $companyid, $parentfolderId)
	{
		$this->db->select('folderid');
		$this->db->where('foldername', $newfoldername);
		$this->db->where('parentfolderid', $parentfolderId);
		$this->db->where ('companyid', $companyid);
		$result = $this->db->get('folder');
		$query = $result->row();
		return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
	
	
	function getAllParentFolders($parentFolderid)
	{
		$this->db->select('*');
		$this->db->where('parentfolderid', $parentFolderid);
		$query = $this->db->get('folder');
		return $query->result_array();
	}
}
?>