<?php
class Employee_model extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}

function getAllEmployees($companyid) //api
{
//	$this->db->select('*');
//	$this->db->where('company_id', $companyid);
//	$query = $this->db->get('employee');
//	return $query->result_array();

	$this->db->select(" DISTINCT (user_id) FROM wp_usermeta WHERE user_id IN ( SELECT user_id FROM wp_usermeta WHERE meta_key = 'company_id'
AND meta_value = $companyid)");
$query = $this->db->get();
	return $query->result_array();
}

function getEmployeeDetails($empid)
{
	$this->db->select("SELECT * from wp_usermeta WHERE user_id =$empid");
	$query = $this->db->get();
	return $query->result_array();
}
function getAllDepts($companyid)//api
{ 
	$this->db->select('department');
	$this->db->where('company_id', $companyid);
	$query = $this->db->get('employee');
	return $query->result_array();

}
}
?>