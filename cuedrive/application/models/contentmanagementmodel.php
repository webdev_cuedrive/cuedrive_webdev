<?php
class Contentmanagementmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function getContent($type,$id)
	{
	        if($id!=0)
	        {
	        $sql = "select * from contentmanagement where contentid = {$this->db->escape($id)}";
	        }
	        else
	        {
		$sql = "select * from contentmanagement where contentid = 1";
		}
		$query = $this->db->query($sql);
		return $query->row();
		
	}
	
	function UpdateContent($content,$type,$contentid)
	{
		if($type == 'article')
		{
			//$sql = "UPDATE `contentmanagement` SET `articles`='{".base64_encode(stripslashes($content))."}' WHERE `contentid`={$this->db->escape($contentid)}";
			$sql = "UPDATE `contentmanagement` SET `articles`='{".base64_encode($content)."}' WHERE `contentid`={$this->db->escape($contentid)}";
			
		}
		if($type == 'privacy')
		{	
			$sql = "UPDATE `contentmanagement` SET `privacy`='{".base64_encode($content)."}' WHERE `contentid`={$this->db->escape($contentid)}";
		}
		if($type == 'terms')
		{
			$sql = "UPDATE `contentmanagement` SET `terms`='{".base64_encode($content)."}' WHERE `contentid`={$this->db->escape($contentid)}";
			
		}
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	
	
	function AddArticle($content, $title, $cat_id)
	{
	$sql = "INSERT INTO `contentmanagement`(`articles`, `articletitle`, `category_id`) VALUES('{".base64_encode($content)."}', '{$title}', '{$cat_id}')";
	$this->db->query($sql);
	return $this->db->insert_id();
	}
	
	function UpdateArticle($content, $contentid, $title, $cat_id)
	{
	
		$sql = "UPDATE `contentmanagement` SET `articles`='{".base64_encode($content)."}', `articletitle` = '{$title}', `category_id` = '{$cat_id}' WHERE `contentid`={$this->db->escape($contentid)}";
	
	$this->db->query($sql);
	return $this->db->affected_rows();
	}
	
	
	
	
	function GetContentbycompanyid()
	{
		$sql = "select * from contentmanagement
			where companyid = 0";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['contentid']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	
	
	function GetContentDetailsbycompanyid($options = array())
	{
		$this->db->select('*');
	
		$this->db->where('companyid', '0');
		$this->db->order_by("order_id", "asc");
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		
		//$this->db->group_by("contactus.companyid");
		$query = $this->db->get('contentmanagement');
		
		if($query->num_rows() == 0) return false;
	
		if(isset($options['contentid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	function adddoc($options = array())
	{
	$qualificationArray = array('articles','companyid');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('contentmanagement');
		return $this->db->insert_id();
		
	}
	function updatedoc($options = array())
	{
	$qualificationArray = array('articles','companyid','contentid');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->where('contentid',$options['contentid']);
		$this->db->update('contentmanagement');
		return $this->db->affected_rows();
	}
	
	
	function deletearticle()
	{
	$id = $_REQUEST['id'];
		$this->db->where('contentid', $id);
		$this->db->delete('contentmanagement');
		return $this->db->affected_rows();
	}
	
	
	
	function fetcharticle($id)
	{
	      
	        $sql = "select * from contentmanagement where contentid = {$this->db->escape($id)}";
		$query = $this->db->query($sql);
		return $query->row();
	
	}
	
	
	
	
	function getTerms()
	{
	        $id = 1;
	        $sql = "select * from contentmanagement where contentid = {$this->db->escape($id)}";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
		$query = $query->row();
		return base64_decode($query->terms);
		}
		else
		{
		return "";
		}
	}
	
	
	
	
	
	function getPrivacy()
	{
	        $id = 1;
	        $sql = "select * from contentmanagement where contentid = {$id}";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
		$query = $query->row();
		return base64_decode($query->privacy);
		}
		else
		{
		return "";
		}
	}
	
	
	function getHelpDocuments()
	{
	
	  $rslt = array();
	  $sql = "SELECT articles,articletitle FROM contentmanagement LIMIT 1, 1000 ";
	  $query = $this->db->query($sql);
	  
	  $query = $query->result_array();
	  foreach($query as $row)
	  {
	    $rs['articles'] = base64_decode($row['articles']);
	    $rs['articletitle'] = $row['articletitle'];
	  
	    $rslt[] = $rs;
	  
	  }
	  
	  return $rslt;
		
	}
	
	
	
	
	
	
	
}
	?>