<?php
class Companyusermodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function userAuth($username,$password,$deviceid)
	{

		if($this->userLogin($username, $password,$deviceid))
		{
			if ( $this->userActive($username) == TRUE )
			{
				$this->db->select('companyuser.*');
				$this->db->from('companyuser');
				$this->db->join('companydevice', 'companydevice.companyid= companyuser.companyid');
				$this->db->where('companydevice.deviceid', $deviceid);
				$this->db->where('companyuser.username', $username);	
				$this->db->where('companyuser.password', $password);
				$result = $this->db->get();
				return ($result->num_rows() == 1) ? $result->row_array() : "noaccesstodevice";
			}
		       else {
			        return "notactive";
			    }
		}
		else
		{
		
		        if($this->adminLogin($username, $password,$deviceid)) {
		
		            if( $this->adminActive($username) == TRUE ) {
		            
		                  return "companyadmin";
		             }
		        }
		        else {
		        
			  return "invalidlogin";
			}
		}
	}
	function userLogin($username,$password,$deviceid)
	{
		$this->db->select('*');
		$this->db->from('companyuser');
		$this->db->join('companydevice', 'companydevice.companyid= companyuser.companyid');
		$this->db->where('companydevice.deviceid', $deviceid);
		$this->db->where('companyuser.username', $username);	
		$this->db->where('companyuser.password', $password);
		$result = $this->db->get();
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	
	
	
	function adminLogin($username,$password,$deviceid) {
	
	    $this->db->select('*');
		$this->db->from('companyadmin');
		$this->db->join('companydevice', 'companydevice.companyid= companyadmin.companyid');
		$this->db->where('companydevice.deviceid', $deviceid);
		$this->db->where('companyadmin.username', $username);	
		$this->db->where('companyadmin.password', $password);
		$result = $this->db->get();
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	
	}
	
	function fetchAdminID($username,$password) {
	
	    $this->db->select('companyid');
		$this->db->from('companyadmin');
		$this->db->where('companyadmin.username', $username);	
		$this->db->where('companyadmin.password', $password);
		$result = $this->db->get();
		return ($result->num_rows() == 1) ? $result->row()->companyid : 0;
	
	}
	
	
	
	
	
	function userActive($username)
	{
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('isactive', 'yes');
		$result =  $this->db->get('companyuser');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}


	function adminActive($username)
        {
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('isactive', 'yes');
		$result =  $this->db->get('companyadmin');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
        }



	function getUsersByCompany($companyid)
	{
			$this->db->select('*');
			$this->db->where('companyid', $companyid);
			$query = $this->db->get('companyuser');
			return $query->result_array();
	}
	
	function getAllUsers($num, $offset,$companyid,$searchtext)
	{
			$this->db->select('*');
			$this->db->like('username', $searchtext);
			$this->db->where('companyid', $companyid);
			$this->db->limit($num, $offset);
			$query = $this->db->get('companyuser');
			return $query->result_array();
	}
	function getUserCount($companyid,$searchtext)
	{
		$this->db->like('username', $searchtext);
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyuser');
		return $query->num_rows();		
	}	
	function getUserDetails($userid)
	{
		$this->db->select('*');
		$this->db->where('companyuserid', $userid);
		$result = $this->db->get('companyuser');
		return $result->row_array();
	}
	function userExists($username)
	{	
        $bool = FALSE;	
		$this->db->select('username');
		$this->db->where('username', $username);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() > 0) {
		 $bool = TRUE;
		 } 
		else
		{
		  $this->db->select('username');
		  $this->db->where('username', $username);
		  $result1 =  $this->db->get('companyadmin');
		  if($result1->num_rows() > 0) {
		    $bool = TRUE;
		   }  
		
		}
	return $bool;	 
	}
	
	function userEmailExists($email)
	{	

        $bool = FALSE;	
		$this->db->select('email');
		$this->db->where('email', $email);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() > 0) {
		 $bool = TRUE;
		 } 
		else
		{
		  $this->db->select('email');
		  $this->db->where('email', $email);
		  $result1 =  $this->db->get('companyadmin');
		  if($result1->num_rows() > 0) {
		    $bool = TRUE;
		   }  
		
		}
	return $bool;	 
	}
	function userExistsWithID($username,$userid)
	{		
		
		$bool = FALSE;
		$this->db->select('companyuserid');
		$this->db->where('username', $username);
		$this->db->where('companyuserid !=', $userid);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() > 0) 
		{ 
		  $bool = TRUE; 
		}
		else
		{
		  $this->db->select('username');
		  $this->db->where('username', $username);
		  $result1 =  $this->db->get('companyadmin');
		  if($result1->num_rows() > 0) {
		    $bool = TRUE;
		   }
		
		}
		
	  return $bool;
	}
	
	function userEmailExistsWithID($email,$userid)
	{		
		$bool = FALSE;
		$this->db->select('companyuserid');
		$this->db->where('email', $email);
		$this->db->where('companyuserid !=', $userid);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() > 0) 
		{ 
		  $bool = TRUE; 
		}
		else
		{
		  $this->db->select('companyid');
		  $this->db->where('email', $email);
		  $result1 =  $this->db->get('companyadmin');
		  if($result1->num_rows() > 0) {
		    $bool = TRUE;
		   }
		
		}
		
	  return $bool;
	}
	
	function updateCompanyUser($datacc,$userid)
	{
		if ($userid != 0 ) { //update
			$this->db->where('companyuserid', $userid);
			$this->db->update('companyuser', $datacc);
		}
		else // insert
		{
		    $this->db->insert('companyuser', $datacc);
		     $userid = $this->db->insert_id();
		}
		//return $this->db->affected_rows();
		return $userid;
	}
	
	function deleteUser($userid)
	{
		$companyid = $this->session->userdata("companyid");
		 if(Permissionsmodel::checkValidCompanyUser($userid,$companyid) == 1) {
		
		$this->db->where('companyuserid', $userid);
      	$this->db->limit(1,0);
      		$query = $this->db->delete('companyuser');
      		return $this->db->affected_rows();
		 }
	}
	
	function deleteUserFromSiteOwnerUser($userid, $companyid)
	{
		/*$companyid = $this->session->userdata("companyid");*/
		 if(Permissionsmodel::checkValidCompanyUser($userid,$companyid) == 1) {
		
		$this->db->where('companyuserid', $userid);
      	$this->db->limit(1,0);
      		$query = $this->db->delete('companyuser');
      		return $this->db->affected_rows();
		 }
	}
	
	function changeStatus($userid)
	{
		$this->db->query("UPDATE companyuser SET isactive=(SELECT CASE isactive WHEN 'yes' THEN 'no' ELSE 'yes' END) WHERE companyuserid='$userid'"); 
	}
	
	function userauthWeb($username, $password)
	{
	
			if($this->userLoginWeb($username, $password))
			{			
					if($this->userActive($username))
					{
						$this->db->select('*');
						$this->db->where('companyuser.username', $username);
						$this->db->where ('companyuser.password', $password);
						$this->db->join('companyadmin','companyadmin.companyid = companyuser.companyid');
						$result = $this->db->get('companyuser');
						$query = $result->row();
						if(strtotime($query->expirydate) >= strtotime(date('Y-m-d H:i:s'))) 
						{
							$userDetails = $result->row_array();
							if($this->userCompanyIsActive($userDetails['companyid']))
							 {	
								//return $result->row_array();
								return $userDetails;
								
							 }else{
									return "companynotactive";
								   }
						}
						else
						{
							return "expired";
						}
					}
					 else{
							return "notactive";
					}			
			}
			else
			{
				return "invalid";
			}
		 
		
	}
	
/*	function userCompanyIsActive($username,$password)
	{
			$this->db->select('companyuser.companyid');
			$this->db->where('companyuser.username', $username);
			$this->db->where ('companyuser.password', $password);
			$this->db->join('companyadmin','companyadmin.companyid = companyuser.companyid');
			$this->db->where('companyadmin.isactive', 'yes');
			$result = $this->db->get('companyuser');
			$query = $result->row();
			return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
*/
	function userCompanyIsActive($comapnyId)
	{
			$this->db->select('companyid');
			$this->db->where('companyid', $comapnyId);
			$this->db->where('isactive', 'yes');
			$result = $this->db->get('companyadmin');
			$query = $result->row();
			return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}

	
	
	function userLoginWeb($username,$password)
	{
			$this->db->select('companyid');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$result = $this->db->get('companyuser');
			$query = $result->row();
			return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
	
	
	
	public function getCompanyidFromCompanyUserId($userid)
	{
	  $sql = "select companyid from companyuser where companyuserid = {$userid}";
          $query = $this->db->query($sql);
          $query = $query->row();
          return $query->companyid;
	
	}
	
	
	function userExistsWithUsername($username,$userid){		
		$bool = 0;
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('companyuserid !=', $userid);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() == 1) 
		{ 
		 $bool = 1;
		}
		else
		{ 
		    $this->db->select('username');
		    $this->db->where('username', $username);
		
		    $result1 =  $this->db->get('companyadmin');
		    if($result1->num_rows() == 1) 
		    { 
		       $bool = 1;
		    }
		    else
		    {
		      $bool = 0;
		    }
		
		}
		return $bool;
	}
	
	function userExistsWithEmail($email,$userid){		
		$bool = 0;
		$this->db->select('email');
		$this->db->where('email', $email);
		$this->db->where('companyuserid !=', $userid);
		$result =  $this->db->get('companyuser');
		if($result->num_rows() == 1) 
		{
		  $bool = 1;
		}
		else { 
		
		    $this->db->select('email');
		    $this->db->where('email', $email);
		
		    $result1 =  $this->db->get('companyadmin');
		    if($result1->num_rows() == 1) 
		    { 
		       $bool = 1;
		    }
		    else
		    {
		      $bool = 0;
		    }
		
		
		}
		return $bool;
	}
	
	
	
	function updateAllUpdatedbyuser($userid)
	{
	   $sql = "update folder set createdbyuser = NULL,updatedbyuser = NULL where createdbyuser = {$userid} or updatedbyuser = {$userid}";
	   $sql1 ="update folderfile set createdbyuser = NULL,updatedbyuser = NULL where createdbyuser = {$userid} or updatedbyuser = {$userid}";
	   $sql2 = "update folderalloweddevices set updatedbyuser = NULL where updatedbyuser = {$userid}";
	   $sql3 = "update folderallowedusers set updatedbyuser = NULL where updatedbyuser = {$userid}"; 
	   $sql4 = "DELETE FROM `auditlog` WHERE `companyuserid` = {$userid}";  
	   
	   $this->db->query($sql);
	   $this->db->query($sql1);
	   $this->db->query($sql2);
	   $this->db->query($sql3);
	   $this->db->query($sql4);
	
	
	}
	
	
	function genratePrimaryKey($serverid)
        {
		$primarykey=$serverid.time().mt_rand(1000, 9999);
		if($primarykey>9223372036854775807) //max of 64 bit int
		{
			genratePrimaryKey($serverid);
		}
		return $primarykey;
        }
        
        
     function fetchUserID($username , $password)
     {
     	$sql = "select companyuserid from companyuser where username = '{$username}' and password = '{$password}'";
     	$query = $this->db->query($sql);
     	$query = $query->row();
     	return $query->companyuserid;
    
     }
	
	function validateAccountActive($username)
	{
		$this->db->select('companyuser.companyid');
		$this->db->where('companyuser.username', $username);
		$this->db->where('companyuser.isactive', 'yes');
		$this->db->where('companyadmin.isactive', 'yes');
		$this->db->join('companyadmin','companyadmin.companyid = companyuser.companyid');
		$result =  $this->db->get('companyuser');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
}
?>