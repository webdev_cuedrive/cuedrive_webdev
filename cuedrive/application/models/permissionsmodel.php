<?php
class Permissionsmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function getPermissionName($permissionid)
	{
		$this->db->select('name');
		$this->db->where('permissionid', $permissionid);
		$query = $this->db->get('permissions');
		$query = $query->row();
		return $query->name;
	} 
		function getAllPermissions()
	{
		$this->db->select('*');
		$query = $this->db->get('permissions');
		return $query->result_array();
	}
	
	
	function checkValidDevice($deviceid,$companyid)
	{
		$response = 0;
		$query = $this->db->get_where('companydevice', array('deviceid' => $deviceid,'companyid' => $companyid));
		
		if($query->num_rows() > 0)
		{
			$response = 1;
		}
		return  $response;
	}
	
	
	function checkValidCompanyUser($companyuserid,$companyid)
	{
		$response = 0;
		$query = $this->db->get_where('companyuser', array('companyuserid' => $companyuserid,'companyid' => $companyid));
		if($query->num_rows() > 0)
		{
			$response = 1;
		}
		return  $response;
	}
	
	function checkValidTicket($ticketid,$companyid)
	{
		$response = 0;
		$query = $this->db->get_where('contactus', array('ticketid' => $ticketid,'companyid' => $companyid));
		if($query->num_rows() > 0)
		{
			$response = 1;
		}
		return  $response;
	}
	
	function checkValidFolder($folderid,$companyid)
	{
		$response = 0;
		$query = $this->db->get_where('folder', array('folderid' => $folderid,'companyid' => $companyid));
		if($query->num_rows() > 0)
		{
			$response = 1;
		}
		return  $response;
	}
	
	function checkValidFile($fileid,$companyid)
	{
		$folderid = Folderfilemodel::getFolderid($fileid);
		if($folderid!=0){
		$response = Permissionsmodel::checkValidFolder($folderid,$companyid);
		return $response;
		}
		else
		{
		return 0;
		
		}
	}
	
}