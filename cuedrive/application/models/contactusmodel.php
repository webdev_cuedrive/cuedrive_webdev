<?php
class Contactusmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetTickets()
	{
		$sql = "select contactus.* , ca.name from contactus
				join companyadmin ca on ca.companyid = contactus.companyid 
				where contactus.type = 'support'";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	function GetTicketsbycompanyid()
	{
		$companyid = 0;
		if($this->session->userdata('companyid'))
		{
		  $companyid = $this->session->userdata('companyid');
		}
		$sql = "SELECT `contactus`.*, `companyadmin`.`name`, `companyadmin`.`username` FROM (`contactus`) INNER JOIN `companyadmin` ON `companyadmin`.`companyid` = `contactus`.`companyid` WHERE `contactus`.`status` = 'open' AND `contactus`.`type` = 'support' AND `contactus`.`companyid` = {$companyid} GROUP BY `contactus`.`ticketid` ORDER BY `id` desc";
		
		 $query = $this->db->query($sql);
		 return $query->result();
	}
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetFeedbacks()
	{
		$sql = "select contactus.* , ca.name from contactus
				join companyadmin ca on ca.companyid = contactus.companyid
				where contactus.type = 'feedback'";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetFeedbacksbycompanyid()
	{
		$sql = "select contactus.* , ca.name from contactus
				join companyadmin ca on ca.companyid = contactus.companyid
				where contactus.type = 'feedback' and contactus.companyid = {$this->session->userdata('companyid')}";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	function Getticketconversation($id)
	{
	    $sql = "select contactus.* from contactus where ticketid = {$this->db->escape($id)} order by id desc";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 * @return boolean
	 */
	function GetTicketsDetails($options = array())
	{
		$this->db->select('contactus.*,companyadmin.name,companyadmin.username');
	    $qualificationArray = array('like');
		$this->db->where('contactus.status', 'open');
		$this->db->where('contactus.type', 'support');
		if(isset($options['like']))
			{
				$this->db->like('ticketid', $options['like']);
			}
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->join('companyadmin','companyadmin.companyid = contactus.companyid','INNER');
		
		$this->db->group_by("contactus.ticketid");
		$this->db->order_by("id","desc");
		$query = $this->db->get('contactus');
		
		if($query->num_rows() == 0) return false;
	
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	function GetTicketsDetailsbycompanyid($options = array())
	{
	    $companyid = 0;
		if($this->session->userdata('companyid'))
		{
		  $companyid = $this->session->userdata('companyid');
		}
		$this->db->select('contactus.*,companyadmin.name,companyadmin.username');
	    $qualificationArray = array('like');
		$this->db->where('contactus.status', 'open');
		$this->db->where('contactus.type', 'support');
		$this->db->where('contactus.companyid', $companyid);
		       if(isset($options['like']))
			{
				$this->db->like('ticketid', $options['like']);
				
			}
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->join('companyadmin','companyadmin.companyid = contactus.companyid','INNER');
		
		$this->db->group_by("contactus.ticketid");
		$this->db->order_by("id","desc");
		$query = $this->db->get('contactus');
		
		if($query->num_rows() == 0) return false;
	
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
		
		
		
	}
	
	function GetFeedbacksDetails($options = array())
	{

		$this->db->select('contactus.*,companyadmin.name,companyadmin.username,companyadmin.email as companymail');
		
		
		$this->db->where('contactus.type', 'feedback');
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->join('companyadmin','companyadmin.companyid = contactus.companyid','INNER');
		$this->db->order_by('id','desc');
		$query = $this->db->get('contactus');
		
		if($query->num_rows() == 0) return false;
		
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	function Getticketconversationdetails($options = array())
	{
	        $this->db->select('contactus.*');
                $this->db->where('ticketid',($options['ticketid']));
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->order_by('id','desc');
		$query = $this->db->get('contactus');
		
		if($query->num_rows() == 0) return false;
		
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	
	
	}
	
	function GetFeedbacksDetailsbycompanyid($options = array())
	{

		$this->db->select('contactus.*,companyadmin.name,companyadmin.username');
		
		
		$this->db->where('contactus.type', 'feedback');
		$this->db->where('contactus.companyid', $this->session->userdata('companyid'));
		
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->join('companyadmin','companyadmin.companyid = contactus.companyid','INNER');
		$this->db->order_by('id','desc');
		$query = $this->db->get('contactus');
		
		if($query->num_rows() == 0) return false;
		
		if(isset($options['id']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}

	
	/**
	 * Added by Debartha
	 */
	function deletefeedback()
	{
		$id = $_REQUEST['id'];
		$this->db->where('id', $id);
		$this->db->delete('contactus');
		return $this->db->affected_rows();
	}
	
	function deleteticket()
	{
		$id = $_REQUEST['id'];
		$this->db->where('ticketid', $id);
		$this->db->delete('contactus');
		return $this->db->affected_rows();
	}
	
	function deletearticle()
	{
		$id = $_REQUEST['id'];
		$this->db->where('contentid', $id);
		$this->db->delete('contentmanagement');
		return $this->db->affected_rows();
	}
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetTicketsConversation($companyid,$type='support')
	{
		$sql = "select * from contactus
				where type = 'support' and companyid = {$this->db->escape($companyid)}";
		$query = $this->db->query($sql);
		if($query->num_rows() == 0) return false;
		if(isset($options['id']) )
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	/**
	 * Added by Debartha
	 */
	function sendReply($options = array())
	{
		$qualificationArray = array('companyid','email','phone','type','comments','createdon');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('contactus');
		return $this->db->insert_id();
	}
	
	
	
	/**
	 * Added by Debartha
	 */
	function addticket($options = array())
	{
		$qualificationArray = array('type','priority','email','phone','comments','companyid','createdon','sent','ticketid','companyuserid');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('contactus');
		return $this->db->insert_id();
	}
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetArticleList()
	{
		$this->db->select('*');
		$this->db->where('companyid','0');
		$query = $this->db->get('contentmanagement');
		if($query->num_rows() == 0) return false;
		if(isset($options['contentid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 * @return boolean
	 */
	function GetArticleListDetails($options = array())
	{
		$this->db->select('*');
		$this->db->where('companyid','0');
		$this->db->order_by("order_id", "asc");
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
	
		$query = $this->db->get('contentmanagement');
	
		if($query->num_rows() == 0) return false;
	
		if(isset($options['contentid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	function getMarkerValue($ticketid)
	{
	    $sql = "select * from contactus where ticketid = {$this->db->escape($ticketid)} order by id desc limit 1";
	    $query = $this->db->query($sql);
	    $res = $query->row();
	    if($query->num_rows() > 0 && $res->sent == 1)
	    {
	       return 'green';
	    }
	    else
	    {
	       return 'red';
	    }
	
	}
	
	
	public function createTicketFromApp($datacc)
	{
	
		$this->db->insert('contactus', $datacc);
		return $this->db->affected_rows();
	
	}
	
	public function fetchTicketApp($companyuserid,$adminauth)
	{
	
	
	       if($adminauth == 0) {

		 $sql = "SELECT `contactus`.* FROM (`contactus`) WHERE `contactus`.`status` = 'open'  AND `contactus`.`type` = 'support' AND `contactus`.`companyuserid` = {$companyuserid} GROUP BY ticketid ORDER BY `id` desc";
		 
		 }
		 else if($adminauth == 1)
		 {
		 
		 
		 $sql = "SELECT `contactus`.* FROM (`contactus`) WHERE `contactus`.`status` = 'open'  AND `contactus`.`type` = 'support' AND `contactus`.`companyid` = {$companyuserid} GROUP BY ticketid ORDER BY `id` desc";
		 
		 
		 }
		
		 $query = $this->db->query($sql);
		 
		 if($query->num_rows() > 0) {
		 $data = $query->result();
		 foreach($data as $row)
		 {
		    $retDATA1 = array();
		    $ticketid = $row->ticketid;
		    $sql1 = "SELECT `contactus`.* FROM (`contactus`) WHERE ticketid = '{$ticketid}' ORDER BY `id` desc";
		    $query1 = $this->db->query($sql1);
		    $query1 = $query1->result();
		    
		    foreach($query1 as $row1)
		    {
		      $retDATA = array();
		      $retDATA['comments'] = $row1->comments;
		      $retDATA['createdon'] = date('Y-m-d g:i a',strtotime($row1->createdon));
		      $retDATA['priority'] = $row1->priority;
		      $retDATA['sent'] = $row1->sent;
		      $retDATA['ticketid'] = $row1->ticketid;
		      
		      $retDATA1[] = $retDATA;
		      
		     
		    }
		    
		   $retDATA2[] = $retDATA1;
		    
		 }
		 
		 
		 return $retDATA2;
	  }
	  else
	  {
	  
	   $retDATA3 = array();
	   return $retDATA3;
	  
	  
	  }
		
		
		
	
	}
	
	public function fetchCompanyuseridByTicketid($ticketid)
	{
	         $sql = "SELECT `contactus`.* FROM (`contactus`) WHERE ticketid = {$this->db->escape($ticketid)} LIMIT 1";
		 $query = $this->db->query($sql);
		 $query =  $query->row();
		 return $query->companyuserid;
	}

	
}
	?>