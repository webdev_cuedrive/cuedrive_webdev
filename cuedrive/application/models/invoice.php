<?php
class Invoice extends CI_Model {

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		include_once "xero.php";
		define('XERO_KEY','4RVEQBHQD09RAMCYB5WQERXEGYANYK');
		define('XERO_SECRET','ZKW6CGPHRM43MLRXXWE6SLDQYO6P6Y');
        
	}
	// -------------------------------------------------------------------------------------------------
	public function createInvoice($customername, $date, $price, $noofdevice, $priceperdevice, $accounttype, $amountAlreadyPaid)
	{
		// Get the company details and the correct amount of allowed devices
		$companyid = $this->session->userdata("companyid");
		$totalAllowedDevices = Companyadminmodel::fetchNumberOfAllowedDevices($companyid);
		$accountNumber = Companyadminmodel::getCompanyAccountNumber($companyid);
		    
		// Does this need to subtract 2 devices here???
		$noofdevices = ( $totalAllowedDevices + $noofdevice ) - 2;
		 
		// Why is this value hard-coded and not calculated properly here???    
		// $priceperdevice = Invoice::ceil_dec((($priceperdevice * 10)/11),3,".");
		$priceperdevice = 13.64;
	
		// Allocate the correct Xero account number for accounting purposes
	    if($accounttype == 'basic')
        {
             $accountcode = '200-100';
        }
        else if($accounttype == 'standard')
        {
             $accountcode = '200-101';
        }
        else if($accounttype == 'premium' || $accounttype == 'pro')
        {
             $accountcode = '200-102';
        }
        else if($accounttype == 'business')
        {
             $accountcode = '200-103';
        }
	
		// Create the data for the Xero invoice
	    $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );
	    $new_invoice = array(
        	array(
            "Type"=>"ACCREC",
            "Contact" => array(
            	"Name" => $customername
                ),
            "Date"            => $date,
            "DueDate"         => $date,
            "Status"          => "AUTHORISED",
            "LineAmountTypes" => "Exclusive",
            "LineItems" => array(
            	"LineItem" => array(
                	array(
                    	"Description" => "Package Amount",             
                        "UnitAmount"  => $price,
                        "AccountCode" => $accountcode
                    ),
                    array(
                        "Description" => "Devices",
                        "Quantity"    => $noofdevices,
                        "UnitAmount"  => $priceperdevice,
                        "AccountCode" => "200-200"
                         ),
                	array(
                		"Description" => "Amount already paid",
                		"UnitAmount"  => -1 * $amountAlreadyPaid,
                		"AccountCode" => "200-200",
                		"TaxType"     => "NONE"
                		),
				 	array(
                		"Description"          => "Company account number",
                		"CompanyAccountNumber" => $accountNumber
                		)
                     )
                  )
                )
             );

	  	/* echo "<pre>";
	   	print_r($new_invoice);
			echo '</pre>';
	  	*/
		// Get the result of the invoice back
		$invoice_result = $xero->Invoices( $new_invoice );
	   
		/*   echo "<pre>";
	   		print_r($invoice_result);
		 	echo '</pre>';
	   		die;*/
	   
	   //echo json_encode($invoice_result);
	   $invoiceid = $invoice_result['Invoices']['Invoice']['InvoiceID'];
	   $invoicenumber = $invoice_result['Invoices']['Invoice']['InvoiceNumber'];
	   $data = array('invid' => $invoiceid , 'invnumber' => $invoicenumber);
	   
	   return $data;
	}
	// -------------------------------------------------------------------------------------------------	
	public function createDeviceInvoice($name, $date, $quantity, $deviceprice)
	{
		// Get company details
		$companyid = $this->session->userdata("companyid");
		$accountNumber = Companyadminmodel::getCompanyAccountNumber($companyid);
			    	 
		// Why is device hardcoded???
		// $priceperdevice = Invoice::ceil_dec((($priceperdevice * 10)/11),3,".");
		$priceperdevice = 13.64;
			
		/*if($this->session->userdata("companyid") == 224)		{*/
		
		$dayRemaining = Companyadminmodel::getExpiryDate($companyid);
			
		if($dayRemaining <= 15 && $dayRemaining >= 0)	
		{
			// Remove hard-coding
			//$priceperdevice = (1 * 15 / 30) * $dayRemaining;				
			$priceperdevice = 6.82;
		}
		/*	$filefp = fopen('/home/cuedrive/public_html/log.txt', 'a+');
			fwrite($filefp, "\n priceperdevice ->".$priceperdevice);
			fwrite($filefp, "\n quantity ->".$quantity);
			fwrite($filefp, "\n dayRemaining ->".$dayRemaining);
			fwrite($filefp, "\n\n\n\n");
			
			fclose($filefp);*/
		/*}*/
	
	     $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );
	     $new_invoice = array(
            array(
            "Type"=>"ACCREC",
            "Contact" => array(
				"Name" => $name
                ),
			"Date" => $date,
            "DueDate" => $date,
            "Status"  => "AUTHORISED",
            "LineAmountTypes" => "Exclusive",
            "LineItems" => array(
            	"LineItem" => array(
					array(
                    	"Description" => "Extra Devices",
                        "Quantity" => $quantity,
                        "UnitAmount" => $priceperdevice,
                        "AccountCode" => "200-200"
                        ),
				 	array(
                		"Description" => "Company account number",
                		"CompanyAccountNumber" => $accountNumber
						)
					)
				)
			)
		);

		$invoice_result = $xero->Invoices($new_invoice);
	   	/*
	   		if($this->session->userdata("companyid") == 224)		{
			$filefp = fopen('/home/cuedrive/public_html/log.txt', 'a+');
			fwrite($filefp, "\n invoice_result ->".json_encode($invoice_result));
	    	fclose($filefp);
	   	}*/
	   	$invoiceid = $invoice_result['Invoices']['Invoice']['InvoiceID'];
	   	$invoicenumber = $invoice_result['Invoices']['Invoice']['InvoiceNumber'];
	   	$data = array('invid' => $invoiceid , 'invnumber' => $invoicenumber);
	   
	   	return $data;
	}
	// -------------------------------------------------------------------------------------------------	
	public function createInvoiceCron($customername, $date, $price, $noofdevice, $deviceprice, $accounttype)
	{
		// Remove hard-coded value 
		//$priceperdevice = Invoice::ceil_dec((($deviceprice * 10)/11),3,".");
	   	//$priceperdevice = 9.0909;
		$priceperdevice = 13.64;
		 
		/*	 if($this->session->userdata("companyid") == 224)		{	*/
		$dayRemaining = Companyadminmodel::getExpiryDate($companyid);
		if($dayRemaining <= 15 && $dayRemaining >= 0)	{
		
			// Remove hard-coded values
			//$priceperdevice = (1 * 15 / 30) * $dayRemaining;
			$priceperdevice = 6.82;
		}
		/*	
			$filefp = fopen('/home/cuedrive/public_html/log.txt', 'a+');
			fwrite($filefp, "\n priceperdevice ->".$priceperdevice);
			fwrite($filefp, "\n quantity ->".$quantity);
			fwrite($filefp, "\n dayRemaining ->".$dayRemaining);
			fwrite($filefp, "\n price ->".$price);
			fwrite($filefp, "\n\n\n\n\n");
			fclose($filefp);
		*/
		/*	}	*/
		 
		if($accounttype == 'basic')
        {
        	$accountcode = '200-100';
        }
        else if($accounttype == 'standard')
        {
             $accountcode = '200-101';
        }
        else if($accounttype == 'premium' || $accounttype == 'pro')
        {
             $accountcode = '200-102';
        }
        else if($accounttype == 'business')
        {
             $accountcode = '200-103';
        }
	
	    $today = strtotime($date);
	    $due = strtotime('+7 days',$today);
	    $duedate = date("Y-m-d",$due);
	    $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json' );
		$new_invoice = array(
            array(
            "Type"=>"ACCREC",
            "Contact" => array(
            "Name" => $customername
                 ),
            "Date"            => $date,
            "DueDate"         => $duedate,
            "Status"          => "AUTHORISED",
            "LineAmountTypes" => "Exclusive",
            "LineItems"       => array(
        		"LineItem" => array(
			    	array(
                    	"Description" => "Package Amount",             
                        "UnitAmount"  => $price,
                        "AccountCode" => $accounttype
                    ),
                    array(
                    	"Description" => "Devices",
                        "Quantity"    => $noofdevice,
                        "UnitAmount"  => $priceperdevice,
                        "AccountCode" => "200-200"
						)
					)
				)
			)
		);

		// Get the result of the invoice being created
		$invoice_result = $xero->Invoices( $new_invoice );
	  	// echo json_encode($invoice_result);
	   	$invoiceid = $invoice_result['Invoices']['Invoice']['InvoiceID'];
	   	$invoicenumber = $invoice_result['Invoices']['Invoice']['InvoiceNumber'];
	   	$data = array('invid' => $invoiceid , 'invnumber' => $invoicenumber);
		
		return $data;
	}	
	// -------------------------------------------------------------------------------------------------	
	public function payInvoice($invoicenumber, $date, $amount, $accounttype)
	{
		if($accounttype == 'basic')
        {
        	$accountcode = '200-100';
        }
        else if($accounttype == 'standard')
        {
            $accountcode = '200-101';
        }
        else if($accounttype == 'premium' || $accounttype == 'pro')
        {
            $accountcode = '200-102';
        }
        else if($accounttype == 'business')
        {
            $accountcode = '200-103';
        }
        else
        {
            $accountcode = '200-200';
        }
	
	    $xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json');
		$new_payment = array(
	    	array(
	        	"Invoice" => array(
	            	"InvoiceNumber" => $invoicenumber
	            ),
	            "Account" => array(
	            	"Code" => $accountcode
	            ),
	            "Date"   => $date,
	            "Amount" =>$amount
	        )
	    );
		
	    $payment_result = $xero->Payments( $new_payment );	
		//echo json_encode($payment_result);
	}
	// -------------------------------------------------------------------------------------------------	
	function downloadInvoice($invoiceid='1b7d51df-0be6-4964-b3b0-c72542240ab5')
	{  		
		$xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json');
		$pdf_invoice = $xero->Invoices($invoiceid, '', '', '', 'pdf');
		/*if (strpos($pdf_invoice,'oauth_problem') == false)
		 {
			echo PDFERROR;
			exit;
		 }else{*/
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="the.pdf"');
		echo ($pdf_invoice);
		// }
	}
	// -------------------------------------------------------------------------------------------------	
	function saveDownloadInvoice($id)
	{  		
		$xero = new Xero(XERO_KEY, XERO_SECRET, 'publickey.cer', 'privatekey.pem', 'json');
		$pdf_invoice = $xero->Invoices($id, '', '', '', 'pdf');			
		$filefp = fopen($_SERVER['DOCUMENT_ROOT'].'/cuedrive/uploads/invoice.pdf', 'w+');
		fwrite($filefp, $pdf_invoice);
		fclose($filefp);
		
		return 1;
	}
	// -------------------------------------------------------------------------------------------------	
	public function emailInvoice($invoiceid, $email)
	{
		$emailContent = "
			<!DOCTYPE HTML>
			<html>
			<head></head>
			<body style='background-color:#f9f9f9;'>
			<center>
			<div style='width:900px; height:auto; padding-top:50px; padding-bottom:100px;'>
				<div style='margin:0;padding:0; height:60px; background-color:Black;'>
				<center><img alt='cuedrive' style='padding-top:15px;' src='".base_url()."cuedriveLogowhite.png' width='150'/></center>
				</div>
				<div style='text-align:left; background-color:#fff; padding:20px;'>
					<p><span style='font-size:16px'><span style='<?php echo FONTFAMILY;?>'><b>Your account has been paid</b></span></span></p>
					<p><span style='font-size:16px'><span style='<?php echo FONTFAMILY;?>'>Thanks for your continued partnership with cuedrive, please find attached your invoice. </span></span></p>
					<p>&nbsp;</p>
					<p><span style='font-size:16px'><span style='<?php echo FONTFAMILY;?>'>Need extra help? Our support team <a href='https://cuedrive.com'>https://cuedrive.com</a> has the answers you need to get up and running fast. Contact us by phone or email, or visit the Help Centre <a href='https://cuedrive.com/login/faq.php'>https://cuedrive.com/login/faq.php</a>.</span></span></p>
					<p>&nbsp;</p>
					<p><span style='font-size:16px'><span style='<?php echo FONTFAMILY;?>'>Sincerely,<br />
					<strong>The cuedrive team</strong></span></span></p>
					<p>&nbsp;</p>
				</div>
			</div>
			</center>
		</body>
		</html>";
		
		$file = $_SERVER['DOCUMENT_ROOT'].'/cuedrive/uploads/invoice.pdf';			
		$re = $this->invoice->saveDownloadInvoice($invoiceid);
		$file_size = filesize($file);
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		$content = chunk_split(base64_encode($content));
		$filename = "invoice.pdf";
		$from_name = "cuedrive";
		$from_mail = "admin@cuedrive.com";
		$mailto = $email;
		$replyto = "admin@cuedrive.com";
		$subject = "cuedrive attachment";
		
		$uid = md5(uniqid(time()));
		$header = "From: ".$from_name." <".$from_mail.">\r\n";
		$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$header .= "This is a multi-part message in MIME format.\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-type:text/html; charset=\"UTF-8\"\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$header .= $emailContent."\r\n\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
		$header .= "Content-Transfer-Encoding: base64\r\n";
		$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
		$header .= $content."\r\n\r\n";
		$header .= "--".$uid."--";
		$mail = mail($mailto, $subject, "", $header);
		
		/*$newurl = base_url()."index.php/payment/downloadInvoice?invoiceid=".$invoiceid;
		date_default_timezone_set("Asia/Kolkata");
		$filename = 'invoice.pdf';
		$to = $email;
		$htmlbody = $emailContent;
		$subject = "cuedrive attachment";	
		$from="admin@cuedrive.com";
		$headers = "From: $from\r\nReply-To: $from";
		$random_hash = md5(date('r', time()));
		$headers .= "\r\nContent-Type: multipart/mixed;boundary=\"PHP-mixed-".$random_hash."\"";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		// $attachment = chunk_split($pdf_invoice);
		$attachment = chunk_split(base64_encode(file_get_contents(str_replace("&amp;","&",$newurl))));
		$message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative;boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
		$message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/plain;charset=\"iso-8859-1\"\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= $htmlbody;
		$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
		$message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/pdf;name=\"".$filename."\"\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";
		$message .= $attachment;
		$message .= "/r/n--PHP-mixed-$random_hash--";
		$mail = mail( $to, $subject , $message, $headers );*/
	}
	// -------------------------------------------------------------------------------------------------	
	public function emailInvoiceCron($invid, $email)
	{
		$newurl = base_url()."index.php/payment/downloadInvoice?invoiceid=".$invid;
        
		// Set default time to Kolkata???
		date_default_timezone_set("Asia/Kolkata");
        $filename = 'invoice.pdf';
        $to = $email;
		$htmlbody = " Please find the attachment below.";
	    $subject = "cuedrive attachment";	
	    $from="admin@cuedrive.com";
	    $headers = "From: $from\r\nReply-To: $from";
	    $random_hash = md5(date('r', time()));
	    $headers .= "\r\nContent-Type: multipart/mixed;boundary=\"PHP-mixed-".$random_hash."\"";
	    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	    
		// $attachment = chunk_split($pdf_invoice);
	    $attachment = chunk_split(base64_encode(file_get_contents(str_replace("&amp;","&",$newurl))));
	    $message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative;boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
	    $message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/plain;charset=\"iso-8859-1\"\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
	    $message .= $htmlbody;
	    $message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
	    $message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/pdf;name=\"".$filename."\"\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";
	    $message .= $attachment;
	    $message .= "/r/n--PHP-mixed-$random_hash--";
	    $mail = mail( $to, $subject , $message, $headers);
	}
	// -------------------------------------------------------------------------------------------------	
	function fetchInvoice($companyid)
	{ 
		$sql = "SELECT * FROM `transaction` WHERE `companyid` = {$companyid} AND `ispaid` = 0 order by id desc limit 1";
	  	$data = $this->db->query($sql);
	  
	  	return $data->row_array();
	}
	// -------------------------------------------------------------------------------------------------	
	function ceil_dec($number, $precision, $separator)
	{
		// Convert number to decimal
		$numberpart=explode($separator,$number);  
	    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
	    
		if($numberpart[0]>=0)
	    {
			$numberpart[1]=ceil($numberpart[1]);
		}
	    else
	    {
			$numberpart[1]=floor($numberpart[1]);
		}
	
	    $ceil_number = array($numberpart[0],$numberpart[1]);
		
	    return implode($separator,$ceil_number);
	}
	// -------------------------------------------------------------------------------------------------	
	function calculateTotalAmountToPay($upgradePackagePrice, $totalAllowedDevices, $totalCurrentDevicesToAdd, $pricePerDevice)
	{
	   $totalAmountToPay = $upgradePackagePrice + (($totalAllowedDevices + $totalCurrentDevicesToAdd - 2) * $pricePerDevice);
	   
	   return $totalAmountToPay;
	}
	// -------------------------------------------------------------------------------------------------	
	function calculateAmountToBePaid($totalAmountToPay, $totalAmountToPayInLastTransaction, $amountAlreadyUsed)
	{
		$amountToBePaid = $totalAmountToPay - ($totalAmountToPayInLastTransaction - $amountAlreadyUsed);
		
		if(strpos($amountToBePaid,".") > -1)
			$amountToBePaid = $this->ceil_dec($amountToBePaid, 2, ".");
			
		return $amountToBePaid;
	}                                                                
	// -------------------------------------------------------------------------------------------------	
	function calculateAmountAlreadyUsed($currentPackagePrice, $noOfUsedDays, $pricePerDevice, $totalAllowedDevices)
	{
		if($noOfUsedDays > 30)
		{
			$noOfUsedDays = 30;
		}
		$amountAlreadyUsed = (($currentPackagePrice / 30) * $noOfUsedDays) + (($pricePerDevice / 30) * ($totalAllowedDevices - 2) * $noOfUsedDays);
		
		if(strpos($amountAlreadyUsed,".") > -1)
			$amountAlreadyUsed = $this->ceil_dec($amountAlreadyUsed,2,".");

		return  $amountAlreadyUsed;
	}
	// -------------------------------------------------------------------------------------------------		
	function calculateAmountAlreadyPaid($totalAmountToPayInLastTransaction, $amountAlreadyUsed)
	{
		$amountAlreadyPaid = ($totalAmountToPayInLastTransaction - $amountAlreadyUsed);
		
		if(strpos($amountAlreadyPaid,".") > -1)
			$amountAlreadyPaid = $this->ceil_dec($amountAlreadyPaid, 2, ".");
		
		return $amountAlreadyPaid;
	}
	// -------------------------------------------------------------------------------------------------	
	function calculateAmountAlreadyUsedOfPackage($currentPackagePrice, $noOfUsedDays)
	{
		if($noOfUsedDays > 30)
		{
			$noOfUsedDays = 30;
		}
		
		$amountAlreadyUsed = (($currentPackagePrice / 30) * $noOfUsedDays);
		
		if(strpos($amountAlreadyUsed,".") > -1)
			$amountAlreadyUsed = $this->ceil_dec($amountAlreadyUsed, 2, ".");
			
		return  $amountAlreadyUsed;
	}
	// -------------------------------------------------------------------------------------------------	
	function calculateAmountAlreadyUsedOfDevice($noOfUsedDays, $pricePerDevice, $totalAllowedDevices)
	{
		if($noOfUsedDays > 30)
		{
			$noOfUsedDays = 30;
		}
		
		$amountAlreadyUsed = (($pricePerDevice / 30) * ($totalAllowedDevices) * $noOfUsedDays);
		
		if(strpos($amountAlreadyUsed,".") > -1)
			$amountAlreadyUsed = $this->ceil_dec($amountAlreadyUsed, 2, ".");
			
		return  $amountAlreadyUsed;
	}
}
?>
