<?php
class Companyadminmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	
	function userauth($username, $password)
	{
		if($this->userLogin($username, $password))
		{
			if(true)// $this->userApproved($username, $password)
			{
				if($this->userActive($username, $password))
				{
					$this->db->select('*');
					$this->db->where('username', $username);
					$this->db->where('isdeleted', 0);
					$this->db->where ('password', $password);
					$result = $this->db->get('companyadmin');
					if($result->num_rows() > 0) {
						$query = $result->row();
						$this->session->set_userdata('companyid', $query->companyid);	
						//if(strtotime($query->expirydate) >= strtotime(date('Y-m-d H:i:s'))) 
						
					/*	if($query->companyid == '224')	{	*/
					
						if(strtotime(date('Y-m-d',strtotime($query->expirydate.' + 30 days'))) >= strtotime(date('Y-m-d H:i:s')))		
						{							
						  return $query->companyid; 
						}
						else
						{
							return "expired";
						}
						
					/*	}	else	{
							
							if(strtotime($query->expirydate) >= strtotime(date('Y-m-d H:i:s'))) 
							{
								
								  return $query->companyid; 
							}

							else
							{
							  return "expired";
							}
						}*/
						
						
						
					}
					else
					{
					  return "invalid";
					}
				}
				else{
					return "notactive";
				}
			}
			else {
				return "notapproved";
			}
		}
		else
		{
			return "invalid";
		}
	}
	
		function FolderActivation($folderid = '', $checked = '')	{
	
		if($checked != '' && $folderid != '')	{		
			$this->db->where('folderid', $folderid);
			$this->db->update('folder', array('active_mode' => $checked));		
			if($checked)	{
				echo 'Folder automatic activation started.';
			}	else	{
				echo 'Folder automatic activation paused.';
			}	
		}	else	{
			echo false;
		}
		
	}
        function getCompanyUsername($companyid){
		
		$this->db->select('username');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		$query = $query->row();
		return $query->username;
	}
	
	function getCompanyName($companyid){
		
		$this->db->select('name');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		$query = $query->row();
		return $query->name;
	}
	
   function getCompanyAccountNumber($companyid){
		
		$this->db->select('account_number');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		$query = $query->row();
		return $query->account_number;
	}

	
	function getPackageSize($companyid){		
		$this->db->select('storagespace');
		$this->db->from('companyadmin');
		$this->db->join('package', 'companyadmin.packageid= package.packageid');
		$this->db->where('companyadmin.companyid', $companyid);
		$query = $this->db->get();
		$query = $query->row();
		return $query->storagespace;
	}
	
	
	function updateCompanyAdmin($datacc,$companyId)
	{			
		if ($companyId != 0 ) { //update
			$this->db->where('companyid', $companyId);
			$this->db->update('companyadmin', $datacc);
		}
		else // insert
		{
		    $this->db->insert('companyadmin', $datacc);
		    $companyId = $this->db->insert_id();
		}
		return $companyId;
	
	}
	function userLogin($username,$password)
	{
		
			$this->db->select('companyid');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$result = $this->db->get('companyadmin');
			$query = $result->row();
			return ($result->num_rows() == 1) ? TRUE : FALSE;	
	}
	function userActive($username,$password)
	{
			$this->db->select('companyid');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$this->db->where('isactive', 'yes');
			$result =  $this->db->get('companyadmin');
			return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function userApproved($username,$password)
	{
			$this->db->select('companyid');
			$this->db->where('username', $username);
			$this->db->where ('password', $password);
			$this->db->where('status', 'approved');
			$result =  $this->db->get('companyadmin');
			return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	function getAllowedDevices($companyid)
	{
			$this->db->select('noofdevicesallowed');
			$this->db->where('companyid', $companyid);
			$query = $this->db->get('companyadmin');
			$query = $query->row();
			return $query->noofdevicesallowed;
	}
	
	
	/*
	Added by Debartha
	
	*/
	function processCorporateRequest($abn,$acn,$email,$companyid,$tempdata,$username,$phone)
	{
	  $sql = "UPDATE `companyadmin` SET `abn`='{$abn}',`acn`='{$acn}',`type`='corporate',`status`='waiting',`tempdata`='{$tempdata}' WHERE `companyid`={$companyid}";
	  $this->db->query($sql);
	  return $this->db->affected_rows();
	}
	
	
	/**
	 * Added by Debartha
	 * @return boolean
	 */
	function GetCompanyAdmins($options = array(), $flag = '')
	{
		$this->db->select('*');
		$qualificationArray = array('companyid', 'email', 'account_number', 'isactive','like','name', 'expirydate');
		
		$this->db->where('isdeleted', 0);
		if($flag == 'isactiveyes')	{
			$this->db->where('companyadmin.isactive', 'yes');
			$this->db->where('DATEDIFF(CURDATE(),companyadmin.expirydate) <=', 30);
		}	else if($flag == 'archive')	{
			$this->db->where('companyadmin.isactive', 'no');
			$this->db->where('DATEDIFF(CURDATE(),companyadmin.expirydate) >', 30);
			
		}	
		
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
			  if($qualifier == 'like' )
				{
					$this->db->like('companyadmin.name', $options[$qualifier]);
					$this->db->or_like('companyadmin.email', $options[$qualifier]);
					$this->db->or_like('companyadmin.username', $options[$qualifier]);
					$this->db->or_like('companyadmin.account_number', $options[$qualifier]);
				}
				else{
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		
		$query = $this->db->get('companyadmin');
		if($query->num_rows() == 0) return false;
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	
	function GetPendingApprovals()
	{
		$this->db->select('*');
		$this->db->where('isdeleted', 0);
		$this->db->where('type', 'corporate');
		$this->db->where('status !=', 'approved');
		$query = $this->db->get('companyadmin');
		if($query->num_rows() == 0) return false;
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	}
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 * @return boolean
	 */
	function GetCompanyAdminDetails($options = array(), $flag = '')
	{
		//$this->db->select('companyadmin.*,package.price,package.accounttype,package.storagespace,package.chargeperday,package.mobile,package.emailsupport,package.additionaldeviceprice,package.creditlimit');
		$this->db->select('companyadmin.*,package.price,package.accounttype,package.storagespace,package.noofdevices,package.mobile,package.emailsupport,package.additionaldeviceprice,package.creditlimit');
		$qualificationArray = array('companyid', 'email', 'account_number', 'isactive','like','name', 'expirydate');
	
		$this->db->where('companyadmin.isdeleted', 0);
		
		if($flag == 'isactiveyes')	{
			$this->db->where('companyadmin.isactive', 'yes');
		}	else if($flag == 'archive')	{
			$this->db->where('companyadmin.isactive', 'no');
		}
		
		
		if($flag == 'archive') {
			$this->db->where('DATEDIFF(CURDATE(),companyadmin.expirydate) >', 30);
		} else {
			$this->db->where('DATEDIFF(CURDATE(),companyadmin.expirydate) <=', 30);
		}
		//$this->db->order_by("companyadmin.isactive", "ASC"); 
		//$this->db->order_by("companyadmin.createdon", "DESC"); 
		if($flag == 'archive') {
			$this->db->order_by('companyadmin.name','ASC');
		}	elseif($flag == 'dashboard')	{
			$this->db->order_by('companyadmin.createdon','DESC');
		}	else	{
			$this->db->order_by('companyadmin.expirydate','ASC');
		}	
		
		
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
			  
				if($qualifier == 'companyid')
				{
					$this->db->where('companyadmin.companyid', $options[$qualifier]);
				}
				else if($qualifier == 'email')
				{
					$this->db->where('companyadmin.email', $options[$qualifier]);
				}
				else if($qualifier == 'account_number')
				{
					$this->db->where('companyadmin.account_number', $options[$qualifier]);
				}
	
				else if($qualifier == 'like' )
				{
					$this->db->like('companyadmin.name', $options[$qualifier]);
					$this->db->or_like('companyadmin.email', $options[$qualifier]);
					$this->db->or_like('companyadmin.username', $options[$qualifier]);
					$this->db->or_like('companyadmin.account_number', $options[$qualifier]);
				}
				else{
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		$this->db->join('package','package.packageid = companyadmin.packageid','LEFT');
	
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
	
		$query = $this->db->get('companyadmin');
		if($query->num_rows() == 0) return false;
	
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	
	function GetPendingApprovalsDetails($options = array())
	{
		//$this->db->select('companyadmin.*,package.price,package.accounttype,package.storagespace,package.chargeperday,package.mobile,package.emailsupport,package.additionaldeviceprice,package.creditlimit');
		$this->db->select('companyadmin.*,package.price,package.accounttype,package.storagespace,package.noofdevices,package.mobile,package.emailsupport,package.additionaldeviceprice,package.creditlimit');
		$qualificationArray = array('companyid', 'email', 'account_number','isactive','like','name', 'expirydate');
		$this->db->where('isdeleted', 0);
		$this->db->where('companyadmin.type', 'corporate');
		$this->db->where('companyadmin.status !=', 'approved');
		$this->db->order_by("createdon", "desc"); 
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
			  
				if($qualifier == 'companyid')
				{
					$this->db->where('companyadmin.companyid', $options[$qualifier]);
				}
				else if($qualifier == 'email')
				{
					$this->db->where('companyadmin.email', $options[$qualifier]);
				}
				else if($qualifier == 'account_number')
				{
					$this->db->where('companyadmin.account_number', $options[$qualifier]);
				}
				else if($qualifier == 'like' )
				{
					$this->db->like('companyadmin.name', $options[$qualifier]);
					$this->db->or_like('companyadmin.email', $options[$qualifier]);
					$this->db->or_like('companyadmin.account_number', $options[$qualifier]);
				}
				else{
					$this->db->where($qualifier, $options[$qualifier]);
				}
				
				
			}
		}
		$this->db->join('package','package.packageid = companyadmin.packageid','LEFT');
	
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
	
	
		$query = $this->db->get('companyadmin');
	
		if($query->num_rows() == 0) return false;
	
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 */
	function UpdateCompany($options = array())
	{
		$qualificationArray = array('username','email','name','address','phone','type','packageid','expirydate','balanceamount','status','acn','abn','noofdevicesallowed','city','country','zipcode','firstname','lastname');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->where('companyid', $options['companyid']);
		$this->db->update('companyadmin');
		
		return $this->db->affected_rows();
	
	}
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 */
	function addCompany($options = array())
	{
		$qualificationArray = array('username','email','name','address','phone','type','password','packageid');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('companyadmin');
		return $this->db->insert_id();
	}
	/**
	 * Added by Debartha
	 */
	function bannedCompany()
	{
		$companyid = $_REQUEST['companyid'];
		$isactive = (($_REQUEST['isactive'] == 1) ? 'yes' : 'no');
		$this->db->set('isactive', $isactive);
		$this->db->where('companyid', $companyid);
		$this->db->update('companyadmin');
		return $this->db->affected_rows();
	}
	function approveCompany()
	{
		$companyid = $_REQUEST['companyid'];
		$isactive = (($_REQUEST['isactive'] == 1) ? 'approved' : 'denied');
		$this->db->set('status', $isactive);
		$this->db->where('companyid', $companyid);
		$this->db->update('companyadmin');
		return $this->db->affected_rows();
	}
	
	/**
	 * Added by Debartha
	 */
	function totalCompany()
	{
		$sql = "select * from companyadmin where isdeleted = 0";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Added by Debartha
	 */
	function countCorporateCompany()
	{
		$sql = "select * from companyadmin where type = 'corporate' and isdeleted = 0";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Added by Debartha
	 */
	function countBusinessCompany()
	{
		$sql = "select * from companyadmin where type = 'business' and isdeleted = 0";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Added by Debartha
	 */
	function deleteCompany()
	{
		$id = $_REQUEST['companyid'];
		$this->db->where('companyid', $id);
		$this->db->set('isdeleted', 1);
	 
		$this->db->update('companyadmin');
		return $this->db->affected_rows();
	}
	
	
	function resetPassword($email,$password)
	{
		  $sql = "UPDATE `companyadmin` SET  `password`={$this->db->escape($password)} WHERE `email`={$this->db->escape($email)}";
		  $this->db->query($sql);
		  return $this->db->affected_rows();
	}
	
	function resetPasswordCompanyUser($email,$password)
	{
		  $sql = "UPDATE `companyuser` SET  `password`={$this->db->escape($password)} WHERE `email`={$this->db->escape($email)}";
		  $this->db->query($sql);
		  return $this->db->affected_rows();
	}
	
	function getUsername($email)
	{
			$sql = "select * from companyadmin where `email`={$this->db->escape($email)}";
		$query = $this->db->query($sql);
		$res = $query->row();
		return $res->username;
	
	}
	
	function getCompanyUsername1($email)
	{
		$sql = "select * from companyuser where `email`={$this->db->escape($email)}";
		$query = $this->db->query($sql);
		$res = $query->row();
		return $res->username;
	
	}
	
	function userIsExists($email)
	{
		$sql = "select * from companyadmin where `email`={$this->db->escape($email)}";
		$query = $this->db->query($sql);
		return $query->num_rows();
	
	}
	
	function userIsExistsInCompanyUser($email)
	{
		$sql = "select * from companyuser where `email`={$this->db->escape($email)}";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	
	}
	
			function getCompanyEmail($companyid){
			
			$this->db->select('email');
			$this->db->where('companyid', $companyid);
			$query = $this->db->get('companyadmin');
			$res = $query->row();
			return $res->email;
		}
		
		function updateCompanyPassword($username,$newpass,$email,$companyid)
		{
		
		   $sql = "UPDATE `companyadmin` SET  `username`='{$username}', `password`='{$newpass}', `email`='{$email}' WHERE `companyid`={$companyid}";
		   $this->db->query($sql);
		   return $this->db->affected_rows();
		
		}
		
		function checkUsernameIsExist($username)
		{
			$this->db->select('username');
			$this->db->where('username', trim($username));
			$result =  $this->db->get('companyadmin');
			return ($result->num_rows() == 1) ? 1 : 0;
		
		}
		function checkEmailIsExist($email)
		{
			$this->db->select('email');
			$this->db->where('email', trim($email));
			$result =  $this->db->get('companyadmin');
			return ($result->num_rows() > 0) ? 1 : 0;
		}
		
		public function fetchCompanyAdminDetails($companyid)
		{
		
		  $query = $this->db->get_where('companyadmin', array('companyid' => $companyid));
		  return $query->row_array();
		
		}
		/*added by Bhairavi
		*/
		function getCurretServerTimestamp(){
			
			
			//return $this->db->query("SELECT CURRENT_TIMESTAMP;")->row()->CURRENT_TIMESTAMP;
			return $this->db->query("SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP) as newdate;")->row()->newdate;
			
		}
		
		function fetchProfileUsedForDays($companyid)
		{
		   $sql = "select expirydate from companyadmin where `companyid`={$this->db->escape($companyid)}";
		   $query = $this->db->query($sql);
		   $res = $query->row();
		   $expiry =  $res->expirydate;
		   $expirytime = strtotime($expiry);
		   $upgrade = strtotime('-30 days',$expirytime);
		   $upgradedate= date("Y-m-d",$upgrade);
		   $today =(date("Y-m-d"));
		   $date1=date_create($upgradedate);
		   $date2=date_create($today);
		   $diff=date_diff($date1,$date2);
		   return  $diff->format("%a");
		}
		
		function fetchNumberOfAllowedDevices($companyid)
		{
		   $sql = "select noofdevicesallowed from companyadmin where `companyid`={$this->db->escape($companyid)}";
		   $query = $this->db->query($sql);
		   if($query->num_rows() > 0)
		   {
			   $res = $query->row();
			   return $res->noofdevicesallowed;
		   }
		   else 
		   {
				return 0;
		   }
	
		}
		
	
		function fetchCurrentPackageID($companyid)
		{
		   $sql = "select package.* from companyadmin join package on package.packageid = companyadmin.packageid where companyadmin.companyid={$this->db->escape($companyid)}";
		   $query = $this->db->query($sql);
		   $res = $query->row();
		   return $res;
			
			
		}
		
		function fetchLastTransactionTotalAmountToPay($companyid)
		{
		   $sql = "select id,totalamounttopay from transaction where `companyid`={$this->db->escape($companyid)} and comment='package' order by id desc limit 1";
		   $query = $this->db->query($sql);
		   if($query->num_rows() > 0)
		   {
			   $res = $query->row();
			   $totalamount = $res->totalamounttopay;
			   $id = $res->id;
			   $deviceprice = Companyadminmodel::fetchExtraDevicePriceForTheMonth($companyid , $id);
			   if($totalamount > 0)
			   {
			   		$totalamount = 0;
					
			   }else{
			   		
				   $totalamount = $totalamount; 
			   }	   
				   $totalamount += $deviceprice;
				   return $totalamount;
		   }
		   else 
		   {
				return 0;
		   }
		}
		
		function fetchExtraDevicePriceForTheMonth($companyid , $offset)
		{
			   $sum = 0;
			   $sql100 = "SELECT * FROM `transaction` where comment = 'device' and companyid = {$companyid} and id > {$offset}";
			   $query100 = $this->db->query($sql100);
			   if($query100->num_rows() > 0)
			   {
				   $res = $query100->row();
				   $totalamount = $res->totalamounttopay;
				   if($totalamount > 0)
				   {
						$totalamount = 0;
						
				   }else{
						
					   $totalamount = $totalamount; 
				   }	   

				   return $totalamount;
				   
  			   }else{
					return 0;
			   		}
		}
	
		function fetchLastTransactionTotalAmountOfPackage($companyid)
		{
		   $sql = "select id, totalamounttopay, amount from transaction where `companyid`={$this->db->escape($companyid)} and comment='package' order by id desc limit 1";
		   $query = $this->db->query($sql);
		   if($query->num_rows() > 0)
		   {
			   $res = $query->row();
			   //$totalamount = $res->totalamounttopay;
			   $totalamount = $res->amount;
			   return $totalamount;
		   }
		   else 
		   {
				return 0;
		   }
		}
		
		function fetchLastTransactionTotalAmountOfDevice($companyid)
		{
			  $sum = 0;
			  $sql = "SELECT * FROM `transaction` where comment = 'device' and companyid = {$companyid} order by id desc limit 1";
			   $query = $this->db->query($sql);
			   if($query->num_rows() > 0)
			   {
				   $res = $query->row();
				   //$totalamount = $res->totalamounttopay;
				   $totalamount = $res->amount;
				   return $totalamount;
				   
			   }else{
					 return 0;
			   		}
		}


	/**
	 * Added by Sunil
	 * @return boolean
	 */
	function GetCountryList()
	{
		$this->db->select('country_name, country_code');
		$query = $this->db->get('countries');
		return $query->result();
	 }
	
	
	/**
	 * Added by Sunil
	 * @return boolean
	 */
	function GetArchiveList()
	{
		$this->db->select('*');
		$this->db->where('isdeleted', 1);
		$query = $this->db->get('companyadmin');
		if($query->num_rows() == 0) return false;
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result_array();
		}
	 }
	
	/**
	 * Added by Sunil
	 * @param unknown_type $options
	 * @return boolean
	 */
	function GetArchiveAccountDetails($options = array())
	{
		$this->db->select('companyadmin.*,package.price,package.accounttype,package.storagespace,package.chargeperday,package.mobile,package.emailsupport,package.additionaldeviceprice,package.creditlimit');
		$qualificationArray = array('companyid', 'email', 'account_number', 'isactive','like','name');
		$this->db->where('isdeleted', 1);
		
		
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
			  
				if($qualifier == 'companyid')
				{
					$this->db->where('companyadmin.companyid', $options[$qualifier]);
				}
				else if($qualifier == 'email')
				{
					$this->db->where('companyadmin.email', $options[$qualifier]);
				}
				else if($qualifier == 'account_number')
				{
					$this->db->where('companyadmin.account_number', $options[$qualifier]);
				}
	
				else if($qualifier == 'like' )
				{
					$this->db->like('companyadmin.name', $options[$qualifier]);
					$this->db->or_like('companyadmin.email', $options[$qualifier]);
					$this->db->or_like('companyadmin.username', $options[$qualifier]);
					$this->db->or_like('companyadmin.account_number', $options[$qualifier]);
				}
				else{
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		$this->db->join('package','package.packageid = companyadmin.packageid','LEFT');
	
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
	
	
		$query = $this->db->get('companyadmin');
	
		if($query->num_rows() == 0) return false;
	
		if(isset($options['companyid']) && isset($options['email']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	 }


	function userIsActive($email)
	{
		$sql = "select * from companyadmin where `email`={$this->db->escape($email)} and `isactive` = 'yes'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	
	}
	
	
	function companyUserIsActive($email)
	{
		$sql = "select * from companyuser where `email`={$this->db->escape($email)} and `isactive` = 'yes'";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	
	}
	
	function validateAccountActive($username)
	{
		$this->db->select('companyid');
		$this->db->where('username', $username);
		$this->db->where('isactive', 'yes');
		$result =  $this->db->get('companyadmin');
		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}
	
	
	function userExistsWithUsername($username,$companyid){		
		$bool = 0;
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('companyid !=', $companyid);
		$result =  $this->db->get('companyadmin');
		if($result->num_rows() == 1) 
		{ 
		 $bool = 1;
		}
		else
		{ 
		    $this->db->select('username');
		    $this->db->where('username', $username);
		
		    $result1 =  $this->db->get('companyuser');
		    if($result1->num_rows() == 1) 
		    { 
		       $bool = 1;
		    }
		    else
		    {
		      $bool = 0;
		    }
		
		}
		return $bool;
	}
	
	function userExistsWithEmail($email,$companyid){		
		$bool = 0;
		$this->db->select('email');
		$this->db->where('email', $email);
		$this->db->where('companyid !=', $companyid);
		$result =  $this->db->get('companyadmin');
		if($result->num_rows() == 1) 
		{
		  $bool = 1;
		}
		else { 
		
		    $this->db->select('email');
		    $this->db->where('email', $email);
		
		    $result1 =  $this->db->get('companyuser');
		    if($result1->num_rows() == 1) 
		    { 
		       $bool = 1;
		    }
		    else
		    {
		      $bool = 0;
		    }
		
		}
		return $bool;
	}
	
	function companyNameExists($companyname,$companyid)
	{		
		$this->db->select('name');
		$this->db->where('name', $companyname);
		$this->db->where('companyid !=', $companyid);
		$result =  $this->db->get('companyadmin');
		return ($result->num_rows() >= 1) ? TRUE : FALSE;
	}
	
	function checkCompanyNameIsExist($companyname)
	{
		$this->db->select('name');
		$this->db->where('name', trim($companyname));
		$result =  $this->db->get('companyadmin');
		return ($result->num_rows() > 0) ? 1 : 0;
	}
	
	function getExpiryDate($companyid = '')	{
		$this->db->select('expirydate');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		
		
		if($query->num_rows() > 0)	{
			
			foreach($query->result() as $row):				
				$expiryDate = strtotime($row->expirydate);
			endforeach;
			
			$currentDate = strtotime(date("Y-m-d"));
			
			$diff = $expiryDate - $currentDate;
			
			return $days = floor($diff/ (60*60*24));
								
		} else	{
			return false;
		}
		
		
	}
	
	function returnExpiryDate($companyid = '')	{
		$this->db->select('expirydate');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		
		
		if($query->num_rows() > 0)	{
			
			foreach($query->result() as $row):				
				$expiryDate = strtotime($row->expirydate);				
			endforeach;			
			$currentDate = strtotime(date("Y-m-d"));			
			$diff = $expiryDate - $currentDate;			
			$days = floor($diff/ (60*60*24));
			if($days < 0)	{
				return date('d/m/Y', $expiryDate);
			}	else	{
				return false;
			}

		} else	{
			return false;
		}
		
		
	}
	
	
	function getPassword($companyid = '', $password = '')	{
		
		$this->db->select('password');
		$this->db->where('companyid', $companyid);
		$this->db->where('isdeleted', 0);
		$query = $this->db->get('companyadmin');
		
		if($query->num_rows() > 0)	{
			foreach($query->result() as $row)	:		
				if(md5($password) == $row->password)	{
					return true;
				} else {
					return false;
				}
			endforeach;
		} else {
			return false;
		}
	}
	
	function cancelAccount($companyid = '', $isactive = '')	{
		
		//$companyid = $_REQUEST['companyid'];
		$isactive = (($isactive == 1) ? 'yes' : 'no');
		$this->db->set('isactive', $isactive);
		$this->db->where('companyid', $companyid);
		$this->db->update('companyadmin');
		return true;
	}
	
	function updatebucketdataflag($companyid = '', $flag = '')	{
		
		$this->db->set('bucketdata', $flag);
		$this->db->where('companyid', $companyid);
		$this->db->update('companyadmin');
		
		return true;
	}
	
	function checkAccountType($companyid = '')	{
		
		$this->db->select('type');
		$this->db->where('companyid', $companyid);
		$this->db->where('isdeleted', 0);
		$query = $this->db->get('companyadmin');
		
		if($query->num_rows() > 0)	{
			foreach($query->result() as $row)	:		
				if('corporate' == $row->type)	{
					return true;
				} else {
					return false;
				}
			endforeach;
		} else {
			return false;
		}
		
	}
	
	function verifycompanyexistance($companyid)	{
		
		$this->db->select('name');
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('companyadmin');
		
		if($query->num_rows() > 0)	{
			return true;	
		}	else	{
			return false;	
		}
		
	}
	
	function getcompanuuserdetailsfromcomapnyadmin($companyId = '')	{
		$this->db->select('username,companyuserid');
		$this->db->where('companyid', $companyId);
		$query = $this->db->get('companyuser');
		
		if($query -> num_rows() > 0)	{
		
			return $query->result_array();			
		}	else	{
			return false;
		}		
	}
	
	function getTokenFromCompanyAdmin($companyId)	{
		
		$this->db->select('customerToken');
		$this->db->where('companyid', $companyId);
		$query = $this->db->get('companyadmin');
		
		if($query -> num_rows() > 0)	{	
			foreach($query->result() as $row):		
				return $row->customerToken;			
			endforeach;
		}	else	{
			return false;
		}
	}
}
?>