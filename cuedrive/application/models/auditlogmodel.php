<?php
class Auditlogmodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}
	// -------------------------------------------------------------------------------------------------  	
	public function updateAuditLog($datacc)
	{
		$this->db->insert('auditlog', $datacc);

		return $this->db->affected_rows();
	}
	// -------------------------------------------------------------------------------------------------  
	function getAuditlogDetails($options = array())
	{
		$queryA = NULL;
		$queryB = NULL;
		$queryC = NULL;
	    $qualificationArray = array('like','to','from');
		
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]))
			{
				if($qualifier == 'like' )
				{
					$queryA = "AND (`auditlog`.`action` LIKE '%".$options[$qualifier]."%' OR `auditlog`.`activitydetails` LIKE '%".$options[$qualifier]."%' OR `auditlog`.`datetime` LIKE '%".$options[$qualifier]."%')";
				}
				
				if($qualifier == 'to' || $qualifier == 'from')
				{
					$queryC = "AND `datetime` BETWEEN '".$options['from']."' AND '".$options['to']."'";
				}
			}
		}
		if(isset($options['limit']) && isset($options['offset']))
		{
			$queryB = "ORDER BY `logid` desc LIMIT ".$options['offset'].",".$options['limit'];
		}
		else if(isset($options['limit']))
		{
			$queryB = "ORDER BY `logid` desc LIMIT ".$options['limit'];
		}
		
		$sql = "SELECT * FROM (`auditlog`) WHERE `auditlog`.`companyid` = '".$this->session->userdata('companyid')."'".$queryA.$queryB.$queryC;
		$query = $this->db->query($sql);
		
		//Auditlogmodel::exportDataLog($sql);
		if($query->num_rows() == 0) return false;
	
		if(isset($options['logid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
}	
	
?>