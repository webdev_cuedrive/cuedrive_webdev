<?php
class Packagemodel extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

		$this->load->database();
	}

	function getBusinessPackages()
	{
		$this->db->where('accounttype', 'business');
		$this->db->limit(5, 1);
		$query = $this->db->get('package');
		return $query->result_array();
	}
	
	function getDetails($packageid)
	{
		$this->db->select('*');
		$this->db->where('packageid', $packageid);
		$query = $this->db->get('package');
		return $query->row_array();
	}
	
	function getBusinessPackage()
	{
		$this->db->where('accounttype', 'business');
		//$this->db->limit(5, 1);
		$query = $this->db->get('package');
		return $query->result_array();
	}
	
	
	function getActivePackageDetails($id)
	{
	
	  $sql = "select package.*, companyadmin.noofdevicesallowed from companyadmin join package on companyadmin.packageid = package.packageid where companyid = {$id}";
	  $query = $this->db->query($sql);
	  return $query->result_array();
	
	}
	
	function getActivePackageDetailsByUsername($username)
	{
	
	  $sql = "select package.* from companyadmin join package on companyadmin.packageid = package.packageid where username = '{$username}'";
	  $query = $this->db->query($sql);
	  return $query->row_array();
	
	}
	
		
	function getUserDetails($id)
	{
		$this->db->where('companyid', $id);
		//$this->db->limit(5, 1);
		$query = $this->db->get('companyadmin');
		return $query->result_array();
	}

	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 * @return boolean
	 */
	function getBusinessPackagesDetails($options = array())
	{
		$this->db->select('*');
		$qualificationArray = array('packageid', 'type','accounttype','like','price');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
				if($qualifier == 'packageid')
				{
						
					$this->db->where('package.packageid', $options[$qualifier]);
				}
				else if($qualifier == 'like' )
				{
						
					//$this->db->like('package.price', $options[$qualifier]);
					$this->db->or_like('package.type', $options[$qualifier]);
					
				}
				else{
	
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
	
		$this->db->where('accounttype', 'business');
		$query = $this->db->get('package');
	
		if($query->num_rows() == 0) return false;
	
		if(isset($options['packageid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	
	/**
	 * Added by Debartha
	 */
	function getCorporatePackages()
	{
		$this->db->where('accounttype', 'corporate');
		$query = $this->db->get('package');
	
	
		return $query->result_array();
	
	}
	
	
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 * @return boolean
	 */
	function getCorporatePackagesDetails($options = array())
	{
		$this->db->select('*');
		$qualificationArray = array('packageid', 'type','accounttype','like','price');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier]) )
			{
				if($qualifier == 'packageid')
				{
						
					$this->db->where('package.packageid', $options[$qualifier]);
				}
				else if($qualifier == 'like' )
				{
						
					$this->db->like('package.price', $options[$qualifier]);
					$this->db->or_like('package.type', $options[$qualifier]);
				}
				else{
						
					$this->db->where($qualifier, $options[$qualifier]);
				}
			}
		}
		if(isset($options['limit']) && isset($options['offset'])) $this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit'])) $this->db->limit($options['limit']);
		$this->db->where('accounttype', 'corporate');
		$query = $this->db->get('package');
	
		if($query->num_rows() == 0) return false;
	
		if(isset($options['packageid']))
		{
			return $query->row(0);
		}
		else
		{
			return $query->result();
		}
	}
	
	/**
	 * Added by Debartha
	 */
	function getPackages($packageid)
	{
		$this->db->where('package.packageid', $packageid);
		$query = $this->db->get('package');
		return $query->result();
	}
	
	/**
	 * Added by Debartha
	 */
	function getBusinessAllPackages()
	{
		$this->db->where('accounttype','business');
		$query = $this->db->get('package');
		return $query->result();
	}
	
	function getAllPackages()
	{
		$this->db->where('accounttype','corporate');
		$query = $this->db->get('package');
		return $query->result();
	}
	
	/**
	 * Added by Debartha
	 */
	function UpdatePackage($options = array())
	{	
		$qualificationArray = array('accounttype','price','name','forduration','type','storagespace','additionaldeviceprice','noofdevices','emailsupport','creditlimit');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->where('packageid', $options['packageid']);
		$this->db->update('package');
		return $this->db->affected_rows();
	}
	
	/**
	 * Added by Debartha
	 * @param unknown_type $options
	 */
	function addPackage($options = array())
	{	
		$qualificationArray = array('accounttype','price','name','forduration','type','storagespace','additionaldeviceprice','noofdevices','mobile','emailsupport','createdon','creditlimit');
		foreach($qualificationArray as $qualifier)
		{
			if(isset($options[$qualifier])) $this->db->set($qualifier, $options[$qualifier]);
		}
		$this->db->insert('package');
		return $this->db->insert_id();
	}
	
	/**
	 * Added by Debartha
	 */
	function deletepackage()
	{
		$id = $_REQUEST['packageid'];

		$this->db->where('packageid', $id);
		$this->db->delete('package');
		return $this->db->affected_rows();
	}
	
	
	
	
	
}
	?>