<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

 function creditamount($companyid)
  {
		$CI = & get_instance();
		$CI->load->model('companyadminmodel');
		$CI->load->model('invoice');
		$currentPackageDetails = $CI->companyadminmodel->fetchCurrentPackageID($companyid);
		$currentPackagePrice = $currentPackageDetails->price;
		$pricePerDevice = $currentPackageDetails->additionaldeviceprice;
		$totalAllowedDevices = $CI->companyadminmodel->fetchNumberOfAllowedDevices($companyid);
		$noOfUsedDays = $CI->companyadminmodel->fetchProfileUsedForDays($companyid);
		//Package
	    $totalAmountToPayInLastTransactionOfPackage = $CI->companyadminmodel->fetchLastTransactionTotalAmountOfPackage($companyid);
	    $amountAlreadyUsedOfPackage = $CI->invoice->calculateAmountAlreadyUsedOfPackage($currentPackagePrice, $noOfUsedDays);
	   //Device
	    $totalAmountToPayInLastTransactionOfDevice = $CI->companyadminmodel->fetchLastTransactionTotalAmountOfDevice($companyid);
	    $amountAlreadyUsedOfDevice = $CI->invoice->calculateAmountAlreadyUsedOfDevice($noOfUsedDays, $pricePerDevice, $totalAllowedDevices);
		//already paid extra
	    $extraPaidForDeviceAndPackage = $CI->companyadminmodel->fetchLastTransactionTotalAmountToPay($companyid);
	    $amountAlreadyPaidOfPackage = $CI->invoice->calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfPackage, $amountAlreadyUsedOfPackage);
	    $amountAlreadyPaidOfDevice = $CI->invoice->calculateAmountAlreadyPaid($totalAmountToPayInLastTransactionOfDevice, $amountAlreadyUsedOfDevice);
		$creditamount = (ltrim($extraPaidForDeviceAndPackage, '-') + ($amountAlreadyPaidOfPackage + $amountAlreadyPaidOfDevice));
		return number_format($creditamount, 2, '.', '');
		
 }
