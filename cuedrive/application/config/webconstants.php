<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Messages
define('INVALIDLOGIN', 'Invalid Username or Password');
define('NOTAPPROVED', 'Sorry, you do not have permission to login');
define('NOTACTIVE', 'Sorry, you do not have permission to login');
define('NOFOLDERNAME', 'Please enter folder name');
define('NOPARENTFOLDER', 'Please select a folder');
define('NOFILESELECTED', 'Please select a file');
define('NOFILEFOLDERSELECTED', 'Please select a file or folder');
define('CREATEFOLDERFAIL', 'Oops, something went wrong when creating the folder, please try again');
define('FOLDERNAMEEXIST', 'Folder name already exists, please select another name');
define('DEVICENAMEEXIST', 'Device name already exists, please select another name');
define('COMPANYNAMEEXIST', 'Company name already exists, please select another name');
define('NEWRECORDADDED', "Document Added");
define('RECORDUPDATED', "Document Updated");
define('DEVICEEXIST', 'This device already exists');
define('CREATEFOLDER', 'Create Folder');
define('UPLOADFILE', 'Upload File');
define('ACCOUNTEXPIRED', 'Sorry, your account has expired');
define('PDFERROR', 'Sorry, this Invoice is not available');
define('EDITFOLDER', 'Edit Folder');
define('PASSWORDWITHSPCCHAR', 'Sorry, we need your password to contain at least one number, or a special character(?$#@!%^&*)');
/* End of Messages and webconstants */
/* End of file webconstants.php */
/* Location: ./application/config/webconstants.php */
