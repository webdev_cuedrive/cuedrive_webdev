<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* Messages and constants 
 * Added by Bhairavi
 * */
 
 /*
 * amazon Bucket
 */
define('ROOTBUCKET', 'cuedrive');
define('COMPANYFOLDERNAME', 'cuedrive');

//Messages
define('APIDEVNTFND', 'Device not found, please try again');
define('APIDEVDSBL', 'Sorry, this device has been disabled, please contact your office to enquire about this issue');
define('APINOFILE', 'File or Folder cannot be found to download');
define('APISESSEXP', 'Session has expired, please login again');
define('APIUSRLOGNFAIL', 'Username or Password is incorrect, please try again.');
define('APIUSRNOACCESS', 'Sorry, you do not have permission to access this device');
define('APIUSRNOACTIVE', 'Sorry, you do not have permission to access this device');
define('APIFILEUPLOADFAIL', 'Oops, something went wrong when uploading the file, please try again');
define('APIFILEUPLOADSUCCESS', 'File Uploaded');
define('APIFILEUPDATEFAIL', 'Oops, something went wrong when updating the file, please try again');
define('APIFILEUPDATESUCCESS', 'File Updated');
define('DELETESUCCESS', 'File Deleted');
define('DELETEFAIL', 'Oops, something went wrong when deleting the file, please try again');
define('APIFILEMOVEFAIL', 'Oops, something went wrong when moving the file, please try again');
define('APIFILEMOVESUCCESS', 'File Moved');
define('APIFILESIZEEXCEED', 'Sorry, uploading this file will exceed your company limit. Please contact your office to increase your package size');
define('APIFILEEXIST', 'File name already exists, please select another name');
define('DELETESUCCESSFOLDER', 'Folder deleted');
define('APIFOLDERMOVESUCCESS', 'Folder moved');
define('APIFOLDERMOVEFAIL', 'Oops, something went wrong when moving the folder, please try again');
define('APIFOLDEREXIST', 'Folder name already exists, please select another name');
define('USEREXIST', 'Username already exists, please select another Username');
define('NOPERMISSION', 'Sorry, you do not have permission to access this folder');

define('INVALIDLOGIN', 'Invalid Username or Password');
define('NOTAPPROVED', 'Sorry, you do not have permission to login');
define('NOTACTIVE', 'Sorry, you do not have permission to login');
define('NOFOLDERNAME', 'Please enter folder name');
define('NOPARENTFOLDER', 'Please select a folder');
define('NOFILESELECTED', 'Please select a file');
define('NOFILEFOLDERSELECTED', 'Please select a file or folder');
define('CREATEFOLDERFAIL', 'Oops, something went wrong when creating the folder, please try again');
define('FOLDERNAMEEXIST', 'Folder name already exists, please select another name');
define('NEWRECORDADDED', "Document Added");
define('RECORDUPDATED', "Document Updated");
define('DEVICEEXIST', 'This device already exists');

define('CREATEFOLDER', 'Create Folder');
define('UPLOADFILE', 'Upload File');

define("FONTFAMILY", "font-family: Helvetica, serif");

//constants
define('DOWNLOADURLEXPIRYTIME', 60);//1 minutes
define('CURRENCY', '$');
define ('PERPAGE','20'); //pagination


define('PERMISSION_INFORMATION', serialize(array("Allow users to view the file only. with read only selected users will not be able to modify or move the file.",
                                        "Allow users to modify or change the file, with write only selected users will not be able to save files to this location.",
                                        "Allow users to email files, they will be able to email to anyone unless the below email fields are filled with one or more emails.",
                                        "Allow users to edit the document and also save to this folder..",
                                        "Allow users to delete files from this folder.",
                                        "This option will send out a notification to users when a file in this folder is changed or a new file is added.")));




/* End of Messages and constants */

/* End of file constants.php */
/* Location: ./application/config/constants.php */