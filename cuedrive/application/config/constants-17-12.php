<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* Messages and constants 
 * Added by Bhairavi
 * */
 
 /*
 * amazon Bucket
 */
define('ROOTBUCKET', 'cuedrive');
define('COMPANYFOLDERNAME', 'cuedrive');

//Messages
define('APIDEVNTFND', 'Device not found!');
define('APIDEVDSBL', 'Device is disabled!');
define('APINOFILE', 'Couldn\'t find any file!');
define('APISESSEXP', 'Session expired!');
define('APIUSRLOGNFAIL', 'Username or Password is incorrect!');
define('APIUSRNOACCESS', 'User is not having access to the device!');
define('APIUSRNOACTIVE', 'User is not active!');
define('APIFILEUPLOADFAIL', 'Something went wrong while uploading your file!');
define('APIFILEUPLOADSUCCESS', 'Successfully uploaded the file!');
define('APIFILEUPDATEFAIL', 'Something went wrong while updating your file!');
define('APIFILEUPDATESUCCESS', 'Successfully updated the file!');
define('DELETESUCCESS', 'Successfully deleted the file!');
define('DELETEFAIL', 'Something went wrong while deleting your file!');
define('APIFILEMOVEFAIL', 'Something went wrong while moving your file!');
define('APIFILEMOVESUCCESS', 'Successfully moved the file!');
define('APIFILESIZEEXCEED', 'File size available package space!');
define('APIFILEEXIST', 'File already exist!');
define('DELETESUCCESSFOLDER', 'Successfully deleted the folder!');
define('APIFOLDERMOVESUCCESS', 'Successfully moved the folder!');
define('APIFOLDERMOVEFAIL', 'Something went wrong while moving your folder!');
define('APIFOLDEREXIST', 'Folder already exist!');
define('USEREXIST', 'Username already exist!');
define('NOPERMISSION', 'You are not having enough permission!');

define('INVALIDLOGIN', 'Invalid Username or Password!');
define('NOTAPPROVED', 'User not approved!');
define('NOTACTIVE', 'User not active!');
define('NOFOLDERNAME', 'Please enter folder name!');
define('NOPARENTFOLDER', 'Please select parent folder !');
define('NOFILESELECTED', 'Please select file!');
define('NOFILEFOLDERSELECTED', 'Please select file or folder!');
define('CREATEFOLDERFAIL', 'Something went wrong while creating new folder!');
define('FOLDERNAMEEXIST', 'Folder name already exist!');
define('NEWRECORDADDED', "New Record has been successfully added!");
define('RECORDUPDATED', "Record has been successfully updated!");
define('DEVICEEXIST', 'DeviceUUID already exist!');
define("FONTFAMILY", "font-family: Helvetica, serif");

//constants
define('DOWNLOADURLEXPIRYTIME', 120);//2 minutes
define('CURRENCY', '$');
define ('PERPAGE','20'); //pagination


define('PERMISSION_INFORMATION', serialize(array("Allow users to view the file only. with read only selected users will not be able to modify or move the file.",
                                        "Allow users to modify or change the file, with write only selected users will not be able to save files to this location.",
                                        "Allow users to email files, they will be able to email to anyone unless the below email fields are filled with one or more emails.",
                                        "Allow users to edit the document and also save to this folder..",
                                        "Allow users to delete files from this folder.",
                                        "This option will send out a notification to users when a file in this folder is changed or a new file is added.")));




/* End of Messages and constants */

/* End of file constants.php */
/* Location: ./application/config/constants.php */