<?php define( 'BASEPATH', true ); require_once('../cuedrive/application/config/config.php');
?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="../index.php"><img alt="cuedrive" src="images/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
               
			  		  <ul class="nav navbar-nav navbar-right ">
                 
        <li class="dropdown" id="menuLogin">
            <!--<a class="dropdown-toggle" href="authenticate.php" data-toggle="dropdown"><img alt="Signin/Signup" src="images/signin.png" ><strong class="caret" style= "color: grey;"></strong></a>-->
              <div class="dropdown-menu" style="padding:17px; margin-top:8%;">

            <h2 style= "margin-top:-5%; font-size: 20px;">Login</h2>
            <hr class="">
              <form class="form" id="companyLoginForm"> 
                <input name="username" id="usernamedrop" placeholder="Username" type="text"><br><br> 
                <input name="userpass" id="passworddrop" placeholder="Password" type="password"><br><br>
               
            	<label class="col-lg-12 control-label" id="userdata" style="color:red; font-size:12px;"></label>
         

               <!-- <button type="button" id="btnLogin" class="btn" style=" width:100%" onclick = "submitData()">Login</button><br><br>-->
				
				<input type="submit" class="btn" style=" width:100%" id="login" value="Login"/><br><br>
				
                <a class=" " href="forgotpassword.php" style="text-decoration: underline; font-size:12px; color:grey;">Forgot Password</a><br>
                <a class=" " href="authenticate.php" style="text-decoration: underline; font-size:12px; color:grey;">Not yet registered? Sign Up here</a>
              </form>
              
              <hr class="colorgraph" style="margin-bottom:-10.5%">
            </div>
          </li>
                    
                    <li ><a href="https://twitter.com/cuedrive" target="_blank" class="twitter"></a>
                    </li>
                    <li ><a href="https://www.facebook.com/cuedriveau" target="_blank" class="facebook"></a>
                    </li>
                    <li><a href="https://www.youtube.com/channel/UCM7Fl2l8YuxYdv0IKGrymfA" target="_blank" class="youtube"></a>
                    </li>
                </ul>
                
            </div> 
                        
        </div>
    </nav>
<script>

 $('#companyLoginForm').on('submit', function(e) {
		e.preventDefault(); 
        var form = $(this); 
        var post_url = form.attr('action'); 
        var post_data = form.serialize();

      	$.ajax({
            	type: 'POST',
            	url: '<?php echo BASE_URL_FOR_LOGIN?>index.php/login/authenticateUserWeb',
            	data: post_data,
            	success: function(data) { 
             
				if(data == 'success')
				{
				  window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
				}
				else if(data == 'successuser')
				{
				  window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyuser";  
				}
				else if(data == 'expired')
				{
				  window.location = '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/selectPacakge';	
				
				}
				else
				{
					$("#userdata").html(data);
				}			   
            }
      	});
 });

</script>
    
        
    
<style type="text/css"> 

.twitter { width: 34px; height:23px; margin-right:10px; display:block; background-image: url('images/twitter.png'); } 
.twitter:hover {width: 34px; height:23px; background-image: url('images/twitterhover.png'); } 


.facebook { width: 14px; height:26px; margin-right:10px; display:block; background-image: url('images/facebook.png'); } 
.facebook:hover {width: 14px; height:26px; background-image: url('images/facebookhover.png'); } 


.youtube { width: 23px; height:27px; margin-right:10px; display:block; background-image: url('images/youtube.png'); } 
.youtube:hover {width: 23px; height:27px; background-image: url('images/youtubehover.png'); } 

</style>
