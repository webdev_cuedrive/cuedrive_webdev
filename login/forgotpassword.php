<!DOCTYPE HTML>
<html lang="en">

<head>
<title>cuedrive | Reset Password</title>
<meta name="description" content="cuedrive | Paperless business solution Forgot Password">
<meta name='keywords' content='cuedrive, best paperless, ipad, iphone, paperless, q drive' />
   <?php include 'head.php';?>
</head>

<body>
<?php include 'header.php';?>

<br><br><br><br><br><br><br>
	
	
<div class="container">
<div class="row">
        <div class="col-lg-12 ">
            <div class="jumbotron">
                 <h2 align="center">Forgot Password</h2>
		<hr class="colorgraph">
              <div class="form-horizontal" role="form" action="" method="post">
              
               <div class="form-group">
            <label class="col-lg-8 control-label" id="fpass" style="color:red;"></label>
          	</div>
          
          <div class="form-group">

            <label class="col-lg-4 control-label">Enter Your Email Address </label>
            <div class="col-lg-8">
            	
              <input class="form-control" value="" id="email" placeholder="example@domain.com" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required > <span class="error"></span>
              
             
            </div>
          </div>
        <br>
          <div class="form-group">
           <div class="col-md-offset-5 col-md-8" >
             <button type="submit" class="btn btn-inverse" onclick = "sendPassword()">Reset Password </button>
           </div>
           </div>
          </div>
            </div>
        </div>
</div>
</div>

<?php include 'footer.php';?>
    
<?php include 'script.php';?>


<script>

function sendPassword()
{
var email=$.trim($("#email").val());

if(email=="")
{ 
alert('Please enter valid Email Id');
}
else
{
$.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/resetPassword',{email:email},function(data){

if(data=="1")
{
alert('Password has been sent to your email successfully');
window.location = "<?php echo BASE_URL_FOR_LOGIN?>";  

}
else
{
$("#fpass").html(data);
//alert(data);
}
});
}
}
</script>
</body>
</html>