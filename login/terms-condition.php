<!DOCTYPE html>
<html lang="en">

<head>
<title>cuedrive | Terms of use</title>
<meta name="description" content="cuedrive | Paperless business solution Terms and Conditions">
<meta name='keywords' content='cuedrive, Terms and Conditions, best paperless solution, best paperless, cue drive, q drive, ipad app' />
   <?php include 'head.php';?>
</head>

<body>
<?php include 'header.php';?>

<br><br><br>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">cuedrive Terms of service</h2>
                
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                
<p>This Agreement between you and CUEDRIVE PTY LTD (we, us or our) contains the terms of service that govern your access to and use of the Service. You accept this Agreement when you create an account in connection with the Service. When you accept this Agreement, you promise us that you have the capacity to enter into legally binding agreements. If you are accepting this Agreement on behalf of a company, you promise that you have the authority to enter into legally binding agreements on behalf of the company.</p>


<h1>1 What is the Service?</h1>

<ul class="b">

<li><strong>1.1 The Service:</strong> The Service allows you to create, upload, edit, store and share files with the users of Registered Devices. You can also use the Service by logging onto our website.</li><br>

<li><strong>1.2 Your account:</strong> You must create an account in connection with the Service. There are different types of accounts that give you a different amount of storage and allow you to access the Service through a different number of devices. When you create your account, you will be asked which type of account you want. Unless you choose to create a free account, you will be charged a periodic fee for your use of the Service. If you want to upgrade your account to one with higher limits on storage or number of Registered Devices, you can do so at any time.</li><br>

<li><strong>1.3 Registering devices:</strong> Before you can use the Service on a device, you must register that device. Devices that can be registered to use the Service include compatible mobile telephones and tablet computers. If you are accessing the Service through a compatible mobile telephone, you will only have access to some of the Service's functions or features. When you register a device, a user of that device will be able to view and edit all files created or uploaded in connection with the use of your account. You can restrict access and editing of certain files or limit access and editing on certain devices by logging into your account through our website.</li><br>

<li><strong>1.4 Username and password:</strong> You must create a username and password in connection with your account. You should keep this username and password secure. We are not liable for any issues or liabilities that may arise out of your failure to keep your username and password secure. You will need your username and password to log into your account through our website. When you log onto your account through our website you will be able to access the Service, manage Registered Devices, register additional devices and edit your account details. You are solely responsible for all activities that occur under your account and Registered Devices.</li>
<br>
<li><strong>1.5 Use of the Service:</strong> You must use the Service legally and appropriately. You agree that you must NOT use the Service:

	<ul class="a">
	<li>to infringe upon or violate any intellectual property rights;</li>
	
	<li>in breach of any laws or regulations;</li>
	
	<li>to create, upload or transmit a virus, malware or any other computer code designed to interfere with the normal function of or data stored on a computer or network;</li>
	
	<li>to engage in any fraudulent or objectionable activity including by pretending or holding yourself out to be somebody you are not, 'phishing', 'spoofing' or spamming.</li>
	
	<br>	
	<p>If you engage in any of these activities, we may close or suspend your account. We may also remove any data stored in connection with your account or disclose information in relation to your account to law enforcement, regulatory bodies or other appropriate parties. We may do these things without telling you.</p>
	
	</ul>


<li><strong>1.6 Inactive accounts:</strong> If account has been inactive for a period of time, we may close your account and delete any data stored in connection with the Service. We will provide you 30 days notice if we choose to do this.</li>
	
</ul>


<h1>2 Fees and payment</h1>

<ul class="b">

<li><strong>2.1 Subscription Fee:</strong> If you choose an account with a periodic Subscription Fee, you must pay the applicable Subscription Fee for your account in advance. You must continue to pay this Subscription Fee until you close your account in accordance with paragraph 4. </li>
<br>
<li><strong>2.2 Payment method:</strong> When you create a paid account, or when you upgrade to a paid account, you will be required to give us the details of your Nominated Payment Method. You authorise us to direct debit any amount payable under this Agreement through your Nominated Payment Method.</li> 
<br>
<li><strong>2.3 Billing cycle:</strong> If your account has an annual Subscription Fee, we generally debit your Nominated Payment Method on the anniversary of the day you created or upgraded to a paid account. If your account has a monthly Subscription Fee, we generally debit your Nominated Payment Method on the same day of the month you created or upgraded to a paid account. If you signed up for an account in a month with 29, 30 or 31 days and the month in which your Nominated Payment Method is to be debited does not have such a day, we will debit you on the last day of that month. The amount you may be debited to your account in each month may differ due to promotional offers, changes to your account, taxes or because of arrears in previous months.</li>
<br>
<li><strong>2.4 Upgrading your account during a billing cycle:</strong> If you upgrade your account during a billing cycle, we will credit your account on a pro-rata basis for any Subscription Fees that you have paid in advance. We will then start a new billing cycle beginning on the day you upgrade your account. </li>
<br>
<li><strong>2.5 Keeping your payment details up to date:</strong> If you want to nominate a different payment method or if you want to change the details of your Nominated Payment Method, you may edit your information by accessing your account through our website. It is your responsibility to keep your contact information and payment information up to date.</li>

</ul>


<h1>3 Intellectual property</h1>

<ul class="b">

<li><strong>3.1 Your rights:</strong> You retain all proprietary rights you have in the content you upload in connection with the Service. You grant us a licence to use, store, host, copy, modify and transmit this information for the purposes of providing you with the Service. If you contact us with comments or suggestions about the service, you grant us a perpetual, fully paid, royalty-free, irrevocable, transferable license, with right of sublicense, to use your ideas.</li>
<br>
<li><strong>3.2 Our rights:</strong> You agree that we or our third party suppliers own all intellectual property rights in connection with the Service. We give you a limited personal, royalty-free, non-assignable and non-
exclusive license to use any software or content provided in connection with this Agreement. You must not copy, modify, distribute, sell, lease, reverse engineer or attempt to extract the source code of any software or content provided in connection with this Agreement. Some software or content may be provided to you under a separate licence, in the event that the separate licence conflicts with the licence under this Agreement, the terms of the separate licence prevail to the extent of the conflict. </li>        

</ul>

<h1>4 Closing your account</h1>

<ul class="b">

<li><strong>4.1 Closing your account:</strong> You can close your account at any time. If your account is closed for any reason (including if we have closed your account because you have breached this Agreement), we will not refund any Subscription Fees that you have already paid and you will remain liable for any amount owing under this Agreement (including for example, any Subscription Fees that are in arrears). If you decide to close your account, you should do so before the beginning of a new billing cycle. </li>

<li><strong>4.2 Your data after your account is closed:</strong> If your account is closed, other than for your failure to comply with the terms of this Agreement, we will make reasonable efforts to make any data you stored in connection with the Service available to you for a period of thirty (30) calendar days. After this period, we may delete your data.</li>

</ul>

<h1>5 Breaching this Agreement</h1>

<ul class="b">

<li><strong>5.1 What happens if you breach this Agreement:</strong> If you breach this Agreement, including if you do not pay the Subscription Fee on time, we may close or suspend your account. If we close your account in this way, we may delete some or all of your data.</li>

</ul>



<h1>6 Disclaimer of warranties</h1>

<ul class="b">

<li><strong>6.1 General:</strong> Subject to any condition, warranty or right implied by law which cannot be excluded by agreement, all implied conditions, warranties and rights are excluded. Where any condition, warranty or right is implied by law and cannot be excluded, we limit our liability for breach of that implied condition, warranty or right, in connection with the Service to one of the following, as we reasonably determine:</li>

	<ul class="a">
	
 		<li>the supplying of the Service again; or </li>

 		<li>payment of the costs of having the Service supplied again.</li>
 	
 	</ul>	


</ul>



<h1>7 Limitation of liability and indemnity</h1>

<ul class="b">

<li><strong>7.1 Our liability to you:</strong> To the extent permitted by law, and without limiting paragraph 6 or 7.2, you agree that we will not be liable for any:</li>

	<ul class="a">
 		<li>indirect or consequential loss or damage;</li>

 		<li>loss of profits, reputation, business, goodwill, customers or labour costs; </li>

 		<li>damage or loss arising from any negligence, wilful or fraudulent act or omission of any </li>

 		<li>loss or damage arising from our acts or omissions, where we have acted or omitted to </li>
 	</ul>
 		

<li><strong>7.2 A cap on our liability:</strong> To the extent permitted by law, you also agree that our maximum liability to you does not exceed: </li>

<ul class="a">
<li>the total amount of Subscription Fees you have paid in respect of the 3 months other person besides us; and act on your instructions. immediately proceeding the time you make a claim against us (if your account has an annual subscription fee, our maximum liability under this paragraph 7.2(a) is equal to one quarter of the last annual subscription fee payable (whether or not you have paid this amount) before the time of the claim); or you make a claim, you agree our maximum liability to you is $60.00 AUD.</li>
<li>if you do not have a paid account or have had one for less than 3 months at the time </li>
</ul>

<li><strong>7.3 Your indemnity:</strong> You agree to indemnify and keep us indemnified against all loss or damage arising as a result of: </li>

<ul class="a">
<li>any breach of your obligations under this Agreement; </li>

<li>any act or omission by you in relation to this Agreement; </li>

<li>any activity occurring under your account or Registered Devices.</li>
</ul>
</ul>

<h1>8 Confidentiality, privacy and security</h1>

<ul class="b">
<li><strong>8.1 Purpose for collecting your personal information:</strong> We collect personal information (as the term is defined in section 6 of the Privacy Act 1988 (Cth)) to provide you with the Service, for accounts and billing purposes, to administer our relationship with you, to protect or improve our service, to develop new services and to otherwise run our business. You agree that if you give us personal information about another individual, you will notify that individual of the matters in paragraphs 8.1, 8.2 and 8.3. If you do not provide us with certain required personal information we may not be able to provide you with access to the Service. Unless you tell us not to, we may use information we collect about you to tell you about products and services that might interest you.</li>

<li><strong>8.2 We respect the confidentiality of your information:</strong> You acknowledge that information stored in connection with the Service may be accessed through your account or by the users of Registered Devices. In order to provide the Service to you, we may disclose your information to our service providers, professional advisers or consultants. These persons will be bound by duties of confidentiality, where appropriate. Some of our service providers may be located overseas in the Philippines. We will not disclose your information for any purpose that is inconsistent with this Agreement unless we are required by law to do so. </li>



<li><strong>8.3 Our privacy policy:</strong> Our privacy policy explains how you can seek to access and/or correct the personal information that we hold about you, as well as how to make a complaint about the way in which we handle your personal information and how we deal with complaints. You can access our privacy policy at <a href="<?=ORG_URL?>privacy-policy.php"><?=ORG_URL?>privacy-policy.php</a>. If you have any questions about our personal information collection and handling practices please contact out Privacy Officer using the following details:<br><br>

<strong >CUEDRIVE PTY LTD</strong><br>

<strong >1300 135 380 | <a href="mailto:info@cuedrive.com" target="_top">info@cuedrive.com</a></strong>

</li>
<br>


<li><strong>8.4 Security of your information:</strong> We take reasonable and appropriate steps to protect the information or data you store in connection with the Service from loss or unauthorised access or disclosure. You should also take steps to protect yourself including by regularly backing up important data and having antivirus software on your Registered Devices and computers you use for the Service.</li>

</ul>


<h1>9 Miscellaneous</h1>

<ul class="b">

<li><strong>9.1 Changing this Agreement:</strong> We can discontinue the Service or change the Service including by adding or removing features or functions at anytime. We can also amend this Agreement at any time. If we amend this Agreement, we will put the amended Agreement on our website. You should check our website regularly. If we increase the Subscription Fee payable in connection with your account, we will notify you of the change before it takes effect.</li>

<li><strong>9.2 Export restrictions:</strong> You agree not to use the Service in contravention of any trade sanctions imposed by Australia.</li>

<li><strong>9.3 Governing law:</strong> This Agreement is governed by the laws of Western Australia and you submit to the jurisdiction of the courts of Western Australia. </li>

<li><strong>9.4 Entire Agreement:</strong> This Agreement records the entire agreement between the parties and supersedes all previous negotiations, understandings, representations and agreements in relation to the Service.</li>

<li><strong>9.5 Severability:</strong> If any part of this Agreement is for any reason unenforceable, that part must be read down to the extent necessary to preserve its operation. If it cannot be read down, it must be deleted.</li>

<li><strong>9.6 No assignment:</strong> You must not assign this Agreement without our consent.</li>

<li><strong>9.7 Notice:</strong> Any notice given in connection with this Agreement must be in writing and addressed to the relevant party. Notices must be sent by email. A notice is taken to be given when sent, unless the sender is notified, by a system or person involved in the delivery of the email, that the email was not successfully sent. </li>

<li><strong>9.8 Force Majeure:</strong> A party is not liable for any failure to observe its obligations under this agreement (other than any failure to pay any amount of money) where such failure is due to a Force Majeure Event.</li>

</ul>

<h1>10 What words mean</h1>

<p><strong>Agreement</strong> means this agreement between you and us. 

<p><strong>Force Majeure Event</strong> includes any cause beyond the control of a party, such as strike, industrial action, war, sabotage, terrorist activity, national emergency, blockade or governmental action, inaction or request, power surge, internet failure, power failure and act of God.</strong></p>

<p><strong>Nominated Payment</strong> Method means the payment method you authorise us to debit your Subscription Fee to. Payment methods we accept include credit card and PayPal.</p>

<p><strong>Registered Devices</strong> means the compatible devices that you register to use the Service.</p>

<p><strong>Service</strong> means the service we provide in connection with this Agreement.</p>

<p><strong>Subscription Fee</strong> means the periodic fee that applies to your account.</p>


</div>

</div>

</div>
<!-- /.container -->

<?php include 'footer.php';?>
    
<?php include 'script.php';?>

</body>

</html>
