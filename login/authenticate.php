<!DOCTYPE HTML>
<html>
<head>
<title>cuedrive | Access Your Account</title>
<meta name="description" content="cuedrive | Paperless business solution">
<meta name='keywords' content='cuedrive, paperless, best, cue drive, q drive, iphone, ipad' />
<?php include 'head.php';?>
</head>
<body>
<?php include 'header.php';?>
<br>
<br>
<br>
<div class="container"> <br>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-6 ">
      <div class="jumbotron">
        <h2 align="center">Login</h2>
        <hr class="colorgraph">
        <div class="form-horizontal" role="form" id="login">
          <div class="form-group">
            <label class="col-lg-8 control-label" id="user" style="color:red;"></label>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Username:</label>
            <div class="col-lg-8">
              <input class="form-control selector" name="username"  placeholder="Username" type="text" id="username">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Password:</label>
            <div class="col-lg-8">
              <input class="form-control selector" name="userpass" placeholder="Password" type="Password" id="userpass">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn btn-inverse"  onclick = "sendData()">Login</button>
              <a class=" " href="forgotpassword.php" style="text-decoration: underline;">Forgot Password</a> </div>
          </div>
        </div>
      </div>
    </div>
    <script>
        
        $('.selector').live('keypress', function (ev) {

           if (ev.which === 13) {
           
                 
                  var username = $("#username").val();
                  var userpass = $("#userpass").val();
                  $.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/login/authenticateUserWeb',{ 'username': username, 'userpass': userpass }, function( data ) {
      
	        if(data == 'success')
	        {
	          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
					
	        }
	        else if(data == 'successuser')
	        {
	          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyuser";  
	        }
	        else if(data == 'expired')
	        {
	          window.location = '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/selectPacakge';	
	        
	        }
	        
	 
	        }); 
	        
	        return false;
           
          } 
          
         return true;
         });
        </script> 
    <script> 
	function sendData() {
        var username = $("#username").val();
        var userpass = $("#userpass").val();
       
        $.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/login/authenticateUserWeb',{ 'username': username, 'userpass': userpass }, function( data ) {
      
        if(data == 'success')
        {
          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
				
        }
        else if(data == 'successuser')
        {
          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyuser";  
        }
        else if(data == 'expired')
        {
          window.location = '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/selectPacakge';	
        
        }
        else
        {
	$("#user").html(data);
	//alert(data);
	}
	
	});

        }
</script>
    <div class="col-md-6 jumbotron">
      <h2>Sign Up</h2>
      <hr class="colorgraph">
      <form class="form-horizontal signup" role="form" id="signup" action="" method="post">
        <div class="form-group">
        <?php /*?><input name="package_id" id="package_id" value="1" type="hidden" ><?php */?>
          <label class="col-lg-8 control-label" id="signuperr" style="color:red;"></label>
        </div>
        <label for="inputEmail1">Email <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control signemail" name="email" id="email" placeholder="Email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  >
        <span class="error"></span> <br>
        <label for="inputEmail2">Verify Email <span class="error" style="color:#f0776c">*</span></label>
        <label style="color:red;" class="emailvalidate"></label>
        <input class="form-control signemail2" name="email2" id="email2" placeholder="Re-enter Email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  >
        <span class="error"></span> <br>
        
        <!--  	<div class="form-group">
            <label class="col-lg-8 control-label" id="userspace" style="color:red;"></label>
          </div> 
            
        -->
        <label for="inputusername" >Username <span class="error" style="color:#f0776c">*</span></label>
        <label style="color:red;" id="userspace"></label>
        <input class="form-control username"  name="username" id="username"  placeholder="Username" type="text" onKeyUp="nospaces(this)" >
        <span class="error"></span> <br>
        <label for="inputPassword1" >Password <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control signpassword"  name="password" id="inputPassword1" placeholder="Min 6 characters - UPPERCASE, lowercase & Numbers"  title="Password must contain at least 6 characters, including UPPERCASE, lowercase and numbers" type="password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onChange=" this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.pwd2.pattern = this.value; ">
        <span class="error"></span> <br>
        <label for="inputPassword1" >Confirm Password <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control signpassword2"  id="inputPassword2" placeholder="Re-enter Password" type="password" title="Please enter the same Password as above"   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd2" onChange=" this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); ">
        <span class="error"></span>
        <input class="form-control"  name="passwordhint" id="passwordhint" value="123" placeholder="Password Hint" type="hidden" >
        <span class="error"></span> <br>
        <label>First Name <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="firstname" id="first"  placeholder="First Name" type="text"  >
        <span class="error"></span> <br>
        <label>Last Name <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="lastname" id="last" placeholder="last Name" type="text"  >
        <span class="error"></span> <br>
        <label >Phone Number <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="phone" id="phone" placeholder="Phone Number" type="text" pattern="[0-9]{10}"  title="Enter Valid Phone Number format: 999999999">
        <span class="error"></span> <br>
        <label>Company <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="company_name" id="company" placeholder="Company Name" type="text"  >
        <span class="error"></span> <br>
        <label>Street Address <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="address" id="address" placeholder="Street Address" type="text"  >
        <span class="error"></span> <br>
        <label>City <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control city" name="city" id="inputcity" placeholder="City" type="text"  >
        <span class="error"></span> <br>
        <label>Zip Code <span class="error" style="color:#f0776c">*</span></label>
        <input class="form-control" name="zipcode" id="zip" placeholder="Zip Code" type="text"  title="Enter Valid Zip Code" >
        <span class="error"></span> <br>
        <label>Country <span class="error" style="color:#f0776c">*</span></label>
        <?php include 'country.php'; ?>
        <span class="error"></span> <br>
        <div class="checkbox">
          <label>
            <input type="checkbox" id="check"  >
            I agree with the <a href="terms-condition.php" target="_blank" style="text-decoration: underline;">Terms and Conditions</a> </label>
        </div>
        <br>
        <button type="submit" id="pass" class="btn btn-inverse" onClick="return emailValidate()" style="padding: 10px 17px;">Sign Up for your Free Trial</button>
        <button type="reset" class="btn btn-inverse" onClick="window.history.go(-1);">Cancel</button>
      </form>
      <div id="thanksignup" style="display: none">
        <h3 align="center"> Thank you for signing up. Login and get started</h3>
        <hr class="colorgraph">
      </div>
    </div>
  </div>
</div>
</div>
<?php include 'footer.php';?>
<?php include 'script.php';?>
<script>
	function nospaces(t)
	{
		if(t.value.match(/\s/g) || t.value.match(/\@/g).length > 0)
		{
			$("#userspace").html('Spaces and special character are not allowed!');
			t.value=t.value.replace(/\s/g,'');
		}
	}
	
	function emailValidate() 
	{
	   flag = true;
	   if($('#email').val() != $('#email2').val())
	   {
		   $(".emailvalidate").html('Email not matched !');
		   flag = false;
	   }
	   return flag;
	}
	
    $('#signup').on('submit', function(e) {
	  	   var signemail 	= $.trim($(".signemail").val());
           var signemail2 	= $.trim($(".signemail2").val());
           var username 	= $.trim($(".username").val());
           var signpassword = $.trim($(".signpassword").val());
           var signpassword2= $.trim($(".signpassword2").val());
           var passwordhint = $.trim($("#passwordhint").val());
           var first 		= $.trim($("#first").val());
           var last 		= $.trim($("#last").val());
           var phone 		= $.trim($("#phone").val());
           var company 		= $.trim($("#company").val());
           var address 		= $.trim($("#address").val());
           var city 		= $.trim($(".city").val());
           var zip 			= $.trim($("#zip").val());
           var country 		= $.trim($("#country").val());
           var check 		= $.trim($("#check").val());

			if(signemail=="" || signemail2=="" || username=="" || signpassword=="" || signpassword2=="" || passwordhint=="" || first=="" || last=="" || phone=="" || company=="" || address=="" || city=="" || zip=="" || country=="" || check=="")
				{ 
					alert('Please complete the required fields');
					return false;
				}else{
						e.preventDefault(); 
						var form = $(this); 
						var post_url = form.attr('action'); 
						var post_data = form.serialize();
						$.ajax({
								type: 'POST',
								url: '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/register',
								data: post_data,
								success: function(msg) { 
									if(msg == 'success'){
										window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
									}else if(msg == 'emailexist'){
										$("#signuperr").html('Email already exists in cuedrive');
									}else if(msg == 'usernameexist'){
										$("#signuperr").html('Username already exists in cuedrive');
									}else if(msg == 'companynameexist'){
										$("#signuperr").html('Company name already exists in cuedrive');
									}else {
										$("#signuperr").html('Some error is occured, please try agin later');
									}
								}
							});
						}
   		});
</script>
</body>
</html>