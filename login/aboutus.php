<!DOCTYPE html>
<html lang="en">

<head>
<title>cuedrive | About Us</title>
<meta name="description" content="cuedrive | Paperless business solution, About Us">
<meta name='keywords' content='cuedrive, about us, cue drive, q drive, best paperless app, paperless app mac, paperless app iphone, paperless app ipad' />
   <?php include 'head.php';?>
</head>

<body>
<?php include 'header.php';?>

<br><br><br>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">About Us</h2>
                
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                <p>cuedrive is an online backup and sync storage company owned by CUEDRIVE PTY LTD.</p>
                
                <p>cuedrive's aim is to protect your files and enable you to access them from anywhere at any time. We partner with some of the world's most famous online and offline electronics retailers. We also sell all-in-one online storage solutions to consumers and businesses directly via our website and through several thousand small businesses.</p>
                
            </div>

        </div>

    </div>
    <!-- /.container -->

    <?php include 'footer.php';?>
    
<?php include 'script.php';?>

</body>

</html>
