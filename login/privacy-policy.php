<!DOCTYPE html>
<html lang="en">

<head>
<title>cuedrive | Privacy Policy</title>
<meta name="description" content="cuedrive | Paperless business solution Privacy Policy">
<meta name='keywords' content='cuedrive, privacy policy, q drive, best paperless, paperless solution, paperless, ipad, iphone' />
   <?php include 'head.php';?>
</head>

<body>
<?php include 'header.php';?>

<br><br><br>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">Privacy Policy</h2>
                
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                



CUEDRIVE PTY LTD, (we, us and our) are committed to protecting the privacy of personal information. We are bound by the Privacy Act 1988 (Cth) (Privacy Act) and, in particular, by the Australian Privacy Principles (APPs), which set out the standards of collecting, holding, using and disclosing personal information. This privacy policy explains the way in which we handle personal information and your right to access, correct and complain about the handling of your personal information.<br>


<h1>What is personal information?</h1>

“Personal Information” is information or opinion (in recorded form or otherwise) about an identified, or reasonably identifiable, individual. This includes an individual's name, date of birth and contact details.

<h1>Collecting personal information</h1>

<p>We may collect the following types of personal information about an individual:</p>

<ul>
<li>name, address and contact details</li>
<li>payment details</li>
<li>information that that a customer stores or uploads in connection with the customer's use of our service</li>
<li>information about an individuals interactions with us, and</li>
<li>any other personal information that is reasonably necessary for the purpose of running our business.</li>
</ul>

<p>We generally collect personal information from an individual when they create and account or when they use our Service.</p>

<h1>Our purposes for handling personal information</h1>

<p>We collect, hold, use and disclose personal information:</p>

<ul>
<li>in order to provide our service</li>
<li>to contact users of our service</li>
<li>to administer and manage our relationship with customers</li>
<li>maintain, protect and improve our service, or to develop new products or services</li>
<li>to provide users with information about products and services that may be of interest to them</li>
<li>for any purpose required or permitted by law, and</li>
<li>to otherwise run our business.</li>
</ul>

<h1>Disclosing personal information</h1>       
<p>We may disclose personal information to users of devices registered under a particular account, our service providers, our consultants and advisers and regulatory bodies and government authorities as required or authorised by law. We may disclose your personal information overseas, for instance to our service providers located in the Philippines.</p>        

<h1>Security of personal information</h1>
<p>We hold personal information in both paper-based and electronic files.  We seek to ensure that personal information is protected from misuse, interference and loss and unauthorised access, modification and disclosure.</p>
<p>The personal information that we hold is treated as confidential and only accessed when necessary.  We seek to ensure that those that we disclose it to are bound by obligations of privacy and confidentiality, where appropriate.  When we no longer require personal information, including when we are no longer required by law to keep the relevant records, we destroy or de-identify the records.</p>

<h1>Accessing your personal information</h1>
<p>You may request access to the personal information that we hold about you by contacting our Privacy Officer at <a href="mailto:info@cuedrive.com">info@cuedrive.com</a> or through our website.  On the rare occasions that we refuse access to your personal information, we will provide you with a written notice which sets out the reasons for the refusal.  We may recover our reasonable costs (if any) in responding to a request for access to your personal information.</p>

<h1>Correcting your personal information</h1>
<p>We seek to ensure that the personal information that we collect is accurate, up-to-date and complete and, in the case of our use or disclosure of that information, relevant as well.  To assist us with this, we encourage you to contact us if you believe that any information that we hold about you is inaccurate, out of date, incomplete, irrelevant or misleading, and make a request that we correct you personal information accordingly.  You may make such a request by contacting our Privacy Officer at <a href="mailto:info@cuedrive.com">info@cuedrive.com</a> or through our website.  If we refuse to correct any of your personal information following your request, we will give you a written notice which sets out the reasons for our refusal. </p>

<h1>Marketing</h1>
<p>We may use personal information to provide an individual who has used our service with information about product and services that may be of interest to them.  We may do this while we have a current business relationship with an individual, even if they are on the Do Not Call Register.  We may also disclose information about an individual to other organisations for specific marketing purposes. If we have collected your personal information, you may opt out of receiving marketing communications from us at any time if you no longer wish to receive these communications.  In order to do so, simply contact our Privacy Officer at <a href="mailto:info@cuedrive.com">info@cuedrive.com</a> or through our website and request that we no longer send marketing communications to you or opt out in the way suggested in our communications.</p>

<h1>Questions and complaints</h1>
<p>If you have any questions, or if you believe that we have not complied with our obligations under the Privacy Act (and, in particular, the APPs), or if you believe that the personal information that we hold about you has been compromised in any way, please contact our Privacy Officer through our website or by using the following details: </p>

<b>	CUEDRIVE PTY LTD <br>
	1300 135 380  <br>  
	info@cuedrive.com</b> 

<p><br>We will respond to any question or complaint as soon as possible.  If you are dissatisfied with our response, you may direct your complaint to the Office of the Australian Information Commissioner by email at enquiries@oaic.gov.au or by telephone on 1300 363 992. </p>

<h1>Changes to this Privacy Policy</h1>
<p>We may make change to this Privacy Policy from time to time without notice. It was last updated on 08/09/2014. </p>

            </div>

        </div>

    </div>
    <!-- /.container -->

<?php include 'footer.php';?>
    
<?php include 'script.php';?>

</body>

</html>