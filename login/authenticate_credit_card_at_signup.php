<!DOCTYPE HTML>
<html>
<head>
<title>cuedrive | Access Your Account</title>
<meta name="description" content="cuedrive | Paperless business solutuion">
<meta name='keywords' content='cuedrive | Paperless business solutuion' />
<?php include 'head.php';?>
</head>
<body>
<?php include 'header.php';?>
<br><br><br>
<div class="container">
<br>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 ">
            <div class="jumbotron">
                 <h2 align="center">Login</h2>
		<hr class="colorgraph">
              <div class="form-horizontal" role="form" id="login">
              
          <div class="form-group">
            <label class="col-lg-8 control-label" id="user" style="color:red;"></label>
          </div>
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Username:</label>
            <div class="col-lg-8">
              <input class="form-control selector" name="username"  placeholder="Username" type="text" id="username">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Password:</label>
            <div class="col-lg-8">
              <input class="form-control selector" name="userpass" placeholder="Password" type="Password" id="userpass">
            </div>
          </div>
          <div class="form-group">
           <div class="col-md-offset-3 col-md-9">
             <button type="submit" class="btn btn-inverse"  onclick = "sendData()">Login</button>
             <a class=" " href="forgotpassword.php" style="text-decoration: underline;">Forgot Password</a>
           </div>
           </div>
          </div>
         </div>
        </div>
        
        <script>
        
        $('.selector').live('keypress', function (ev) {

           if (ev.which === 13) {
           
                 
                  var username = $("#username").val();
                  var userpass = $("#userpass").val();
                  $.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/login/authenticateUserWeb',{ 'username': username, 'userpass': userpass }, function( data ) {
      
	        if(data == 'success')
	        {
	          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
					
	        }
	        else if(data == 'successuser')
	        {
	          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyuser";  
	        }
	        else if(data == 'expired')
	        {
	          window.location = '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/selectPacakge';	
	        
	        }
	        
	 
	        }); 
	        
	        return false;
           
          } 
          
         return true;
         });
        </script>
        
<script> 
	function sendData() {
        var username = $("#username").val();
        var userpass = $("#userpass").val();
       
        $.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/login/authenticateUserWeb',{ 'username': username, 'userpass': userpass }, function( data ) {
      
        if(data == 'success')
        {
          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
				
        }
        else if(data == 'successuser')
        {
          window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyuser";  
        }
        else if(data == 'expired')
        {
          window.location = '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/selectPacakge';	
        
        }
        else
        {
	$("#user").html(data);
	//alert(data);
	}
	
	});

        }
</script>
        
            
        
        <div class="col-md-6 jumbotron">
          <h2>Sign Up</h2>
            <hr class="colorgraph">
            
          
            
          <form class="form-horizontal signup" role="form" id="signup" action="" method="post">
          
		   <div class="form-group">
            <label class="col-lg-8 control-label" id="signuperr" style="color:red;"></label>
          </div> 

           <label for="inputEmail1">Email <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control signemail" name="email" id="email" placeholder="Email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  > <span class="error"></span> <br>
             
             
             <label for="inputEmail2">Verify Email <span class="error" style="color:#f0776c">*</span></label>
             
           	<label style="color:red;" class="emailvalidate"></label>
           	
             <input class="form-control signemail2" name="email2" id="email2" placeholder="Re-enter Email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  > <span class="error"></span> <br>

      <!--  	<div class="form-group">
            <label class="col-lg-8 control-label" id="userspace" style="color:red;"></label>
          </div> 
            
        -->    
           <label for="inputusername" >Username <span class="error" style="color:#f0776c">*</span></label>
           <label style="color:red;" id="userspace"></label>
          <input class="form-control username"  name="username" id="username"  placeholder="Username" type="text" onKeyUp="nospaces(this)" > <span class="error"></span> <br>

           

           
           <label for="inputPassword1" >Password <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control signpassword"  name="password" id="inputPassword1" placeholder="Min 6 characters - UPPERCASE, lowercase & Numbers"  title="Password must contain at least 6 characters, including UPPERCASE, lowercase and numbers" type="password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onChange=" this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.pwd2.pattern = this.value; "> <span class="error"></span> <br>

           

           <label for="inputPassword1" >Confirm Password <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control signpassword2"  id="inputPassword2" placeholder="Re-enter Password" type="password" title="Please enter the same Password as above"   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd2" onChange=" this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); "> <span class="error"></span>
             
             
             
             
           
             <input class="form-control"  name="passwordhint" id="passwordhint" value="123" placeholder="Password Hint" type="hidden" > <span class="error"></span> <br>


		<label>First Name <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="firstname" id="first"  placeholder="First Name" type="text"  > <span class="error"></span> <br>
             
            
             <label>Last Name <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="lastname" id="last" placeholder="last Name" type="text"  > <span class="error"></span> <br>
             
             
          	
          	<label >Phone Number <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="phone" id="phone" placeholder="Phone Number" type="text" pattern="[0-9]{10}"  title="Enter Valid Phone Number format: 999999999"> <span class="error"></span> <br>
             
             
           <label>Company <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="company_name" id="company" placeholder="Company Name" type="text"  > <span class="error"></span> <br>
           
           
            
           <label>Street Address <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="address" id="address" placeholder="Street Address" type="text"  > <span class="error"></span> <br>
           
           
             
           <label>City <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control city" name="city" id="inputcity" placeholder="City" type="text"  > <span class="error"></span> <br>

        
           
             
           <label>Zip Code <span class="error" style="color:#f0776c">*</span></label>
           
             <input class="form-control" name="zipcode" id="zip" placeholder="Zip Code" type="text"  title="Enter Valid Zip Code" > <span class="error"></span> <br>
          
           
             
           	<label>Country <span class="error" style="color:#f0776c">*</span></label>
           <?php include 'country.php'; ?>
			<span class="error"></span> <br>
           
		   <?php function get_client_ip_server() {
					$ipaddress = '';
					if ($_SERVER['HTTP_CLIENT_IP'])
						$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
					else if($_SERVER['HTTP_X_FORWARDED_FOR'])
						$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
					else if($_SERVER['HTTP_X_FORWARDED'])
						$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
					else if($_SERVER['HTTP_FORWARDED_FOR'])
						$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
					else if($_SERVER['HTTP_FORWARDED'])
						$ipaddress = $_SERVER['HTTP_FORWARDED'];
					else if($_SERVER['REMOTE_ADDR'])
						$ipaddress = $_SERVER['REMOTE_ADDR'];
					else
						$ipaddress = 'UNKNOWN';
				 
					return $ipaddress;
				}
				
				$localIP = get_client_ip_server();
			?>
		   
		   <?php if($localIP == '203.193.165.227') 	{	?>
			   <h4 class="modal-header">Credit Card Information </h4>
				<small class="pull-right" style="padding-top:13px;">
					You are paying $
					<span id="payingamount">0.00</span>
				</small>
				<br>
				
				<label>
					Card Number
					<span class="error" style="color:#f0776c">*</span>
				</label>
				<input id="cardnumber" class="form-control city" type="text" placeholder="Credit Card number" name="cardnumber">			
				<br>
				
				<label>
					Name On Card
					<span class="error" style="color:#f0776c">*</span>
				</label>
				<input id="nameoncard" class="form-control city" type="text" placeholder="Name On Card" name="nameoncard">			
				<br>
				
				<div id="">
					<label>
					Expiry Month
					<span class="error" style="color:#f0776c">*</span>
					</label>
				</div>
				
				<select id="EWAY_CARDEXPIRYMONTH" class="form-control" name="EWAY_CARDEXPIRYMONTH" style="width:50%;">
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09" selected="selected">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
				<br>
				
				<label>
					Expiry Year
					<span class="error" style="color:#f0776c">*</span>
				</label>
				
				<select id="EWAY_CARDEXPIRYYEAR" class="form-control" name="EWAY_CARDEXPIRYYEAR" style="width:50%;">
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
				</select>
				
				<span class="error"></span>
				<input type="hidden" name="nodevice" id="nodevice" value="2">
				<input type="hidden" name="selectedpkgid" id="selectedpkgid" value="1">
				
				<div id="eWAYBlock" style="margin-left: 300px; margin-top: -130px">
							<div style="text-align:center;">
									<a href="https://www.eway.com.au/secure-site-seal?i=11&s=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
											<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=1401d560-215c-41ce-8bb4-80864f541d1e&theme=0" />
									</a>
							</div>
					</div>
				<br>  
		   <?php } ?>
         
             <div class="checkbox">
               <label>
                 <input type="checkbox" id="check"  > I agree with the <a href="terms-condition.php" target="_blank" style="text-decoration: underline;">Terms and Conditions</a> 
               </label>
             </div>
          
           <br>
          
             <button type="submit" id="pass" class="btn btn-inverse" onClick="return emailValidate()" style="padding: 10px 17px;">Sign Up for your Free Trial</button>
             <button type="reset" class="btn btn-inverse" onClick="window.history.go(-1);">Cancel</button>
           
          </form>
          
          
          <div id="thanksignup" style="display: none">
		<h3 align="center"> Thank you for signing up. Login and get started</h3>
         	<hr class="colorgraph"></div>
		</div>
		
        </div>
        	
    </div>
</div>

   
<?php include 'footer.php';?>
    
<?php include 'script.php';?>


<script>	

	function nospaces(t)
	{
		if(t.value.match(/\s/g) || t.value.match(/\@/g).length > 0)
		{
			$("#userspace").html('Spaces and special character are not allowed!');
			t.value=t.value.replace(/\s/g,'');
		}
	}
	
	function detectCardType(card)
	{
		  $('#paypalbtn').prop('disabled', false);
		  $('#ewaybtn').prop('disabled', false);
		  
		  if(card.match('^4[0-9]{0,15}$') != null)
		  {
			   //visa
			   return 'visa';
		  }
		  else if(card.match('^5$|^5[1-5][0-9]{0,14}$') != null)
		  {
			   //MasterCard
			   return 'mastercard';
		  }
		  else if(card.match('^3$|^3[47][0-9]{0,13}$') != null)
		  {
			   //American Express
			   return 'amex';
		  }
		  else if(card.match('^3$|^3[068]$|^3(?:0[0-5]|[68][0-9])[0-9]{0,11}$') != null)
		  {
				   // Diners Club
				   return 'dinerclub';
		  }
		  else if(card.match('^6$|^6[05]$|^601[1]?$|^65[0-9][0-9]?$|^6(?:011|5[0-9]{2})[0-9]{0,12}$') != null)
		  {
				  // Discover
				  return 'discover';
		  }
		  else if(card.match('^2[1]?$|^21[3]?$|^1[8]?$|^18[0]?$|^(?:2131|1800)[0-9]{0,11}$|^3[5]?$|^35[0-9]{0,14}$') != null)
		  {
			  //JCB
			  return 'jcb';
		  }

	}	
	
	function emailValidate() 
	{
	   flag = true;
	   if($('#email').val() != $('#email2').val())
	   {
		   $(".emailvalidate").html('Email not matched !');
		   flag = false;
	   }
	   return flag;
	}
	
    	   $('#signup').on('submit', function(e) {   
			   
			   
	  	   var signemail = $.trim($(".signemail").val());
           var signemail2 = $.trim($(".signemail2").val());
           var username = $.trim($(".username").val());
           var signpassword = $.trim($(".signpassword").val());
           var signpassword2 = $.trim($(".signpassword2").val());
           var passwordhint = $.trim($("#passwordhint").val());
           var first = $.trim($("#first").val());
           var last = $.trim($("#last").val());
           var phone = $.trim($("#phone").val());
           var company = $.trim($("#company").val());
           var address = $.trim($("#address").val());
           var city = $.trim($(".city").val());
           var zip = $.trim($("#zip").val());
           var country = $.trim($("#country").val());
           var check = $.trim($("#check").val());
		   
			var cardnumber = $("#cardnumber").val();
			var nameoncard = $("#nameoncard").val();
			var CARDEXPIRYMONTH = $("#EWAY_CARDEXPIRYMONTH").val();
			var CARDEXPIRYYEAR = $("#EWAY_CARDEXPIRYYEAR").val();
			var noofdevices = $("#nodevice").val();
			var selectedpkgid = $("#selectedpkgid").val();

	if(signemail=="" || signemail2=="" || username=="" || signpassword=="" || signpassword2=="" || passwordhint=="" || first=="" || last=="" || phone=="" || company=="" || address=="" || city=="" || zip=="" || country=="" || check=="")
	{ 
	
			alert('Please complete the required fields');
			return false;
		
		<?php if($localIP == '203.193.165.227')	{	?>
			
			 if(cardnumber == "")
			  {
				alert("Please enter Credit Card Number");
				return false;
			  }
			  if(nameoncard == "")
			  {
				alert("Please enter Name on card");
				return false;		
			  }	
			  
			  var cardtype = detectCardType(cardnumber);			
			
				if(cardtype == 'dinerclub' || cardtype == 'amex')
				 {
					 alert("Unfortunately Diners club cards and Amex cards are not accepted throughout this payment");				 
					 return false;
				 }
			 
				var today = new Date();		
				var full_month = today.getMonth(); //January is 0!
				var current_month = full_month < 10 ? '0' + full_month : full_month;
				var full_year = today.getFullYear();
				var current_year = full_year.toString().substr(2,2);

			 //console.log(CARDEXPIRYYEAR + '-' + current_year + '-' + CARDEXPIRYMONTH + '-' + current_month);
			 if(CARDEXPIRYYEAR == current_year && CARDEXPIRYMONTH <= current_month)	{
					alert('Please enter valid expiry date.');
					
					return false;
			 }
				 
				 
			
			
		<?php }	else	{	?>
		
			alert('Please complete the required fields');
			return false;
			
		<?php	}	?>
		
	}	else	{
		
		
		<?php if($localIP == '203.193.165.227') {	?>
		
			
		
			e.preventDefault(); 
			var form = $(this); 
			var post_url = form.attr('action'); 
			var post_data = form.serialize();
			$.ajax({
					type: 'POST',
					url: '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/register',
					data: post_data,
					success: function(msg) { 
					
					if(msg == 'success'){
						
						
						$.post("<?php echo BASE_URL_FOR_LOGIN?>index.php/payment/createTokenCustomer",{cardnumber : cardnumber , nameoncard : nameoncard , expmonth : CARDEXPIRYMONTH , expyear : CARDEXPIRYYEAR, noofdevices : noofdevices ,selectedpkgid : selectedpkgid},function(data)
						 {
							if(data == 'success')
							{
								   $.post('<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/createInvoice',{selectedpkgid : selectedpkgid , noofdevices : noofdevices},function(data)
								   {
									 if(data == 'success')
									 {
									   //$("#loadingdivtokenexist").hide();
									   //window.location = BASEURL+"companyadmin/upgradePacakge/1";
									    window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
									 }
								   });
							}
							else
							{
							  alert('Opps! Some unexpected error occured');
							  clearFields();
							  $('#buttonpay').prop('disabled', false);
							}
						 });
						
						
						
						
						 
						  
						  
					  }else if(msg == 'emailexist'){
						 $("#signuperr").html('Email already exists in cuedrive');
					 }else if(msg == 'usernameexist'){
						 $("#signuperr").html('Username already exists in cuedrive');
					 }else if(msg == 'companynameexist'){
						 $("#signuperr").html('Company name already exists in cuedrive');}
				   }
				});	 
					
				 
				 
			
		   
			
			
			
		
		<?php } else { ?>
		
		e.preventDefault(); 
        var form = $(this); 
        var post_url = form.attr('action'); 
        var post_data = form.serialize();
        $.ajax({
            	type: 'POST',
            	url: '<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/register',
            	data: post_data,
            	success: function(msg) { 
               	if(msg == 'success'){
    	              window.location = "<?php echo BASE_URL_FOR_LOGIN?>index.php/companyadmin/folders";
        	      }else if(msg == 'emailexist'){
                   	 $("#signuperr").html('Email already exists in cuedrive');
               	 }else if(msg == 'usernameexist'){
                   	 $("#signuperr").html('Username already exists in cuedrive');
               	 }else if(msg == 'companynameexist'){
					 $("#signuperr").html('Company name already exists in cuedrive');}
       	   	   }
	      	});
	<?php } ?>	
			
    	}
   });
</script>
</body>
</html>