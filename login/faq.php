<!DOCTYPE html>
<html lang="en">

<head>
<title>cuedrive | Help</title>
<meta name="description" content="cuedrive | Paperless business solution Frequently Asked Questions">
<meta name='keywords' content='cuedrive, Paperless business solution, q drive, qdrive, cue drive, paperless best app, paperless ipad' />
   <?php include 'head.php';?>
</head>

<body>
<?php include 'header.php';?>

<br><br><br>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">How may I help you ?</h2>
                
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                
<ul class="a">
<li>We can provide you with that curtail linkup between a mobile tablet and your office, your business colleges can lookup files on the run.</li>
<li>Only pay for what you use, our pricing is based on per device. eg: Add 5 devices to your cuedrive account with unlimited users.</li>
<li>We have set up an inter device or user chat system that can keep employees connected.</li>
<li>With our PDF editor curtail documents can be signed; inspections can be filled out and signed off instantly.</li>
<li>Our integrated search bar will ensure you can find the document you are looking for.</li>
<li>Procedures and Material safety data sheets can be available to all your staff and you have the peace of mind they are all up to date.</li>
<li>Store plans, catalogues, power point files and images for your employees to be showing the correct information every time.</li>
<li>Keep everyone on the same page with file change notifications; these can be set up to be sent out whenever you would like employees to view a file variation.</li>
<li>Have all the required files available offline with appropriate restrictions set by administrator.</li>
<li>Protect your files today and increase productivity with cuedrive<sup>&reg;</sup>.</li>
  
</ul>                
            </div>

        </div>

    </div>
    <!-- /.container -->

    <?php include 'footer.php';?>
    
<?php include 'script.php';?>

</body>

</html>